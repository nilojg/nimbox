# NimBOX

NimBOX is just a box where I've put some utilities I've been creating through the years. Some methods are trivial because when I need to do something, like using JSON or XML, I learn to use an external library, make a wrapper and forget everything about the thing.

The methods in NimBOX are in some packages:

- `es.nimbox.box`: utility methods, like conversions, parsers, etc
- `es.nimbox.cache`: something like the old Map<?,?> but storing the data in any other place, like MongoDB or the file system, so it's possible to store really big amounts of data using *something-like-a-map*
- `es.nimbox.classloader`: exactly that, some ClassLoader implementations
- `es.nimbox.database`: some utils to execute SQL sentences, mainly in unit tests. It tries to be like RESTAssured and make SQL as easy to test as REST is.
- `es.nimbox.io`: utilities to read and write CSVs or converting the result set of a SQL query into a CSV.
- `es.nimbox.pqc`: a little and multithread implementation of the producer-consumer pattern
- `es.nimbox.gui`: some Swing components and utilities

## `es.nimbox.box`

This package is the real 'box' of NimBOX. Here there are a lot of different things.

- `Utils` : a box of utility methods to format dates, sleep the thread, change string occurences, find files in classpath, etc. 
- `ByteArrays` : utilities to handle byte arrays. Concat, fill, extract subarrays... 
- `UtilsCipher` : utilities to encrypt and decrypt using AES/CBC/PKCS5Padding, with 256 bits keys or RSA/ECB/OAEPWithSHA1AndMGF1Padding, with 2048 bits keys. If you *really* need cryptograpfy, perhaps you should use another framework more capable, but if you are only looking for something that allows you to cypher a password to store it in a database, this class suits perfect for you.
- `UtilsHASH` : some utility methods to handle MD5, SHA1 and SHA256
- `Pair` : a class to store a *pair* of values. Just that.
- `Formats` : utilities to change information between formats, like Hexadecimal dumps, base64, JSON (using GSON)
- `ElasticBox` : a bean that can store a variable number of attributes, importing and exporting them to JSON and XML
- `FileWatcher` : a watchdog to get notified of changes in files or directories
- `WatchDog` : a watchdog to get notified of time events, like time intervals
- `Props` : this class stores the configuration of the application, reading the configuration from properties files, including the System properties as *default* properties. The values can have values between `${}` referencing another properties which will be expanded on reading time. It's a `Properties` class with the BASH expansion functionality. And a property file can include another property file.
- `Reflect` : methods to instance beans using as input a `Map` with the name of the setters to use and the values to set
- `UIDGen` : utility class to generate UIDs
- `PaceMaker` : a class that allows you to slow down the speed of the application at a given pace. This is useful if you are going to access a server that cannot handle too much load, for example.
- `XMLUtils` : this class writes XML from beans annotated with `@XMLClass`, `@XMLElement` and `@XMLAttribute` : 

## `es.nimbox.cache`

This package contains a `Hashtable` capable of store big amounts of data. The interface `ICache` contains the basic methods to put and read information as pairs key-value, and some implementations to write the data in:
- Memory, `HashMemory`
- File system, `HashFile`
- MongoDB, `MongoCache`

`HashMemory` is a regular `HashMap` and exists only to allow you to choose 'memory' to keep the map, but the really important classes are `HashFile` and `MongoCache`, which allow you to create really big maps much bigger than the memory. 

`HashFile` stores the data in a regular file in the file system while the index is in memory. It's quite fast and easy to use because you just need access to the file system to create a `HashFile`, so the size limit is the available free space in your hard drive.

`MongoCache` is a wrapper over MongoDB. The map is stored in a MongoDB collection, which is as scalable as you MongoDB installation. It's more complicated to use because you need a MongoDB.

## `es.nimbox.io`:

This package is quite easy to use because is very near to the `java.io` package, although using `ElasticBox` objects.

- `BoxReader` and `BoxWriter` are interfaces that allow you to read and write `ElasticBox` objects into a streams.
- `CSVReader` and `CSVWriter` are implementations to read and write CSV files
- `SQLReader` is an implementation that allows you to execute a SQL query and get the results as `ElasticBox` objects.


## `es.nimbox.classloader`

The more complicated and difficult to use are the classes in the `es.nimbox.classloader` package. The other packages are ready-to-use classes, but the classes in the `es.nimbox.classloader` need that you prepare your application to use them. 

This means basically that you must create a little launcher application with the `main` function and put your application in a separate jar file. The `main` function will create the ClassLoader, load all the needed jar files, including the jar with your application, and instance your application main class using the ClassLoader.

Well, it's true, that's a fucking pain, but you will be able to create plugin based applications, show a fancy loading progress bar and, if you use the `CriptoCLassLoader`, create applications which *.class* files are encrypted using strong asymmetric key cryptography... I don't know if that's useful, but it's cool.

ClassLoaders are a little complicated, and you can get more information about them in a lot of places in the Internet, like these:

https://www.arquitecturajava.com/el-concepto-de-classloader/
https://www.baeldung.com/java-classloaders

## es.nimbox.database

The aim of this package is to simplify the connection to databases and the execution of SQL sentences. It doesn't try to be big, just to simplify the excecution of SQL sentences, like SELECT, UPDATE or INSERT, in unit test to ensure that the record is in the table.

The syntax is *traditional*, with getters and setters, of *functional*.

- `SQLConn`: handles the connection to the databases
- `SQLExec`: executes the SQL sentence

## es.nimbox.gui

This package contains some Swing components that I used to put in my projects. If you are not going to make Swing applications, this package is not interesting for you.

- `FlipFrame`: this is just a frame that *flips* and shows the back to the from and vice-verse. Imagine that a frame is book with two sides. Well, the frame is the same, and clicking in a button the back side goes to front and the front to back, which is useful to put the help of the application, for example.

  The `FlipFrame` goes transparent with the mouse wheel too.
 
- `WindowBall`: this is a kind of tree of item, where each item is a semi-transparent ball that floats in the desktop and responds to event, so clicking in the ball can trigger actions, like opening another frame.

  There's a hierarchy of balls, so it's like a cool menu.

- `JCancelOkPanel`: just a `JPanel` with two buttons, "OK" and "Cancel". Every dialog needs one of this...

- `JHexTextField`: a `JTextField` that only allows hexadecimal input

- `JTickList`: a `JList` with *ticks* to select and un-select items

- `JKeyValueComboBox`: a `JComboBox` that shows pair key-value

- `GuiUtils`: some general utilities to deal with colors, load icons, center windows in the desktop and so on

## es.nimbox.pqc

This package contains the infrastructure to implement a producer-consumer pattern defining only the *item*, aka the *consumed thing*, or the *message* if you prefer, and the *consumers*, the class that decides what to do with the received item. In the middle of producers and consumers there is a *queue*, so the items created by the producers are stored in the queue and processed by the consumers.

The main class is the `ConsumerProducerEngine`, which creates separate threads for each consumer and manages the threads, the errors and the central queue. The `ConsumerProducerEngine` can enqueue item in the queue, but this is optional, and it's even not possible if the queue is for example an external Kafka queue and the producers are in different servers. 

The way of working is to set a queue in `ConsumerProducerEngine`, add as many consumers as you need, and that's all. There can be error treatment, nut in general is as easy to use as this:
```
  NimQueue<String> queue = ....

  // Producers are optional
  NimProducer<String> prod1 = ....  AbstractNimProducer
  NimProducer<String> prod2 = ....  AbstractNimProducer

  NimConsumer<String> cons1 = ....  AbstractNimConsumer
  NimConsumer<String> cons2 = ....  AbstractNimConsumer
  NimConsumer<String> cons3 = ....  AbstractNimConsumer

  ConsumerProducerEngine<String> engine = new ConsumerProducerEngine();
  engine.addQueue( queue);
  engine.addProducer( prod1);  // optional
  engine.addProducer( prod2);  // optional
  engine.addConsumer( cons1);
  engine.addConsumer( cons2);
  engine.addConsumer( cons3);

  // now it's working, but in the end, call engine.stop() to finish all threads correctly
  engine.stop();
```

These are the class in the package:

- `ConsumerProducerEngine` : the center of the package. It handles the consumers threads and the queue
- `NimQueue`, `NimProducer` and `NimConsumer` are interfaces that define some methods to identify what a queue, a producer and a consumer are. 
- `ErrorReport` and `NimErrorListener` are used in the error treatment 
- `MemoryQueue` is a very simple implementation of a queue, just using the memory.
- `MultiQueue` is a *queue of queues*
- `CreatedItemListener` and `AbsractNimProducer` are classes to make easier to create `NimProducers`. These classes are only needed if the producers are going to be managed by `ConsumerProducerEngine`, but not of the producers enqueue item in the queue by its own, which will be the general case. 

## Dependencies

- `es.nimbox.box`: Jackson  >= 2
- `es.nimbox.cache`: mongodb-driver 3.8.1
  
Sadly, they must be in the `pom.xml` to compile and pass the tests, but you can manually remove them or even change the versions because the methods in NimBOX doesn't make advanced features of the dependencies, so it would be very rare that changing a version causes a disaster, although it could happen, of course... 

Jackson must exist, but almost everything uses Jackson, so it shouldn't be a big deal because probably you are using 
Jackson already. 

## JDK 11+

If you want to use NimBOX with JDK 11 and later, you will need to comment the `javadoc` task in the `build.gradle` file to avoid a problem with the new Javadoc doclet, modules and links to the original JDK javadoc.

It's quite sad, but I prefer to be stuck to Java 1.8 although there are several language improvements and some better GCs for things like that...

## Disclaimer

Of course, if you use them you are using them under your own risk...
