package es.nimbox.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import es.nimbox.box.ElasticBox;
import es.nimbox.box.Utils;

public class CSVReaderTest {
  
  @Test
  public void sweet_1_Test() throws Exception {
    // Everything is OK, 
    // separator=> ; 
    // quotes=> "
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    
    assertEquals( 3, cr.getFieldNames().size());
    
    ElasticBox box = cr.read();
    assertEquals( "1", box.get( "ID")); assertEquals( "Eneas1", box.get( "Name")); assertEquals( "9150808001", box.get( "Phone"));
    assertEquals( 3, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "2", box.get( "ID")); assertEquals( "Eneas2", box.get( "Name")); assertEquals( "9150808002", box.get( "Phone"));
    assertEquals( "unexpected1_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "3", box.get( "ID")); assertEquals( "Eneas3", box.get( "Name")); assertEquals( "9150808003", box.get( "Phone"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "4", box.get( "ID")); assertEquals( "Eneas4", box.get( "Name")); assertEquals( "9150808004", box.get( "Phone"));
    assertEquals( "unexpected2_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "unexpected1_Col2", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( 5, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "5", box.get( "ID")); assertEquals( "Eneas5", box.get( "Name")); assertEquals( "9150808005", box.get( "Phone"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( "unexpected1_Col3", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "6", box.get( "ID")); assertEquals( "Eneas6", box.get( "Name")); assertEquals( "9150808006", box.get( "Phone"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "7", box.get( "ID")); assertEquals( "Eneas7", box.get( "Name")); assertEquals( "9150808007", box.get( "Phone"));
    assertEquals( "unexpected3_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "unexpected3_Col2", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());
    
    assertEquals( 7, cr.getReadRecords());
    
    // Close...
    cr.close();
    
    // ...and fail when read after closed
    try {
      cr.read();
      fail();
    }
    catch ( IOException ex) {}
    
    assertEquals( 0, cr.getReadRecords());
    assertEquals( 0, cr.getFieldNames().size());
  }
  
  @Test
  public void sweet_2_Test() throws Exception {
    // Everything is OK, 
    // separator=> , 
    // quotes=> "
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file2.csv"));
    cr.getCSVParameters().setSeparator( ",");
    
    assertEquals( 3, cr.getFieldNames().size());
    
    ElasticBox box = cr.read();
    assertEquals( "1", box.get( "ID")); assertEquals( "Eneas1", box.get( "Name")); assertEquals( "9150808001", box.get( "Phone"));
    assertEquals( 3, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "2", box.get( "ID")); assertEquals( "Eneas2", box.get( "Name")); assertEquals( "9150808002", box.get( "Phone"));
    assertEquals( "unexpected1_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "3", box.get( "ID")); assertEquals( "Eneas3", box.get( "Name")); assertEquals( "9150808003", box.get( "Phone"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "4", box.get( "ID")); assertEquals( "Eneas4", box.get( "Name")); assertEquals( "9150808004", box.get( "Phone"));
    assertEquals( "unexpected2_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "unexpected1_Col2", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( 5, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "5", box.get( "ID")); assertEquals( "Eneas5", box.get( "Name")); assertEquals( "9150808005", box.get( "Phone"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( "unexpected1_Col3", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "6", box.get( "ID")); assertEquals( "Eneas6", box.get( "Name")); assertEquals( "9150808006", box.get( "Phone"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "7", box.get( "ID")); assertEquals( "Eneas7", box.get( "Name")); assertEquals( "9150808007", box.get( "Phone"));
    assertEquals( "unexpected3_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "unexpected3_Col2", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());
    
    assertEquals( 7, cr.getReadRecords());
    
    // Close...
    cr.close();
    
    // ...and fail when read after closed
    try {
      cr.read();
      fail();
    }
    catch ( IOException ex) {}
    
    assertEquals( 0, cr.getReadRecords());
    assertEquals( 0, cr.getFieldNames().size());
  }
  
  @Test
  public void sweet_3_Test() throws Exception {
    // Everything is OK, 
    // separator=> , 
    // quotes=> none"
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file3.csv"));
    cr.getCSVParameters().setSeparator( ",");
    cr.getCSVParameters().setFieldQuote( "");
    
    assertEquals( 3, cr.getFieldNames().size());
    
    ElasticBox box = cr.read();
    assertEquals( "1", box.get( "ID")); assertEquals( "Eneas1", box.get( "Name")); assertEquals( "9150808001", box.get( "Phone"));
    assertEquals( 3, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "2", box.get( "ID")); assertEquals( "Eneas2", box.get( "Name")); assertEquals( "9150808002", box.get( "Phone"));
    assertEquals( "unexpected1_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "  3", box.get( "ID")); assertEquals( "Eneas3", box.get( "Name")); assertEquals( "9150808003", box.get( "Phone"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "4", box.get( "ID")); assertEquals( "  Eneas4", box.get( "Name")); assertEquals( "9150808004", box.get( "Phone"));
    assertEquals( "unexpected2_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "unexpected1_Col2", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( 5, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "5", box.get( "ID")); assertEquals( "Eneas5", box.get( "Name")); assertEquals( "9150808005", box.get( "Phone"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( "unexpected1_Col3", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "6", box.get( "ID")); assertEquals( "Eneas6", box.get( "Name")); assertEquals( "  9150808006", box.get( "Phone"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "7", box.get( "ID")); assertEquals( "Eneas7", box.get( "Name")); assertEquals( "9150808007", box.get( "Phone"));
    assertEquals( "unexpected3_Col1", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "unexpected3_Col2", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());
    
    assertEquals( 7, cr.getReadRecords());
    
    // Close...
    cr.close();
    
    // ...and fail when read after closed
    try {
      cr.read();
      fail();
    }
    catch ( IOException ex) {}
    
    assertEquals( 0, cr.getReadRecords());
    assertEquals( 0, cr.getFieldNames().size());
  }

  @Test
  public void sweet_4_Test() throws Exception {
    // Everything is OK, 
    // separator=> ; 
    // quotes=> none
    // ** But file DOES contain a separator, ", so every key and every value contains "
    // Choose the right file, dude...
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file2.csv"));
    cr.getCSVParameters().setSeparator( ",");
    cr.getCSVParameters().setFieldQuote( "");
    
    assertEquals( 3, cr.getFieldNames().size());
    
    ElasticBox box = cr.read();
    assertEquals( "\"1\"", box.get( "\"ID\"")); assertEquals( "\"Eneas1\"", box.get( "\"Name\"")); assertEquals( "\"9150808001\"", box.get( "\"Phone\""));
    assertEquals( 3, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "\"2\"", box.get( "\"ID\"")); assertEquals( "\"Eneas2\"", box.get( "\"Name\"")); assertEquals( "\"9150808002\"", box.get( "\"Phone\""));
    assertEquals( "\"unexpected1_Col1\"", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "  \"3\"", box.get( "\"ID\"")); assertEquals( "\"Eneas3\"", box.get( "\"Name\"")); assertEquals( "\"9150808003\"", box.get( "\"Phone\""));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( 4, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "\"4\"", box.get( "\"ID\"")); assertEquals( "  \"Eneas4\"", box.get( "\"Name\"")); assertEquals( "\"9150808004\"", box.get( "\"Phone\""));
    assertEquals( "\"unexpected2_Col1\"", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "\"unexpected1_Col2\"", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( 5, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "\"5\"", box.get( "\"ID\"")); assertEquals( "\"Eneas5\"", box.get( "\"Name\"")); assertEquals( "\"9150808005\"", box.get( "\"Phone\""));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( "\"unexpected1_Col3\"", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "\"6\"", box.get( "\"ID\"")); assertEquals( "\"Eneas6\"", box.get( "\"Name\"")); assertEquals( "  \"9150808006\"", box.get( "\"Phone\""));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());

    box = cr.read();
    assertEquals( "\"7\"", box.get( "\"ID\"")); assertEquals( "\"Eneas7\"", box.get( "\"Name\"")); assertEquals( "\"9150808007\"", box.get( "\"Phone\""));
    assertEquals( "\"unexpected3_Col1\"", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "3"));
    assertEquals( "\"unexpected3_Col2\"", box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "4"));
    assertEquals( null, box.get( AbstractBoxReader.DEFAULT_FIELD_NAME + "5"));
    assertEquals( 6, cr.getFieldNames().size());
    
    assertEquals( 7, cr.getReadRecords());
    
    // Close...
    cr.close();
    
    // ...and fail when read after closed
    try {
      cr.read();
      fail();
    }
    catch ( IOException ex) {}
    
    assertEquals( 0, cr.getReadRecords());
    assertEquals( 0, cr.getFieldNames().size());
  }

  
  @Test
  public void list_Test() throws Exception {
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    List<ElasticBox> list = cr.read( 10);
    assertEquals( 7, list.size());
    cr.close();

    cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    list = cr.read( 0);
    assertEquals( 0, list.size());
    list = cr.read( 3);
    assertEquals( 3, list.size());
    list = cr.read( 4);
    assertEquals( 4, list.size());
    list = cr.read( 4);
    assertEquals( 0, list.size());
    
    cr.close();
    
    // ...and fail when read after closed
    try {
      cr.read();
      fail();
    }
    catch ( IOException ex) {}
  }
  
  @Test
  public void closeable_1_Test() throws Exception {
    try ( CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv")) ) {
      cr.read( 10);
    }
    catch ( Exception ex) {
      fail();
    }
  }
  
  @Test
  public void closeable_2_Test() throws Exception {
    try ( CSVReader cr = new CSVReader( Utils.findFile( "/csvs/fffile1.csv")) ) {
      fail();
    }
    catch ( Exception ex) {
    }
  }
}
