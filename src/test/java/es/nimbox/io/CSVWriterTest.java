package es.nimbox.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Test;

import es.nimbox.box.ElasticBox;
import es.nimbox.box.Utils;

public class CSVWriterTest {
  static String filePrefix = "deleteTestCSVWriter";

  static String CAD1 = "Hola\n\nmundo\rcruel\t\tdecía el\n \n señor O'Hara";
  static String CAD2 = "Hola\\nmundo\\rcruel\\t\\tdecía el\nseñor O'Hara";
  static String CAD3 = "\"El Señor de los Anillos\" es; un libro\ndividido \t en tres tomos.";
  static String CAD4 = "\"El Señor\n\r\n\t\tde los An\\illos\" es, un libro\ndividido \t en\t\ttres tomos.\n\n";
  
  
  
  @AfterClass
  public static void deleteFiles() throws Exception {
    File dir = new File( ".");
    
    for ( File ff : dir.listFiles() ) {
      if ( ff.getName().startsWith( filePrefix) ) {
        ff.delete();
      }
    }
  }

  
  
  @Test
  public void sweet_1_Test() throws Exception {
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    List<ElasticBox> listRead1 = cr.read( 100);
    assertEquals( listRead1.size(), cr.getReadRecords());
    cr.close();
    assertEquals( 0, cr.getReadRecords());
        
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File1.csv");
    for ( ElasticBox box : listRead1) {
      cw.write( box);
    }
    assertEquals( listRead1.size(), cw.getWrittenRecords());
    cw.close();
    assertEquals( 0, cw.getWrittenRecords());
    
    cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File1.csv"));
    List<ElasticBox> listRead2 = cr.read( 100);
    cr.close();
    
    
    
    assertEquals( listRead1.size(), listRead2.size());
    
    
    for ( int i = 0; i < listRead1.size(); i++) {
      ElasticBox br1 = listRead1.get( i);
      ElasticBox br2 = listRead2.get( i);

      assertEquals( br1.get( "ID"), br2.get( "ID"));
      assertEquals( br1.get( "Name"), br2.get( "Name"));
      assertEquals( br1.get( "Phone"), br2.get( "Phone"));
    }
  }

  @Test
  public void setFields_Test() throws Exception {
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    List<ElasticBox> listRead1 = cr.read( 100);
    cr.close();
    
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File2.csv");
    cw.addField( "ID");
    cw.addField( "Phone");
    cw.addField( CSVReader.DEFAULT_FIELD_NAME + "3");
    cw.addField( "Date", "DateTitle", new SimpleDateFormat( "yyyy/MM/dd"));
    for ( ElasticBox box : listRead1) {
      cw.write( box);
    }
    cw.close();
    
    cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File2.csv"));
    List<ElasticBox> listRead2 = cr.read( 100);
    cr.close();
    
    assertEquals( listRead1.size(), listRead2.size());
    
    for ( int i = 0; i < listRead1.size(); i++) {
      ElasticBox br1 = listRead1.get( i);
      ElasticBox br2 = listRead2.get( i);

      assertEquals( br1.get( "ID"), br2.get( "ID"));
      assertNull( br2.get( "Name"));
      assertEquals( br1.get( "Phone"), br2.get( "Phone"));
      assertEquals( "", br2.get( "DateTitle"));

      if ( br1.get( "ID").equals( "2") ) assertEquals( "unexpected1_Col1", br2.get( CSVReader.DEFAULT_FIELD_NAME + "3"));
      if ( br1.get( "ID").equals( "4") ) assertEquals( "unexpected2_Col1", br2.get( CSVReader.DEFAULT_FIELD_NAME + "3"));
      if ( br1.get( "ID").equals( "7") ) assertEquals( "unexpected3_Col1", br2.get( CSVReader.DEFAULT_FIELD_NAME + "3"));
    }
  }
  
  @Test
  public void setFieldsWithDate_Test() throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd");
    
    List<ElasticBox> listRead1 = new ArrayList<ElasticBox>();
    for ( int i = 0; i < 10; i++) {
      listRead1.add( getEB( i));
    }
    
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File3.csv");
    cw.addField( "ID");
    cw.addField( "Name");
    cw.addField( "Phone");
    cw.addField( "Born", "DateTitle", sdf);
    for ( ElasticBox box : listRead1) {
      cw.write( box);
    }
    cw.close();
    
    CSVReader cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File3.csv"));
    List<ElasticBox> listRead2 = cr.read( 100);
    cr.close();
    
    assertEquals( listRead1.size(), listRead2.size());
    
    
    for ( int i = 0; i < listRead1.size(); i++) {
      ElasticBox eb1 = listRead1.get( i);
      ElasticBox eb2 = listRead2.get( i);

      assertEquals( eb1.get( "ID"), eb2.getAsInt( "ID"));
      assertEquals( eb1.get( "Name"), eb2.get( "Name"));
      assertEquals( eb1.get( "Phone"), eb2.get( "Phone"));
      
      Date d1 = eb1.getAsDate( "Born");
      assertEquals( sdf.format( d1), eb2.get( "DateTitle"));
    }
  }

  @Test
  public void sweet_1_NoHeadersTest() throws Exception {
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    List<ElasticBox> listRead1 = cr.read( 100);
    cr.close();
    
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File4.csv");
    cw.getCSVParameters().setHeaderInFirstLine( false);
    for ( ElasticBox box : listRead1) {
      cw.write( box);
    }
    cw.close();
    
    cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File4.csv"));
    cr.getCSVParameters().setHeaderInFirstLine( false);
    List<ElasticBox> listRead2 = cr.read( 100);
    cr.close();
    
    assertEquals( listRead1.size(), listRead2.size());
    
    for ( int i = 0; i < listRead1.size(); i++) {
      ElasticBox br1 = listRead1.get( i);
      ElasticBox br2 = listRead2.get( i);

      assertEquals( br1.get( "ID"), br2.get( "DUMMY_0"));
      assertEquals( br1.get( "Name"), br2.get( "DUMMY_1"));
      assertEquals( br1.get( "Phone"), br2.get( "DUMMY_2"));
    }
  }
  
  @Test
  public void escapeTest() throws Exception {
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File5.csv");
    cw.write( getEB( "Text", CAD1));
    cw.write( getEB( "Text", CAD2));
    cw.write( getEB( "Text", CAD3));
    cw.write( getEB( "Text", CAD4));
    cw.close();
    
    CSVReader cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File5.csv"));
    List<ElasticBox> listRead = cr.read( 100);
    cr.close();
    
    ElasticBox br = listRead.get( 0);
    assertEquals( CAD1, br.get( "Text"));
    assertEquals( CAD1, br.get( "ID"));
    assertEquals( "Eneas_" + CAD1, br.get( "Name"));
    assertEquals( "915080800" + CAD1, br.get( "Phone"));
    
    br = listRead.get( 1);
    assertEquals( CAD2, br.get( "Text"));
    assertEquals( CAD2, br.get( "ID"));
    assertEquals( "Eneas_" + CAD2, br.get( "Name"));
    assertEquals( "915080800" + CAD2, br.get( "Phone"));
    
    br = listRead.get( 2);
    assertEquals( CAD3, br.get( "Text"));
    assertEquals( CAD3, br.get( "ID"));
    assertEquals( "Eneas_" + CAD3, br.get( "Name"));
    assertEquals( "915080800" + CAD3, br.get( "Phone"));
    
    br = listRead.get( 3);
    assertEquals( CAD4, br.get( "Text"));
    assertEquals( CAD4, br.get( "ID"));
    assertEquals( "Eneas_" + CAD4, br.get( "Name"));
    assertEquals( "915080800" + CAD4, br.get( "Phone"));
  }
  
  @Test
  public void escape_2_Test() throws Exception {
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File7.csv");
    cw.getCSVParameters().setHeaderInFirstLine( true);
    cw.getCSVParameters().setSeparator( "---");
    cw.getCSVParameters().setFieldQuote( "****");
    cw.write( getEB( "Text", CAD1));
    cw.write( getEB( "Text", CAD2));
    cw.write( getEB( "Text", CAD3));
    cw.write( getEB( "Text", CAD4));
    cw.close();
    
    CSVReader cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File7.csv"));
    cr.getCSVParameters().setHeaderInFirstLine( true);
    cr.getCSVParameters().setSeparator( "---");
    cr.getCSVParameters().setFieldQuote( "****");
    List<ElasticBox> listRead = cr.read( 100);
    cr.close();
    
    ElasticBox br = listRead.get( 0);
    assertEquals( CAD1, br.get( "Text"));
    assertEquals( CAD1, br.get( "ID"));
    assertEquals( "Eneas_" + CAD1, br.get( "Name"));
    assertEquals( "915080800" + CAD1, br.get( "Phone"));
    
    br = listRead.get( 1);
    assertEquals( CAD2, br.get( "Text"));
    assertEquals( CAD2, br.get( "ID"));
    assertEquals( "Eneas_" + CAD2, br.get( "Name"));
    assertEquals( "915080800" + CAD2, br.get( "Phone"));
    
    br = listRead.get( 2);
    assertEquals( CAD3, br.get( "Text"));
    assertEquals( CAD3, br.get( "ID"));
    assertEquals( "Eneas_" + CAD3, br.get( "Name"));
    assertEquals( "915080800" + CAD3, br.get( "Phone"));
    
    br = listRead.get( 3);
    assertEquals( CAD4, br.get( "Text"));
    assertEquals( CAD4, br.get( "ID"));
    assertEquals( "Eneas_" + CAD4, br.get( "Name"));
    assertEquals( "915080800" + CAD4, br.get( "Phone"));
  }
  
  @Test
  public void escape_3_Test() throws Exception {
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File8.csv");
    cw.getCSVParameters().setHeaderInFirstLine( true);
    cw.getCSVParameters().setSeparator( ",");
    cw.getCSVParameters().setFieldQuote( "'");
    cw.write( getEB( "Text", CAD1));
    cw.write( getEB( "Text", CAD2));
    cw.write( getEB( "Text", CAD3));
    cw.write( getEB( "Text", CAD4));
    cw.close();
    
    CSVReader cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File8.csv"));
    cr.getCSVParameters().setHeaderInFirstLine( true);
    cr.getCSVParameters().setSeparator( ",");
    cr.getCSVParameters().setFieldQuote( "'");
    List<ElasticBox> listRead = cr.read( 100);
    cr.close();
    
    ElasticBox br = listRead.get( 0);
    assertEquals( CAD1, br.get( "Text"));
    assertEquals( CAD1, br.get( "ID"));
    assertEquals( "Eneas_" + CAD1, br.get( "Name"));
    assertEquals( "915080800" + CAD1, br.get( "Phone"));
    
    br = listRead.get( 1);
    assertEquals( CAD2, br.get( "Text"));
    assertEquals( CAD2, br.get( "ID"));
    assertEquals( "Eneas_" + CAD2, br.get( "Name"));
    assertEquals( "915080800" + CAD2, br.get( "Phone"));
    
    br = listRead.get( 2);
    assertEquals( CAD3, br.get( "Text"));
    assertEquals( CAD3, br.get( "ID"));
    assertEquals( "Eneas_" + CAD3, br.get( "Name"));
    assertEquals( "915080800" + CAD3, br.get( "Phone"));
    
    br = listRead.get( 3);
    assertEquals( CAD4, br.get( "Text"));
    assertEquals( CAD4, br.get( "ID"));
    assertEquals( "Eneas_" + CAD4, br.get( "Name"));
    assertEquals( "915080800" + CAD4, br.get( "Phone"));
  }
  
  @Test
  public void sweet_PersonalizedOutputTest() throws Exception {
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    List<ElasticBox> listRead1 = cr.read( 100);
    cr.close();
    
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File6.csv");
    cw.getCSVParameters().setHeaderInFirstLine( true);
    cw.getCSVParameters().setSeparator( "---");
    cw.getCSVParameters().setFieldQuote( "****");
    for ( ElasticBox box : listRead1) {
      cw.write( box);
    }
    cw.close();
    
    cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File6.csv"));
    cr.getCSVParameters().setHeaderInFirstLine( true);
    cr.getCSVParameters().setSeparator( "---");
    cr.getCSVParameters().setFieldQuote( "****");
    List<ElasticBox> listRead2 = cr.read( 100);
    cr.close();
    
    assertEquals( listRead1.size(), listRead2.size());
    
    for ( int i = 0; i < listRead1.size(); i++) {
      ElasticBox br1 = listRead1.get( i);
      ElasticBox br2 = listRead2.get( i);

      assertEquals( br1.get( "ID"), br2.get( "ID"));
      assertEquals( br1.get( "Name"), br2.get( "Name"));
      assertEquals( br1.get( "Phone"), br2.get( "Phone"));
    }
  }
  
  @Test
  public void sweet_Personalized2OutputTest() throws Exception {
    CSVReader cr = new CSVReader( Utils.findFile( "/csvs/file1.csv"));
    List<ElasticBox> listRead1 = cr.read( 100);
    cr.close();
    
    CSVWriter cw = new CSVWriter( "./" + filePrefix + "File7.csv");
    cw.getCSVParameters().setHeaderInFirstLine( true);
    cw.getCSVParameters().setSeparator( ",");
    cw.getCSVParameters().setFieldQuote( "'");
    for ( ElasticBox box : listRead1) {
      cw.write( box);
    }
    cw.close();
    
    cr = new CSVReader( Utils.findFile( "./" + filePrefix + "File7.csv"));
    cr.getCSVParameters().setHeaderInFirstLine( true);
    cr.getCSVParameters().setSeparator( ",");
    cr.getCSVParameters().setFieldQuote( "'");
    List<ElasticBox> listRead2 = cr.read( 100);
    cr.close();
    
    assertEquals( listRead1.size(), listRead2.size());
    
    for ( int i = 0; i < listRead1.size(); i++) {
      ElasticBox br1 = listRead1.get( i);
      ElasticBox br2 = listRead2.get( i);

      assertEquals( br1.get( "ID"), br2.get( "ID"));
      assertEquals( br1.get( "Name"), br2.get( "Name"));
      assertEquals( br1.get( "Phone"), br2.get( "Phone"));
    }
  }
  
  
  
  
  private ElasticBox getEB( int i) {
    ElasticBox res = new ElasticBox();
    
    long baseDate = 1577836800000L;  // 2020/01/01 00:00:00
    
    long thisDate = baseDate + 24*3600*1000*i;

    res.set( "ID", i);
    res.set( "Name", "Eneas" + i);
    res.set( "Phone", "915080800" + i);
    res.set( "Born", new Date( thisDate));
    
    return res;
  }
  
  private ElasticBox getEB( String field, String value) {
    ElasticBox res = new ElasticBox();
    
    res.set( field, value);
    res.set( "ID", value);
    res.set( "Name", "Eneas_" + value);
    res.set( "Phone", "915080800" + value);
    
    return res;
  }
}
