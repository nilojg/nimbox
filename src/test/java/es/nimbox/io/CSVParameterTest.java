package es.nimbox.io;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class CSVParameterTest {

  
  @Test
  public void split_1_Test() throws Exception {
    CSVParameters params = new CSVParameters();
    // With default values
    
    List<String> res = params.parseCSVLine( "\"En un lugar\";\"de la Mancha\";\"de cuyo\"");
    assertEquals( 3, res.size());
    assertEquals( "En un lugar", res.get( 0));
    assertEquals( "de la Mancha", res.get( 1));
    assertEquals( "de cuyo", res.get( 2));
    
    res = params.parseCSVLine( "\"En \"un\" lugar\";\"de;la Mancha\";\"de cuyo\"");
    assertEquals( 3, res.size());
    assertEquals( "En \"un\" lugar", res.get( 0));
    assertEquals( "de;la Mancha", res.get( 1));
    assertEquals( "de cuyo", res.get( 2));
    
    res = params.parseCSVLine( "\"En \\\"\"un\" lugar\";\"de;la Mancha\";\"de cuyo\"");
    assertEquals( 3, res.size());
    assertEquals( "En \\\"\"un\" lugar", res.get( 0));
    assertEquals( "de;la Mancha", res.get( 1));
    assertEquals( "de cuyo", res.get( 2));
  }
  
  @Test
  public void split_2_Test() throws Exception {
    CSVParameters params = new CSVParameters();
    params.setSeparator( ",");
    params.setFieldQuote( "'");
    params.setEnd( "\n");
    
    List<String> res = params.parseCSVLine( "'\"En un, lugar\"','de la Mancha' , 'de cuyo'  ");
    assertEquals( 3, res.size());
    assertEquals( "\"En un, lugar\"", res.get( 0));
    assertEquals( "de la Mancha", res.get( 1));
    assertEquals( "de cuyo", res.get( 2));
    
    res = params.parseCSVLine( "     'En \\'un\" lugar\",\"de;la Mancha'  ,    'de , cuyo' ");
    assertEquals( 2, res.size());
    assertEquals( "En \\'un\" lugar\",\"de;la Mancha", res.get( 0));
    assertEquals( "de , cuyo", res.get( 1));
  }
  
  @Test
  public void split_3_Test() throws Exception {
    CSVParameters params = new CSVParameters();
    params.setSeparator( "+++");
    params.setFieldQuote( "");
    params.setEnd( "\n");
    
    List<String> res = params.parseCSVLine( "\"En un, lugar\"+++de la Mancha +++ de cuyo  ");
    assertEquals( 3, res.size());
    assertEquals( "\"En un, lugar\"", res.get( 0));
    assertEquals( "de la Mancha ", res.get( 1));
    assertEquals( " de cuyo  ", res.get( 2));
    
    res = params.parseCSVLine( "     'En \\'un\" lugar\",\"de;la+++ Mancha'  +++    'de , cuyo' ");
    assertEquals( 3, res.size());
    assertEquals( "     'En \\'un\" lugar\",\"de;la", res.get( 0));
    assertEquals( " Mancha'  ", res.get( 1));
    assertEquals( "    'de , cuyo' ", res.get( 2));
  }
  
  @Test
  public void limits_Test() throws Exception {
    CSVParameters params = new CSVParameters();
    params.setFieldQuote( "");
    params.setSeparator( ",");
    
    assertEquals( 0, params.parseCSVLine( "\n").size());
    assertEquals( 0, params.parseCSVLine( " \n").size());
    assertEquals( 1, params.parseCSVLine( "a\n").size());
    assertEquals( 1, params.parseCSVLine( "a \n").size());
    assertEquals( 1, params.parseCSVLine( " a\n").size());
    assertEquals( 2, params.parseCSVLine( " a,b\n").size());
    assertEquals( 2, params.parseCSVLine( "a,b\n").size());
    assertEquals( 2, params.parseCSVLine( "a,b \n").size());
    assertEquals( 2, params.parseCSVLine( " a, b \n").size());
    assertEquals( 2, params.parseCSVLine( "a, b \n").size());
    assertEquals( 3, params.parseCSVLine( "a, b ,\n").size());
    assertEquals( 3, params.parseCSVLine( "a, b , \n").size());
    assertEquals( 3, params.parseCSVLine( "a, b , c\n").size());
    assertEquals( 3, params.parseCSVLine( "a, b , c \n").size());
    assertEquals( 3, params.parseCSVLine( "a, b ,c \n").size());
    
    params = new CSVParameters();
    params.setFieldQuote( "");
    
    assertEquals( 0, params.parseCSVLine( "\n").size());
    assertEquals( 0, params.parseCSVLine( " \n").size());
    assertEquals( 1, params.parseCSVLine( "a\n").size());
    assertEquals( 1, params.parseCSVLine( "a \n").size());
    assertEquals( 1, params.parseCSVLine( " a\n").size());
    assertEquals( 2, params.parseCSVLine( " a;b\n").size());
    assertEquals( 2, params.parseCSVLine( "a;b\n").size());
    assertEquals( 2, params.parseCSVLine( "a;b \n").size());
    assertEquals( 2, params.parseCSVLine( " a; b \n").size());
    assertEquals( 2, params.parseCSVLine( "a; b \n").size());
    assertEquals( 3, params.parseCSVLine( "a; b ;\n").size());
    assertEquals( 3, params.parseCSVLine( "a; b ; \n").size());
    assertEquals( 3, params.parseCSVLine( "a; b ; c\n").size());
    assertEquals( 3, params.parseCSVLine( "a; b ; c \n").size());
    assertEquals( 3, params.parseCSVLine( "a; b ;c \n").size());
  }
}
