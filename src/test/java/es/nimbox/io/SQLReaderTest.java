package es.nimbox.io;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import es.nimbox.box.ElasticBox;


public class SQLReaderTest {
  private static Connection con;
  
  static SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
  static Date base;
  

  @BeforeClass
  public static void setUp() throws Exception {
    String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    String jdbcURL = "jdbc:derby:memory:testDB;create=true";
    Class.forName( driver);
    
    con = DriverManager.getConnection( jdbcURL);
    
    base = sdf.parse( "2020-01-01 10:00:00");
    
    populate();
  }

  @AfterClass
  public static void cleanUp() throws Exception {
    dropTable();
  }
  
  
  @Test
  public void emptyQueryTest() throws Exception {
    try ( SQLReader cr = new SQLReader( con) ) {
      cr.read( 10);
      fail();
    }
    catch ( Exception ex) {}
  }

  @Test
  public void selectSingleTest() throws Exception {
    try ( SQLReader cr = new SQLReader( con) ) {
      cr.setQuery( "select * from Usuarios");
      
      ElasticBox box = cr.read();

      assertEquals( 0, (int)box.getAsInt( "ID"));
      assertEquals( base, box.getAsDate( "BORNDATE"));
      assertEquals( "Eneas Pirulez0", box.get( "PERSONNAME"));
      assertEquals( 0, (int)box.getAsInt( "AGE"));
      
      // Starts in 1 because follows with the cursor
      for ( int i = 1; i < 100; i++) {
        box = cr.read();

        assertEquals( i, (int)box.getAsInt( "ID"));
        assertEquals( base.getTime() + (i*1000), box.getAsDate( "BORNDATE").getTime());
        assertEquals( "Eneas Pirulez" + i, box.get( "PERSONNAME"));
        assertEquals( i, (int)box.getAsInt( "AGE"));
      }
      
      box = cr.read();

      assertNull( box);
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      fail();
    }
  }
  
  @Test
  public void reOpenTest() throws Exception {
    try ( SQLReader cr = new SQLReader( con) ) {
      cr.setQuery( "select * from Usuarios");
      
      ElasticBox box = cr.read();

      assertEquals( 0, (int)box.getAsInt( "ID"));
      assertEquals( base, box.getAsDate( "BORNDATE"));
      assertEquals( "Eneas Pirulez0", box.get( "PERSONNAME"));
      assertEquals( 0, (int)box.getAsInt( "AGE"));
      
      cr.close();
      
      // Starts again in ZERO because the cursor is reopened
      for ( int i = 0; i < 100; i++) {
        box = cr.read();

        assertEquals( i, (int)box.getAsInt( "ID"));
        assertEquals( base.getTime() + (i*1000), box.getAsDate( "BORNDATE").getTime());
        assertEquals( "Eneas Pirulez" + i, box.get( "PERSONNAME"));
        assertEquals( i, (int)box.getAsInt( "AGE"));
      }
      
      box = cr.read();

      assertNull( box);
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      fail();
    }
  }
  
  @Test
  public void listTest() throws Exception {
    try ( SQLReader cr = new SQLReader( con) ) {
      cr.setQuery( "select * from Usuarios");
      
      List<ElasticBox> list = cr.read( 10);
      
      assertEquals( 10, list.size());

      for ( int i = 0; i < 10; i++) {
        ElasticBox box = list.get( i);

        assertEquals( i, (int)box.getAsInt( "ID"));
        assertEquals( base.getTime() + (i*1000), box.getAsDate( "BORNDATE").getTime());
        assertEquals( "Eneas Pirulez" + i, box.get( "PERSONNAME"));
        assertEquals( i, (int)box.getAsInt( "AGE"));
      }
      
      list = cr.read( 100);
      
      // The table contains 100 records, and 10 are read yet
      assertEquals( 90, list.size());

      for ( int i = 10; i < 100; i++) {
        ElasticBox box = list.get( i-10);

        assertEquals( i, (int)box.getAsInt( "ID"));
        assertEquals( base.getTime() + (i*1000), box.getAsDate( "BORNDATE").getTime());
        assertEquals( "Eneas Pirulez" + i, box.get( "PERSONNAME"));
        assertEquals( i, (int)box.getAsInt( "AGE"));
      }
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      fail();
    }
  }
  
  @Test
  public void singleAndListTest() throws Exception {
    try ( SQLReader cr = new SQLReader( con) ) {
      cr.setQuery( "select * from Usuarios");
      
      ElasticBox box = cr.read();

      int i = 0;
      assertEquals( i, (int)box.getAsInt( "ID"));
      assertEquals( base.getTime() + (i*1000), box.getAsDate( "BORNDATE").getTime());
      assertEquals( "Eneas Pirulez" + i, box.get( "PERSONNAME"));
      assertEquals( i++, (int)box.getAsInt( "AGE"));
      
      List<ElasticBox> list = cr.read( 10);
      
      assertEquals( 10, list.size());

      for ( ElasticBox eb : list) {
        assertEquals( i, (int)eb.getAsInt( "ID"));
        assertEquals( base.getTime() + (i*1000), eb.getAsDate( "BORNDATE").getTime());
        assertEquals( "Eneas Pirulez" + i, eb.get( "PERSONNAME"));
        assertEquals( i++, (int)eb.getAsInt( "AGE"));
      }
      
      box = cr.read();

      assertEquals( i, (int)box.getAsInt( "ID"));
      assertEquals( base.getTime() + (i*1000), box.getAsDate( "BORNDATE").getTime());
      assertEquals( "Eneas Pirulez" + i, box.get( "PERSONNAME"));
      assertEquals( i++, (int)box.getAsInt( "AGE"));
      
      list = cr.read( 100);
      
      // The table contains 100 records, and 10 are read yet
      assertEquals( 88, list.size());

      for ( ElasticBox eb : list) {
        assertEquals( i, (int)eb.getAsInt( "ID"));
        assertEquals( base.getTime() + (i*1000), eb.getAsDate( "BORNDATE").getTime());
        assertEquals( "Eneas Pirulez" + i, eb.get( "PERSONNAME"));
        assertEquals( i++, (int)eb.getAsInt( "AGE"));
      }
      
      box = cr.read();
      
      assertNull( box);
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      fail();
    }
  }








  private static void populate() throws Exception {
    Statement st = con.createStatement();

    st.execute( "create table Usuarios (\n" +
                        "    ID integer\n" +
                        "  , personName VARCHAR(50)\n" +
                        "  , bornDate TIMESTAMP\n" +
                        "  , age integer\n" +
                        ")");

    for ( int i = 0; i < 100; i++) {
      Date born = new Date( base.getTime() + (1000*i));

      String ins = "insert into Usuarios VALUES (" + i + ", 'Eneas Pirulez" + i + "', '" + sdf.format( born) + "', " + i + ")";

      st.execute( ins);
    }

    st.close();
  }

  private static void dropTable() throws Exception {
    Statement st = con.createStatement();

    st.execute( "drop table Usuarios");
    st.close();
  }
}
