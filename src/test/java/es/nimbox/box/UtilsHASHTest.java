package es.nimbox.box;

import org.junit.*;

import static org.junit.Assert.*;


public class UtilsHASHTest {
  
  String value = "Hola Caracola";
  byte[] valueByte = value.getBytes();
  
  
  String expectMD5 = "09f6122eb82c736dc71ababd15b796ea";
  String expectSHA1 = "cce6d0a4ce02498c4cbaabcf0eaf9ab0e6a19339";
  String expectSHA256 = "8ef2e45f9fa9af4afd280f2fdacfb2ca12679ff59f845afce42043bc8158d7d9";
  
  
  @Test
  public void genericHash() throws Exception {
    assertEquals( expectMD5, Formats.getHexDump( UtilsHASH.getHASHByteArray( UtilsHASH.HASH_TYPE.MD5, valueByte, 0, valueByte.length)));
    assertEquals( expectSHA1, Formats.getHexDump( UtilsHASH.getHASHByteArray( UtilsHASH.HASH_TYPE.SHA1, valueByte, 0, valueByte.length)));
    assertEquals( expectSHA256, Formats.getHexDump( UtilsHASH.getHASHByteArray( UtilsHASH.HASH_TYPE.SHA256, valueByte, 0, valueByte.length)));
  }
  
  @Test
  public void md5Get() throws Exception {
    assertEquals( expectMD5, UtilsHASH.getMD5( value));
    assertEquals( expectMD5, UtilsHASH.getMD5( valueByte));
    assertEquals( expectMD5, UtilsHASH.getMD5( valueByte, 0, valueByte.length));
    
    assertEquals( expectMD5, Formats.getHexDump( UtilsHASH.getMD5ByteArray( valueByte)));
    assertEquals( expectMD5, Formats.getHexDump( UtilsHASH.getMD5ByteArray( valueByte, 0, valueByte.length)));
  }

  @Test
  public void SHA1Get() throws Exception {
    assertEquals( expectSHA1, UtilsHASH.getSHA1( value));
    assertEquals( expectSHA1, UtilsHASH.getSHA1( valueByte));
    assertEquals( expectSHA1, UtilsHASH.getSHA1( valueByte, 0, valueByte.length));
    
    assertEquals( expectSHA1, Formats.getHexDump( UtilsHASH.getSHA1ByteArray( valueByte)));
    assertEquals( expectSHA1, Formats.getHexDump( UtilsHASH.getSHA1ByteArray( valueByte, 0, valueByte.length)));
  }
  
  @Test
  public void SHA256Get() throws Exception {
    assertEquals( expectSHA256, UtilsHASH.getSHA256( value));
    assertEquals( expectSHA256, UtilsHASH.getSHA256( valueByte));
    assertEquals( expectSHA256, UtilsHASH.getSHA256( valueByte, 0, valueByte.length));
    
    assertEquals( expectSHA256, Formats.getHexDump( UtilsHASH.getSHA256ByteArray( valueByte)));
    assertEquals( expectSHA256, Formats.getHexDump( UtilsHASH.getSHA256ByteArray( valueByte, 0, valueByte.length)));
  }
  
  @Test
  public void hexDump() {
    byte[] buf = Formats.getArrayFromHexString( "0123456789ABCDEF");
    String cad = Formats.getHexDump( buf);
    
    assertEquals( cad, "0123456789abcdef");
  }
}



