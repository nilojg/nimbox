package es.nimbox.box;


public class PaceMakerTest {  // NOSONAR

  /* Everything is commented because these tests are:
   * - slooooooow, they take so much time that the compilation is really boring
   * - quite manual
   * 
   * So everything is commented
   *
   *
  @Test
  public void sweetTest() {
    PaceMaker.getPaceMaker().addPace( "sweet", PaceMaker.TYPE.PER_MINUTE, 600);
    PaceMaker.getPaceMaker().makePace( "sweet");
    
    long t1 = System.currentTimeMillis();
    
    for ( int i = 0; i < 120; i++) {
      PaceMaker.getPaceMaker().makePace( "sweet");
    }
    
    long t2 = System.currentTimeMillis();
    
    
    System.out.println( (t2-t1));
  }
  
  @Test
  public void multiThreadTest() throws InterruptedException {
    Server server1 = new Server();
    Server server2 = new Server();
    
    List<MiTestClass> lThreads = new ArrayList<MiTestClass>();
    
    for ( int i = 0; i < 1; i++ ) {
      lThreads.add( new MiTestClass( "pace1", server1));
    }
    
    for ( int i = 0; i < 15; i++ ) {
      lThreads.add( new MiTestClass( "pace2", server2));
    }

    int minutos = 2;

    PaceMaker.getPaceMaker().addPace( "pace1", PaceMaker.TYPE.PER_MINUTE, 800);
    PaceMaker.getPaceMaker().addPace( "pace2", PaceMaker.TYPE.PER_MINUTE, 20);

    long t1 = System.currentTimeMillis();
    System.out.println( "Inicio " + Utils.formatTime( t1));
    
    for ( MiTestClass tc : lThreads ) {
      tc.start();
    }
    
    Utils.sleep( minutos * 60 * 1000);

    for ( MiTestClass tc : lThreads ) {
      tc.stop = true;
    }
    
    long t2 = System.currentTimeMillis();
    System.out.println( "Espera " + Utils.formatTime( t2));
    
    for ( MiTestClass tc : lThreads ) {
      tc.join();
    }
    
    System.out.println( "server1=" + server1.intent);
    System.out.println( "server2=" + server2.intent);
    
    long t3 = System.currentTimeMillis();
    System.out.println( "Fin " + Utils.formatTime( t3));

    System.out.println( "Orden : " + (t2-t1));
    System.out.println( "Total : " + (t3-t1));
    
    System.out.println( "Server1: ");
    System.out.println( "serverCount : " + server1.intent + "    paceAccount : " + PaceMaker.getPaceMaker().getAccounting( "pace1"));
    System.out.println( "   expected = " + (PaceMaker.getPaceMaker().getPace( "pace1") * minutos));
    System.out.println( " exp. error = " + (PaceMaker.getPaceMaker().getPace( "pace1") * ((t3-t1)/1000) / (60)));
    System.out.println( " per second = " + ((t3-t1)/server1.intent) + "  interval =" + PaceMaker.getPaceMaker().getInterval( "pace1"));
    

    System.out.println( "Server2: ");
    System.out.println( "serverCount : " + server2.intent + "    paceAccount : " + PaceMaker.getPaceMaker().getAccounting( "pace2"));
    System.out.println( "   expected = " + (PaceMaker.getPaceMaker().getPace( "pace2") * minutos));
    System.out.println( " exp. error = " + (PaceMaker.getPaceMaker().getPace( "pace2") * ((t3-t1)/1000) / (60)));
    System.out.println( " per second = " + ((t3-t1)/server2.intent) + "  interval =" + PaceMaker.getPaceMaker().getInterval( "pace2"));
    
    System.out.println( "Paused1=" + PaceMaker.getPaceMaker().getPausedTime( "pace1"));
    System.out.println( "Paused2=" + PaceMaker.getPaceMaker().getPausedTime( "pace2"));
  }
  
  
  private class Server {
    public int intent = 0;
    
    public void inc() {
      intent++;
    }
  }
  
  private class MiTestClass extends Thread {
    Server server;
    String id;
    boolean stop;
    
    MiTestClass( String id, Server server) {
      this.id = id;
      this.server = server;
      stop = false;
    }
    
    public void run() {
//      System.out.println( id + " start");
      long t1 = System.currentTimeMillis();
      int thinc = 0;
      while ( !stop ) {
        server.inc();
        thinc++;
        PaceMaker.getPaceMaker().makePace( id);
        Utils.sleep( 100);
      }
      
      long t2 = System.currentTimeMillis();
      System.out.println( id + " end");
      
      int seg = (int)((t2-t1) / 1000);
      
      System.out.println( id + " serverCount : " + server.intent + "    paceAccount : " + PaceMaker.getPaceMaker().getAccounting( id));
      System.out.println( id + "        done = " + thinc + "     " + (t2-t1) + "ms   " + seg + "s");
      System.out.println( id + " per second = " + ((t2-t1)/thinc) + "  interval =" + PaceMaker.getPaceMaker().getInterval( id));
    }
  }
  
  */
}
