package es.nimbox.box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class FileWatcherTest {
  
  // Let's create some files and directory test names
  static String basePath = null; 
  static String fileName = null;
  static {
    basePath = System.getProperty("java.io.tmpdir");
    if ( !basePath.endsWith( File.separator) ) {
      basePath += File.separator;
    }
    
    basePath += "dirFileWatcher" + File.separator;
    fileName = basePath + "fileFileWatcher";
  }

  
  private class EL implements FileWatcherListener {
    public List<FileWatcherEvent> lEvents = new ArrayList<>();
    
    public void fileWatcherAlert( FileWatcherEvent ev) {
      //System.out.println( ev);
      lEvents.add( ev);
    }
    
    FileWatcherEvent getEvent( int minusIndex) {
      if ( (lEvents.size() -1 -minusIndex) >= 0 ) {
        return lEvents.get( lEvents.size() -1 -minusIndex);
      }

      return null;
    }
  }
  
  @Test
  public void sweetTest() throws Exception {
    File dir = new File( basePath);
    if ( dir.exists() && !dir.delete() ) {
      System.out.println( "*****************************************************************");
      System.out.println( "* CANNOT DELETE TEST DIRECTORY, SO MAIN FileWatcherTest SKIPPED *");
      System.out.println( "*****************************************************************");
      return;
    }
    
    EL el = new EL();
    FileWatcher fw = new FileWatcher();
    fw.addListener( el);
    fw.start();
    
    int alarmDir = fw.addFileToWatch( new File( basePath), FileWatcher.CHANGE_DIR | FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
    int alarmFile = fw.addFileToWatch( new File( fileName), 
                                       FileWatcher.CHANGE_MD5 | FileWatcher.CHANGE_SIZE | FileWatcher.CHANGE_TIME | 
                                       FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
    
    
    dir.mkdir();
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    FileWatcherEvent ev = el.getEvent( 0);
    assertEquals( alarmDir, ev.getAlarmId());
    assertTrue( 0 != (ev.getEventType() & FileWatcher.FILE_CREATE) );
    
    // CREATE file and CHANGE DIR,   ** TWO EVENTS **
    File file = new File( fileName);
    writeFile( file, "abcde");  
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    ev = el.getEvent( 0);
    if ( alarmDir == ev.getAlarmId() ) {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    }
    else {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.FILE_CREATE) );
    }
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_DELETE) );
    
    ev = el.getEvent( 1);
    if ( alarmDir == ev.getAlarmId() ) {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    }
    else {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.FILE_CREATE) );
    }
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_DELETE) );

    // change TIME, SIZE and MD5
    writeFile( file, "abcdef"); 
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    ev = el.getEvent( 0);
    assertEquals( alarmFile, ev.getAlarmId());
    assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_CREATE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_DELETE) );
    
    // change TIME and MD5
    writeFile( file, "abcdew"); 
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    ev = el.getEvent( 0);
    assertEquals( alarmFile, ev.getAlarmId());
    assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_CREATE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_DELETE) );

    // change TIME
    writeFile( file, "abcdew"); // change TIME
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    ev = el.getEvent( 0);
    assertEquals( alarmFile, ev.getAlarmId());
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_CREATE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_DELETE) );
    
    file.delete(); // CREATE file and CHANGE DIR,   ** TWO EVENTS **
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    ev = el.getEvent( 0);
    if ( alarmDir == ev.getAlarmId() ) {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    }
    else {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.FILE_DELETE) );
    }
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_CREATE) );
    
    ev = el.getEvent( 1);
    if ( alarmDir == ev.getAlarmId() ) {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    }
    else {
      assertTrue( 0 != (ev.getEventType() & FileWatcher.FILE_DELETE) );
    }
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_CREATE) );

    // Let's finish and clean everything
    dir.delete(); // delete DIR
    Utils.sleep( FileWatcher.GENERAL_INTERVAL * 5);
    
    ev = el.getEvent( 0);
    assertEquals( alarmDir, ev.getAlarmId());
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_MD5) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_TIME) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_SIZE) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.CHANGE_DIR) );
    assertTrue( 0 == (ev.getEventType() & FileWatcher.FILE_CREATE) );
    assertTrue( 0 != (ev.getEventType() & FileWatcher.FILE_DELETE) );
    
    fw.stopFileWatcher();
  }
  
  @Test
  public void settersTest() {
    FileWatcher fw = new FileWatcher();

    assertEquals( FileWatcher.GENERAL_INTERVAL, fw.getThreadSleep());
    
    fw.setThreadSleep( 500);
    assertEquals( 500, fw.getThreadSleep());
  }


  private void writeFile( File file, String cad) throws IOException {
    OutputStream os = new FileOutputStream( file);
    Utils.writeInOutputStream( os, cad);
    os.close();
  }
}
