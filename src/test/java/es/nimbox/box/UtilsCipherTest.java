package es.nimbox.box;

import static org.junit.Assert.assertEquals;

import java.security.Key;
import java.security.KeyPair;

import org.junit.Test;

public class UtilsCipherTest {
  public String SAMPLE = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt  ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  public String SHORT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor";
  
  
  
  @Test
  public void generateSim_Test() throws Exception {
    Key key = UtilsCipher.generateSymmetricKey();
    assertEquals( UtilsCipher.DEFAULT_SYMMETRIC_ALGORITHM, key.getAlgorithm());
    assertEquals( UtilsCipher.DEFAULT_SYMMETRIC_SIZE / 4, Formats.getHexDump( key.getEncoded()).length());
    
    key = UtilsCipher.generateSymmetricKey( 128);
    assertEquals( "AES", key.getAlgorithm());
    assertEquals( 128 / 4, Formats.getHexDump( key.getEncoded()).length());
  }
  
  @Test
  public void generateAsim_Test() throws Exception {
    KeyPair key = UtilsCipher.generateAsymmetricKeys();
    assertEquals( UtilsCipher.DEFAULT_ASYMMETRIC_ALGORITHM, key.getPublic().getAlgorithm());
    assertEquals( UtilsCipher.DEFAULT_ASYMMETRIC_ALGORITHM, key.getPrivate().getAlgorithm());
    
    key = UtilsCipher.generateAsymmetricKeys( 512);
    assertEquals( "RSA", key.getPublic().getAlgorithm());
    assertEquals( "RSA", key.getPrivate().getAlgorithm());
  }
  
  @Test
  public void sweetSimmetricTest() throws Exception {
    Key key = UtilsCipher.generateSymmetricKey();
    
    UtilsCipher cip = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip.setKey( key);
    
    byte buf[] = cip.encode( SAMPLE, "UTF-8");
    String clear = cip.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
  }
  
  @Test
  public void sweetAsimmetricTest() throws Exception {
    KeyPair keyPair = UtilsCipher.generateAsymmetricKeys();
    
    UtilsCipher cipEnc = new UtilsCipher( UtilsCipher.TYPE_ASYMMETRIC);
    cipEnc.setKey( keyPair.getPrivate());
    
    byte buf[] = cipEnc.encode( SHORT, "UTF-8");
    
    cipEnc.setKey( keyPair.getPublic());
    
    String clear = cipEnc.decode( buf, "UTF-8");
    
    assertEquals( SHORT, clear);
  }
  
  @Test
  public void sweetSimmetricDoKeyTest() throws Exception {
  	String passwd = "gatito";
  	
  	Key key1 = UtilsCipher.doSimmetricKeyWithPassword( passwd);
  	UtilsCipher cip1 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip1.setKey( key1);
    
    byte buf[] = cip1.encode( SAMPLE, "UTF-8");
    
    Key key2 = UtilsCipher.doSimmetricKeyWithPassword( passwd);
  	UtilsCipher cip2 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip2.setKey( key2);
    
    String clear = cip2.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
    
    Key key3 = UtilsCipher.doSimmetricKeyWithPasswordHex( Formats.getHexDump( passwd.getBytes()));
  	UtilsCipher cip3 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip3.setKey( key3);
    
    clear = cip3.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
    
    Key key4 = UtilsCipher.doSimmetricKeyWithPassword( passwd.getBytes());
  	UtilsCipher cip4 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip4.setKey( key4);
    
    clear = cip4.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
  }
  
  @Test
  public void sweetSimmetricDoKeyLongTest() throws Exception {
  	String passwd = "gatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatitogatito";
  	
  	Key key1 = UtilsCipher.doSimmetricKeyWithPassword( passwd);
  	UtilsCipher cip1 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip1.setKey( key1);
    
    byte buf[] = cip1.encode( SAMPLE, "UTF-8");
    
    Key key2 = UtilsCipher.doSimmetricKeyWithPassword( passwd);
  	UtilsCipher cip2 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip2.setKey( key2);
    
    String clear = cip2.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
    
    Key key3 = UtilsCipher.doSimmetricKeyWithPasswordHex( Formats.getHexDump( passwd.getBytes()));
  	UtilsCipher cip3 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip3.setKey( key3);
    
    clear = cip3.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
    
    Key key4 = UtilsCipher.doSimmetricKeyWithPassword( passwd.getBytes());
  	UtilsCipher cip4 = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
    cip4.setKey( key4);
    
    clear = cip4.decode( buf, "UTF-8");
    
    assertEquals( SAMPLE, clear);
  }

  @Test
  public void sweetAsimmetricDoKeyTest() throws Exception {
  	KeyPair pair = UtilsCipher.generateAsymmetricKeys();
  	
  	String publ = Formats.getHexDump( pair.getPublic().getEncoded());
  	String priv = Formats.getHexDump( pair.getPrivate().getEncoded());

    Key kPriv = UtilsCipher.doAsimmetricPrivateKey( priv);
    
    UtilsCipher cip1 = new UtilsCipher( UtilsCipher.TYPE_ASYMMETRIC);
    cip1.setKey( kPriv);
    
    byte buf[] = cip1.encode( SHORT, "UTF-8");
    

    Key kPubl = UtilsCipher.doAsimmetricPublicKey( publ);
    
    UtilsCipher cip2 = new UtilsCipher( UtilsCipher.TYPE_ASYMMETRIC);
    cip2.setKey( kPubl);
    
    String clear = cip2.decode( buf, "UTF-8");
    
    assertEquals( SHORT, clear);
  }
}






