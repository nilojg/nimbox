package es.nimbox.box;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Test;

public class ByteArraysTest {

  @Test
  public void concatTest() {
    byte a[] = Formats.getArrayFromHexString( "00112233445566778899");
    byte aa[] = Formats.getArrayFromHexString( "0011223344556677889900112233445566778899");
    byte b[] = Formats.getArrayFromHexString( "AABBCCDDEEFF");
    byte c[] = Formats.getArrayFromHexString( "");
    
    String resStr = Formats.getHexDump( ByteArrays.concat( a, b));
    assertEquals( "00112233445566778899AABBCCDDEEFF", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.concat( a, c));
    assertEquals( "00112233445566778899", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.concat( c, b));
    assertEquals( "AABBCCDDEEFF", resStr.toUpperCase());

    assertNotNull( ByteArrays.concat( null, null));
    assertEquals( 10, ByteArrays.concat( a, null).length);
    assertArrayEquals( a, ByteArrays.concat( a, null));
    assertEquals( 10, ByteArrays.concat( null, a).length);
    assertArrayEquals( a, ByteArrays.concat( null, a));
    assertEquals( 20, ByteArrays.concat( a, a).length);
    assertArrayEquals( aa, ByteArrays.concat( a, a));
  }
  
  @Test
  public void subarrayTest() {
    assertNull( ByteArrays.subarray( null, 0));
    assertNull( ByteArrays.subarray( null, 0, 0));
    
  	byte a[] = Formats.getArrayFromHexString( "00112233445566778899AABBCCDDEEFF");
    
    String resStr = Formats.getHexDump( ByteArrays.subarray( a, 10));
    assertEquals( "AABBCCDDEEFF", resStr.toUpperCase());

    resStr = Formats.getHexDump( ByteArrays.subarray( a, 16));
    assertEquals( "", resStr.toUpperCase());

    resStr = Formats.getHexDump( ByteArrays.subarray( a, 0));
    assertEquals( "00112233445566778899AABBCCDDEEFF", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.subarray( a, 10, 3));
    assertEquals( "AABBCC", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.subarray( a, 0, 3));
    assertEquals( "001122", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.subarray( a, 16, 0));
    assertEquals( "", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.subarray( a, 0, 16));
    assertEquals( "00112233445566778899AABBCCDDEEFF", resStr.toUpperCase());
    
    resStr = Formats.getHexDump( ByteArrays.subarray( a, 0, 30));
    assertEquals( "00112233445566778899AABBCCDDEEFF", resStr.toUpperCase());
  }
  
  @Test
  public void fillTest() {
    byte a[] = Formats.getArrayFromHexString( "00112233445566778899");
    String resStr = Formats.getHexDump( ByteArrays.fill( a, (byte)32));
    assertEquals( "20202020202020202020", resStr.toUpperCase());
    
    a = Formats.getArrayFromHexString( "00112233445566778899");
    resStr = Formats.getHexDump( ByteArrays.fill( a, (byte)32, 5));
    assertEquals( "00112233442020202020", resStr.toUpperCase());
    
    a = Formats.getArrayFromHexString( "00112233445566778899");
    resStr = Formats.getHexDump( ByteArrays.fill( a, (byte)32, 5, 3));
    assertEquals( "00112233442020208899", resStr.toUpperCase());
    
    a = Formats.getArrayFromHexString( "00112233445566778899");
    resStr = Formats.getHexDump( ByteArrays.fill( a, (byte)32, 5, 10));
    assertEquals( "00112233442020202020", resStr.toUpperCase());
    
    try {
      a = Formats.getArrayFromHexString( "00112233445566778899");
      resStr = Formats.getHexDump( ByteArrays.fill( a, (byte)32, 15, 10));
      
      fail( "Expected IndexOutOfBoundsException");
    }
    catch ( IndexOutOfBoundsException ex) {}
    
    try {
      a = Formats.getArrayFromHexString( "00112233445566778899");
      resStr = Formats.getHexDump( ByteArrays.fill( a, (byte)32, -15, 10));
      
      fail( "Expected IndexOutOfBoundsException");
    }
    catch ( IndexOutOfBoundsException ex) {}
  }

  @Test
  public void indexOfTest() {
    byte a[] = Formats.getArrayFromHexString( "00112233445566778899");
    
    int ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "334455"));
    assertEquals( 3, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "33"));
    assertEquals( 3, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "AA"));
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( ""));
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "778899"));
    assertEquals( 7, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "001122"));
    assertEquals( 0, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "00112233445566778899"));
    assertEquals( 0, ix);
  }
  


  @Test
  public void indexOfFromTest() {
    byte a[] = Formats.getArrayFromHexString( "00112233445566778899");
    
    int ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "334455"), 3);
    assertEquals( 3, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "334455"), 4);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "33"), 3);
    assertEquals( 3, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "33"), 4);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "AA"), 0);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( ""), 0);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "778899"), 0);
    assertEquals( 7, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "778899"), 7);
    assertEquals( 7, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "778899"), 8);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "001122"), 0);
    assertEquals( 0, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "001122"), 5);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "00112233445566778899"), 0);
    assertEquals( 0, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "00112233445566778899"), 3);
    assertEquals( -1, ix);
    
    ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "99"), 10);
    assertEquals( -1, ix);
    
    try {
      ix = ByteArrays.indexOf( a, Formats.getArrayFromHexString( "778899"), -5);
      
      fail( "Expected IndexOutOfBoundsException");
    }
    catch ( IndexOutOfBoundsException ex) {}
  }
}
