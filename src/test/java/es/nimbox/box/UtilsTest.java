package es.nimbox.box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class UtilsTest {

  long testEPOCH = 1536226174015L;
  long testEPOCHnotMillis = 1536226174000L;
  
  String testEPOCHstring = "2018/09/06 11:29:34";
  String testEPOCHwithMillis = "2018/09/06 11:29:34.015";
  String testEPOCHdate = "2018/09/06";
  String testEPOCHtime = "11:29:34";
  
  @Test
  public void testFormatDate() throws Exception {
    String regDate = "yyyy/MM/dd HH:mm:ss";
    String onlyDate = "yyyy/MM/dd";
    String onlyTime = "HH:mm:ss";
    
    Date dateFec = new Date( testEPOCH);
    
    assertEquals( testEPOCHwithMillis, Utils.formatTime( testEPOCH));
    assertEquals( testEPOCHwithMillis, Utils.formatTime( dateFec));
    
    assertEquals( testEPOCHstring, Utils.formatTime( testEPOCH, regDate));
    assertEquals( testEPOCHstring, Utils.formatTime( dateFec, regDate));
    
    assertEquals( testEPOCHdate, Utils.formatTime( testEPOCH, onlyDate));
    assertEquals( testEPOCHdate, Utils.formatTime( dateFec, onlyDate));
    
    assertEquals( testEPOCHtime, Utils.formatTime( testEPOCH, onlyTime));
    assertEquals( testEPOCHtime, Utils.formatTime( dateFec, onlyTime));
  }
  
  @Test
  public void testParseTime() throws Exception {
    String regDate = "yyyy/MM/dd HH:mm:ss";

    assertEquals( testEPOCH, Utils.parseTime( testEPOCHwithMillis).getTime());
    
    assertNotEquals( testEPOCH, Utils.parseTime( testEPOCHwithMillis, regDate).getTime());
    assertEquals( testEPOCHnotMillis, Utils.parseTime( testEPOCHstring, regDate).getTime());
  }
  
  @Test
  public void testFormatDirName() {
    assertEquals( null, Utils.formatDirName( null));
    assertEquals( "/", Utils.formatDirName( "/"));
    assertEquals( "/", Utils.formatDirName( ""));

    assertEquals( "asd/", Utils.formatDirName( "asd"));
    assertEquals( "asd/", Utils.formatDirName( "asd/"));

    assertEquals( "asd/", Utils.formatDirName( "asd", false));
    assertEquals( "asd/", Utils.formatDirName( "asd/", false));
    
    if ( File.separator.equals( "\\") ) {
      assertEquals( "asd\\", Utils.formatDirName( "asd", true));
      assertEquals( "asd\\", Utils.formatDirName( "asd/", true));
      assertEquals( "\\asd\\", Utils.formatDirName( "\\asd", true));
      assertEquals( "\\asd\\", Utils.formatDirName( "\\asd\\", true));
      assertEquals( "\\..\\.\\asd\\", Utils.formatDirName( "\\../.\\asd", true));
      assertEquals( "\\..\\.\\asd\\", Utils.formatDirName( "\\../.\\asd\\", true));
      assertEquals( "/asd/", Utils.formatDirName( "\\asd"));
      assertEquals( "/asd/", Utils.formatDirName( "\\asd\\"));
      assertEquals( "/.././asd/", Utils.formatDirName( "\\../.\\asd"));
      assertEquals( "/.././asd/", Utils.formatDirName( "\\../.\\asd\\"));
      assertEquals( "/asd/", Utils.formatDirName( "\\asd", false));
      assertEquals( "/asd/", Utils.formatDirName( "\\asd\\", false));
      assertEquals( "/.././asd/", Utils.formatDirName( "\\../.\\asd", false));
      assertEquals( "/.././asd/", Utils.formatDirName( "\\../.\\asd\\", false));
    }
    else {
      assertEquals( "asd/", Utils.formatDirName( "asd/", true));
      assertEquals( "asd/", Utils.formatDirName( "asd", true));
    }
  }
  
  @Test
  public void testSleep() {
    long len = 500;
    long maxError = 50;
    
    long fIni = System.currentTimeMillis();
    Utils.sleep( len);
    long fFin = System.currentTimeMillis();
    
    long gap = (fFin - fIni) - len;
    
    assertTrue( gap <= maxError);
  }
  
  @Test
  public void testFindFile() throws IOException {
    assertNull( Utils.findFile( "dummy/dummy.txt"));  // Este no lo debería encontrar
    assertNotNull( Utils.findFile( "/dummy/dummy.txt"));  // Este si porque esta en el CLASSPATH
  }
  
  @Test
  public void testChangeAll() {
    String origText = "En un lugar de la Mancha";
    String destText = "Eb ub lugar de la Mabcha";

    assertEquals( destText, Utils.changeAll( origText, "n", "b"));
    assertEquals( origText, Utils.changeAll( origText, "x", "b"));
  }
  
  @Test
  public void testRemoveAll() {
    String clean = Utils.removeAll( "Françoise O'Brian\t\n from \"l'Hospitalet", "'\"\t\nç");
    assertEquals( "Franoise OBrian from lHospitalet", clean);
    
    clean = Utils.removeAll( null, "'\"\t\nç");
    assertNull( clean);
    
    clean = Utils.removeAll( "Françoise O'Brian\t\n from \"l'Hospitalet", null);
    assertEquals( "Françoise O'Brian\t\n from \"l'Hospitalet", clean);
  }
  
  @Test
  public void testCutString() {
    String cad = "En un lugar de la Mancha de cuyo\nnombre no quiero acordarme...";
    
    assertEquals( "En un lugar de la Mancha de cuyo", Utils.cutString( cad, 0));
    assertEquals( "En un lugar de la Mancha de cuyo", Utils.cutString( cad, -5));
    assertEquals( "En un luga...", Utils.cutString( cad, 13));
  }
  
  @Test
  public void testArrayList() {
    String[] arr = { "aa", "bb", "cc", "dd"};
    
    assertEquals( "'aa', 'bb', 'cc', 'dd'", Utils.getArrayListed( arr));
  }
  
  @Test
  public void textRegex() throws Exception {
    String cad = "En un lugar de la Mancha de cuyo\nnombre no quiero acordarme...";
    
    List<String> res = Utils.extractRegex( cad, "\\hl");
    
    assertEquals( 2, res.size());
    
    assertEquals( " Mancha", Utils.extractFirstRegex( cad, "\\hMancha"));
    assertEquals( " Mancha ", Utils.extractFirstRegex( cad, "(?<=la).*?(?=de)"));
  }
  
  @Test
  public void testFill() throws Exception {
    assertEquals( "", Utils.fill( " ", 0));
    assertEquals( "", Utils.fill( " ", -1));
    assertEquals( "   ", Utils.fill( " ", 3));
    assertEquals( "asdasdasd", Utils.fill( "asd", 3));
  }
}
