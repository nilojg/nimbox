package es.nimbox.box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.w3c.dom.Node;

public class ElasticBoxTest {
  private Date date  = Utils.parseTime( "2019/11/22 20:00:00.000");
  
  private ElasticBox getElasticBox() {
    ElasticBox eb = new ElasticBox();
    
    eb.setId( "1");
    eb.set( "A", "a");
    eb.set( "B", 1);
    eb.set( "C", 1.2);
    eb.set( "D", 2l);
    eb.set( "E", 2.3f);
    eb.set( "F", true);
    eb.set( "G", date);
    
    return eb;
  }

  private ElasticBox getSonElasticBox() {
    ElasticBox eb = new ElasticBox();
    
    eb.setId( "1");
    eb.set( "val1", "en un lugar");
    eb.set( "val2", "de la mancha");
    eb.set( "val3", "de cuyo nombre");
    eb.set( "val4", "no quiero acordarme");
    
    return eb;
  }
  
  private void validateJSON( String json) {
    assertTrue( Formats.isValidJSON( json));
  }
  
  
  @Test
  public void sweetTest() {
    ElasticBox eb = getElasticBox();
    
    assertEquals( "1", eb.getId());
    assertEquals(  "a", eb.get( "A"));
    assertTrue(  1 == eb.getAsInt( "B"));
    assertTrue(  1.2 == eb.getAsDouble( "C"));
    assertTrue(  2l == eb.getAsLong( "D"));
    assertTrue(  2.3f == eb.getAsFloat( "E"));
    assertTrue(  true == eb.getAsBoolean( "F"));
    assertEquals(  date, eb.getAsDate( "G"));
  }
  
  @Test
  public void toXML_JSON_Test() {
    ElasticBox eb = getElasticBox();
    
    ElasticBox son = new ElasticBox();
    son.setId( "1");
    son.set( "name", "Garfield");
    son.set( "surname", "The Cat");
    eb.set( "pet", son);
    
    List<String> list = new ArrayList<String>();
    list.add( "hola");
    list.add( "y");
    list.add( "adios");
    list.add( "mundo");
    list.add( "cruel");
    
    List<ElasticBox> list2 = new ArrayList<ElasticBox>();
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    
    Map<String,ElasticBox> map = new HashMap<>();
    map.put( "m1", getSonElasticBox());
    map.put( "m2", getSonElasticBox());
    map.put( "m3", getSonElasticBox());
    
    Map<String,Object> map2 = new HashMap<>();
    map2.put( "m1", "Azul");
    map2.put( "m2", 23);
    map2.put( "m3", date);

    eb.set( "lista", list);
    eb.set( "lista2", list2);
    eb.set( "mapa1", map);
    eb.set( "mapa1", map2);
    
    String json = eb.toJSON();
    validateJSON( json);
    
    assertEquals( "{\"id\": \"1\", \"A\": \"a\", \"B\": \"1\", \"C\": \"1.2\", \"D\": \"2\", \"E\": \"2.3\", \"F\": \"true\", \"G\": \"2019/11/22 20:00:00.000\", \"lista\": [\"hola\", \"y\", \"adios\", \"mundo\", \"cruel\"], \"lista2\": [{\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}, {\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}, {\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}, {\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}], \"mapa1\": [\"Azul\", \"23\", \"2019/11/22 20:00:00.000\"], \"pet\": {\"id\": \"1\", \"name\": \"Garfield\", \"surname\": \"The Cat\"}}", 
                  json);
    assertEquals( "{\"id\": \"1\", \"A\": \"a\", \"B\": \"1\", \"C\": \"1.2\", \"D\": \"2\", \"E\": \"2.3\", \"F\": \"true\", \"G\": \"2019/11/22 20:00:00.000\", \"lista\": [\"hola\", \"y\", \"adios\", \"mundo\", \"cruel\"], \"lista2\": [{\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}, {\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}, {\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}, {\"id\": \"1\", \"val1\": \"en un lugar\", \"val2\": \"de la mancha\", \"val3\": \"de cuyo nombre\", \"val4\": \"no quiero acordarme\"}], \"mapa1\": [\"Azul\", \"23\", \"2019/11/22 20:00:00.000\"], \"pet\": {\"id\": \"1\", \"name\": \"Garfield\", \"surname\": \"The Cat\"}}", 
                  eb.toString());
    assertEquals( "<id><![CDATA[1]]></id><A type='java.lang.String'><![CDATA[a]]></A><B type='java.lang.Integer'><![CDATA[1]]></B><C type='java.lang.Double'><![CDATA[1.2]]></C><D type='java.lang.Long'><![CDATA[2]]></D><E type='java.lang.Float'><![CDATA[2.3]]></E><F type='java.lang.Boolean'><![CDATA[true]]></F><G type='java.util.Date'><![CDATA[2019/11/22 20:00:00.000]]></G><lista type='java.util.ArrayList'><item0 type='java.lang.String'><![CDATA[hola]]></item0><item1 type='java.lang.String'><![CDATA[y]]></item1><item2 type='java.lang.String'><![CDATA[adios]]></item2><item3 type='java.lang.String'><![CDATA[mundo]]></item3><item4 type='java.lang.String'><![CDATA[cruel]]></item4></lista><lista2 type='java.util.ArrayList'><item0 type='es.nimbox.box.ElasticBox'><id><![CDATA[1]]></id><val1 type='java.lang.String'><![CDATA[en un lugar]]></val1><val2 type='java.lang.String'><![CDATA[de la mancha]]></val2><val3 type='java.lang.String'><![CDATA[de cuyo nombre]]></val3><val4 type='java.lang.String'><![CDATA[no quiero acordarme]]></val4></item0><item1 type='es.nimbox.box.ElasticBox'><id><![CDATA[1]]></id><val1 type='java.lang.String'><![CDATA[en un lugar]]></val1><val2 type='java.lang.String'><![CDATA[de la mancha]]></val2><val3 type='java.lang.String'><![CDATA[de cuyo nombre]]></val3><val4 type='java.lang.String'><![CDATA[no quiero acordarme]]></val4></item1><item2 type='es.nimbox.box.ElasticBox'><id><![CDATA[1]]></id><val1 type='java.lang.String'><![CDATA[en un lugar]]></val1><val2 type='java.lang.String'><![CDATA[de la mancha]]></val2><val3 type='java.lang.String'><![CDATA[de cuyo nombre]]></val3><val4 type='java.lang.String'><![CDATA[no quiero acordarme]]></val4></item2><item3 type='es.nimbox.box.ElasticBox'><id><![CDATA[1]]></id><val1 type='java.lang.String'><![CDATA[en un lugar]]></val1><val2 type='java.lang.String'><![CDATA[de la mancha]]></val2><val3 type='java.lang.String'><![CDATA[de cuyo nombre]]></val3><val4 type='java.lang.String'><![CDATA[no quiero acordarme]]></val4></item3></lista2><mapa1 type='java.util.HashMap'><m1 type='java.lang.String'><![CDATA[Azul]]></m1><m2 type='java.lang.Integer'><![CDATA[23]]></m2><m3 type='java.util.Date'><![CDATA[2019/11/22 20:00:00.000]]></m3></mapa1><pet type='es.nimbox.box.ElasticBox'><id><![CDATA[1]]></id><name type='java.lang.String'><![CDATA[Garfield]]></name><surname type='java.lang.String'><![CDATA[The Cat]]></surname></pet>", 
                  eb.toXML());
    
    ElasticBox eb2 = new ElasticBox();
    eb2.setId( "1");
    eb2.set( "v1", "v1");
    eb2.set( "v2", null);
    eb2.set( "v3", "v3");
    
    json = eb2.toString();
    validateJSON( json);
  }
  
  @Test
  public void copyAndEqualsTest() {
    ElasticBox eb1 = getElasticBox();
    ElasticBox eb2 = new ElasticBox( eb1);
    
    assertEquals( eb1, eb2);
    assertFalse( eb1.equals( null));
    assertTrue( eb1.equals( eb2));
    assertTrue( eb2.equals( eb1));
  }
  
  @Test
  public void getFieldsTest() {
    ElasticBox eb1 = getElasticBox();
    
    StringBuilder ff = new StringBuilder();
    for ( String f : eb1.getSortedFields() ) {
      ff.append( f);
    }
    
    assertEquals( "ABCDEFG", ff.toString());
    
    ff = new StringBuilder();
    for ( String f : eb1.getSortedFields( "[ACF]") ) {
      ff.append( f);
    }
    
    assertEquals( "ACF", ff.toString());
  }
  
  @Test
  public void fromSimpleJsonTest() {
    ElasticBox eb1 = getElasticBox();
    String json1 = eb1.toJSON();
    validateJSON( json1);
        
    ElasticBox eb2 = ElasticBox.fromJSON( json1);
    String json2 = eb2.toJSON();
    validateJSON( json2);
    
    assertEquals( json1, json2);
    
    json1 = eb1.toJSON();
    validateJSON( json1);
    
    ElasticBox eb3 = ElasticBox.fromJSON( json1);
    String json3 = eb3.toJSON();
    validateJSON( json3);
    
    assertEquals( json1, json3);
    
    assertEquals( "1", eb2.getId());
    assertEquals(  "a", eb2.get( "A"));
    assertTrue(  1 == eb2.getAsInt( "B"));
    assertTrue(  1.2 == eb2.getAsDouble( "C"));
    assertTrue(  2l == eb2.getAsLong( "D"));
    assertTrue(  2.3f == eb2.getAsFloat( "E"));
    assertTrue(  true == eb2.getAsBoolean( "F"));
    assertEquals(  date, eb2.getAsDate( "G"));
    
    assertEquals( "1", eb3.getId());
    assertEquals(  "a", eb3.get( "A"));
    assertTrue(  1 == eb3.getAsInt( "B"));
    assertTrue(  1.2 == eb3.getAsDouble( "C"));
    assertTrue(  2l == eb3.getAsLong( "D"));
    assertTrue(  2.3f == eb3.getAsFloat( "E"));
    assertTrue(  true == eb3.getAsBoolean( "F"));
    assertEquals(  date, eb3.getAsDate( "G"));
  }
  
  @Test
  public void fromSimpleJsonWithQuotesTest() {
    String withQuotes = "Mr O'Donnel, a string with a \" that, of course, should break something somewhere";
    
    ElasticBox eb1 = new ElasticBox();
    eb1.set( "quote", withQuotes);
    String json1 = eb1.toJSON();
    validateJSON( json1);
    
    ElasticBox eb2 = ElasticBox.fromJSON( json1);
    String json2 = eb2.toJSON();
    validateJSON( json2);
    
    assertEquals( withQuotes, eb2.get( "quote"));
  }
  
  @Test
  public void fromComplexJsonTest() {
    ElasticBox eb1 = getElasticBox();
    
    ElasticBox son = new ElasticBox();
    son.setId( "1");
    son.set( "name", "Garfield");
    son.set( "surname", "The Cat");
    eb1.set( "pet", son);
    
    List<String> list = new ArrayList<String>();
    list.add( "hola");
    list.add( "y");
    list.add( "adios");
    list.add( "mundo");
    list.add( "cruel");
    eb1.set( "lista", list);
    
    List<ElasticBox> list2 = new ArrayList<ElasticBox>();
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    eb1.set( "lista2", list2);
    
    Map<String,ElasticBox> map = new HashMap<>();
    map.put( "m1", getSonElasticBox());
    map.put( "m2", getSonElasticBox());
    map.put( "m3", getSonElasticBox());
    eb1.set( "mapa1", map);
    
    Map<String,Object> map2 = new HashMap<>();
    map2.put( "m1", "Azul");
    map2.put( "m2", 23);
    map2.put( "m3", date);
    eb1.set( "mapa2", map2);

    String json1 = eb1.toJSON();
    validateJSON( json1);
            
    ElasticBox eb2 = ElasticBox.fromJSON( json1);
    String json2 = eb2.toJSON();
    validateJSON( json2);
    
    assertEquals( json1, json2);
    
    assertEquals( "1", eb2.getId());
    assertEquals(  "a", eb2.get( "A"));
    assertTrue(  1 == eb2.getAsInt( "B"));
    assertTrue(  1.2 == eb2.getAsDouble( "C"));
    assertTrue(  2l == eb2.getAsLong( "D"));
    assertTrue(  2.3f == eb2.getAsFloat( "E"));
    assertTrue(  true == eb2.getAsBoolean( "F"));
    assertEquals(  date, eb2.getAsDate( "G"));
  }
  
  
  @Test
  public void fromJSONTest() throws Exception {
    String json1 = "[ \"val1\", \"val2\", \"val3\"]";
    
    ElasticBox eb1 = ElasticBox.fromJSON( json1);
    assertEquals( 3, ((List<?>)eb1.get( "anonymous0")).size());
    
    String json2 = "{ \"list1\" : [ \"val1\", \"val2\", \"val3\"], \"list2\" : [ \"val1\", \"val2\"]  }";
    
    ElasticBox eb2 = ElasticBox.fromJSON( json2);
    assertEquals( 3, ((List<?>)eb2.get( "list1")).size());
    assertEquals( 2, ((List<?>)eb2.get( "list2")).size());
    
    String json3 = "{ \"list\" : [ \"Danny O'Connor\", \"Un \\\"tonto\\\" util\", \"val3\"] }";
    
    ElasticBox eb3 = ElasticBox.fromJSON( json3);
    @SuppressWarnings( "unchecked")
    List<String> ll = (List<String>)eb3.get( "list");
    assertEquals( 3, ll.size());
    assertEquals( "Danny O'Connor", ll.get( 0));
    assertEquals( "Un \"tonto\" util", ll.get( 1));
    assertEquals( "val3", ll.get( 2));
    
    String json4 = "{\n" + 
        "  \"queries\" : [\n" + 
        "    \"select * from asd where id is null and nombre <> 'gatito'\",\n" + 
        "    \"select * from asd where id is null and nombre <> 'gatito' and fecha >= '2020/01/01 12:56:00'\",\n" + 
        "    \"select * from asd where id is null and nombre <> 'gatito' and fecha >= '2020/01/01 12:56:00' \",\n" + 
        "    \"select * from asd where id is null and (nombre <> 'gatito' or nombre = 'perrito')\"\n" + 
        "  ]\n" + 
        "}";
    
    ElasticBox eb4 = ElasticBox.fromJSON( json4);
    @SuppressWarnings( "unchecked")
    List<String> oo = (List<String>)eb4.get( "queries");
    assertEquals( "select * from asd where id is null and nombre <> 'gatito'", oo.get( 0));
    assertEquals( "select * from asd where id is null and nombre <> 'gatito' and fecha >= '2020/01/01 12:56:00'", oo.get( 1));
    assertEquals( "select * from asd where id is null and nombre <> 'gatito' and fecha >= '2020/01/01 12:56:00' ", oo.get( 2));
    assertEquals( "select * from asd where id is null and (nombre <> 'gatito' or nombre = 'perrito')", oo.get( 3));
  }
  
  @Test
  public void fromSimpleXMLTest() throws Exception {
    ElasticBox eb1 = getElasticBox();
    String xml1 = "<a>" + eb1.toXML() + "</a>";
    
    ElasticBox eb2 = ElasticBox.fromXML( xml1);
    String xml2 = "<a>" + eb2.toXML() + "</a>";
    
    assertEquals( xml1, xml2);
  }
  
  @Test
  public void fromComplexXMLTest() {
    ElasticBox eb1 = getElasticBox();
    
    ElasticBox son = new ElasticBox();
    son.setId( "1");
    son.set( "name", "Garfield");
    son.set( "surname", "The Cat");
    eb1.set( "pet", son);
    
    List<String> list = new ArrayList<String>();
    list.add( "hola");
    list.add( "y");
    list.add( "adios");
    list.add( "mundo");
    list.add( "cruel");
    eb1.set( "lista", list);
    
    List<ElasticBox> list2 = new ArrayList<ElasticBox>();
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    eb1.set( "lista2", list2);
    
    Map<String,ElasticBox> map = new HashMap<>();
    map.put( "m1", getSonElasticBox());
    map.put( "m2", getSonElasticBox());
    map.put( "m3", getSonElasticBox());
    eb1.set( "mapa1", map);
    
    Map<String,Object> map2 = new HashMap<>();
    map2.put( "m1", "Azul");
    map2.put( "m2", 23);
    map2.put( "m3", date);
    eb1.set( "mapa2", map2);

    String xml1 = "<a>" + eb1.toXML() + "</a>";
    
    ElasticBox eb2 = ElasticBox.fromXML( xml1);
    String xml2 = "<a>" + eb2.toXML() + "</a>";
    
    assertEquals( xml1, xml2);
  }
  
  @Test
  public void fromSimpleLightXMLTest() throws Exception {
    ElasticBox eb1 = getElasticBox();
    String xml1 = "<a>" + eb1.toXML( false) + "</a>";
    
    ElasticBox eb2 = ElasticBox.fromXML( xml1);
    String xml2 = "<a>" + eb2.toXML( false) + "</a>";
    
    assertEquals( xml1, xml2);
  }
  
  @Test
  public void fromComplexLightXMLTest() {
    ElasticBox eb1 = getElasticBox();
    
    ElasticBox son = new ElasticBox();
    son.setId( "1");
    son.set( "name", "Garfield");
    son.set( "surname", "The Cat");
    eb1.set( "pet", son);
    
    List<String> list = new ArrayList<String>();
    list.add( "hola");
    list.add( "y");
    list.add( "adios");
    list.add( "mundo");
    list.add( "cruel");
    eb1.set( "lista", list);
    
    List<ElasticBox> list2 = new ArrayList<ElasticBox>();
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    list2.add( getSonElasticBox());
    eb1.set( "lista2", list2);
    
    Map<String,ElasticBox> map = new HashMap<>();
    map.put( "m1", getSonElasticBox());
    map.put( "m2", getSonElasticBox());
    map.put( "m3", getSonElasticBox());
    eb1.set( "mapa1", map);
    
    Map<String,Object> map2 = new HashMap<>();
    map2.put( "m1", "Azul");
    map2.put( "m2", 23);
    map2.put( "m3", date);
    eb1.set( "mapa2", map2);

    String xml1 = "<a>" + eb1.toXML( false) + "</a>";
    
    ElasticBox eb2 = ElasticBox.fromXML( xml1);
    String xml2 = "<a>" + eb2.toXML( false) + "</a>";
    
    assertEquals( "<a><id><![CDATA[1]]></id><A><![CDATA[a]]></A><B><![CDATA[1]]></B><C><![CDATA[1.2]]></C><D><![CDATA[2]]></D><E><![CDATA[2.3]]></E><F><![CDATA[true]]></F><G><![CDATA[2019/11/22 20:00:00.000]]></G><lista><item0><![CDATA[hola]]></item0><item1><![CDATA[y]]></item1><item2><![CDATA[adios]]></item2><item3><![CDATA[mundo]]></item3><item4><![CDATA[cruel]]></item4></lista><lista2><item0><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item0><item1><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item1><item2><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item2><item3><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item3></lista2><mapa1><item0><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item0><item1><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item1><item2><item0><![CDATA[1]]></item0><item1><![CDATA[en un lugar]]></item1><item2><![CDATA[de la mancha]]></item2><item3><![CDATA[de cuyo nombre]]></item3><item4><![CDATA[no quiero acordarme]]></item4></item2></mapa1><mapa2><item0><![CDATA[Azul]]></item0><item1><![CDATA[23]]></item1><item2><![CDATA[2019/11/22 20:00:00.000]]></item2></mapa2><pet><item0><![CDATA[1]]></item0><item1><![CDATA[Garfield]]></item1><item2><![CDATA[The Cat]]></item2></pet></a>", 
                  xml2);
  }
  
  @Test
  public void nullXML_JSON_Test() throws Exception {
    assertNull( ElasticBox.fromXML( (String)null));
    assertNull( ElasticBox.fromXML( (Node)null));
    assertNull( ElasticBox.fromXML( ""));

    assertNull( ElasticBox.fromJSON( null));
    assertNull( ElasticBox.fromJSON( ""));
  }
  
  @Test ( expected = ParseException.class)
  public void wrongXMLTest() {
    ElasticBox.fromXML( "<a><id>azul</a>");
  }
  
  @Test ( expected = ParseException.class)
  public void wrongJSONTest() {
    ElasticBox.fromJSON( "{ \"asd\" }");
  }
  
  @Test
  public void getNullTest() {
    ElasticBox eb = new ElasticBox();

    assertNull( eb.get( "_A"));
    assertNull( eb.getAsInt( "_B"));
    assertNull( eb.getAsDouble( "_C"));
    assertNull( eb.getAsLong( "_D"));
    assertNull( eb.getAsFloat( "_E"));
    assertNull( eb.getAsBoolean( "_F"));
    assertNull( eb.getAsDate( "_G"));
    
    eb.set( "A", null);
    eb.set( null, "isNull");
    
    assertNull( eb.get( "A"));
    assertNull( eb.get( "A222"));
    assertEquals( "isNull", eb.get( null));
  }
  
  @Test
  public void formattedTest() throws Exception {
    SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd");
    
    ElasticBox eb = new ElasticBox();
    
    eb.set( "a", "2000/12/12", sdf);
    assertEquals( sdf.parse( "2000/12/12"), eb.getAsDate( "a"));
    
    eb.set( "b", "2000/12/12", "yyyy/MM/dd");
    assertEquals( sdf.parse( "2000/12/12"), eb.getAsDate( "b"));
  }
}
