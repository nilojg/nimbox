
package es.nimbox.box;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;

import org.junit.AfterClass;
import org.junit.Test;

import dummy.ToXMLTestSimple;

import static org.junit.Assert.*;

public class FormatsTest {
  public static final String FILE_PREFIX = "test.file.";

  @AfterClass
  public static void cleanFile() {
    File toDelete[] = new File( ".").listFiles();
    
    for ( File ff : toDelete ) {
      if ( ff.getName().startsWith( FILE_PREFIX) ) {
        ff.delete();
      }
    }
  } 
  
  
  
  
  @Test
  public void hexTest() {
    String text = "En un lugar de la Mancha de cuyo nombre no quiero acordarme";
    
    String textHEX = Formats.getHexDump( text.getBytes());
    byte [] textBackByte = Formats.getArrayFromHexString( textHEX);
    String textBack = new String( textBackByte);

    assertEquals( text, textBack);

    assertEquals( "", Formats.getHexDump( null));
    assertEquals( 0, Formats.getArrayFromHexString( null).length);
  }
  
  @Test
  public void base64Test() {
    String text = "En un lugar de la Mancha de cuyo nombre no quiero acordarme";
    String textBase64 = "RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1eW8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=";
    
    assertEquals( textBase64, Formats.encodeBase64( text));
    assertEquals( text, Formats.decodeBase64( textBase64));
  }
  
  @Test
  public void base64FilesTest() throws Exception {
    String text = "En un lugar de la Mancha de cuyo nombre no quiero acordarme";
    
    Formats.encodeBase64( text, FILE_PREFIX + "b64.text.txt");
    
    byte read[] = Formats.decodeBase64File( FILE_PREFIX + "b64.text.txt");
    assertEquals( text, new String( read));
  }
  
  @Test
  public void base64FilesDirectCopyTest() throws Exception {
    byte bFileClear[] = Utils.readBytesFromInputStream( Utils.findFile( "pom.xml"));
    String sFileB64 = Formats.encodeBase64( bFileClear);
    
    // Write B64 version reading directly original file
    try ( OutputStream os = new FileOutputStream( FILE_PREFIX + "pom.xml.b64"); ) { 
      Formats.encodeBase64( Utils.findFile( "pom.xml"), os);
    }
    catch ( Exception ex) {
      fail( ex.getMessage());
    }
    
    // Ensure the written B64 is the same as expected
    try ( InputStream is = Utils.findFile( FILE_PREFIX + "pom.xml.b64")) {
      String writenB64 = Utils.readFromInputStream( is);
      assertEquals( sFileB64, writenB64);
    }
    catch ( Exception ex) {
      fail( ex.getMessage());
    }
    
    // Copy a clear version of the B64 file
    try ( InputStream is = Utils.findFile( FILE_PREFIX + "pom.xml.b64");
          OutputStream os = new FileOutputStream( FILE_PREFIX + "pom.xml.clear"); ) { 
      Formats.decodeBase64( is, os);
    }
    catch ( Exception ex) {
      fail( ex.getMessage());
    }
    
    // Ensure the written clear is the same as expected
    try ( InputStream is = Utils.findFile( FILE_PREFIX + "pom.xml.clear")) {
      byte writenClear[] = Utils.readBytesFromInputStream( is);
      assertArrayEquals( bFileClear, writenClear);
    }
    catch ( Exception ex) {
      fail( ex.getMessage());
    }
  }
  
  @Test
  public void testMalformedBase64Test() {
    String text = "En un lugar de la Mancha de cuyo nombre no quiero acordarme";
    String textBase64_1 = "   RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1eW8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=   ";
    String textBase64_2 = "RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1e" + "\n" + "W8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=";
    String textBase64_3 = "RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1e" + "\n\r" + "W8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=";
    String textBase64_4 = "RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1e" + "\r\n" + "W8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=";
    String textBase64_5 = "RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1e" + "\t" + "W8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=";
    String textBase64_6 = "RW4gdW4gbHVnYXIgZGUgbGEgTWFuY2hhIGRlIGN1e" + " " + "W8gbm9tYnJlIG5vIHF1aWVybyBhY29yZGFybWU=";
    
    assertEquals( text, Formats.decodeBase64( textBase64_1));
    assertEquals( text, Formats.decodeBase64( textBase64_2));
    assertEquals( text, Formats.decodeBase64( textBase64_3));
    assertEquals( text, Formats.decodeBase64( textBase64_4));
    assertEquals( text, Formats.decodeBase64( textBase64_5));
    assertEquals( text, Formats.decodeBase64( textBase64_6));
    
    assertEquals( text, Formats.decodeBase64( textBase64_1, true));
    assertEquals( text, Formats.decodeBase64( textBase64_2, true));
    assertEquals( text, Formats.decodeBase64( textBase64_3, true));
    assertEquals( text, Formats.decodeBase64( textBase64_4, true));
    assertEquals( text, Formats.decodeBase64( textBase64_5, true));
    assertEquals( text, Formats.decodeBase64( textBase64_6, true));
    
    try {
      assertEquals( text, Formats.decodeBase64( textBase64_1, false));
      fail();
    }
    catch ( IllegalArgumentException ex) {}

    try {
      assertEquals( text, Formats.decodeBase64( textBase64_2, false));
      fail();
    }
    catch ( IllegalArgumentException ex) {}

    try {
      assertEquals( text, Formats.decodeBase64( textBase64_3, false));
      fail();
    }
    catch ( IllegalArgumentException ex) {}

    try {
      assertEquals( text, Formats.decodeBase64( textBase64_4, false));
      fail();
    }
    catch ( IllegalArgumentException ex) {}

    try {
      assertEquals( text, Formats.decodeBase64( textBase64_5, false));
      fail();
    }
    catch ( IllegalArgumentException ex) {}

    try {
      assertEquals( text, Formats.decodeBase64( textBase64_6, false));
      fail();
    }
    catch ( IllegalArgumentException ex) {}
  }
  
  @Test
  public void validJSON_Test() {
    assertTrue( Formats.isValidJSON( "{ \"a\" : \"a1\"}"));
    assertFalse( Formats.isValidJSON( "{ \"a\"}"));
    assertFalse( Formats.isValidJSON( "{ \"a\" : \"a1\""));
//    assertFalse( Formats.isValidJSON( " \"a\" : \"a1\"}"));
//    assertFalse( Formats.isValidJSON( "\"a\" : \"a1\""));
  }
  
  @Test
  public void json_Test() {
    ToXMLTestSimple t1 = new ToXMLTestSimple( "Gatopardo", 23, 50L, 10e56);
    
    String jsonT1 = Formats.toJson( t1);
    
    assertEquals( "{\"field1\":\"Gatopardo\",\"field2\":23,\"field3\":50,\"field4\":1.0E57,\"l1\":[\"Gatopardo\",23,50,1.0E57]}", jsonT1);
    
    ToXMLTestSimple t2 = Formats.fromJson( jsonT1, ToXMLTestSimple.class);

    assertEquals( t1.field1, t2.field1);
    assertEquals( t1.field2, t2.field2);
    assertEquals( t1.field3, t2.field3);
    assertEquals( t1.field4, t2.field4);
    
    String jsonT2 = Formats.toJsonPretty( t1);
    
    // This is just to avoid problems when testing on \n operative systems
    jsonT2 = Utils.changeAll( jsonT2, "\r", "");
    
    assertEquals( "{\n"
        + "  \"field1\" : \"Gatopardo\",\n"
        + "  \"field2\" : 23,\n"
        + "  \"field3\" : 50,\n"
        + "  \"field4\" : 1.0E57,\n"
        + "  \"l1\" : [ \"Gatopardo\", 23, 50, 1.0E57 ]\n"
        + "}", jsonT2);

    String jsonT3 = Formats.toJsonPretty( jsonT2.replace( "\n", ""));

    assertEquals( jsonT2, jsonT3.replace( "\r", ""));
  }
  
  @Test
  @SuppressWarnings( "deprecation")  // This test tests deprecated methods....
  public void json_deprecatedTest() {
    ToXMLTestSimple[] ar1 = new ToXMLTestSimple[5];
    
    for ( int i = 0; i < ar1.length; i++) {
      ar1[i] = new ToXMLTestSimple( "Gatopardo", i, 50L, 10e56);
    }
    
    String jsonT1_Class = Formats.toJson( ar1);
    String jsonT1_Type = Formats.toJson( ar1, ar1.getClass().getComponentType());
    
    assertEquals( jsonT1_Class, jsonT1_Type);
    
    ToXMLTestSimple[] t2_Class = Formats.fromJson( jsonT1_Class, ToXMLTestSimple[].class);
    ToXMLTestSimple[] t2_Type = Formats.fromJson( jsonT1_Type, (Type)ar1.getClass());
    
    assertEquals( Formats.toJson( t2_Class), Formats.toJson( t2_Type));
  }

  @Test
  public void wrapTest() {
    String text = "This is a text line";
    String wrp3 = "Thi\ns i\ns a\n te\nxt \nlin\ne";

    String wrapped = Formats.lineWrap( text, 3);

    testLineLengths( wrapped.split( "\n"), 3);

    assertEquals( wrp3, wrapped);

    wrapped = Formats.lineWrap( text, 6, true);
    testLineLengths( wrapped.split( "\n"), 6);

    wrapped = Formats.lineWrap( text, 2, true);
    testLineLengths( wrapped.split( "\n"), 2);

    wrapped = Formats.lineWrap( text, 1, true);
    testLineLengths( wrapped.split( "\n"), 1);

    assertThrows( IllegalArgumentException.class, ()-> Formats.lineWrap( text, 0));
    assertThrows( IllegalArgumentException.class, ()-> Formats.lineWrap( text, -1));
  }

  private void testLineLengths(String[] lines, int length) {
    for ( String line : lines) {
      if ( line.length() > length) {
        fail( "Long line : [" + line + "]");
      }
    }
  }
}
