package es.nimbox.box;


import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class NimboxVersionTest {

  @Test
  public void sweetTest() throws Exception {
    assertEquals( "orion", NimboxVersion.getProperty( "git.build.host"));
    assertEquals( "5f37afd5ce2702e2c8a5f984eaaff5b0236cf47a", NimboxVersion.getProperty( "git.commit.id"));
    assertNull( NimboxVersion.getProperty( "gatito"));

    assertEquals( "orion", NimboxVersion.getProperty( NimboxVersion.getBuildPropertiesFor( "nimbox"), "git.build.host"));
    assertEquals( "5f37afd5ce2702e2c8a5f984eaaff5b0236cf47a", NimboxVersion.getProperty( NimboxVersion.getBuildPropertiesFor( "nimbox"), "git.commit.id"));
    assertNull( NimboxVersion.getProperty( NimboxVersion.getBuildPropertiesFor( "nimbox"), "gatito"));

    try {
      NimboxVersion.getProperty( "dog", "git.build.host");
      fail( "NimboxVersion.getProperty should throw an exception here because the file doesn't exist");
    }
    catch ( IOException ex ) {}

  }


  @Test
  public void sweetFromManifest() throws Exception {
    assertEquals( "orion", NimboxVersion.getManifestPropertyFromJar("BOOT-INF/lib/nimbox-0.1.40.jar", "nimbox_git_build_host"));
    assertEquals( "5f37afd5ce2702e2c8a5f984eaaff5b0236cf47a", NimboxVersion.getManifestPropertyFromJar("BOOT-INF/lib/nimbox-0.1.40.jar", "nimbox_git_commit_id"));
    assertNull( NimboxVersion.getManifestPropertyFromJar("BOOT-INF/lib/nimbox-0.1.40.jar", "gatito"));

  }
}



/*
Manifest-Version: 1.0
Created-By: Maven JAR Plugin 3.3.0
Build-Jdk-Spec: 17
nimbox_artifactId: nimbox
nimbox_git_build_host: orion
nimbox_git_build_time: 2023-03-25T17:47:31+0100
nimbox_git_build_user_email: nilo.j.gonzalez@gmail.com
nimbox_git_build_user_name: Nilo J. Gonzalez
nimbox_git_build_version: 0.1.40
nimbox_git_closest_tag_name: 0.1.32
nimbox_git_commit_author_time: 2023-03-04T11:22:23+0100
nimbox_git_commit_committer_time: 2023-03-04T11:22:23+0100
nimbox_git_commit_id: 5f37afd5ce2702e2c8a5f984eaaff5b0236cf47a
nimbox_git_commit_id_abbrev: 5f37afd
nimbox_git_commit_time: 2023-03-04T11:22:23+0100
nimbox_git_commit_user_email: nilo.j.gonzalez@gmail.com
nimbox_git_commit_user_name: Nilo J. Gonzalez
nimbox_git_dirty: true
nimbox_git_local_branch_ahead: 0
nimbox_git_local_branch_behind: 0
nimbox_git_remote_origin_url: git@gitlab.com:nilojg/nimbox.git
nimbox_groupId: es.nimbox.box
nimbox_java_version: 1.8
nimbox_version: 0.1.40
 */