package es.nimbox.box;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import dummy.ToXMLTestAnnotated;
import dummy.ToXMLTestAnnotated2;
import dummy.ToXMLTestAnnotated3;
import dummy.ToXMLTestAnnotated4;
import dummy.ToXMLTestSimple;
import dummy.ToXMLTestSimpleArray;

public class XMLTest {
  private String xmlTest =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
                            "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n" + 
                            "    <soap:Body xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"\r\n" + 
                            "               xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                            "               xmlns:typ=\"http://www.www.com/2012/\" " +
                            "               xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\r\n" + 
                            
                            "        <typ:TuriruAlfa></typ:TuriruAlfa>\r\n" + 
                            
                            "        <typ:TuriruAlfa>\r\n" + 
                            "            <typ:TuriruBeta>\r\n" + 
                            "                <codError>0</codError>\r\n" + 
                            
                            "                <typ:TuriruGamma>data23</typ:TuriruGamma>\r\n" + 
                            "                <TuriruGamma />\r\n" + 
                            
                            "                <TuriruDelta>data55</TuriruDelta>\r\n" + 
                            "            </typ:TuriruBeta>\r\n" + 
                            "        </typ:TuriruAlfa>\r\n" + 
                            "    </soap:Body>\r\n" + 
                            "</soapenv:Envelope>";
  
  private String xmlT1 = "<ToXMLTestSimple><field1>gato</field1><field2>5</field2><field3>4</field3><field4>3.4</field4><l1><_l1>gato</_l1><_l1>5</_l1><_l1>4</_l1><_l1>3.4</_l1></l1></ToXMLTestSimple>"; 

  private String xmlT2 = "<ToXMLTestSimple><field2>2</field2><field3>3</field3><l1><_l1>null</_l1><_l1>2</_l1><_l1>3</_l1><_l1>null</_l1></l1></ToXMLTestSimple>"; 
  
  private String xmlT2_i2 = 
      "<ToXMLTestSimple>\n" + 
      "  <field2>2</field2>\n" + 
      "  <field3>3</field3>\n" + 
      "  <l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "    <_l1>2</_l1>\n" + 
      "    <_l1>3</_l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "  </l1>\n" + 
      "</ToXMLTestSimple>"; 
  
  private String xmlT2_i2_b3 = 
      "   <ToXMLTestSimple>\n" + 
      "     <field2>2</field2>\n" + 
      "     <field3>3</field3>\n" + 
      "     <l1>\n" + 
      "       <_l1>null</_l1>\n" + 
      "       <_l1>2</_l1>\n" + 
      "       <_l1>3</_l1>\n" + 
      "       <_l1>null</_l1>\n" + 
      "     </l1>\n" + 
      "   </ToXMLTestSimple>"; 
  
  private String xmlT2_i2_b3_nmsp = 
      "   <nmsp:ToXMLTestSimple>\n" + 
      "     <nmsp:field2>2</nmsp:field2>\n" + 
      "     <nmsp:field3>3</nmsp:field3>\n" + 
      "     <nmsp:l1>\n" + 
      "       <nmsp:_l1>null</nmsp:_l1>\n" + 
      "       <nmsp:_l1>2</nmsp:_l1>\n" + 
      "       <nmsp:_l1>3</nmsp:_l1>\n" + 
      "       <nmsp:_l1>null</nmsp:_l1>\n" + 
      "     </nmsp:l1>\n" + 
      "   </nmsp:ToXMLTestSimple>"; 
  
  private String xmlT2_nmsp = "<nmsp:ToXMLTestSimple><nmsp:field2>2</nmsp:field2><nmsp:field3>3</nmsp:field3><nmsp:l1><nmsp:_l1>null</nmsp:_l1><nmsp:_l1>2</nmsp:_l1><nmsp:_l1>3</nmsp:_l1><nmsp:_l1>null</nmsp:_l1></nmsp:l1></nmsp:ToXMLTestSimple>"; 
  
  
  
  private String xmlArray_T1 = "<ToXMLTestSimpleArray><field1>gato</field1><field2>5</field2><field3>4</field3><field4>3.4</field4><l1><_l1>gato</_l1><_l1>5</_l1><_l1>4</_l1><_l1>3.4</_l1></l1></ToXMLTestSimpleArray>"; 

  private String xmlArray_T2 = "<ToXMLTestSimpleArray><field2>2</field2><field3>3</field3><l1><_l1>null</_l1><_l1>2</_l1><_l1>3</_l1><_l1>null</_l1></l1></ToXMLTestSimpleArray>"; 
  
  private String xmlArray_T2_i2 = 
      "<ToXMLTestSimpleArray>\n" + 
      "  <field2>2</field2>\n" + 
      "  <field3>3</field3>\n" + 
      "  <l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "    <_l1>2</_l1>\n" + 
      "    <_l1>3</_l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "  </l1>\n" + 
      "</ToXMLTestSimpleArray>"; 
  
  private String xmlArray_T2_i2_b3 = 
      "   <ToXMLTestSimpleArray>\n" + 
      "     <field2>2</field2>\n" + 
      "     <field3>3</field3>\n" + 
      "     <l1>\n" + 
      "       <_l1>null</_l1>\n" + 
      "       <_l1>2</_l1>\n" + 
      "       <_l1>3</_l1>\n" + 
      "       <_l1>null</_l1>\n" + 
      "     </l1>\n" + 
      "   </ToXMLTestSimpleArray>"; 
  
  private String xmlArray_T2_i2_b3_nmsp = 
      "   <nmsp:ToXMLTestSimpleArray>\n" + 
      "     <nmsp:field2>2</nmsp:field2>\n" + 
      "     <nmsp:field3>3</nmsp:field3>\n" + 
      "     <nmsp:l1>\n" + 
      "       <nmsp:_l1>null</nmsp:_l1>\n" + 
      "       <nmsp:_l1>2</nmsp:_l1>\n" + 
      "       <nmsp:_l1>3</nmsp:_l1>\n" + 
      "       <nmsp:_l1>null</nmsp:_l1>\n" + 
      "     </nmsp:l1>\n" + 
      "   </nmsp:ToXMLTestSimpleArray>"; 
    
  private String xmlArray_T2_nmsp = "<nmsp:ToXMLTestSimpleArray><nmsp:field2>2</nmsp:field2><nmsp:field3>3</nmsp:field3><nmsp:l1><nmsp:_l1>null</nmsp:_l1><nmsp:_l1>2</nmsp:_l1><nmsp:_l1>3</nmsp:_l1><nmsp:_l1>null</nmsp:_l1></nmsp:l1></nmsp:ToXMLTestSimpleArray>"; 
  
  
  private String xmlAnnT1 = "<Turiru identification='3'>\n" + 
      "    <nombre>gato</nombre>\n" + 
      "    <field3>4</field3>\n" + 
      "    <lista>\n" + 
      "        <Son sonId='0'>\n" + 
      "            <sonNombre>son_0</sonNombre>\n" + 
      "        </Son>\n" + 
      "        <Son sonId='1'>\n" + 
      "            <sonNombre>son_1</sonNombre>\n" + 
      "        </Son>\n" + 
      "        <Son sonId='2'>\n" + 
      "            <sonNombre>son_2</sonNombre>\n" + 
      "        </Son>\n" + 
      "    </lista>\n" + 
      "    <Son sonId='8'>\n" + 
      "        <sonNombre>son_8</sonNombre>\n" + 
      "    </Son>\n" + 
      "</Turiru>";
  
  private String xmlAnnT2 = "<Turiru identification='4'>\n" + 
      "    <field3>3</field3>\n" + 
      "    <lista>\n" + 
      "        <Son sonId='0'>\n" + 
      "            <sonNombre>son_0</sonNombre>\n" + 
      "        </Son>\n" + 
      "        <Son sonId='1'>\n" + 
      "            <sonNombre>son_1</sonNombre>\n" + 
      "        </Son>\n" + 
      "        <Son sonId='2'>\n" + 
      "            <sonNombre>son_2</sonNombre>\n" + 
      "        </Son>\n" + 
      "        <Son sonId='3'>\n" + 
      "            <sonNombre>son_3</sonNombre>\n" + 
      "        </Son>\n" + 
      "    </lista>\n" + 
      "    <Son sonId='9'>\n" + 
      "        <sonNombre>son_9</sonNombre>\n" + 
      "    </Son>\n" + 
      "</Turiru>";
  
  
  
  private String xmlAnnT1_i3 = 
      "<Turiru identification='3'>\n" + 
      "   <nombre>gato</nombre>\n" + 
      "   <field3>4</field3>\n" + 
      "   <lista>\n" + 
      "      <Son sonId='0'>\n" + 
      "         <sonNombre>son_0</sonNombre>\n" + 
      "      </Son>\n" + 
      "      <Son sonId='1'>\n" + 
      "         <sonNombre>son_1</sonNombre>\n" + 
      "      </Son>\n" + 
      "      <Son sonId='2'>\n" + 
      "         <sonNombre>son_2</sonNombre>\n" + 
      "      </Son>\n" + 
      "   </lista>\n" + 
      "   <Son sonId='8'>\n" + 
      "      <sonNombre>son_8</sonNombre>\n" + 
      "   </Son>\n" + 
      "</Turiru>";  
  
  private String xmlAnnT1_i3_nmsp = 
      "<nmsp:Turiru identification='3'>\n" + 
      "   <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "   <nmsp:field3>4</nmsp:field3>\n" + 
      "   <nmsp:lista>\n" + 
      "      <nmsp:Son sonId='0'>\n" + 
      "         <nmsp:sonNombre>son_0</nmsp:sonNombre>\n" + 
      "      </nmsp:Son>\n" + 
      "      <nmsp:Son sonId='1'>\n" + 
      "         <nmsp:sonNombre>son_1</nmsp:sonNombre>\n" + 
      "      </nmsp:Son>\n" + 
      "      <nmsp:Son sonId='2'>\n" + 
      "         <nmsp:sonNombre>son_2</nmsp:sonNombre>\n" + 
      "      </nmsp:Son>\n" + 
      "   </nmsp:lista>\n" + 
      "   <nmsp:Son sonId='8'>\n" + 
      "      <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "   </nmsp:Son>\n" + 
      "</nmsp:Turiru>";
  
  private String xmlAnnT1_i3_b5 = 
      "     <Turiru identification='3'>\n" + 
      "        <nombre>gato</nombre>\n" + 
      "        <field3>4</field3>\n" + 
      "        <lista>\n" + 
      "           <Son sonId='0'>\n" + 
      "              <sonNombre>son_0</sonNombre>\n" + 
      "           </Son>\n" + 
      "           <Son sonId='1'>\n" + 
      "              <sonNombre>son_1</sonNombre>\n" + 
      "           </Son>\n" + 
      "           <Son sonId='2'>\n" + 
      "              <sonNombre>son_2</sonNombre>\n" + 
      "           </Son>\n" + 
      "        </lista>\n" + 
      "        <Son sonId='8'>\n" + 
      "           <sonNombre>son_8</sonNombre>\n" + 
      "        </Son>\n" + 
      "     </Turiru>";
  
  private String xmlAnnT1_i3_b5_nmsp = 
      "     <nmsp:Turiru identification='3'>\n" + 
      "        <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "        <nmsp:field3>4</nmsp:field3>\n" + 
      "        <nmsp:lista>\n" + 
      "           <nmsp:Son sonId='0'>\n" + 
      "              <nmsp:sonNombre>son_0</nmsp:sonNombre>\n" + 
      "           </nmsp:Son>\n" + 
      "           <nmsp:Son sonId='1'>\n" + 
      "              <nmsp:sonNombre>son_1</nmsp:sonNombre>\n" + 
      "           </nmsp:Son>\n" + 
      "           <nmsp:Son sonId='2'>\n" + 
      "              <nmsp:sonNombre>son_2</nmsp:sonNombre>\n" + 
      "           </nmsp:Son>\n" + 
      "        </nmsp:lista>\n" + 
      "        <nmsp:Son sonId='8'>\n" + 
      "           <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "     </nmsp:Turiru>";
  
  private String xmlAnnT1_flat_nmsp = "<nmsp:Turiru identification='3'><nmsp:nombre>gato</nmsp:nombre><nmsp:field3>4</nmsp:field3><nmsp:lista><nmsp:Son sonId='0'><nmsp:sonNombre>son_0</nmsp:sonNombre></nmsp:Son><nmsp:Son sonId='1'><nmsp:sonNombre>son_1</nmsp:sonNombre></nmsp:Son><nmsp:Son sonId='2'><nmsp:sonNombre>son_2</nmsp:sonNombre></nmsp:Son></nmsp:lista><nmsp:Son sonId='8'><nmsp:sonNombre>son_8</nmsp:sonNombre></nmsp:Son></nmsp:Turiru>";
  
  private String xmlAnnT1_nmsp = 
      "<nmsp:Turiru identification='3'>\n" + 
      "    <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "    <nmsp:field3>4</nmsp:field3>\n" + 
      "    <nmsp:lista>\n" + 
      "        <nmsp:Son sonId='0'>\n" + 
      "            <nmsp:sonNombre>son_0</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "        <nmsp:Son sonId='1'>\n" + 
      "            <nmsp:sonNombre>son_1</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "        <nmsp:Son sonId='2'>\n" + 
      "            <nmsp:sonNombre>son_2</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "    </nmsp:lista>\n" + 
      "    <nmsp:Son sonId='8'>\n" + 
      "        <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "    </nmsp:Son>\n" + 
      "</nmsp:Turiru>";
  
  
  
  private String xmlAnnT2_i3 = 
      "<nimbox:Turiru identification='3'>\n" + 
      "   <nimbox:nombre>gato</nimbox:nombre>\n" + 
      "   <nimbox:field3>4</nimbox:field3>\n" + 
      "   <nimbox:lista>\n" + 
      "      <nimbox:Son sonId='0'>\n" + 
      "         <nimbox:sonNombre>son_0</nimbox:sonNombre>\n" + 
      "      </nimbox:Son>\n" + 
      "      <nimbox:Son sonId='1'>\n" + 
      "         <nimbox:sonNombre>son_1</nimbox:sonNombre>\n" + 
      "      </nimbox:Son>\n" + 
      "      <nimbox:Son sonId='2'>\n" + 
      "         <nimbox:sonNombre>son_2</nimbox:sonNombre>\n" + 
      "      </nimbox:Son>\n" + 
      "   </nimbox:lista>\n" + 
      "   <nimbox:Son sonId='8' age='40'>\n" + 
      "      <nimbox:sonNombre>son_8</nimbox:sonNombre>\n" + 
      "   </nimbox:Son>\n" + 
      "</nimbox:Turiru>";
  
  private String xmlAnnT2_i3_nmsp = 
      "<nmsp:Turiru identification='3'>\n" + 
      "   <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "   <nmsp:field3>4</nmsp:field3>\n" + 
      "   <nmsp:lista>\n" + 
      "      <nmsp:Son sonId='0'>\n" + 
      "         <nmsp:sonNombre>son_0</nmsp:sonNombre>\n" + 
      "      </nmsp:Son>\n" + 
      "      <nmsp:Son sonId='1'>\n" + 
      "         <nmsp:sonNombre>son_1</nmsp:sonNombre>\n" + 
      "      </nmsp:Son>\n" + 
      "      <nmsp:Son sonId='2'>\n" + 
      "         <nmsp:sonNombre>son_2</nmsp:sonNombre>\n" + 
      "      </nmsp:Son>\n" + 
      "   </nmsp:lista>\n" + 
      "   <nmsp:Son sonId='8' age='40'>\n" + 
      "      <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "   </nmsp:Son>\n" + 
      "</nmsp:Turiru>";
  
  private String xmlAnnT2_i3_b5 = 
      "     <nimbox:Turiru identification='3'>\n" + 
      "        <nimbox:nombre>gato</nimbox:nombre>\n" + 
      "        <nimbox:field3>4</nimbox:field3>\n" + 
      "        <nimbox:lista>\n" + 
      "           <nimbox:Son sonId='0'>\n" + 
      "              <nimbox:sonNombre>son_0</nimbox:sonNombre>\n" + 
      "           </nimbox:Son>\n" + 
      "           <nimbox:Son sonId='1'>\n" + 
      "              <nimbox:sonNombre>son_1</nimbox:sonNombre>\n" + 
      "           </nimbox:Son>\n" + 
      "           <nimbox:Son sonId='2'>\n" + 
      "              <nimbox:sonNombre>son_2</nimbox:sonNombre>\n" + 
      "           </nimbox:Son>\n" + 
      "        </nimbox:lista>\n" + 
      "        <nimbox:Son sonId='8' age='40'>\n" + 
      "           <nimbox:sonNombre>son_8</nimbox:sonNombre>\n" + 
      "        </nimbox:Son>\n" + 
      "     </nimbox:Turiru>";
  
  private String xmlAnnT2_i3_b5_nmsp = 
      "     <nmsp:Turiru identification='3'>\n" + 
      "        <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "        <nmsp:field3>4</nmsp:field3>\n" + 
      "        <nmsp:lista>\n" + 
      "           <nmsp:Son sonId='0'>\n" + 
      "              <nmsp:sonNombre>son_0</nmsp:sonNombre>\n" + 
      "           </nmsp:Son>\n" + 
      "           <nmsp:Son sonId='1'>\n" + 
      "              <nmsp:sonNombre>son_1</nmsp:sonNombre>\n" + 
      "           </nmsp:Son>\n" + 
      "           <nmsp:Son sonId='2'>\n" + 
      "              <nmsp:sonNombre>son_2</nmsp:sonNombre>\n" + 
      "           </nmsp:Son>\n" + 
      "        </nmsp:lista>\n" + 
      "        <nmsp:Son sonId='8' age='40'>\n" + 
      "           <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "     </nmsp:Turiru>";
  
  private String xmlAnnT2_flat_nmsp = "<nmsp:Turiru identification='3'><nmsp:nombre>gato</nmsp:nombre><nmsp:field3>4</nmsp:field3><nmsp:lista><nmsp:Son sonId='0'><nmsp:sonNombre>son_0</nmsp:sonNombre></nmsp:Son><nmsp:Son sonId='1'><nmsp:sonNombre>son_1</nmsp:sonNombre></nmsp:Son><nmsp:Son sonId='2'><nmsp:sonNombre>son_2</nmsp:sonNombre></nmsp:Son></nmsp:lista><nmsp:Son sonId='8' age='40'><nmsp:sonNombre>son_8</nmsp:sonNombre></nmsp:Son></nmsp:Turiru>";
  
  private String xmlAnnT2_nmsp = 
      "<nmsp:Turiru identification='3'>\n" + 
      "    <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "    <nmsp:field3>4</nmsp:field3>\n" + 
      "    <nmsp:lista>\n" + 
      "        <nmsp:Son sonId='0'>\n" + 
      "            <nmsp:sonNombre>son_0</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "        <nmsp:Son sonId='1'>\n" + 
      "            <nmsp:sonNombre>son_1</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "        <nmsp:Son sonId='2'>\n" + 
      "            <nmsp:sonNombre>son_2</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "    </nmsp:lista>\n" + 
      "    <nmsp:Son sonId='8' age='40'>\n" + 
      "        <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "    </nmsp:Son>\n" + 
      "</nmsp:Turiru>"; 

  
 
  private String xmlAnnT3_i3 = 
      "<nimbox:Turiru identification='3'>\n" + 
      "   <nimbox:nombre>gato</nimbox:nombre>\n" + 
      "   <nimbox:field3>4</nimbox:field3>\n" + 
      "   <nimbox:lista>\n" + 
      "      <nimbox:_lista>Gatito0</nimbox:_lista>\n" + 
      "      <nimbox:_lista>Gatito1</nimbox:_lista>\n" + 
      "      <nimbox:_lista>Gatito2</nimbox:_lista>\n" + 
      "   </nimbox:lista>\n" + 
      "   <nimbox:Son sonId='8' age='40'>\n" + 
      "      <nimbox:sonNombre>son_8</nimbox:sonNombre>\n" + 
      "   </nimbox:Son>\n" + 
      "</nimbox:Turiru>";
  
  private String xmlAnnT3_i3_nmsp = 
      "<nmsp:Turiru identification='3'>\n" + 
      "   <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "   <nmsp:field3>4</nmsp:field3>\n" + 
      "   <nmsp:lista>\n" + 
      "      <nmsp:_lista>Gatito0</nmsp:_lista>\n" + 
      "      <nmsp:_lista>Gatito1</nmsp:_lista>\n" + 
      "      <nmsp:_lista>Gatito2</nmsp:_lista>\n" + 
      "   </nmsp:lista>\n" + 
      "   <nmsp:Son sonId='8' age='40'>\n" + 
      "      <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "   </nmsp:Son>\n" + 
      "</nmsp:Turiru>";
  
  private String xmlAnnT3_i3_b5 = 
      "     <nimbox:Turiru identification='3'>\n" + 
      "        <nimbox:nombre>gato</nimbox:nombre>\n" + 
      "        <nimbox:field3>4</nimbox:field3>\n" + 
      "        <nimbox:lista>\n" + 
      "           <nimbox:_lista>Gatito0</nimbox:_lista>\n" + 
      "           <nimbox:_lista>Gatito1</nimbox:_lista>\n" + 
      "           <nimbox:_lista>Gatito2</nimbox:_lista>\n" + 
      "        </nimbox:lista>\n" + 
      "        <nimbox:Son sonId='8' age='40'>\n" + 
      "           <nimbox:sonNombre>son_8</nimbox:sonNombre>\n" + 
      "        </nimbox:Son>\n" + 
      "     </nimbox:Turiru>";
  
  private String xmlAnnT3_i3_b5_nmsp = 
      "     <nmsp:Turiru identification='3'>\n" + 
      "        <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "        <nmsp:field3>4</nmsp:field3>\n" + 
      "        <nmsp:lista>\n" + 
      "           <nmsp:_lista>Gatito0</nmsp:_lista>\n" + 
      "           <nmsp:_lista>Gatito1</nmsp:_lista>\n" + 
      "           <nmsp:_lista>Gatito2</nmsp:_lista>\n" + 
      "        </nmsp:lista>\n" + 
      "        <nmsp:Son sonId='8' age='40'>\n" + 
      "           <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "        </nmsp:Son>\n" + 
      "     </nmsp:Turiru>";
  
  private String xmlAnnT3_flat_nmsp = "<nmsp:Turiru identification='3'><nmsp:nombre>gato</nmsp:nombre><nmsp:field3>4</nmsp:field3><nmsp:lista><nmsp:_lista>Gatito0</nmsp:_lista><nmsp:_lista>Gatito1</nmsp:_lista><nmsp:_lista>Gatito2</nmsp:_lista></nmsp:lista><nmsp:Son sonId='8' age='40'><nmsp:sonNombre>son_8</nmsp:sonNombre></nmsp:Son></nmsp:Turiru>";
  
  private String xmlAnnT3_nmsp = 
      "<nmsp:Turiru identification='3'>\n" + 
      "    <nmsp:nombre>gato</nmsp:nombre>\n" + 
      "    <nmsp:field3>4</nmsp:field3>\n" + 
      "    <nmsp:lista>\n" + 
      "        <nmsp:_lista>Gatito0</nmsp:_lista>\n" + 
      "        <nmsp:_lista>Gatito1</nmsp:_lista>\n" + 
      "        <nmsp:_lista>Gatito2</nmsp:_lista>\n" + 
      "    </nmsp:lista>\n" + 
      "    <nmsp:Son sonId='8' age='40'>\n" + 
      "        <nmsp:sonNombre>son_8</nmsp:sonNombre>\n" + 
      "    </nmsp:Son>\n" + 
      "</nmsp:Turiru>";
  
  
  private String xmlDelete = 
      "<ToXMLTestSimple>\n" + 
      "  <field2>2</field2>\n" + 
      "  <field3>3</field3>\n" + 
      "  <l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "    <_l1>2</_l1>\n" + 
      "    <_l1>3</_l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "  </l1>\n" + 
      "</ToXMLTestSimple>"; 
  
  private String xmlDelete_res1 = 
      "<ToXMLTestSimple>\n" + 
      "  <field2>2</field2>\n" + 
      "  <l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "    <_l1>2</_l1>\n" + 
      "    <_l1>3</_l1>\n" + 
      "    <_l1>null</_l1>\n" + 
      "  </l1>\n" + 
      "</ToXMLTestSimple>"; 
  
  private String xmlDelete_res2 = 
      "<ToXMLTestSimple>\n" + 
      "  <field2>2</field2>\n" + 
      "  <field3>3</field3>\n" + 
      "  <l1/>\n" +  
      "</ToXMLTestSimple>";
  
  
  private String xmlSurround = "<a>\n" + 
      "  <b1>\n" + 
      "    <b1c1></b1c1>\n" + 
      "    <b1c2></b1c2>\n" + 
      "    <b1c3></b1c3>\n" + 
      "  </b1>\n" + 
      "  <b2>\n" + 
      "    <b2c1></b2c1>\n" + 
      "    <b2c2></b2c2>\n" + 
      "    <b2c3></b2c3>\n" + 
      "  </b2>\n" + 
      "  <b3>\n" + 
      "    <b3c1></b3c1>\n" + 
      "    <b3c2></b3c2>\n" + 
      "    <b3c3></b3c3>\n" + 
      "  </b3>\n" + 
      "</a>";
  
  private String xmlSurround_res1 = "<newRoot>\n" +
      "    <a>\n" + 
      "        <b1>\n" + 
      "            <b1c1/>\n" + 
      "            <b1c2/>\n" + 
      "            <b1c3/>\n" + 
      "        </b1>\n" +
      "        <xsl:if>\n" +
      "            <b2>\n" + 
      "                <b2c1/>\n" + 
      "                <b2c2/>\n" + 
      "                <b2c3/>\n" + 
      "            </b2>\n" + 
      "        </xsl:if>\n" +
      "        <b3>\n" + 
      "            <b3c1/>\n" + 
      "            <b3c2/>\n" + 
      "            <b3c3/>\n" + 
      "        </b3>\n" + 
      "    </a>\n" +
      "</newRoot>\n";
  
  private String xmlSurround_res2 = "<a>\n" + 
      "    <b1>\n" + 
      "        <b1c1/>\n" + 
      "        <b1c2>\n" + 
      "            <gatito color=\"azul\" edad=\"1\"/>\n" + 
      "        </b1c2>\n" +
      "        <b1c3/>\n" + 
      "    </b1>\n" + 
      "    <b2>\n" + 
      "        <b2c1/>\n" + 
      "        <b2c2/>\n" + 
      "        <b2c3/>\n" + 
      "    </b2>\n" + 
      "    <b3>\n" + 
      "        <b3c1/>\n" + 
      "        <b3c2/>\n" + 
      "        <b3c3/>\n" + 
      "    </b3>\n" + 
      "    <xsl:if/>\n" + 
      "    <gatito color=\"azul\" edad=\"1\"/>\n" +
      "</a>\n";
  
  
  private String mixXmlT1 = "<ToXMLTestSimple>\n"
      + "  <field>gato</field>\n"
      + "  <field>5</field>\n"
      + "  <field>4</field>\n"
      + "  <field>3.4</field>\n"
      + "</ToXMLTestSimple>";
  
  private String mixXslT1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
      + "<xsl:stylesheet version=\"1.0\"\n"
      + "  xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\"\n"
      + "  xmlns:fn=\"http://www.w3.org/2005/xpath-functions\">\n"
      + "\n"
      + "<xsl:output method=\"text\" />"
      + "<xsl:template match=\"/\">\n"
      + "  <xsl:for-each select=\"/ToXMLTestSimple/field\">\n"
      + "    <xsl:value-of select=\".\" />\n"
      + "  </xsl:for-each>\n"
      + "</xsl:template>\n"
      + "\n"
      + "</xsl:stylesheet>";
  
  @Test
  public void textMixXMLXSL() throws Exception {
    assertEquals( "gato543.4", XMLUtils.applyXSLT( mixXmlT1, mixXslT1));
  }
  
  @Test
  public void testFirstXPATH() throws Exception {
    Node xml = XMLUtils.str2node( xmlTest);

    Node node = XMLUtils.getFirstXMLNode( xml, "//codError");
    assertEquals( "<codError>0</codError>", XMLUtils.indent( node).trim());
    
    node = XMLUtils.getFirstXMLNode( xml, "//TuriruAlfa/text()");
    assertEquals( "", XMLUtils.indent( node).trim());
    
    node = XMLUtils.getFirstXMLNode( xml, "//TuriruGamma");
    assertEquals( "<TuriruGamma/>", XMLUtils.indent( node).trim());
    
    node = XMLUtils.getFirstXMLNode( xml, "//*[local-name()='TuriruGamma']");
    assertEquals( "<typ:TuriruGamma>data23</typ:TuriruGamma>", XMLUtils.indent( node).trim());
    
    node = XMLUtils.getFirstXMLNode( xml, "//*[local-name()='TuriruGamma']");
    assertEquals( "<typ:TuriruGamma>data23</typ:TuriruGamma>", XMLUtils.indent( node).trim());
    
    node = XMLUtils.getFirstXMLNode( xml, "//*[local-name()='TuriruGamma']/text()");
    assertEquals( "data23", XMLUtils.indent( node).trim());
    
    node = XMLUtils.getFirstXMLNode( xml, "//TuriruDelta/text()");
    assertEquals( "data55", XMLUtils.indent( node).trim());
    
  }

  
  @Test
  public void testDeleteNode() throws Exception {
    assertEquals( XMLUtils.indentXML( xmlDelete), XMLUtils.deleteNodes( xmlDelete, "no_exists"));
    assertEquals( XMLUtils.indentXML( xmlDelete_res1), XMLUtils.deleteNodes( xmlDelete, "field3"));
    assertEquals( XMLUtils.indentXML( xmlDelete_res2), XMLUtils.deleteNodes( xmlDelete, "//_l1"));
  }
  
  @Test
  public void testCreateNode() throws Exception {
    Document doc = XMLUtils.str2doc( xmlSurround);
    
    Map<String,String> attr = new HashMap<String,String>();
    attr.put( "color", "azul");
    attr.put( "edad", "1");

    assertEquals( "<xsl:if/>\n", XMLUtils.indent( XMLUtils.createNode( doc, "xsl:if", null)));
    assertEquals( "<gatito color=\"azul\" edad=\"1\"/>\n", XMLUtils.indent( XMLUtils.createNode( doc, "gatito", attr)));
    
    Node father = XMLUtils.getFirstXMLNode( doc, "//b1c2");
    XMLUtils.createNode( doc, father, "gatito", attr);
    
    assertEquals( xmlSurround_res2, XMLUtils.indent( doc));
  }
  
  @Test
  public void testSurroundNode()throws Exception {
    Document doc = XMLUtils.str2doc( xmlSurround);
    
    Node root = doc.getDocumentElement();
    Node node = XMLUtils.getFirstXMLNode( root, "b2");

    XMLUtils.surroundNode( doc, node, "xsl:if");
    XMLUtils.surroundNode( doc, root, "newRoot");
    
    assertEquals( xmlSurround_res1, XMLUtils.indent( doc));
  }
  
  @Test
  public void testToXMLSimple_1() throws Exception {
    ToXMLTestSimple t1 = new ToXMLTestSimple( "gato", 5, 4, 3.40);
    assertEquals( xmlT1, XMLUtils.toXML( t1));
    
    ToXMLTestSimple t2 = new ToXMLTestSimple( null, 2, 3, null);
    assertEquals( xmlT2, XMLUtils.toXML( t2));
    assertEquals( xmlT2_i2, XMLUtils.toXML( t2, 2));
    assertEquals( xmlT2_i2_b3, XMLUtils.toXML( t2, 2, 3));
    assertEquals( xmlT2_i2_b3_nmsp, XMLUtils.toXML( t2, 2, 3, "nmsp"));
    assertEquals( xmlT2_nmsp, XMLUtils.toXML( t2, "nmsp"));
    
  }
  
  @Test
  public void testToXMLSimple_2() throws Exception {
    ToXMLTestSimpleArray t1 = new ToXMLTestSimpleArray( "gato", 5, 4, 3.40);
    assertEquals( xmlArray_T1, XMLUtils.toXML( t1));
    
    ToXMLTestSimpleArray t2 = new ToXMLTestSimpleArray( null, 2, 3, null);
    assertEquals( xmlArray_T2, XMLUtils.toXML( t2));
    assertEquals( xmlArray_T2_i2, XMLUtils.toXML( t2, 2));
    assertEquals( xmlArray_T2_i2_b3, XMLUtils.toXML( t2, 2, 3));
    assertEquals( xmlArray_T2_i2_b3_nmsp, XMLUtils.toXML( t2, 2, 3, "nmsp"));
    assertEquals( xmlArray_T2_nmsp, XMLUtils.toXML( t2, "nmsp"));
    
  }
  
  
  @Test
  public void testToXMLAnnotated_1() throws Exception {
    ToXMLTestAnnotated t1 = new ToXMLTestAnnotated( 3, "gato", 5, 4, 3.40);
    assertEquals( xmlAnnT1, XMLUtils.toXML( t1));
    
    ToXMLTestAnnotated t2 = new ToXMLTestAnnotated( 4, null, 2, 3, null);
    assertEquals( xmlAnnT2, XMLUtils.toXML( t2));
  }
  
  @Test
  public void testToXMLAnnotated_2() throws Exception {
    ToXMLTestAnnotated t1 = new ToXMLTestAnnotated( 3, "gato", 5, 4, 3.40);
    assertEquals( xmlAnnT1_i3, XMLUtils.toXML( t1, 3));
    assertEquals( xmlAnnT1_i3_nmsp, XMLUtils.toXML( t1, 3, "nmsp"));
    assertEquals( xmlAnnT1_i3_b5, XMLUtils.toXML( t1, 3, 5));
    assertEquals( xmlAnnT1_i3_b5_nmsp, XMLUtils.toXML( t1, 3, 5, "nmsp"));
    assertEquals( xmlAnnT1_flat_nmsp, XMLUtils.toXML( t1, 0, 0, "nmsp"));
    assertEquals( xmlAnnT1_nmsp, XMLUtils.toXML( t1, "nmsp"));
  }
  
  @Test
  public void testToXMLAnnotated_3() throws Exception {
    ToXMLTestAnnotated2 t2 = new ToXMLTestAnnotated2( 3, "gato", 5, 4, 3.40);
    assertEquals( xmlAnnT2_i3, XMLUtils.toXML( t2, 3));
    assertEquals( xmlAnnT2_i3_nmsp, XMLUtils.toXML( t2, 3, "nmsp"));
    assertEquals( xmlAnnT2_i3_b5, XMLUtils.toXML( t2, 3, 5));
    assertEquals( xmlAnnT2_i3_b5_nmsp, XMLUtils.toXML( t2, 3, 5, "nmsp"));
    assertEquals( xmlAnnT2_flat_nmsp, XMLUtils.toXML( t2, 0, 0, "nmsp"));
    assertEquals( xmlAnnT2_nmsp, XMLUtils.toXML( t2, "nmsp"));
  }
  
  @Test
  public void testToXMLAnnotated_4() throws Exception {
    ToXMLTestAnnotated3 t2 = new ToXMLTestAnnotated3( 3, "gato", 5, 4, 3.40);
    assertEquals( xmlAnnT2_i3, XMLUtils.toXML( t2, 3));
    assertEquals( xmlAnnT2_i3_nmsp, XMLUtils.toXML( t2, 3, "nmsp"));
    assertEquals( xmlAnnT2_i3_b5, XMLUtils.toXML( t2, 3, 5));
    assertEquals( xmlAnnT2_i3_b5_nmsp, XMLUtils.toXML( t2, 3, 5, "nmsp"));
    assertEquals( xmlAnnT2_flat_nmsp, XMLUtils.toXML( t2, 0, 0, "nmsp"));
    assertEquals( xmlAnnT2_nmsp, XMLUtils.toXML( t2, "nmsp"));
  }  
  
  @Test
  public void testToXMLAnnotated_5() throws Exception {
    ToXMLTestAnnotated4 t2 = new ToXMLTestAnnotated4( 3, "gato", 5, 4, 3.40);
    assertEquals( xmlAnnT3_i3, XMLUtils.toXML( t2, 3));
    assertEquals( xmlAnnT3_i3_nmsp, XMLUtils.toXML( t2, 3, "nmsp"));
    assertEquals( xmlAnnT3_i3_b5, XMLUtils.toXML( t2, 3, 5));
    assertEquals( xmlAnnT3_i3_b5_nmsp, XMLUtils.toXML( t2, 3, 5, "nmsp"));
    assertEquals( xmlAnnT3_flat_nmsp, XMLUtils.toXML( t2, 0, 0, "nmsp"));
    assertEquals( xmlAnnT3_nmsp, XMLUtils.toXML( t2, "nmsp"));
  }
}
