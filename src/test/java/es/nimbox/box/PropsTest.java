package es.nimbox.box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Vector;

import org.junit.Test;

public class PropsTest {
  
  @Test
  public void testConstructors() {
    Props cf1 = new Props();
    Props cf2 = new Props( cf1);
    Props cf3 = new Props( true);

    assertEquals( 0, cf1.size());
    assertEquals( 0, cf2.size());
    assertEquals( 0, cf3.size());
    assertEquals( 0, Props.common.size());
    
    Props.common.setProperty( "a", "a");
    assertEquals( 1, Props.common.size());
    
    Props cf4 = new Props( true);
    assertEquals( 1, cf4.size());
    
    Props cf5 = new Props( false);
    assertEquals( 0, cf5.size());
    
    Props cf6 = new Props( cf4);
    assertEquals( 1, cf6.size()); 
    
    // Limpiamos para los siguientes
    Props.common.clear();
  }
  
  @Test
  public void testSystem() {
    Props cf1 = new Props();
    
    cf1.setProperty( "a", "a${java.io.tmpdir}");
    
    String tmpDir = System.getProperty( "java.io.tmpdir");
    
    assertEquals( "a" + tmpDir, cf1.getProperty( "a"));
    
    for ( String key : cf1.getKeys( ".*") ) {
      if ( key.toLowerCase().equals( "java.io.tmpdir") ) {
        fail( "System.properties entries shouldn't be returned");
      }
    }
    
    for ( String key : cf1.getKeys() ) {
      if ( key.toLowerCase().equals( "java.io.tmpdir") ) {
        fail( "System.properties entries shouldn't be returned");
      }
    }
  }
  
  @Test
  public void testClear() {
    Props cf1 = new Props();
    
    cf1.setProperty( "a", "a");
    assertEquals( 1, cf1.size());
    cf1.clear();
    assertEquals( 0, cf1.size());
  }

  @Test
  public void testGettersSetters() {
    Props cf1 = new Props();
    
    cf1.setProperty( "string", "a");
    cf1.setProperty( "int", 10);
    cf1.setProperty( "long", 10L);
    cf1.setProperty( "double", 30.56);
    cf1.setProperty( "truer", true);
    cf1.setProperty( "falser", false);
    
    assertEquals( "a", cf1.getProperty( "string"));
    assertTrue( 10 == cf1.getPropertyAsInt( "int"));
    assertTrue( 10L == cf1.getPropertyAsLong( "long"));
    assertTrue( 30.56 == cf1.getPropertyAsDouble( "double"));
    assertEquals( true, cf1.getPropertyAsBoolean( "truer"));
    assertEquals( false, cf1.getPropertyAsBoolean( "falser"));

    assertNull( cf1.getProperty( "kk"));
    assertNull( cf1.getPropertyAsInt( "kk"));
    assertNull( cf1.getPropertyAsLong( "kk"));
    assertNull( cf1.getPropertyAsDouble( "kk"));
    assertNull( cf1.getPropertyAsBoolean( "kk"));
  }
  
  @Test
  public void testRemove() {
    Props cf1 = new Props();
    
    cf1.setProperty( "string", "a");
    cf1.setProperty( "long", 10);
    cf1.setProperty( "double", 30.56);
    cf1.setProperty( "truer", true);
    cf1.setProperty( "falser", false);
    
    assertEquals( "a", cf1.getProperty( "string"));
    
    assertEquals( "a", cf1.removeProperty( "string"));

    assertNull( cf1.getProperty( "string"));
  }
  
  @Test
  public void testAddFile() throws Exception {
    Props cf1 = new Props();
    
    assertEquals( 0, cf1.size());
    
    cf1.addFile( "/config1.props");
    assertEquals( 5, cf1.size());
    
    cf1.addFile( "/config2.props");
    assertEquals( 8, cf1.size());
    
    cf1.addFile( "/config3.props");
    assertEquals( 11, cf1.size());
    
    assertEquals( "a", cf1.getProperty( "string"));
    assertTrue( 10 == cf1.getPropertyAsLong( "long"));
    assertTrue( 30.56 == cf1.getPropertyAsDouble( "double"));
    assertEquals( true, cf1.getPropertyAsBoolean( "truer"));
    assertEquals( false, cf1.getPropertyAsBoolean( "falser"));
    assertEquals( "val1", cf1.getProperty( "val1"));
    assertEquals( "val2", cf1.getProperty( "val2"));
    assertEquals( "val5", cf1.getProperty( "val5"));
    assertEquals( "val6", cf1.getProperty( "val6"));

    assertNull( cf1.getProperty( "kk"));
  }

  @Test
  public void importFileTest() throws Exception {
    Props cf1 = new Props();

    cf1.addFile( "/propsImport-1.props");

    assertEquals( "val-11", cf1.getProperty( "key11"));
    assertEquals( "val-12", cf1.getProperty( "key12"));
    assertEquals( "val-13", cf1.getProperty( "key13"));
    assertEquals( "val-14-val-11", cf1.getProperty( "key14"));
    assertEquals( "val-15-val-21", cf1.getProperty( "key15"));
    assertEquals( "val-16-val-31", cf1.getProperty( "key16"));

    assertEquals( "val-21", cf1.getProperty( "key21"));
    assertEquals( "val-22", cf1.getProperty( "key22"));
    assertEquals( "val-23", cf1.getProperty( "key23"));
    assertEquals( "val-24-val-11", cf1.getProperty( "key24"));
    assertEquals( "val-25-val-21", cf1.getProperty( "key25"));
    assertEquals( "val-26-val-31", cf1.getProperty( "key26"));

    assertEquals( "val-31", cf1.getProperty( "key31"));
    assertEquals( "val-32", cf1.getProperty( "key32"));
    assertEquals( "val-33", cf1.getProperty( "key33"));
    assertEquals( "val-34-val-11", cf1.getProperty( "key34"));
    assertEquals( "val-35-val-21", cf1.getProperty( "key35"));
    assertEquals( "val-36-val-31", cf1.getProperty( "key36"));

    cf1.clear();

    cf1.addFile( "/propsImport-1.props");

    assertEquals( "val-11", cf1.getProperty( "key11"));
    assertEquals( "val-12", cf1.getProperty( "key12"));
    assertEquals( "val-13", cf1.getProperty( "key13"));
    assertEquals( "val-14-val-11", cf1.getProperty( "key14"));
    assertEquals( "val-15-val-21", cf1.getProperty( "key15"));
    assertEquals( "val-16-val-31", cf1.getProperty( "key16"));

    assertEquals( "val-21", cf1.getProperty( "key21"));
    assertEquals( "val-22", cf1.getProperty( "key22"));
    assertEquals( "val-23", cf1.getProperty( "key23"));
    assertEquals( "val-24-val-11", cf1.getProperty( "key24"));
    assertEquals( "val-25-val-21", cf1.getProperty( "key25"));
    assertEquals( "val-26-val-31", cf1.getProperty( "key26"));

    assertEquals( "val-31", cf1.getProperty( "key31"));
    assertEquals( "val-32", cf1.getProperty( "key32"));
    assertEquals( "val-33", cf1.getProperty( "key33"));
    assertEquals( "val-34-val-11", cf1.getProperty( "key34"));
    assertEquals( "val-35-val-21", cf1.getProperty( "key35"));
    assertEquals( "val-36-val-31", cf1.getProperty( "key36"));
  }
  
  @Test
  public void testExpansion() {
    Props cf = new Props();
    
    cf.setProperty( "env", "production.com");
    cf.setProperty( "host", "http://${env}/ws/add");
    cf.setProperty( "endpoint", "${host}/param=p1");
    cf.setProperty( "endpoint2", "${host}/param=p2");
    cf.setProperty( "endpoint3", "${host}/param=p3");
    cf.setProperty( "endpoint${num}", "${host}/param=p${num}");
    cf.setProperty( "url", "${endpoint${num}}");
    cf.setProperty( "concat", "${env}-${host}-${endpoint${num}}-${num}");

    assertEquals( "http://${env}/ws/add", cf.getPropertyRaw( "host"));
    assertEquals( "http://production.com/ws/add", cf.getProperty( "host"));
    assertEquals( "http://production.com/ws/add/param=p1", cf.getProperty( "endpoint"));
    assertEquals( "http://production.com/ws/add/param=p1", cf.getProperty( "endpoint"));

    cf.setProperty( "num", "2");
    assertEquals( "http://production.com/ws/add/param=p2", cf.getProperty( "endpoint${num}"));
    cf.setProperty( "num", "3");
    assertEquals( "http://production.com/ws/add/param=p3", cf.getProperty( "endpoint${num}"));
    
    cf.setProperty( "num", "2");
    assertEquals( "http://production.com/ws/add/param=p2", cf.getProperty( "url"));
    cf.setProperty( "num", "3");
    assertEquals( "http://production.com/ws/add/param=p3", cf.getProperty( "url"));
    
    assertEquals( "production.com-http://production.com/ws/add-http://production.com/ws/add/param=p3-3", cf.getProperty( "concat"));
  }
  
  @Test
  public void testFulFill() {
    Props cf = new Props();
    
    cf.setProperty( "prop1", "val1");
    cf.setProperty( "prop2", "val2");
    cf.setProperty( "num", "1");
    cf.setProperty( "PPP", "p");
    cf.setProperty( "RRR", "r");
    cf.setProperty( "OOO", "o");

    assertEquals( null, cf.fulfill( null));
    assertEquals( "", cf.fulfill( ""));
    assertEquals( "gato", cf.fulfill( "gato"));
    assertEquals( "g${ato", cf.fulfill( "g${ato"));
    assertEquals( "gat}o", cf.fulfill( "gat}o"));
    assertEquals( "gat${}o", cf.fulfill( "gat${}o"));
    assertEquals( "val1", cf.fulfill( "${prop1}"));
    assertEquals( "val1-gato", cf.fulfill( "${prop1}-gato"));
    assertEquals( "gato-val1", cf.fulfill( "gato-${prop1}"));

    assertEquals( "val1-gato", cf.fulfill( "${${PPP}rop1}-gato"));
    assertEquals( "gato-val1", cf.fulfill( "gato-${${PPP}${RRR}${OOO}${PPP}${num}}"));
    assertEquals( "gato-val1-aaa", cf.fulfill( "gato-${${PPP}rop1}-aaa"));
    assertEquals( "gato-val1-aaa", cf.fulfill( "gato-${p${RRR}op1}-aaa"));
    assertEquals( "gato-val1-aaa", cf.fulfill( "gato-${p${RRR}${OOO}p${num}}-aaa"));
    assertEquals( "gato-val1-aaa", cf.fulfill( "gato-${${PPP}${RRR}o${PPP}1}-aaa"));
    assertEquals( "gato-val1-aaa", cf.fulfill( "gato-${${PPP}${RRR}${OOO}${PPP}1}-aaa"));
    assertEquals( "gato-val1-val1", cf.fulfill( "gato-${prop1}-${prop${num}}"));
    assertEquals( "gato-val1-${prop${kk}}", cf.fulfill( "gato-${prop1}-${prop${kk}}"));
    
    assertEquals( "gato-${prop3}-val2-val1-aaa", cf.fulfill( "gato-${prop3}-${prop2}-${prop1}-aaa"));
    assertEquals( "gato-${prop3}-${prop4}-val1-aaa", cf.fulfill( "gato-${prop3}-${prop4}-${prop1}-aaa"));
    
    assertEquals( "gato-val1-val2", cf.fulfill( "gato-${prop1}-${prop2}"));
    assertEquals( "gato-val1-val2-${prop3}", cf.fulfill( "gato-${prop1}-${prop2}-${prop3}"));
    assertEquals( "gato-val1-${prop3}-${prop4}-aaa", cf.fulfill( "gato-${prop1}-${prop3}-${prop4}-aaa"));
    
    assertEquals( "select campo='val1' and campo='${prop3}' and id='${id}'", 
    		           cf.fulfill( "select campo='${prop1}' and campo='${prop3}' and id='${id}'"));
    
    cf.setProperty( "prop3", "val3");
    assertEquals( "select campo='val1' and campo='val3' and id='${id}'", 
                  cf.fulfill( "select campo='${prop1}' and campo='${prop3}' and id='${id}'"));
    
    String json = "{ \"name\" : \"${prop1}\", \"var\": \"${prop2}${num}\"}";
    String expected = "{ \"name\" : \"val1\", \"var\": \"val21\"}";
    
    assertEquals( expected, cf.fulfill( json));

  }
  
  @Test
  public void testObjects() {
    Props pr = new Props();
    
    Vector<String> vv = new Vector<>();
    
    pr.setObject( "vector", vv);

    assertNull( pr.getObject( "v"));
    assertTrue( vv == pr.getObject( "vector"));
    
    vv.add( "as");
    vv.add( "as");
    vv.add( "as");
    
    @SuppressWarnings( "unchecked")
    Vector<String> aux = (Vector<String>)(pr.getObject( "vector"));
    assertTrue( 3 == aux.size());
    assertTrue( vv.size() == aux.size());
    
    aux.add( "bb");

    assertTrue( 4 == aux.size());
    assertTrue( 4 == vv.size());
    assertTrue( vv.size() == aux.size());
    
    assertTrue( 1 == pr.sizeObjects());
  }
  
  
  @Test
  public void testLists() {
    Props pr = new Props();

    pr.setProperty( "gatito1", "g1");
    pr.setProperty( "gatito2", "g2");
    pr.setProperty( "gatito3", "g3");
    pr.setProperty( "perrito1", "p1");
    pr.setProperty( "perrito2", "p2");
    pr.setProperty( "perrito3", "p3");
    
    List<String> ll = pr.getKeys( "gatito.*");
    assertEquals( 3, ll.size());
    
    ll = pr.getKeys( "perrito.*");
    assertEquals( 3, ll.size());
    
    ll = pr.getKeys( ".*ito1");
    assertEquals( 2, ll.size());
    
    ll = pr.getKeys( ".*ito.*");
    assertEquals( 6, ll.size());
    
    pr.setObject( "gatito1", "g1");
    pr.setObject( "gatito2", "g2");
    pr.setObject( "gatito3", "g3");
    pr.setObject( "perrito1", "p1");
    pr.setObject( "perrito2", "p2");
    pr.setObject( "perrito3", "p3");
    
    ll = pr.getObjectsKeys( "gatito.*");
    assertEquals( 3, ll.size());
    
    ll = pr.getObjectsKeys( "perrito.*");
    assertEquals( 3, ll.size());
    
    ll = pr.getObjectsKeys( ".*ito1");
    assertEquals( 2, ll.size());
    
    ll = pr.getObjectsKeys( ".*ito.*");
    assertEquals( 6, ll.size());
  }
  
  @Test
  public void testRefeshFile() throws Exception {
    String fBefore = "/fileBefore.props";
    String fAfter = "/fileAfter.props";
    
    File fDummy = new File( "dummy.properties");
    
    copy( fBefore, fDummy);
    
    Props props = new Props();
    props.addFile( fDummy.getAbsolutePath(), 200);

    assertEquals( "value1", props.getProperty( "key1"));
    assertEquals( "value2", props.getProperty( "key2"));
    assertEquals( "value3", props.getProperty( "key3"));
    
    copy( fAfter, fDummy);
    
    Utils.sleep( 400);
    
    assertEquals( "value10", props.getProperty( "key1"));
    assertEquals( "value20", props.getProperty( "key2"));
    assertEquals( "value30", props.getProperty( "key3"));
    
    props.stopRefreshingFile( fDummy.getAbsolutePath());
    
    fDummy.delete();
  }
  
  @Test
  public void jsonTest() throws Exception {
    Props cf1 = new Props( true);
    
    assertEquals( 0, cf1.size());
    
    cf1.addFile( "/config1.props");
    assertEquals( 5, cf1.size());
    
    String json = cf1.toJSON();
    
    Props cf2 = Props.fromJSON( json);
    
    String json2 = cf2.toJSON();
    
    assertEquals( json, json2);
  }
  
  @Test
  public void jsonUsingFormatsTest() throws Exception {
    Props cf1 = new Props( true);
    
    assertEquals( 0, cf1.size());
    
    cf1.addFile( "/config1.props");
    assertEquals( 5, cf1.size());
    assertTrue( 30.56 == cf1.getPropertyAsDouble( "double"));
    assertTrue( cf1.getPropertyAsBoolean( "truer"));
    assertFalse( cf1.getPropertyAsBoolean( "falser"));
    
    String jsonOwn = cf1.toJSON();
    
    String jsonJackson = Formats.toJson( cf1); 
    
    // Serialization using Jackson and own method gives same results
    assertEquals( jsonOwn, jsonJackson);
    
    // Rebuild an object
    Props cf2 = Formats.fromJson( jsonJackson, Props.class);
    
    // Let's see if the object works
    assertEquals( 5, cf2.size());
    assertTrue( 30.56 == cf1.getPropertyAsDouble( "double"));
    assertTrue( cf1.getPropertyAsBoolean( "truer"));
    assertFalse( cf1.getPropertyAsBoolean( "falser"));
    
    String jsonFromJackson = Formats.toJson( cf2); 
    
    assertEquals( jsonOwn, jsonFromJackson);
  }
  
  
  
  private static void copy( String orig, File dest) throws IOException {
    InputStream is = Utils.findFile( orig);
    FileOutputStream os = new FileOutputStream( dest);
    
    byte buf[] = new byte[1024];
    int r = 0;
    while( ( r = is.read( buf)) > 0 ) {
      os.write( buf, 0, r);
    }
    
    is.close();
    os.close();
  }
}



