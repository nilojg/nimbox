package es.nimbox.box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class WatchDogTest {
  
  class WDL implements WatchDogListener {
    public int counter = 0;
    String list[];
    
    public WDL( String args[]) {
      counter = 0;
      list = args;
    }
    
    public void watchDogAlert( WatchDogEvent ev) {
      if ( list == null ) {
        counter++;
      }
      else for ( String ff : list ) {
        if ( ev.getMsg().equals( ff) ) {
          counter++;
          break;
        }
      }
    }      
  }
  

  @Test
  public void testPeriodic() {
    WatchDog simple = new WatchDog();
    WDL wdl1 = new WDL(null);
    WDL wdl2 = new WDL( new String[] { "Alarm1"});

    simple.addListener( wdl1);
    simple.addListener( wdl2);

    simple.registerAlarmInterval( null, "Alarm1", 100);
    simple.registerAlarmInterval( null, "Alarm2", 50);
    
    simple.startWatchDog();
    Utils.sleep( 375);
    simple.stopWatchDog();
    
    assertEquals( 10, wdl1.counter);
    assertEquals( 3, wdl2.counter);
  }
  
  @Test
  public void testOnTime() {
    WatchDog simple = new WatchDog();
    simple.setThreadSleep( 10);
    
    WDL wdl1 = new WDL(null);

    simple.addListener( wdl1);

    long now = System.currentTimeMillis();

    simple.registerAlarmTime( null, "Alarm1", now + 200);
    simple.registerAlarmTime( null, "Alarm2", now + 400);
    simple.registerAlarmTime( null, "Alarm3", now + 200);
    
    simple.startWatchDog();
    
    Utils.sleep( 100);
    assertEquals( 0, wdl1.counter);
    
    Utils.sleep( 150);
    assertEquals( 2, wdl1.counter);
    
    Utils.sleep( 200);
    assertEquals( 3, wdl1.counter);
    
    simple.stopWatchDog();
  }
  
  @Test
  public void deactivateTest() {
    WatchDog simple = new WatchDog();
    WDL wdl1 = new WDL(null);
    
    simple.addListener( wdl1);
    
    int alarmid = simple.registerAlarmInterval( null, "Alarm1", 100);
    simple.startWatchDog();
    
    Utils.sleep( 375);
    
    assertEquals( 3, wdl1.counter);
    
    Utils.sleep( 215);
    
    assertEquals( 5, wdl1.counter);
    
    // Deactivate alarm
    simple.removeAlarm( alarmid);
    
    int lastValue = wdl1.counter;
    
    // Wait some time...
    Utils.sleep( 300);
    
    // and ensure that the alarm wasn't executed again after deactivated
    assertTrue( lastValue == wdl1.counter);
  }
}
