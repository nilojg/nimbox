package es.nimbox.box;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PairTest {
  
  
  @Test
  public void sweetTest() {
    Pair<String,Integer> p1 = new Pair<>();
    p1.first = "Hola";
    p1.second = 5;

    assertEquals( "Hola", p1.first);
    assertEquals( "Hola", p1.getFirst());
    assertEquals( (int)5, (int)p1.second);
    assertEquals( (int)5, (int)p1.getSecond());
    

    Pair<String,Integer> p2 = new Pair<>( "Hola", 5);

    assertEquals( "Hola", p2.first);
    assertEquals( "Hola", p2.getFirst());
    assertEquals( (int)5, (int)p2.second);
    assertEquals( (int)5, (int)p2.getSecond());
  }
  
  @Test
  public void equalTest() {
    Pair<String,Integer> p1 = new Pair<>();
    p1.first = "Hola";
    p1.second = 5;
    
    Pair<String,Integer> p2 = new Pair<>( "Hola", 5);

    assertTrue( p1.equals( p2));
    assertTrue( p2.equals( p1));
    assertEquals( p1.hashCode(), p2.hashCode());
    
    p1.first = null;
    p2.setFirst( null);

    assertTrue( p1.equals( p2));
    assertTrue( p2.equals( p1));
    assertEquals( p1.hashCode(), p2.hashCode());
    
    p1.second = null;
    p2.setSecond( null);

    assertTrue( p1.equals( p2));
    assertTrue( p2.equals( p1));
    assertEquals( p1.hashCode(), p2.hashCode());

    assertTrue( p1.first == null && p2.first == null);
    assertTrue( p1.second == null && p2.second == null);
    
    
  }
}
