
package es.nimbox.cache;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.junit.Test;

public class HashFileTest {
  
  private int MAX_INTENTS = 30000;
  
  
  @Test
  public void generic_Test() throws IOException {
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, ".");
    
    cache.open();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      cache.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }

    for ( int i = 0; i < MAX_INTENTS; i++) {
      assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
                    cache.get( "gato" + i));
    }
    
    cache.close();
  }
  
  @Test
  public void genericUpdate_Test() throws IOException {
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, ".");
    
    cache.open();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      cache.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }
    
    for ( int i = 0; i < MAX_INTENTS; i++) {
      if ( i % 3 == 0 ) {
        cache.put( "gato" + i, "El gato naranja ya no mira ningún sitio, asi que " + i + " elefantes estan solos");
      }
      else if ( i % 5 == 0 ) {
        cache.remove( "gato" + i);
      }
    }

    for ( int i = 0; i < MAX_INTENTS; i++) {
      if ( i % 3 == 0 ) {
        assertEquals( "El gato naranja ya no mira ningún sitio, asi que " + i + " elefantes estan solos", 
                      cache.get( "gato" + i));
      }
      else if ( i % 5 == 0 ) {
        //
      }
      else {
        assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
                      cache.get( "gato" + i));
      }
    }
    
    cache.close();
  }
  
  @Test
  public void genericPersistence_Test() throws IOException {
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, ".");
    cache.setParam( HashFile.HF_NAME, "FileCacheTest");
    
    cache.open();
    //cache.load();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      cache.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }

    for ( int i = 0; i < MAX_INTENTS; i++) {
      assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
          cache.get( "gato" + i));
    }
    
    assertEquals( MAX_INTENTS, cache.size());
    
    cache.store();
    cache.close();
    
    ICache cache2 = new HashFile();
    cache2.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    cache2.setParam( HashFile.HF_NUMFILES, "5");
    cache2.setParam( HashFile.HF_PATHBASE, ".");
    cache2.setParam( HashFile.HF_NAME, "FileCacheTest");
    
    cache2.open();
    cache2.load();
    
    assertEquals( MAX_INTENTS, cache2.size());

    for ( int i = 0; i < MAX_INTENTS; i++) {
      String val = cache2.get( "gato" + i);
      assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
                    val);
    }
    
    // For cleaning
    File[] toDelete = ((HashFile)cache2).getFiles();
    
    cache2.store();
    cache2.close();
    
    for ( File ff : toDelete) {
      ff.delete();
    }
  }
  
  
  
  
  @Test
  public void ownSweet_Test() throws IOException {
    HashFile hf = new HashFile( MAX_INTENTS, 5, ".");
    hf.open();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      hf.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }

    for ( int i = 0; i < MAX_INTENTS; i++) {
      assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
                    hf.get( "gato" + i));
    }
    
    hf.close();
  }
  
  @Test
  public void ownSize_Test() throws IOException {
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, ".");
    cache.setParam( HashFile.HF_NAME, "FileCacheTest");
    
    cache.open();
    //cache.load();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      cache.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }
    
    cache.store();
    
    HashFile theObj = ((HashFile)cache);
    
    // For cleaning and to know the files
    File[] toDelete = theObj.getFiles();
    
    long totalSize = 0;
    for ( File ff : toDelete) {
      if ( !ff.getName().endsWith( HashFile.HF_INDEX_EXTENSION) ) {
        totalSize += ff.length();
      }
    }
    
    assertEquals( MAX_INTENTS, cache.size());
    assertEquals( totalSize, theObj.sizeData());
    
    
    cache.close();
    
    for ( File ff : toDelete) {
      ff.delete();
    }
  }

  @Test( expected = IOException.class)
  public void ownWrong() throws IOException {
    HashFile hf = new HashFile( MAX_INTENTS, 5, ".");
    hf.store();
  }
}
