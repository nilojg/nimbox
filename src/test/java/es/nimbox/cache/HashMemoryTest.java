
package es.nimbox.cache;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class HashMemoryTest {
  private int MAX_INTENTS = 10000;
  
  
  @Test
  public void generic_Test() throws IOException {
    ICache cache = new HashMemory();
    cache.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    
    cache.open();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      cache.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }

    for ( int i = 0; i < MAX_INTENTS; i++) {
      assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
                    cache.get( "gato" + i));
    }
    
    cache.close();
  }
  
  @Test
  public void genericUpdate_Test() throws IOException {
    ICache cache = new HashMemory();
    cache.setParam( HashFile.HF_SIZE, MAX_INTENTS + "");
    
    cache.open();

    for ( int i = 0; i < MAX_INTENTS; i++) {
      cache.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }
    
    for ( int i = 0; i < MAX_INTENTS; i++) {
      if ( i % 3 == 0 ) {
        cache.put( "gato" + i, "El gato naranja ya no mira ningún sitio, asi que " + i + " elefantes estan solos");
      }
      else if ( i % 5 == 0 ) {
        cache.remove( "gato" + i);
      }
    }

    for ( int i = 0; i < MAX_INTENTS; i++) {
      if ( i % 3 == 0 ) {
        assertEquals( "El gato naranja ya no mira ningún sitio, asi que " + i + " elefantes estan solos", 
                      cache.get( "gato" + i));
      }
      else if ( i % 5 == 0 ) {
        //
      }
      else {
        assertEquals( "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante", 
                      cache.get( "gato" + i));
      }
    }
    
    cache.close();
  }
  
}
