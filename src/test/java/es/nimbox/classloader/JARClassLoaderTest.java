package es.nimbox.classloader;

import static org.junit.Assert.assertEquals;

import org.junit.Test;



public class JARClassLoaderTest {
  private static String DIR  = "./src/test/resources";
  private static String FILE = "./src/test/resources/test.jar";
  
  @Test
  public void basicTest() throws Exception {
    JARClassLoader jcl = new JARClassLoader();
    
    assertEquals( "Nim.Esto JARClassLoader", jcl.toString());
    
    jcl.addJar( FILE);
    
    assertEquals( 1, jcl.getJarLoaded().size());
    assertEquals( 19, jcl.getClassLoaded().size());
  }
  
  @Test
  public void eventTest() throws Exception {
    JARClassLoader jcl = new JARClassLoader();
    
    assertEquals( "Nim.Esto JARClassLoader", jcl.toString());
    
    jcl.addEventListener( new ILoaderListener() {
      int numDirClose = 0;
      int numJarClose = 0;
      
      int numFiles = 0;
      
      @Override
      public void actionPerformed( LoaderEvent ev) {
        // Open directory. There are five directories, two with a jar and three directories (5 interesting objects)
        if ( ev.getId() == LoaderEvent.BEGIN_DIR && ev.getObj().endsWith( "resources") ) {
          assertEquals( 5, ev.getNumFiles());
        }
        else if ( ev.getId() == LoaderEvent.BEGIN_DIR && ev.getObj().endsWith( "BOOT-INF")  ) {
          assertEquals( 1, ev.getNumFiles());
        }
        else if ( ev.getId() == LoaderEvent.BEGIN_DIR && ev.getObj().endsWith( "lib")  ) {
          assertEquals( 1, ev.getNumFiles());
        }
        else if ( ev.getId() == LoaderEvent.BEGIN_DIR ) {
          assertEquals( 0, ev.getNumFiles());
        }
        
        // open jar
        if ( ev.getId() == LoaderEvent.BEGIN_JAR ) {
          numJarClose++;
        }
        
        // read class
        if ( ev.getId() == LoaderEvent.READED_CLASS ) {
          numFiles++;
        }
        
        // Close directory
        if ( ev.getId() == LoaderEvent.END_DIR ) {
          numDirClose++;
        }
        if ( ev.getId() == LoaderEvent.END_DIR && ev.getObj().endsWith( "resources") ) {
          assertEquals( 5, numDirClose);
        }
        
        // End jar
        if ( ev.getId() == LoaderEvent.END_JAR ) {
          numJarClose--;
          assertEquals( 0, numJarClose);
          assertEquals( 19, numFiles);
        }
      }}
    );
    
    jcl.addDir( DIR, true);
    
    assertEquals( 1, jcl.getJarLoaded().size());
    assertEquals( 19, jcl.getClassLoaded().size());
  }
  

}
