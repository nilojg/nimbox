package es.nimbox.database;

import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.junit.Assert.*;

public class SqlConnTest {

  private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
  private String jdbcURL = "jdbc:derby:memory:testDB;create=true";

  @Test
  public void sweetTest() throws Exception {
    SqlConn nc = new SqlConn();
    nc.setDriverClass( driver);
    nc.setUrl( jdbcURL);

    Connection con = nc.getConnection();

    assertNotNull( con);

    nc.close();

    try ( SqlConn nc2 = new SqlConn()) {
      nc2.setDriverClass( driver);
      nc2.setUrl( jdbcURL);

      Connection con2 = nc2.getConnection();

      assertNotNull( con2);
    }
  }

  @Test
  public void buildTest() throws Exception {
    try ( Connection con = SqlConn.build().getConnection(); ) {
      fail();
    }
    catch ( SQLException ex) {
      assertNotEquals( -1, ex.getMessage().indexOf( "because is null"));
    }

    try ( Connection con = SqlConn.build().driverClass(driver).getConnection();) {
      fail();
    }
    catch ( SQLException ex) {
      assertNotEquals( -1, ex.getMessage().indexOf( "be null"));
    }

    try ( Connection con = SqlConn.build().driverClass(driver).url(jdbcURL).getConnection(); ) {
      assertNotNull( con);
    }
    catch ( Exception ex) {
      fail();
    }

    try ( SqlConn conn = SqlConn.build().driverClass(driver).url(jdbcURL) ) {
      assertNotNull( conn.getConnection());
    }
    catch ( Exception ex) {
      fail();
    }
  }


}
