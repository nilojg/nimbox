package es.nimbox.database;

import es.nimbox.box.ElasticBox;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class SqlExecTest {
  private static Connection con;

  static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  static Date base;

  @BeforeClass
  public static void setUp() throws Exception {
    String driver = "org.apache.derby.jdbc.EmbeddedDriver";
    String jdbcURL = "jdbc:derby:memory:testDB;create=true";
    Class.forName( driver);

    base = sdf.parse( "2020-01-01 10:00:00");

    con = DriverManager.getConnection(jdbcURL);

    populate();
  }

  @AfterClass
  public static void cleanUp() throws Exception {
    dropTable();
  }


  @Test
  public void sweetTest() throws SQLException {
    SqlExec se = SqlExec.build()
                        .conn( con)
                        .command("select * from Usuarios where age > 5 and age <= 15 order by age");

    int read = se.executeQuery();

    assertEquals( 10, read);

    List<ElasticBox> rs = se.getResults();

    assertEquals( 10, rs.size());
    assertEquals( 6, rs.get(0).getAsInt( "AGE").intValue());
    assertEquals( 15, rs.get(9).getAsInt( "AGE").intValue());
  }

  @Test
  public void maxResultsTest() throws SQLException {
    int max = 5;

    SqlExec se = SqlExec.build()
                        .conn( con)
                        .maxResults( max)
                        .command("select * from Usuarios where age > 5 and age <= 15 order by age");

    int read = se.executeQuery();

    assertEquals( max, read);

    List<ElasticBox> rs = se.getResults();

    assertEquals( max, rs.size());
    assertEquals( 6, rs.get(0).getAsInt( "AGE").intValue());
    assertEquals( 5 + max, rs.get(max-1).getAsInt( "AGE").intValue());
  }

  @Test
  public void maxResultsFunctionalTest() throws SQLException {
    int max = 5;

    List<ElasticBox> rs = SqlExec.build()
                                 .conn( con)
                                 .maxResults( max)
                                 .executeQuery( "select * from Usuarios where age > 5 and age <= 15 order by age")
                                 .getResults();

    assertEquals( max, rs.size());
    assertEquals( 6, rs.get(0).getAsInt( "AGE").intValue());
    assertEquals( 5 + max, rs.get(max-1).getAsInt( "AGE").intValue());
  }

  @Test
  public void wrongQueryTest() throws SQLException {
    SqlExec se = SqlExec.build()
                        .conn(con)
                        .command("select * from Gatos");

    try {
      se.executeQuery();
      fail();
    }
    catch ( SQLException ex ) {
      assertTrue( ex.getCause() instanceof SQLSyntaxErrorException);
      assertNotEquals( -1, ex.getMessage().indexOf( "not exist"));
    }
  }

  @Test
  public void insertTest() throws Exception {
    SqlExec se = new SqlExec( con);

    for ( int i = 0; i < 5; i++) {
      Date born = new Date(base.getTime() + (1000*i));
      se.command("insert into Usuarios VALUES (" + i + ", 'NoFunc-Eneas Pirulez" + i + "', '" + sdf.format(born) + "', " + i + ")");

      assertEquals( 1, se.executeUpdate());
    }

    assertEquals( 1, se.command("select count(*) from usuarios where personName like 'NoFunc%'").executeQuery());
    assertEquals( 5, se.getResults().get( 0).getAsInt( "1").intValue());
  }


  @Test
  public void insertFunctionalTest() throws Exception {
    SqlExec se = new SqlExec( con);

    for ( int i = 0; i < 5; i++) {
      Date born = new Date(base.getTime() + (1000*i));

      se = SqlExec.build().conn( con)
                     .executeUpdate( "insert into Usuarios VALUES (" + i + ", 'Func-Eneas Pirulez" + i + "', '" + sdf.format(born) + "', " + i + ")");
    }

    assertEquals( 1, se.command("select count(*) from usuarios where personName like 'Func%'").executeQuery());
    assertEquals( 5, se.getResults().get( 0).getAsInt( "1").intValue());
  }



  private static void populate() throws Exception {
    Statement st = con.createStatement();

    st.execute( "create table Usuarios (\n" +
                        "    ID integer\n" +
                        "  , personName VARCHAR(50)\n" +
                        "  , bornDate TIMESTAMP\n" +
                        "  , age integer\n" +
                        ")");

    for ( int i = 0; i < 100; i++) {
      Date born = new Date(base.getTime() + (1000*i));

      String ins = "insert into Usuarios VALUES (" + i + ", 'Eneas Pirulez" + i + "', '" + sdf.format( born) + "', " + i + ")";

      st.execute( ins);
    }

    st.close();
  }

  private static void dropTable() throws Exception {
    Statement st = con.createStatement();

    st.execute( "drop table Usuarios");
    st.close();
  }
}
