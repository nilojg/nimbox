package es.nimbox.pqc;

import es.nimbox.box.Utils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PWCTest {


  static int processed = 0;
  static int error = 0;


  public static synchronized void reset() {
    processed = 0;
    error = 0;
  }

  public static synchronized void incProcessed() {
    processed++;
  }

  public static synchronized void incError() {
    error++;
  }


  @Test
  public void sweetTest() throws Exception {
    reset();

    ConsumerProducerEngine<String> cpe = new ConsumerProducerEngine<>();

    cpe.addErrorProcessor( new ErrorProcessor());

    //cpe.setSpeedLimit(ConsumerProducerEngine.LimitSpeed.LIMIT_SPEED_PER_THREAD, 1, PaceMaker.TYPE.PER_SECOND);

    NimQueue<String> queue = new MemoryQueue<>();
    cpe.setQueue(queue);

    for ( int i = 0; i < 10; i++) {
      cpe.addConsumer( new Consumer(i));
    }

    //Add producers
    List<Thread> producers = new ArrayList<>();

    for ( int i = 0; i < 3; i++) {
      Producer pp = new Producer( i);

      cpe.addProducer( pp);
      producers.add( new Thread( pp));
    }

    // Start producers
    for ( Thread pp : producers) {
      pp.start();
    }

    // Wait runners
    for ( Thread pp : producers) {
      pp.join();
    }

    // Wait until everything is processed
    do {
      Utils.sleep( 10);
    }
    while ( cpe.getRemain() > 0 );

    // Stop all threads
    cpe.stop();

    assertEquals( 0, cpe.getRemain());
    assertEquals( 3000, processed);
    assertEquals( processed, cpe.getProcessed());
    assertEquals( error, cpe.getFailed());
    assertEquals( cpe.getProcessed(), cpe.getSuccess() + cpe.getFailed());
  }



  static public class ErrorProcessor implements NimErrorListener<String> {
    @Override
    public void processError(ErrorReport<String> error) {
      incError();
    }
  }


  static public class Consumer implements NimConsumer<String> {
    int num = 0;
    int id = 0;

    public Consumer( int id) {
      this.id = id;
    }

    @Override
    public void process(String item) {
      Utils.sleep( 10);

      incProcessed();
      num++;

      if ( num % 100 == 1 ) {
        throw new NullPointerException();
      }

    }
  }

  static public  class Producer extends AbstractNimProducer<String> implements Runnable {
    int id = 0;

    public Producer( int id) {
      this.id = id;
    }

    @Override
    public void run() {
      for ( int i = 0; i < 1000; i++) {
        enqueueItem( "PRODUCER " + id + ", item " + i);
        Utils.sleep( 5);
      }
    }
  }
}
