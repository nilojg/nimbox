package dummy;

import es.nimbox.box.XMLAttribute;
import es.nimbox.box.XMLClass;
import es.nimbox.box.XMLElement;

@XMLClass( name="Son", indent=2)
public class ToXMLTestAnnotatedSon {
  @XMLAttribute( name = "sonId")
  int id;
  
  @XMLElement( name = "sonNombre")
  String field1;
  
  int field2;
  
  ToXMLTestAnnotatedSon( int id) {
    this.id = id;
    this.field1 = "son_" + id;
    this.field2 = id * 2;
  }
}
