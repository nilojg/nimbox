package dummy;


public class ToXMLTestSimpleArray {
  String field1;
  int field2;
  long field3;
  Double field4;
  
  Object l1[];

  public ToXMLTestSimpleArray( String field1, int field2, long field3, Double field4) {
    super();
    this.field1 = field1;
    this.field2 = field2;
    this.field3 = field3;
    this.field4 = field4;
    
    l1 = new Object[4];
    l1[0] = field1;
    l1[1] = field2;
    l1[2] = field3;
    l1[3] = field4;
  }
}
