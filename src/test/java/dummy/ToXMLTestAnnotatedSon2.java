package dummy;

import es.nimbox.box.XMLAttribute;
import es.nimbox.box.XMLClass;
import es.nimbox.box.XMLElement;

@XMLClass( name="Son", namespace="nim", indent=2)
public class ToXMLTestAnnotatedSon2 {
  @XMLAttribute( name = "sonId")
  int id;
  
  @XMLAttribute( name = "age")
  int age;
  
  @XMLElement( name = "sonNombre")
  String field1;
  
  int field2;
  
  ToXMLTestAnnotatedSon2( int id) {
    this.id = id;
    this.age = id * 5;
    this.field1 = "son_" + id;
    this.field2 = id * 2;
  }
}
