package dummy;

import java.util.ArrayList;
import java.util.List;

import es.nimbox.box.XMLAttribute;
import es.nimbox.box.XMLClass;
import es.nimbox.box.XMLElement;

@XMLClass( name="Turiru", namespace="nimbox", indent=4)
public class ToXMLTestAnnotated2 {
  @XMLAttribute( name = "identification")
  int id;
  
  @XMLElement( name = "nombre")
  String field1;
  int field2;
  
  @XMLElement
  long field3;
  
  @DummyAnnotation  // Nonsense annotation, just to add some annotations to the reflection API
  Double field4;
  
  @XMLElement( name="lista")
  List<ToXMLTestAnnotatedSon> l1;
  
  @XMLElement( name="theSon")
  ToXMLTestAnnotatedSon2 aSon;

  public ToXMLTestAnnotated2( int id, String field1, int field2, long field3, Double field4) {
    super();
    this.id = id;
    this.field1 = field1;
    this.field2 = field2;
    this.field3 = field3;
    this.field4 = field4;
    
    aSon = new ToXMLTestAnnotatedSon2( id+5);
    
    l1 = new ArrayList<ToXMLTestAnnotatedSon>();
    for ( int i = 0; i < id; i++ ) {
      l1.add(  new ToXMLTestAnnotatedSon( i));
    }
  }
}