package dummy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This is a dummy annotation just to put some noise in unit tests
 * 
 * @author nilo
 */
@Retention(value=RetentionPolicy.RUNTIME) 
@Target(value=ElementType.FIELD)
public @interface DummyAnnotation {

}
