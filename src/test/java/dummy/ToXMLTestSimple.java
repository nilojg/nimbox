package dummy;

import java.util.ArrayList;
import java.util.List;

public class ToXMLTestSimple {
  public String field1;
  public int field2;
  public long field3;
  public Double field4;
  
  List<Object> l1;

  public ToXMLTestSimple() {}

  public ToXMLTestSimple( String field1, int field2, long field3, Double field4) {
    super();
    this.field1 = field1;
    this.field2 = field2;
    this.field3 = field3;
    this.field4 = field4;
    
    l1 = new ArrayList<Object>();
    l1.add( field1);
    l1.add( field2);
    l1.add( field3);
    l1.add( field4);
  }
  
  public List<Object> getL1() {
    return l1;
  }
  
  public void setL1( List<Object> l1) {
    this.l1 = l1;
  }
}
