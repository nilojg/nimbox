package es.nimbox.pqc;


/**
 * <p>This interface defines a very simple and basic queue. It's a very little subset of <code>LinkedBlockingQueue</code>, so
 * you can define a very basic implementation of this interface using one, like <code>MemoryQueue</code></p>
 *
 * @param <T> the item type
 *
 * @author nilo.gonzalez
 *
 * @see MemoryQueue
 */
public interface NimQueue<T> {

  /**
   * <p>This method returns the number of items in the queue</p>
   * @return the number of items in the queue
   */
  int getSize();

  /**
   * <p>This method returns the first item available in the queue, blocking the thread for <code>timeout</code> milliseconds
   * if there isn't any item available. If the specified waiting time elapses before an element is available, the method
   * returns <code>null</code>.</p>
   * @param timeout the timeout in milliseconds
   * @return the first item available in the queue, or <code>null</code> i the specified waiting time elapses before an
   * element is available
   */
  T poll(long timeout);

  /**
   * <p>This method inserts the <code>item</code> in the queue and returns the same element.</p>
   * @param item the item to be inserted
   * @return the parameter <code>item</code>
   */
  T put( T item);
}
