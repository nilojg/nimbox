package es.nimbox.pqc;

/**
 * <p>This interface defines the method that a <code>NimProducer</code> must define to fit correctly
 * with the <code>{@link ConsumerProducerEngine}</code>.</p>
 *
 * <p>The point is that each time that the producer "creates" a new item, it notifies the listeners
 * that there is something new to process. Usually, the listener is <code>{@link ConsumerProducerEngine}</code>
 * which will enqueue the item so the consumer can process it.</p>
 *
 * <p>This way the producers doesn't need to know nothing about queues, they just crete items and say "I have
 * something new".</p>
 *
 * <p>But a producer could handle the queue by its own or even be in a different process running
 * in another machine. For example, there could be a producer running in a k8s pod, adding items to a Kafka
 * stream, and the <code>{@link ConsumerProducerEngine}</code> could be in another pod handling some
 * dozens of <code>{@link ThreadedConsumer}</code> getting the items from the Kafka stream. In this case,
 * how the producer enqueues the item by its own, the producer doesn't need to implement <code>NimProducer</code>
 * because it's not going to use the <code>{@link ConsumerProducerEngine}</code>
 *
 * @param <T> the produced items
 *
 * @author nilo.gonzalez
 *
 */
public interface NimProducer<T> {

  /**
   * <p>This methods registers <code>listener</code> in the list of listeners</p>
   * @param listener the listener
   */
  void addCreatedItemlistener( CreatedItemListener<T> listener);

  /**
   * <p>This method removes <code>listener</code> from the list of listeners</p>
   * @param listener the listener
   */
  void removeCreatedItemlistener( CreatedItemListener<T> listener);
}
