package es.nimbox.pqc;


import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class processes errors calling the registered error listeners. The error report is separated in it's own thread
 * to avoid that a slow error routine could stop the consumers.</p>
 *
 * @author nilo.gonzalez
 *
 */
class ThreadedErrorReporter<T> extends Thread {

  private boolean stop;
  private boolean running;

  private List<NimErrorListener<T>> errorListeners;

  private NimQueue<ErrorReport<T>> errorQueue;


  public ThreadedErrorReporter() {
    this.stop = false;
    this.running = false;

    this.errorListeners = new ArrayList<>();
  }

  /**
   * <p>This method sets the queue where the errors will be enqueued to be processed. If the error queue is not set,
   * the error processor will use a <code>MemoryQueue</code></p>
   * @param errorQueue the queue where the errors will be enqueued to be processed
   */
  public void setErrorQueue( NimQueue<ErrorReport<T>> errorQueue) {
    this.errorQueue = errorQueue;
  }


  /**
   * <p>This method adds the error described by the exception <code>e</code>, raised processing the <code>item</code>,
   * to the queue to be processed</p>
   * @param item the item which raised the error
   * @param e the exception
   */
  public void processError( T item, Exception e) {
    errorQueue.put( new ErrorReport<>( item, e));
  }

  /**
   * <p>This method tells the error processor that it must stop <b>after processing the remaining errors</b></p>
   */
  public void stopThread() {
    this.stop = true;
  }

  /**
   * <p>This method returns <code>true</code> if the thread is currently running and <code>false</code> if it hasn't
   * been started or has been stopped.</p>
   * @return <code>true</code> if the thread is currently running and <code>false</code> if it hasn't been started or
   *         has been stopped
   */
  public boolean isRunning() {
    return running;
  }

  @Override
  public void run() {
    running = true;

    // If the user hasn't set a queue, well, let's use a memory queue
    if ( errorQueue == null ) {
      errorQueue = new MemoryQueue<>();
    }

    // If there are any error, the loop cannot finish, because errors cannot be hidden under the carpet
    while ( !stop || errorQueue.getSize() > 0 ) {
      ErrorReport<T> error = errorQueue.poll( 10);

      if ( error == null ) {
        continue;
      }

      for ( NimErrorListener<T> el : errorListeners) {
        el.processError( error);
      }
    }

    running = false;
  }


  public void addErrorProcessor( NimErrorListener<T> errorListener) {
    errorListeners.add( errorListener);
  }

  public void removeErrorProcessor( NimErrorListener<T> errorListener) {
    errorListeners.remove( errorListener);
  }
}
