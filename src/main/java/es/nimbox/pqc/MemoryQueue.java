package es.nimbox.pqc;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * <p>This class is a very basic implementation of a <code>NimQueue</code>. In fact, it's only a <code>LinkedBlockingQueue</code>,
 * which can be accessed using the method <code>LinkedBlockingQueue</code>.</p>
 * <p>This is a <i>horrible</i> practice in OOP, I know, but this class is so basic that it's just for testing purposes and
 * very basic applications, and in those things having the possibility to clean the queue can be very handy. And in the end, it's less that
 * 50 lines of codes (with a lot of spaces in the middle), so if you don't like this class it's not going to be very
 * hard to create your own implementation.</p>
 *
 * @param <T> the item type
 *
 * @author nilo.gonzalez
 *
 */
public class MemoryQueue<T> implements NimQueue<T> {

  private LinkedBlockingQueue<T> queue;


  public MemoryQueue() {
    queue = new LinkedBlockingQueue<>();
  }


  public LinkedBlockingQueue<T> getRealQueue() {
    return queue;
  }

  @Override
  public int getSize() {
    return queue.size();
  }

  @Override
  public T poll(long timeout) {
    try {
      return queue.poll(timeout, TimeUnit.MILLISECONDS);
    }
    catch ( InterruptedException e ) {
      Thread.currentThread().interrupt();
      return null;
    }
  }

  @Override
  public T put(T item) {
    try {
      queue.put( item);

      return item;
    }
    catch ( InterruptedException e ) {
      Thread.currentThread().interrupt();
      return null;
    }
  }
}
