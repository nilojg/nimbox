package es.nimbox.pqc;

import es.nimbox.box.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class implements a <i>queue of queues</i>, a queue that contains as many queues as you need. The point of this class
 * is to create a system where there are one <code>NimProducer</code> which puts item in the main queue, the <code>MultiQueue</code>,
 * and several <code>NimConsumer</code> where each one gets a reference for one of the contained queues, so each consumer has its
 * own <code>NimQueue</code> to read items, so the consumers don't compete to read from the queues because each one has it's own
 * queue. If the consumers are created with a reference to the <code>MultiQueue</code>, the point of the class is to mix queus of
 * different types or running over different places.</p>
 *
 * @param <T> the item type
 */
public class MultiQueue<T> implements NimQueue<T> {

  private int chapters;
  private Class<NimQueue<T>> queueType;

  private List<NimQueue<T>> queues;


  private int roundRobin;


  /**
   * <p>This constructor creates and empty <code>MultiQueue</code></p>
   */
  public MultiQueue() {
    this.queueType = null;
    this.queues = new ArrayList<>();
    this.roundRobin = 0;
  }


  /**
   * <p>This constructor creates a <code>MultiQueue</code> of <code>chapters</code> number of queues of type <code>queueType</code></p>
   * @param queueType the type of the queues
   * @param chapters the number of the queues
   * @throws Exception if there is some kind of problem creating the queue
   */
  public MultiQueue( Class<NimQueue<T>> queueType, int chapters) throws Exception {
    this();

    this.queueType = queueType;

    for ( int i = 0; i < chapters; i++) {
      addChapter();
    }
  }



  /**
   * <p>This method adds a new chapter to the queue</p>
   * @throws Exception if there is any kind of problem creating the new chapter or the <code>MultiQueue</code> was created
   *                   with the default constructor
   */
  public void addChapter() throws Exception {
    if ( this.queueType == null ) {
      throw new Exception( "The queueType is not set. Use 'addChapter( queue)' instead of 'addChapter()'");
    }

    addChapter( queueType.getConstructor().newInstance());
  }

  /**
   * <p>This method adds the <code>queue</code> to the <code>MultiQueue</code></p>
   * @param queue the new <code>queue</code> in the <code>MultiQueue</code>
   */
  public synchronized void addChapter( NimQueue<T> queue) {
    this.chapters++;

    queues.add( queue);
  }

  /**
   * <p>This method returns the queue with the number <code>chapter</code>. If there isn't a <code>chapter</code>
   * with this number, well, you will get an <code>ArrayIndexOutOfBoundsException</code></p>
   * @param chapter the chapter index
   * @return the queue
   * @throws ArrayIndexOutOfBoundsException if <code>chapter</code> is too high
   */
  public NimQueue<T> getQueue( int chapter) {
    return this.queues.get( chapter);
  }


  /**
   * <p>This method returns the sum of all the sizes of the contained queues. For performance, this is not synchronized,
   * so the sum could not be accurate.</p>
   * @return the sum of all the sizes of the contained queues
   */
  @Override
  public int getSize() {
    int acum = 0;

    for ( NimQueue<T> qq : queues ) {
      acum += qq.getSize();
    }

    return acum;
  }

  /**
   * <p>This method returns the next item in any of the queues contained in this <code>MultiQueue</code>. If
   * all queues are empty, the method locks the thread until there is an available item.</p>
   * @return the next item in any of the queues contained in this <code>MultiQueue</code>
   */
  @Override
  public T poll(long timeout) {
    T res = null;

    int tries = 0;

    // While the loop doesn't find something to return, it will be looping again and again
    while ( res == null ) {
      int ix = getNextChapter();

      res = getQueue( ix).poll(timeout);
      tries++;

      // If there's nothing to return and has tries with all the queues, let's sleep
      // for some time to allow the rest to use the CPU
      if ( ( res == null ) && ( tries % chapters == 0) ) {
        Utils.sleep( 10);
      }
    }

    return res;
  }

  /**
   * <p>This method adds the <code>item</code> to any of the queues contained in this <code>MultiQueue</code>.</p>
   * @param item the new item
   * @return the added item
   */
  @Override
  public T put( T item) {
    int ix = getNextChapter();

    return getQueue( ix).put( item);
  }

  /**
   * This method returns the next chapter, in RoundRobin
   * @return the next chapter, in RoundRobin
   */
  private synchronized int getNextChapter() {
    return roundRobin >= queues.size() ? 0 : roundRobin++ ;
  }

}
