package es.nimbox.pqc;

/**
 * <p>This interface defines the method that a class must implement to <i>consume</i> the items of the
 * producer-consumer pattern. It has a single method, <code>process</code>, which obviously will be invoked by
 * the engine each time an item is created by the producer.</p>
 *
 * <p>There are more information in the class <code>ConsumerProducerEngine</code>.</p>
 *
 * @param <T> the item type
 *
 * @author nilo.gonzalez
 *
 * @see ConsumerProducerEngine
 */
public interface NimConsumer<T>  {

  /**
   * <p>This method will be invoked whenever an item is ready to be processed by the consumer.</p>
   * @param item the item to be processed
   */
  void process( T item);
}
