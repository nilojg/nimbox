package es.nimbox.pqc;

import es.nimbox.box.PaceMaker;
import es.nimbox.box.Utils;

/**
 * <p>This class implements a thread-wrapper for consumers. The <code>NimConsumer</code> implementations
 * just have to worry about processing items because this class will create a thread and handle the stats.</p>
 *
 * @param <T> the items
 *
 * @author nilo.gonzalez
 *
 */
class ThreadedConsumer<T> extends Thread {

  private NimQueue<T> queue;

  private NimConsumer<T> consumer;

  private boolean stop, running;

  private long processed, success, failed;

  private long queueSleep = ConsumerProducerEngine.EMPTY_QUEUE_SLEEP;

  private String paceMakerId;

  /**
   * This thread is just to process the errors in a different thread
   */
  private ThreadedErrorReporter<T> errorReporter;


  public ThreadedConsumer( NimConsumer<T> consumer) {
    this.stop = false;
    this.running = false;
    this.processed = 0;
    this.success = 0;
    this.failed = 0;

    this.paceMakerId = null;

    this.consumer = consumer;
  }

  /**
   * <p>This method sets the id of the assigned <code>{@link PaceMaker}</code>, used to reduce the processing speed.
   * If there isn't an assigned <code>{@link PaceMaker}</code>, the items are processed as far as the machine can...</p>
   * @param paceMakerId the assigned <code>PaceMaker</code> id
   */
  void setPaceMakerId( String paceMakerId) {
    this.paceMakerId = paceMakerId;
  }

  /**
   * <p>This method returns the id of the assigned <code>{@link PaceMaker}</code>, used to reduce the processing speed.</p>
   * @return the id of the assigned <code>{@link PaceMaker}</code>, used to reduce the processing speed
   */
  String getPaceMakerId() {
    return paceMakerId;
  }

  /**
   * <p>This method returns the time in millis that the thread will wait if a thread is empty</p>
   * @return the time in millis that the thread will wait if a thread is empty
   */
  public long getQueueSleep() {
    return queueSleep;
  }

  /**
   * <p>This method sets the time in millis that the thread will wait if a thread is empty</p>
   * @param queueSleep the time in millis that the thread will wait if a thread is empty
   */
  public void setQueueSleep(long queueSleep) {
    this.queueSleep = queueSleep;
  }

  public void setQueue(NimQueue<T> queue) {
    this.queue = queue;
  }

  /**
   * <p>This method tells the consumer that it must stop after processing the current item</p>
   */
  public void stopConsumer() {
    this.stop = true;
  }

  /**
   * <p>This method returns <code>true</code> if the thread is currently running and <code>false</code> if it hasn't
   * been started or has been stopped.</p>
   * @return <code>true</code> if the thread is currently running and <code>false</code> if it hasn't been started or
   *         has been stopped
   */
  public boolean isRunning() {
    return running;
  }

  /**
   * <p>This method sets the <code>ThreadedErrorReporter</code> for the consumer. If the consumer finds and exception
   * when processing items, it will send a notification to the <code>ThreadedErrorReporter</code> with the item which
   * raised the error and the exception.</p>
   * <p>If the user doesn't set a <code>ThreadedErrorReporter</code>, well, the errors will simple forgotten...</p>
   * @param errorReporter the error reported for this consumer.
   */
  public void setErrorReporter( ThreadedErrorReporter<T> errorReporter) {
    this.errorReporter = errorReporter;
  }

  @Override
  public void run() {
    running = true;

    while ( !stop ) {
      // This is just to prevent NullPointerExceptions if the user forgets to register a queue before adding consumers
      if ( queue == null || queue.getSize() == 0 ) {
        Utils.sleep( queueSleep);
        continue;
      }

      T item = queue.poll(queueSleep);

      if ( item == null ) {
        continue;
      }

      processed++;

      try {
        PaceMaker.getPaceMaker().makePace( paceMakerId);
        consumer.process( item);
        success++;
      }
      catch ( Exception e ) {
        failed++;

        if ( errorReporter != null ) {
          errorReporter.processError( item, e);
        }
      }
    }

    running = false;
  }

  /**
   * <p>This method returns the total number of processed items</p>
   * @return the total number of processed items
   */
  public long getProcessed() {
    return processed;
  }

  /**
   * <p>This method returns the number of successfully processed items</p>
   * @return the number of successfully processed items
   */
  public long getSuccess() {
    return success;
  }

  /**
   * <p>This method returns the number of processed items with error</p>
   * @return the number of processed items with error
   */
  public long getFailed() {
    return failed;
  }
}
