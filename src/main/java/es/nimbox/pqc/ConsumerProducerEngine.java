package es.nimbox.pqc;

import es.nimbox.box.PaceMaker;
import es.nimbox.box.Utils;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * <pre>
 *   NimQueue&lt;String&gt; queue = ....
 *
 *   NimProducer&lt;String&gt; prod1 = ....  AbstractNimProducer
 *   NimProducer&lt;String&gt; prod2 = ....  AbstractNimProducer
 *
 *   NimConsumer&lt;String&gt; cons1 = ....  AbstractNimConsumer
 *   NimConsumer&lt;String&gt; cons2 = ....  AbstractNimConsumer
 *   NimConsumer&lt;String&gt; cons3 = ....  AbstractNimConsumer
 *
 *   ConsumerProducerEngine&lt;String&gt; engine = new ConsumerProducerEngine();
 *   engine.addQueue( queue);
 *   engine.addProducer( prod1);
 *   engine.addProducer( prod2);
 *   engine.addConsumer( cons1);
 *   engine.addConsumer( cons2);
 *   engine.addConsumer( cons3);
 *
 *   // now it is working, but in the end, call engine.stop();
 *   engine.stop();
 * </pre>
 *
 * @param <T> The type of the consumed items
 */
public class ConsumerProducerEngine<T> implements CreatedItemListener<T> {
  private static final long SLEEP_TIME_ON_STOPPING = 10;
  private static final String SPEED_LIMIT_ID_PREFIX = "SPEED_LIMIT_ID_PREFIX:";


  /**
   * This property defines the default time that each thread will wait if the queue is empty.
   */
  public static final long EMPTY_QUEUE_SLEEP = 10;

  /**
   * <p>This enumeration defines the type of speed limit to use:</p>
   * <ul>
   *   <li><code>LIMIT_SPEED_PER_THREAD</code> : a limit per consumer thread, to ensure that each
   *   independent consumer is not too fast, although the addition of all the consumers can be fast</li>
   *   <li><code>LIMIT_SPEED_PER_ENGINE</code> : a limit to the engine, so the addition of all the threads
   *   aren't too fast </li>
   * </ul>
   */
  public enum LimitSpeed { LIMIT_SPEED_PER_ENGINE, LIMIT_SPEED_PER_THREAD}

  private List<NimProducer<T>> producers;
  private NimQueue<T> queue;
  private List<ThreadedConsumer<T>> consumers;

  private String speedLimitEngineId;
  private LimitSpeed speedLimitType;
  private int speedLimitPace;
  private PaceMaker.TYPE speedLimitPaceMakerType;

  /**
   * This thread is just to process the errors in a different thread
   */
  private ThreadedErrorReporter<T> errorReporter;


  private long queueSleep = EMPTY_QUEUE_SLEEP;


  public ConsumerProducerEngine() {
    this.producers = new ArrayList<>();
    this.consumers = new ArrayList<>();
    this.queue = null;
    this.speedLimitType = null;

    this.errorReporter = new ThreadedErrorReporter<>();
  }


  public void setSpeedLimit( LimitSpeed limitType, int pace, PaceMaker.TYPE paceMakerType) {
    this.speedLimitType = limitType;
    this.speedLimitPace = pace;
    this.speedLimitPaceMakerType = paceMakerType;

    this.speedLimitEngineId = SPEED_LIMIT_ID_PREFIX + this.getClass().hashCode();

    if ( speedLimitType == LimitSpeed.LIMIT_SPEED_PER_ENGINE ) {
      PaceMaker.getPaceMaker().addPace( speedLimitEngineId, speedLimitPaceMakerType, speedLimitPace);
    }
  }

  /**
   * <p>This method returns the number of unprocessed events in the queue</p>
   * @return the number of unprocessed events in the queue
   */
  public int getRemain() {
    return queue.getSize();
  }

  /**
   * <p>This method returns the time in millis that the consumers will wait if a thread is empty</p>
   * @return the time in millis that the consumers will wait if a thread is empty
   */
  public long getQueueSleep() {
    return queueSleep;
  }

  /**
   * <p>This method sets the time in millis that the consumers will wait if a thread is empty, and updates all the consumers with the new value</p>
   * @param queueSleep the time in millis that the consumers will wait if a thread is empty
   */
  public void setQueueSleep(long queueSleep) {
    this.queueSleep = queueSleep;

    // Set the new queue to all the consumers
    for ( ThreadedConsumer<T> consumer : consumers) {
      consumer.setQueueSleep( getQueueSleep());
    }
  }


  /**
   * <p>This method registers the <code>producer</code> in this engine. Basically, registers this engine as a listener for <code>CreatedItemListener</code> events.</p>
   * <p>The engine starts to listen for events and adding them in the queue <b>immediately</b>, so be sure to register the queue before, or at least, that the producers
   * don't start to produce items before having a queue to store the items.</p>
   * @param producer the producer
   * @throws NullPointerException if the producer is <code>null</code>
   */
  public void addProducer( NimProducer<T> producer) {
    if ( producer == null ) {
      throw new NullPointerException();
    }

    // Register the producer
    producers.add( producer);

    // Add THIS engine as listener of the producer
    producer.addCreatedItemlistener( this);
  }

  /**
   * <p>This method unregisters the <code>producer</code> in this engine. Basically, unregisters this engine as a listener for <code>CreatedItemListener</code> events</p>
   * @param producer the producer
   * @return the removed producer or <code>null</code> if the <code>producer</code> doesn't exist
   * @throws NullPointerException if the producer is <code>null</code>
   */
  public NimProducer<T> removeProducer( NimProducer<T> producer) {
    if ( producer == null ) {
      return null;
    }

    // Register the producer
    if ( !producers.remove( producer) ) {
      return null;
    }

    // Add THIS engine as listener of the producer
    producer.removeCreatedItemlistener( this);

    return producer;
  }

  /**
   * <p>this method sets the <code>queue</code> in the engine. The engine registers the queue in all the consumers.</p>
   * <p>If the method is invoked repeatedly, it just overwrites the last value with the new value.</p>
   * @param queue the queue
   * @throws NullPointerException if the <code>queue</code> is null
   */
  public void setQueue(NimQueue<T> queue) {
    if ( queue == null ) {
      throw new NullPointerException();
    }

    // Register the queue
    this.queue = queue;

    // Set the new queue to all the consumers
    for ( ThreadedConsumer<T> consumer : consumers) {
      consumer.setQueue( queue);
    }
  }

  /**
   * <p>This method registers the <code>consumer</code> in this engine. The engine encapsulates the <code>consumer</code> in its own thread, so each consumer works
   * independently of the rest. This means that, if you are going to add the same consumer several times, <b>be sure that the consumer is thread-safe</b>. If the
   * consumer is not thread-safe, create a new instance of the consumer and register each instance.</p>
   * <p>This methods register the queue in the consumer, so be sure that you have registered a queue before adding consumers.</p>
   * <p>After the consumer registration, the consumer starts reading the queue <i>immediately</i>.</p>
   * @param consumer the new consumer
   * @throws NullPointerException if <code>consumer</code> is <code>null</code>
   */
  public void addConsumer( NimConsumer<T> consumer) {
    if ( consumer == null ) {
      throw new NullPointerException();
    }

    // Ensure that the error processor is ready before starting a consumer
    ensureErrorProcessorIsRunning();

    // Create a new threaded consumer
    ThreadedConsumer<T> tc = new ThreadedConsumer<>( consumer);

    // Register the consumer
    this.consumers.add( tc);

    // Set some things in the consumer and start it
    tc.setQueue( this.queue);
    tc.setQueueSleep( getQueueSleep());
    tc.setErrorReporter( errorReporter);

    // If there's a PaceMaker set, then prepare the values and set the consumer PaceMaker id
    if ( speedLimitType!= null ) {
      if ( speedLimitType == LimitSpeed.LIMIT_SPEED_PER_THREAD ) {
        String threadPaceMakerId = speedLimitEngineId + ":" + consumers.size();

        PaceMaker.getPaceMaker().addPace(threadPaceMakerId, speedLimitPaceMakerType, speedLimitPace);
        tc.setPaceMakerId(threadPaceMakerId);
      }
      else {
        tc.setPaceMakerId( speedLimitEngineId);
      }
    }

    tc.start();
  }

  /**
   * <p>This method sets the queue where the errors will be enqueued to be processed. If the error queue is not set,
   * the error processor will use a <code>MemoryQueue</code></p>
   * @param errorQueue the queue where the errors will be enqueued to be processed
   */
  public void setErrorQueue( NimQueue<ErrorReport<T>> errorQueue) {
    this.errorReporter.setErrorQueue( errorQueue);
  }

  /**
   * <p>This method unregisters this engine as listener to the producers and stops all the consumers, waiting until all
   * the consumers finally stop.</p>
   */
  public void stop() {
    // Tell the producers that this object is not going to consume anymore
    for ( NimProducer<T> producer : producers) {
      producer.removeCreatedItemlistener( this);
    }

    // Stop the consumers
    for ( ThreadedConsumer<T> consumer : consumers) {
      consumer.stopConsumer();
    }

    // Let's wait until all the consumers stop
    boolean oneRunning;

    do {
      Utils.sleep( SLEEP_TIME_ON_STOPPING);
      oneRunning = false;

      for ( ThreadedConsumer<T> consumer : consumers) {
        oneRunning |= consumer.isRunning();
      }
    }
    while ( oneRunning );

    // Let's wait until all the error processor stops
    errorReporter.stopThread();

    do {
      Utils.sleep( SLEEP_TIME_ON_STOPPING);
    }
    while ( errorReporter.isRunning() );
  }


  @Override
  public void process( T item) {
    // Just adds the item in the queue
    this.queue.put( item);
  }


  /**
   * <p>This method returns a JSON with the resume of the execution</p>
   * @return JSON with the resume of the execution
   */
  public String getResume() {
    StringBuilder res = new StringBuilder( "[\n");

    boolean first = true;

    for ( ThreadedConsumer<T> consumer : consumers) {
      if ( first ) {
        first = false;
        res.append( "  { \n");
      }
      else {
        res.append( ", { \n");
      }

      res.append( "    \"processed\" : \"" + consumer.getProcessed() + "\",\n");
      res.append( "    \"success\" : \"" + consumer.getSuccess() + "\",\n");
      res.append( "    \"fail\" : \"" + consumer.getFailed() + "\"\n");
      res.append( "  }\n");
    }

    res.append( "]");

    return res.toString();
  }



  /**
   * <p>This method returns the total number of processed items</p>
   * @return the total number of processed items
   */
  public long getProcessed() {
    long res = 0;

    for ( ThreadedConsumer<T> consumer : consumers) {
      res += consumer.getProcessed();
    }

    return res;
  }

  /**
   * <p>This method returns the number of successfully processed items</p>
   * @return the number of successfully processed items
   */
  public long getSuccess() {
    long res = 0;

    for ( ThreadedConsumer<T> consumer : consumers) {
      res += consumer.getSuccess();
    }

    return res;
  }

  /**
   * <p>This method returns the number of processed items with error</p>
   * @return the number of processed items with error
   */
  public long getFailed() {
    long res = 0;

    for ( ThreadedConsumer<T> consumer : consumers) {
      res += consumer.getFailed();
    }

    return res;
  }


  /**
   * <p>This method adds <code>errorListener</code> to the list of error listeners</p>
   * @param errorListener the new error listener
   */
  public void addErrorProcessor( NimErrorListener<T> errorListener) {
    if ( errorReporter != null ) {
      errorReporter.addErrorProcessor( errorListener);
    }
  }

  /**
   * <p>This method removes <code>errorListener</code> from the list of error listeners</p>
   * @param errorListener the error listener to remove
   */
  public void removeErrorProcessor( NimErrorListener<T> errorListener) {
    if ( errorReporter != null ) {
      errorReporter.removeErrorProcessor(errorListener);
    }
  }



  private void ensureErrorProcessorIsRunning() {
    // Let's start the error reporter before starting the whole thing
    if ( !errorReporter.isRunning() ) {
      errorReporter.start();
    }
  }
}
