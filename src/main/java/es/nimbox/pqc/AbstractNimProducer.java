package es.nimbox.pqc;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class just provides the <i>listener stuff</i> to make easier to implement the <code>NimProducer</code>
 * interface.</p>
 *
 * <p>The producer implementation must invoke the method <code>enqueueItem</code> each time an item is created.
 * this way the <code>{@link ConsumerProducerEngine}</code> enqueues the item and the consumers process it.</p>
 *
 * <p>Obviously, if your producer enqueues the item by its own, your producer doesn't need to implement
 * <code>NimProducer</code> because your producer it's not going to use the <code>{@link ConsumerProducerEngine}</code>
 * infrastructure.</p>
 *
 * @param <T> The produced item
 *
 * @author nilo.gonzalez
 *
 * @see NimProducer
 */
public class AbstractNimProducer<T> implements NimProducer<T>{

  /**
   * This list stores the registered listeners
   */
  protected List<CreatedItemListener<T>> listeners = new ArrayList<>();


  @Override
  public void addCreatedItemlistener( CreatedItemListener<T> listener) {
    this.listeners.add( listener);
  }

  @Override
  public void removeCreatedItemlistener( CreatedItemListener<T> listener) {
    this.listeners.remove( listener);
  }

  public void enqueueItem( T item) {
    for ( CreatedItemListener<T> listener : listeners) {
      listener.process( item);
    }
  }
}
