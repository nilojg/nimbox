package es.nimbox.pqc;

/**
 * <p>This interface defines a method that will be invoked each time a producer creates an item. Usually, the only
 * implementation of this interface will be <code>{@link ConsumerProducerEngine}</code>, which will enqueue the item
 * in the queue to allow the consumers to process it.</p>
 * <p>Of course, the producer can enqueue the item by itself, so it's an election...</p>
 *
 * @param <T> the item type
 *
 * @author nilo.gonzalez
 *
 * @see ConsumerProducerEngine
 */
public interface CreatedItemListener<T> {

  /**
   * <p>This method will be invoked each time the producer creates an item</p>
   * @param item the new item
   */
  void process( T item);
}
