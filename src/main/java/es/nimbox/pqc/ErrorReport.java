package es.nimbox.pqc;

class ErrorReport<T> {
  private long threadId;
  private long timestamp;

  private T item;
  private Exception ex;

  public ErrorReport( T item, Exception ex) {
    this.threadId = Thread.currentThread().getId();
    this.timestamp = System.currentTimeMillis();

    this.item = item;
    this.ex = ex;
  }

  public long getThreadId() {
    return threadId;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public T getItem() {
    return item;
  }

  public Exception getException() {
    return ex;
  }
}
