package es.nimbox.pqc;

/**
 * This interface defines the classes that will be notified when ever an item cannot
 * be processed by the consumers.
 *
 * @param <T> the item type
 *
 * @author nilo.gonzalez
 *
 */
public interface NimErrorListener<T> {

  /**
   * <p>This method is invoked on every error</p>
   * @param error the error
   */
  void processError( ErrorReport<T> error);
}
