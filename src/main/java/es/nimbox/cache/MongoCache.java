package es.nimbox.cache;

import java.io.IOException;

import org.bson.Document;

import com.mongodb.*;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.MongoClient;
import static com.mongodb.client.model.Filters.*;


/**
 * Esta clase implementa un ICache guardando los datos en <a href="http://www.mongodb.org/">MongoDB</a>.
 * El cache funciona de una manera similar a todos los demás, y aunque probablemente es el cache mas lento permite 
 * almacenar cantidades enormes de datos porque MongoDB es una base de datos NoSQL que escala hasta guardar grandes 
 * cantidades de datos.<br>
 * Por supuesto es necesario entender bien como trabajar MongoDB para sacar partido de esta clase. Por ejemplo, la
 * edicion de 32bits de MongoDB tiene un tamaño maximo de base de datos de 2GB.<br>
 * La clase necesita una conexion con MongoDB, y como es buena idea que en su aplicacion exista un unico pool de 
 * conexiones, una buena practica es crear el pool de conexiones en su aplicacion y despues pasarselo a la cache
 * con el metodo setMongoPool(), pero if MongoCache es el unico codigo de su aplicacion que usa MongoDB, puede olvidar
 * ese paso y permitir que sea MongoCache la cree el pool para su uso usando los metodos setParam: Por ejemplo:   
 * 
 *  * <pre>
   ICache ca = new MongoCache();
   
   // MongoDB esta en localhost en el puerto 2707 
   ca.setParam( MDB_SERVER, "localhost");
   ca.setParam( MDB_PORT, "2707");
    
   // Conexion 
   ca.open();
    
   // Unos cuantos puts
   for ( int i = 0; i &lt; num; i++) {
     String key = "gatoamarillo" + i;
     String value = i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante...";
    
     ca.put( key, value);
   }

   // Unos gets
   for ( int i = 0; i &lt; num; i++) {
     String key = "gatoamarillo" + i;
     String value = i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante...";
      
     res = ca.get( key);
     if ( !value.equals(res)) {
       System.out.println( "ERROORRRRR " + i);
     }
   }
   
   // Guardamos los datos en una coleccion llamada "elefantes" 
   ca.setParam( MDB_COLLECTION, "elefantes");
   ca.store();
   
   // Y cerramos
   ca.close();
 * </pre>
 * No es un uso muy diferente al de cualqueir otro ICache. La unica diferencia esta en los parametros de incializacion usados
 * en los metodos setParam. Esos parametros tienen valores por defecto, asi que es probable que no sea necesario fijarlos
 * todos.<br>
 * Estos son los valores por defecto:
 * <ul>
 *   <li>MDB_SERVER : <b>localhost</b></li>
 *   <li>MDB_PORT : <b>27017</b></li>
 *   <li>MDB_DB : <b>MONGO_CACHE</b></li>
 *   <li>MDB_COLLECTION : <b>MONGO_TEMP_CACHE</b></li>
 * </ul>
 * Estos parametros se conectaran a un servidor de MongoDB arrancado en localhost en el puerto por defecto, y crearan una base 
 * de datos llamada "MONGO_CACHE" y una coleccion llamada "MONGO_TEMP_CACHE". El cache guardara datos usando un campo llamado
 * "_id" como clave, y un campo llamado "CACHE_VALUE" para guardar los datos.<br>
 * Invocar el metodo close borrara la coleccion por defecto, "MONGO_TEMP_CACHE". Si desea mantener los datos para leerlos mas
 * tarde, debe dar un nombre a la coleccion e invocar al metodo store, como se ve en el ejemplo anterior. Para usar datos
 * guardados previamente, puede usar un codigo como este:  
 * <pre>
   ICache ca = new MongoCache();
   
   // Se conecta a un MongoDB en localhost:27017  
   // y carga la coleccion "elefantes" 
   ca.setParam( MDB_COLLECTION, "elefantes");
   
   // conexion
   ca.open();
   
   // unos gets
   for ( int i = 0; i &lt; num; i++) {
     String key = "gatoamarillo" + i;
     String value = i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante...";
      
     res = ca.get( key);
     if ( !value.equals(res)) {
       System.out.println( "ERRORORRRRR " + i);
     }
   }
   
   // Y cerrar
   ca.close();
 * </pre>
 * Esta vez no es necesario invocar al metodo store porque al no estar usando la coleccion por defecto el metodo close no lo
 * borrara al cerrase, y por otro lado.<br>  
 * MongoCache no implmenta el metodo load.
 * 
 * @author Nilo J. Gonzalez 2011
 *
 */
public class MongoCache extends AbstractCache {
  /**
   * The property to set the connection String to MongoDB server
   */
  public static final String MDB_URL = "MDB_URL";
  
  /**
   * The property to set the data base to store the data. If it isn't provided, will use MDB_DEFAULT_DB
   */
  public static final String MDB_DB = "MDB_DB";
  
  /**
   * The property to set the collection to store the data. If it isn't provided, will use MDB_DEFAULT_COLLECTION
   */
  public static final String MDB_COLLECTION = "MDB_COLLECTION";
  
  /**
   * Default URL (mongodb://localhost)
   */
  public static final String MDB_DEFAULT_URL = "mongodb://localhost";
  
  /**
   * Default database (NIMBOX_CACHE)
   */
  public static final String MDB_DEFAULT_DB = "NIMBOX_CACHE";
  
  /**
   * Default collection (NIMBOX_TEMP_CACHE)
   */
  public static final String MDB_DEFAULT_COLLECTION = "NIMBOX_TEMP_CACHE";
  
  /**
   * MongoDB default key. It's "_id".
   */
  public static final String MDB_KEY_VALUE = "_id";
  
  /**
   * Name for the payload field (CACHE_VALUE)
   */
  public static final String MDB_CACHE_VALUE = "CACHE_VALUE";
  
  

  /**
   * Connection to MongoDB
   */
  protected MongoClient mongoClient;
  
  /**
   * MongoDB data base
   */
  protected MongoDatabase mongoDb;
  
  /**
   * MongoDB collection 
   */
  protected MongoCollection<Document> mongoCol;
  
  /**
   * This variable indicates that the collection must be deleted in the end
   */
  protected boolean delAtEnd;
  
  /**
   * This variable indicates that the connection with MongoDB is created outside the object
   */
  protected boolean externalClient;
  
  
  
  /**
   * The default constructor
   */
  public MongoCache() {
    super();
    
    externalClient = false;
  }
  
  /**
   * This method assigns an external MongoDB connection
   * @param mongoClient the MongoDB connection
   * @see #load
   */
  public void setMongoConnection( MongoClient mongoClient) {
    this.mongoClient = mongoClient;
    externalClient = true;
  }
  
  /**
   * This method returns the connection with MongoDB 
   * @return the connection with MongoDB 
   */
  public MongoClient getMongoClient() {
    return mongoClient;
  }
  
  /**
   * Este metodo abre una conexion con MongoDB usando las propiedades:
   * <ul>
   *   <li>MDB_SERVER : el servidor de MongoDB. Por defecto, "localhost"</li>
   *   <li>MDB_PORT : el puerto. Por defecto, 27017</li>
   *   <li>MDB_DB : la base de datos. Por defecto, MDB_DEFAULT_DB.</li>
   *   <li>MDB_COLLECTION : la coleccion. Por defecto, MDB_DEFAULT_COLLECTION.</li>
   *   <li>MDB_USER : el usuario</li>
   *   <li>MDB_PASSWORD : el password</li>
   * </ul>
   * Si MDB_USER o MDB_PASSWORD no se fijan, se conecta sin autenticacion.<br>
   * Si MDB_COLLECTION no se fija, la coleccion se borrara en el metodo close.<br>
   * Si se ha invocado setMongoPool, MDB_SERVER y MDB_PORT se ignoran.<br>
   * El modo de escritura es <b>WriteConcern.SAFE</b>, pero si no sabe lo que es esto, no es importante
   * @see #close 
   * @throws IOException when shit happens
   */
  @Override
  public void open() throws IOException {
    String mongoURL = MDB_DEFAULT_URL;
    String dbname = MDB_DEFAULT_DB;
    String colName = MDB_DEFAULT_COLLECTION;
    
    // By default, delete the cache in the end
    delAtEnd = true;
    
    if ( props.getProperty( MDB_URL) != null ) {
      mongoURL = props.getProperty( MDB_URL);
    }
    
    if ( props.getProperty( MDB_DB) != null ) {
      dbname = props.getProperty( MDB_DB);
    }
    
    if ( props.getProperty( MDB_COLLECTION) != null ) {
      colName = props.getProperty( MDB_COLLECTION);
      
      // External cache, so don't delete it
      delAtEnd = false;
    }
    
    if ( !externalClient ) {
      mongoClient = MongoClients.create( mongoURL);
    }
    
    mongoDb = mongoClient.getDatabase( dbname).withWriteConcern( new WriteConcern( 1));

    mongoCol = mongoDb.getCollection( colName);
  }



  /**
   * This method closes the connection. If it's using its own connection, closes the connection. If it's using the default
   * collection, deletes the collection.
   * @see #store
   * @throws IOException when something goes wrong
   */
  @Override
  public void close() throws IOException {
    if ( delAtEnd ) {
      mongoCol.drop();
    }
    
    if ( !externalClient ) {
      mongoClient.close();
    }
  }
  
  @Override
  public void put( String key, String value) throws IOException {
    Document doc = new Document( key, value);
    
    mongoCol.updateOne( eq( MDB_KEY_VALUE, key), 
                        doc, 
                        new UpdateOptions().upsert( true));
  }
  
  @Override
  public synchronized void put( String key, byte[] value) throws IOException {
    put( key, new String( value));
  }

  @Override
  public String get( String key) throws IOException {
    Document res = mongoCol.find( eq( MDB_KEY_VALUE, key)).first();
    
    if ( res == null ) {
      return null;
    }
    
    return res.getString( MDB_KEY_VALUE);
  }

  public byte[] getBytes( String key) throws IOException {
    String res = get( key);
    
    if ( res == null ) {
      return null;
    }
    
    return res.getBytes();
  }
  
  @Override
  public String remove( String key) throws IOException {
    String res = get( key);
    
    if ( res != null ) {
      mongoCol.deleteOne( eq( MDB_KEY_VALUE, key));
    }
    
    return res;
  }

  @Override
  public boolean containsKey( String key) throws IOException {
    return null != get(key);
  }

  @Override
  public long size() throws IOException {
    return mongoCol.countDocuments();
  }
}
