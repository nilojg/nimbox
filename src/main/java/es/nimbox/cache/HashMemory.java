package es.nimbox.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is only a wrapper around a <code>HashMap</code>. Why this class exists? Just to allow you to create an
 * <code>ICache</code> object and use an old <code>HashMap</code> with it without changing your code.
 * 
 * @author Nilo J. Gonzalez 2011
 *
 */
public class HashMemory extends AbstractCache {
  /**
   * This constant set the preferred size of the cache. The final size can be very different, but the cache will be
   * Optimized for this size. The default value is <code>HF_DEFAULT_SIZE</code>, 1000000.
   */
  public static final String HM_SIZE       = "HM_SIZE";
  
  /**
   * Default cache size
   */
  public static final int HF_DEFAULT_SIZE  = 1000000;
  
  private Map<String,String> hTable;
  
  /**
   * Default constructor
   */
  public HashMemory() {
    super();
    
    props.setProperty( HashMemory.HM_SIZE, HF_DEFAULT_SIZE + "");
  }

  /**
   * This constructor creates a cache optimized for an inital value of <code>size</code> 
   * @param size the initial size
   */
  public HashMemory( int size) {
    super();
    
    props.setProperty( HashMemory.HM_SIZE, Integer.toString( size));
  }
  
  /**
   * Creates the cache
   */
  public void open() {
    int size = Integer.parseInt( props.getProperty( HM_SIZE));
    
    hTable = new HashMap<String,String>( size);
  }
  
  /**
   * Frees resources
   */
  public void close() {
    hTable.clear();
  }
  
  /**
   * Associates the specified value with the specified key in this cache 
   * @param key the key
   * @param value the value
   * @throws NullPointerException if <code>key</code> or <code>value</code> are<code>null</code>
   */
  @Override
  public void put( String key, String value)  {
    hTable.put( key, value);
  }
  
  /**
   * Associates the specified value with the specified key in this cache 
   * @param key the key
   * @param value the value
   * @throws NullPointerException if <code>key</code> or <code>value</code> are<code>null</code>
   */
  @Override
  public synchronized void put( String key, byte[] value) {
    put( key, new String( value));
  }

  /**
   * Returns the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key.
   * @param key the key
   * @return the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public String get( String key)  {
    return hTable.get( key);
  }
  
  /**
   * Returns the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key.
   * @param key the key
   * @return the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public byte[] getBytes( String key) {
    return get( key).getBytes();
  }

  /**
   * Removes the mapping for a key from this map if it is present
   * @param key key whose mapping is to be removed
   * @return the previous value associated with key, or <code>null</code> if there was no mapping for key.
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public String remove( String key)  {
    return hTable.remove( key);
  }

  /**
   * Returns <code>true</code> if this cache contains the <code>key</code> or <code>false</code> otherwise
   * @return <code>true</code> if this cache contains the <code>key</code> or <code>false</code> otherwise
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public boolean containsKey(String key) {
    return hTable.containsKey( key);
  }

  /**
   * This method returns the number of stored records
   * @return the number of stored records
   */
  @Override
  public long size() {
    return hTable.size();
  }
}
