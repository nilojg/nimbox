package es.nimbox.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Properties;

import es.nimbox.box.Utils;

/**
 * This class implements some methods from <code>ICache</code> to simplify the creation of caches. This class
 * handles the configuration stuff and empty implementations of <code>store</code> and <code>load</code>, that
 * only must be implemented by persistent caches.  
 * 
 * @author Nilo J. Gonzalez 2011
 *
 */
public abstract class AbstractCache implements ICache {
  
  /**
   * This stores the configuration values
   */
  protected Properties props;
  
  /**
   * Default constructor
   */
  protected AbstractCache() {
    props = new Properties();
  }

  /**
   * This method sets a parameter for the initialization of the cache
   * @param param the name of the parameter
   * @param value the parameter value
   */
  @Override
  public void setParam( String param, String value) {
    props.setProperty( param, value);
  }

  /**
   * Returns the value for a configuration parameter  
   * @param param the name of the parameter
   * @return the value for a configuration parameter
   */
  @Override
  public String getParam( String param) {
    return props.getProperty( param);
  }
  
  /**
   * This method loads all the parameters from a property file.
   * @param fProps the property file
   * @throws IOException when shit happens
   */
  public void addParamsFromFile( String fProps) throws IOException {
    InputStream fis = Utils.findFile( fProps);
    props.load( fis);
    fis.close();
  }
  
  /**
   * This method loads all the parameters from a <code>Properties</code> object.
   * @param paramProps the properties
   */
  public void setParameters( Properties paramProps) {
    for ( String key : paramProps.stringPropertyNames() ) {
      String val = paramProps.getProperty( key);
      
      setParam( key, val);
    }
  }
  
  /**
   * This method show in standard output all the stored values. For debug...
   */
  public void printParams() {
    printParams( System.out);
  }
  
  /**
   * This method writes in <code>out</code> all the stored values. For debug...
   * @param out the output
   */
  public void printParams( PrintStream out) {
    for ( String key : props.stringPropertyNames()) {
      String val = props.getProperty( key);
      
      out.println( "[" + key + "]=[" + val + "]");
    }
  }
  
  /**
   * Empty
   * @throws IOException when shit happens
   */
  @Override
  public void store() throws IOException {
  }

  /**
   * Empty
   * @throws IOException when shit happens
   */
  @Override
  public void load() throws IOException {
  }
}
