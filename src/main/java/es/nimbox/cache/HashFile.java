
package es.nimbox.cache;

import java.io.*;
import java.util.*;


/**
 * <p>This class implements an <code>ICache</code> that stores the key in a memory <code>Map</code> and the values
 * in a random access file. The target of this class is to store big tables with big values. The memory footprint
 * is supposed to be little, because only the keys are stored in RAM, while the size of the data is limited
 * only by the file system limits. In the bad, the access is slower that accessing to a pure-memory-Map, but it
 * should be faster than accessing the typical alternatives, like MongoDB or a relational database.
 * 
 * <p>It can be used almost like a regular <code>Map</code> </p>
 * <pre>
    // Creates the HashFile, optimized for 1000000 records, with values stored in 5 files in '/tmp' directory
    HashFile hf = new HashFile( 1000000, 5, "/tmp");
    
    // Initialize
    hf.open();

    // Add some records
    for ( int i = 0; i &lt; 1000000; i++) {
      hf.put( "gato" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }

    // Read some records
    for ( int i = 0; i &lt; 1000000; i++) {
      System.out.println( hf.get( "gato" + i));
    }
    
    // Close everything and delete files
    hf.close();
 * </pre>
 * <p>But perhaps the best way to use <code>HashFile</code> is to use it like an <code>ICache</code> implementation. </p>
   <pre>
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, "1000000");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, "/tmp");
      
    cache.open();
    
    for ( int i = 0; i &lt; 100000; i++) {
      cache.put( "KEY" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }
    
    for ( int i = 0; i &lt; 100000; i++) {
      System.out.println( cache.get( "KEY" + i));
    }
    
    cache.close();
   </pre>
 * <p>The performance of this implementation depends on the stored data. If the data size is about dozens of bytes, it's
 * quite fast, but if the data are about dozens of KBs, there's a lot of time transforming data between String type
 * to byte[] and back, which is a quite slow transformation for long strings. If it's possible, work with
 * byte[], but if your application works with String, which is the common case, doesn't mind where the conversion
 * is done, because sooner or later the data must be converted 
 *   
 * <p>This implementation of <code>ICache</code> can be persisted to be used again and again, like a (very) limited
 * NoSQL database.
 * <p>Add a name for the files passing the <code>HashFile.HF_NAME</code> parameter and call the <code>store</code> method
 * to persist the data. The next times, use the <code>load</code> method to read the previously stored:</p>
 * <pre>
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, "1000000");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, "/tmp");
    <b>cache.setParam( HashFile.HF_NAME, "data");</b>  // Set a name...
    
    cache.open();
    
    for ( int i = 0; i &lt; 100000; i++) {
      cache.put( "KEY" + i, "El gato naranja miraba atentamente como " + i + " elefantes se balanceaban bajo la tela de una araña, y como veian que no se caian fueron a llamar a otro elefante");
    }
    
    <b>cache.store();</b>
    cache.close();
 * </pre>
 * <p>This time the <code>close</code> method will not delete the data files and will create a <b>.hdifx</b> file with the index.
 * You can move the files (all files, like a group) to any other directory if you want. Then, use the files like this:</p>
 * <pre>
    ICache cache = new HashFile();
    cache.setParam( HashFile.HF_SIZE, "1000000");
    cache.setParam( HashFile.HF_NUMFILES, "5");
    cache.setParam( HashFile.HF_PATHBASE, "/tmp");
    cache.setParam( HashFile.HF_NAME, "data");
    
    cache.open();
    <b>cache.load();</b>
    
    for ( int i = 0; i &lt; 100000; i++) {
      System.out.println( cache.get( "KEY" + i));
    }
    
    <b>cache.store();</b>  // It's a good idea to store the files in the end because you could change some data 
    cache.close();
 * </pre>
 *  
 *
 * 
 * @author Nilo J. Gonzalez 2011
 *
 */
public class HashFile extends AbstractCache {
  /**
   * This constant set the preferred size of the cache. The final size can be very different, but the cache will be
   * Optimized for this size. The default value is <code>HF_DEFAULT_SIZE</code>, 1000000.
   */
  public static final String HF_SIZE = "HF_SIZE";
  
  /**
   * This component set the number of data files. The default value is 1
   */
  public static final String HF_NUMFILES = "HF_NUMFILES";
  
  /**
   * This constant defines the directory for the data files. The default value is ".", the working directory
   */
  public static final String HF_PATHBASE = "HF_PATHBASE";
  
  /**
   * This constant defines the name of the data files. If this property is defined, the cache is persistent. The default
   * value is <code>HF_DEFAULT_NAME</code>
   */
  public static final String HF_NAME = "HF_NAME";
  
  /**
   * Default file name
   */
  public static final String HF_DEFAULT_NAME = "HashFile";
  
  /**
   * Default file size
   */
  public static final int    HF_DEFAULT_SIZE       = 1000000;
  
  /**
   * Default index file extension
   */
  public static final String HF_INDEX_EXTENSION    = "hfidx";
  
  /**
   * Default data file extension 
   */
  public static final String HF_DATA_EXTENSION     = "data";
  
  /**
   * This class keeps each record data
   * 
   * @author nilo.gonzalez
   */
  private class Data {
    public long offset;
    public int size;
    public int fileIndex;
    
    public Data( long offset, int size, int fileIndex) {
      this.offset = offset;
      this.size = size;
      this.fileIndex = fileIndex;
    }
    
    public Data( String cad) {
      offset = Long.parseLong( cad.substring( cad.indexOf( ":")+1, cad.indexOf( ";")));
      size = Integer.parseInt( cad.substring( cad.indexOf( ";")+1, cad.indexOf( "_")));
      fileIndex = Integer.parseInt( cad.substring( cad.indexOf( "_")+1));
    }
    
    public String toString() {
      return ":" + offset + ";" + size + "_" + fileIndex;
    }
  }
  
  /**
   * This class keeps information of the data files
   * @author nilo.gonzalez
   */
  private class DataFile {
    public RandomAccessFile raf;
    public long seek;
    
    public DataFile( RandomAccessFile file, long seek) {
      this.raf = file;
      this.seek = seek;
    }
  }
  
  /**
   * This <code>List</code> stores the data files. Scheduled with RoundRobin
   */
  private List<DataFile> vRaf;
  
  /**
   * The index to access to vRaf
   */
  private int index;
  
  /**
   * This <code>Map</code> store the keys
   */
  private Map<String,Data> htKeys;

  /**
   * This <code>List</code> stores the data files. Scheduled with RoundRobin
   */
  protected List<File> vFich;
  
  /**
   * To save time in the <code>store</code> and only store if something has changed
   */
  private boolean updated;
  
  /**
   * The index file. Can be <code>null</code> if the cache isn't persistent.
   */
  private File fIdx;
  
  /**
   * Creates a cache with the default values
   * <ul>
   *   <li>HF_SIZE = 1000000</li>
   *   <li>HF_NUMFILES = 1</li>
   *   <li>HF_PATHBASE = .</li>
   * </ul>
   */
  public HashFile() {
    super();
    
    props.setProperty( HashFile.HF_SIZE, HF_DEFAULT_SIZE + "");
    props.setProperty( HashFile.HF_NUMFILES, "1");
    props.setProperty( HashFile.HF_PATHBASE, ".");
    props.setProperty( HashFile.HF_NAME, HF_DEFAULT_NAME);
    
    updated = false;
    fIdx = null;
  }
  
  /**
   * Creates a cache with the parameters data:
   * * <ul>
   *   <li>HF_SIZE = size</li>
   *   <li>HF_NUMFILES = numFiles</li>
   *   <li>HF_PATHBASE = pathBase</li>
   * </ul>
   * @param size the optimized size of the table
   * @param numFiles number of data files
   * @param pathBase directory for the data files
   */
  public HashFile( int size, int numFiles, String pathBase) {
    this();
    
    props.setProperty( HashFile.HF_SIZE, String.valueOf( size));
    props.setProperty( HashFile.HF_NUMFILES, String.valueOf( numFiles));
    props.setProperty( HashFile.HF_PATHBASE, pathBase);
  }

  /**
   * This method creates all the required files and starts a new cache. It uses the data set with the <code>setParam</code>
   * method.  
   * @throws IOException if there are any problem with the data files
   */
  @Override
  public void open() throws IOException {
    int size = Integer.parseInt( props.getProperty( HF_SIZE));
    int numFiles = Integer.parseInt( props.getProperty( HF_NUMFILES));
    String pathBase = props.getProperty( HF_PATHBASE);
    String hfName = props.getProperty( HF_NAME);
    
    htKeys = new HashMap<String, Data>( size);
    
    vFich = new ArrayList<File>();
    vRaf = new ArrayList<DataFile>();
    index = 0;
    
    if ( !pathBase.endsWith( File.separator) ) {
      pathBase += File.separator;
    }
    
    for ( int i = 0; i < numFiles; i++) {
      String name = pathBase + hfName + "." + new Date().getTime() + "." + i + "." + HF_DATA_EXTENSION;
      File fich = new File( name);
    
      fich.createNewFile();   //NOSONAR    because if the file already exists, the will be re-used
      RandomAccessFile raf = new RandomAccessFile( fich, "rw");    // NOSONAR    because this file MUST NOT be closed here, but in the this.close() method
      
      vFich.add( fich);
      vRaf.add( new DataFile( raf, raf.length()));
    } 
  }
  
  /**
   * Frees resources and deletes data files if the data files name isn't set
   * @throws IOException when shit happens
   */
  @Override
  public void close() throws IOException {
    htKeys.clear();
    
    for ( DataFile raf : vRaf ) {
      raf.raf.close();
    }
    if ( fIdx == null ) {
      for ( File fich : vFich ) {
        fich.delete();    // NOSONAR    because if the file cannot be closed... well, what can be done?
      }
    }
    
    vRaf.clear();
    vFich.clear();
    fIdx = null;
  }
  
  
  /**
   * Persists the contents of the cache in files with the name set with the parameter <code>HashFile.HF_NAME</code>
   * @throws IOException if the <code>HashFile.HF_NAME</code> parameter hasn't been set or when shit happens
   * @see #load()
   */
  @Override
  public void store() throws IOException {
    String pathBase = props.getProperty( HF_PATHBASE);
    String hfName = props.getProperty( HF_NAME);
    
    if ( !pathBase.endsWith( File.separator) ) {
      pathBase += File.separator;
    }
        
    if ( hfName.equals( HF_DEFAULT_NAME) ) {
      throw new IOException( "Property HashFile.HF_NAME hasn't been set");
    }
    
    // Si no se le ha dado nombre o no se ha actualizado, se deja como esta y nos vamos
    if ( !updated ) {
      return;
    }
    
    fIdx = new File( pathBase + hfName + "." + HF_INDEX_EXTENSION);
    try ( FileOutputStream bos = new FileOutputStream( fIdx); ) {
    
      bos.write( (Integer.toString( htKeys.size()) + "\n").getBytes());
      bos.write( (Integer.toString( vFich.size()) + "\n").getBytes());
      
      for ( File fich : vFich ) {
        bos.write( (fich.getName() + "\n").getBytes());
      }
      
      synchronized ( htKeys) {
        for ( String key : htKeys.keySet()) {
          Data val = htKeys.get( key);
          bos.write( (key + val + "\n").getBytes());
        }
      }
    }
    catch ( Exception ex) {
      if ( ex instanceof IOException ) {
        throw ex;
      }
      else {
        throw new IOException( "CANNOT store the cache : " + ex.getMessage(), ex);
      }
    }
    
    updated = false;
  }
  
  /**
   * Deletes the stored values in the cache and loads all the data from the previously created cache which
   * name is <code>indexFile</code>. This method is just a shortcut that sets the <code>HashFile.HF_NAME</code> parameter 
   * and calls the <code>load</code> method
   * @param indexFile the name of the cache to load
   * @throws IOException IOException if the <code>HashFile.HF_NAME</code> parameter hasn't been set or when shit happens
   * @see #load()
   * @see #store()
   */
  public void load( String indexFile) throws IOException {
    props.setProperty( HF_NAME, indexFile);
    
    load();
  }
  
  /**
   * Deletes the stored values in the cache and loads all the data from the previously created cache which
   * name has been set with the parameter <code>HashFile.HF_NAME</code>.
   * @throws IOException if the <code>HashFile.HF_NAME</code> parameter hasn't been set or when shit happens
   * @see #store()
   */
  @Override
  public void load() throws IOException {
    String pathBase = props.getProperty( HF_PATHBASE);
    String hfName = props.getProperty( HF_NAME);
    
    if ( !pathBase.endsWith( File.separator) ) {
      pathBase += File.separator;
    }
        
    // Si no se le ha dado nombre, lanzamos una excepcion, para que la proxima vez nos diga que hay que cargar
    if ( hfName.equals(HF_DEFAULT_NAME) ) {
      throw new IOException( "Property HashFile.HF_NAME hasn't been set");
    }
    
    // Primero, hacemos limpieza de lo que haya
    close();
    
    // Ahora hay que leer el fichero de indice
    fIdx = new File( pathBase + hfName + "." + HF_INDEX_EXTENSION);
    
    try ( LineNumberReader lnr = new LineNumberReader( new FileReader( fIdx)); ) {
      String line = lnr.readLine();
      int numKeys = Integer.parseInt( line);
      
      line = lnr.readLine();
      int numFiles = Integer.parseInt( line);
      
      // Cargamos los ficheros
      for ( int i = 0; i < numFiles; i++) {
        line = lnr.readLine();
        
        File fich = new File( pathBase + line);
        RandomAccessFile raf = new RandomAccessFile( fich, "rw");   // NOSONAR    because this file MUST NOT be closed here, but in this.close()
        
        vFich.add( fich);
        vRaf.add( new DataFile( raf, raf.length()));
      }
      
      // Cargamos las claves
      for ( int i = 0; i < numKeys; i++) {
        line = lnr.readLine();
        String key = line.substring( 0, line.indexOf( ":"));
        
        Data obj = new Data( line);
        htKeys.put( key, obj);
      }
      
      lnr.close();
    }
    catch ( Exception ex) {
      if ( ex instanceof IOException ) {
        throw ex;
      }
      else {
        throw new IOException( "CANNOT load the cache : " + ex.getMessage(), ex);
      }
    }
    
    updated = false;
  }
  
  
  
  /**
   * this method returns the next RAF. If reaches the max value, starts in zero again 
   * @return the next RAF
   */
  protected int nextRAF() {
    if ( (++index) == vRaf.size() ) {
      index = 0;
    }
    
    return index;
  }
  
  /**
   * Associates the specified value with the specified key in this cache 
   * @param key the key
   * @param value the value
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> or <code>value</code> are<code>null</code>
   */
  @Override
  public synchronized void put( String key, byte[] value) throws IOException {
    int ix = nextRAF();
    DataFile raf = vRaf.get( ix);
    
    // Escribimos en el fichero (si casca, que sea aqui, antes de meterlo en la tabla)
    raf.raf.seek( raf.seek);
    raf.raf.write( value);
    
    // Y ahora metemos en la tabla de indices
    Data obj = new Data( raf.seek, value.length, ix);
    htKeys.put( key, obj);
    
    // Actualizamos el valor del offset al final
    raf.seek += value.length;
    
    updated = true;
  }
  
  /**
   * Associates the specified value with the specified key in this cache
   * @param key the key
   * @param value the value
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> or <code>value</code> are<code>null</code>
   */
  @Override
  public void put( String key, String value) throws IOException {
    put( key, value.getBytes());
  }
  
  /**
   * Returns the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key.
   * @param key the key
   * @return the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public String get( String key) throws IOException {
    byte bGetBuf[] = getBytes( key);
    
    if ( bGetBuf == null ) {
      return null;
    }
    
    return new String( bGetBuf);
  }
  
  /**
   * Returns the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key.
   * @param key the key
   * @return the value to which the specified key is mapped, or <code>null</code> if this map contains no mapping for the key
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public synchronized byte[] getBytes( String key) throws IOException {
    Data obj = htKeys.get( key);
    
    if ( obj == null ) {  // si no esta, pues no esta...
      return null;
    }
    
    byte bGetBuf[] = new byte[obj.size];
    
    DataFile fich = vRaf.get( obj.fileIndex);
    
    // ponemos el puntero en su sitio
    fich.raf.seek( obj.offset);
    fich.raf.read( bGetBuf, 0, obj.size);
    
    return bGetBuf;
  }
  
  /**
   * Removes the mapping for a key from this map if it is present
   * @param key key whose mapping is to be removed
   * @return the previous value associated with key, or <code>null</code> if there was no mapping for key.
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public String remove( String key) throws IOException {
    String value = get( key);
    
    synchronized ( htKeys) {
      htKeys.remove( key);
    }
    
    updated = true;
    
    return value;
  }
  
  /**
   * Returns <code>true</code> if this cache contains the <code>key</code> or <code>false</code> otherwise
   * @return <code>true</code> if this cache contains the <code>key</code> or <code>false</code> otherwise
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  @Override
  public boolean containsKey( String key) {
    return htKeys.containsKey( key);
  }
  
  /**
   * This method returns the number of stored records
   * @return the number of stored records
   * @throws IOException when shit happens
   */
  @Override
  public long size() throws IOException {
    return htKeys.size();
  }
  
  /**
   * This method returns all the keys from the cache. Be careful with what you do with it because you could
   * raise a ConcurrentModificationException in a multithread environment.
   * @return all the keys from the cache
   */
  public Set<String> keySet() {
    return htKeys.keySet();
  }
  
  /**
   * This method returns the size of all the stored files. Keep in mind that this may include the size of all
   * the deleted and modified records, because to increase performance when a record is deleted or unpated the old
   * value is left in the file and a new one is added to the end of one of the files 
   * @return the size of all the files
   * @throws IOException when shit happens
   */
  public long sizeData() throws IOException {
    long acc = 0;
    for ( DataFile raf : vRaf ) {
      acc += raf.raf.length();
    }
    return acc;
  }  
  
  /**
   * This method returns the list of all the created files by the cache.
   * @return the list of all the created files by the cache
   */
  public File[] getFiles() {
    File[] res = vFich.toArray( new File[vFich.size() + 1]);
    
    res[vFich.size()] = getIndexFile();
    
    return res;
  }
  
  /**
   * This method returns the index file
   * @return the index file
   */
  public File getIndexFile() {
    return fIdx;
  }
}

