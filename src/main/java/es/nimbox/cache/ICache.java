
package es.nimbox.cache;

import java.io.IOException;

/**
 * <p>This interface defines the methods that must implement a cache, a class that stores data in a key-value
 * format, being the key an String and the value an String or a byte array. It's like the regular Map&lt;String,String&gt;
 * but its implementations will store the data in memory, a file or any other place, like MongoDB, for example. Or
 * even they could use two devices, for example storing the keys in memory and the data in the file system.
 * 
 * <p>The <code>ICache</code> doesn't implements the <code>Map</code> interface because it tries to be very simple,
 * just a subset of all the possible operations to avoid asking something that a device cannot do. 
 * 
 * <p>The main goal of this interface and its implementations is have the <code>Map</code> functionality without the
 * limits of the memory. For example, if in the key-value pair the value has a big size, for example 1 or 2 MB, and 
 * you want to store on e million records the mount of memory you need is... a lot. With this you just have to 
 * choose between all the implementations, each one with advantages and disadvantages, the implementation that better
 * suits your needs.   
 * 
 * <p>Like the device used to store the data can be optimized to get byte[] or String, there are two methods, one 
 * using String and another one using byte[], and the implementations must handle the transformations and, ideally, 
 * documenting the faster choice.
 * 
 * <p>To simplify, the thrown exceptions are always <code>IOException</code>. If an implementation stores data in a 
 * database the possible <code>SQLException</code> will be transformed in <code>IOException</code>.
 * 
 * <p>There's a <code>setParam</code> method to allow the configuration of the store before creating the cache, because
 * each store device will have different needs, for example, a cache that stores data in a file will need a
 * file name, and a cache that stores data in a database will need a JDBC URL, a driver, an user and a password to
 * connect with the database. Each implementation of the <code>ICache</code> must create (and document) the parameter 
 * keys and their meaning needed by the cache. Like there could be big differences between the different implementations
 * in the name and even the number of possible properties, using the methods of the <code>Reflect</code> class could
 * help creating the object.
 * 
 *<p> Another advantage of <code>ICache</code> is that most of the implementations will be persistent between executions.
 * I mean, if you create a cache in the file system, the cache will be there while the file is there, doesn't mind if
 * you kill the JVM.  
 * 
 * <p>A typical use of a persistent cache:
  <pre>
  ICache cache = ....
  cache.setParam( "PATH", "/tmp/");
  cache.setParam( "NAME", "Tmp.Cache");
  
  cache.open();
  
  for ( int i = 0; i &lt; 100000; i++) {
    cache.put( "KEY" + i, "un gato amarillo miraba por la ventana");
  }
  
  cache.store();
  
  cache.close();
  </pre>
 * <p>Now you can use this code to get the results:
  <pre>
  ICache cache = ....
  cache.setParam( "PATH", "/tmp/");
  cache.setParam( "NAME", "Tmp.Cache");
  
  cache.load();
  cache.open();
  
  for ( int i = 0; i &lt; 100000; i++) {
    System.out.println( cache.get( "KEY" + i));
  }
  
  cache.store();
  
  cache.close();
  </pre>
 * 
 * <p>Important: <b>not all the implementations must implement all the methods</b>. This is for the methods <code>load</code> and
 * <code>store</code>. These methods... well, they load the data in the creation of the cache and store in the destroy of the cache,
 * which it's very important if the device is a file, but is nonsense if the device is MongoDB and you persist the data in the
 * <code>put</code> method.   </p>
 * 
 * @author Nilo J. Gonzalez 2011
 * @see es.nimbox.box.Reflect
 */

public interface ICache {
  /**
   * Adds the pair key-value to the cache
   * @param key the key
   * @param value the value
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> or <code>value</code> are <code>null</code>
   */
  public void put( String key, String value) throws IOException;
  
  /**
   * Adds the pair key-value to the cache
   * @param key the key
   * @param value the value
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> or <code>value</code> are <code>null</code>
   */
  public void put( String key, byte[] value) throws IOException;

  /**
   * Returns the value for the <code>key</code>, or <code>null</code> if there isn't a value for <code>key</code>
   * @param key the key
   * @return the value or <code>null</code> if there isn't a value for <code>key</code>
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  public String get( String key) throws IOException;
  
  /**
   * Returns the value for the <code>key</code>, or <code>null</code> if there isn't a value for <code>key</code>
   * @param key the key
   * @return the value or <code>null</code> if there isn't a value for <code>key</code>
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  public byte[] getBytes( String key) throws IOException;

  /**
   * Removes the value for the <code>key</code> and returns the deleted value or <code>null</code> if there isn't a value for <code>key</code>
   * @param key the key
   * @return the value or <code>null</code> if there isn't a value for <code>key</code>
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  public String remove( String key) throws IOException;
  
  /**
   * This method returns <code>true</code> if the <code>key</code> exists in the cache and <code>false</code> otherwise
   * @param key the key 
   * @return if the <code>key</code> exits in the cache and <code>false</code> otherwise
   * @throws IOException when shit happens
   * @throws NullPointerException if <code>key</code> is <code>null</code>
   */
  public boolean containsKey( String key) throws IOException;
  
  /**
   * Returns the number of stored objects in the cache
   * @return the number of stored objects in the cache
   * @throws IOException when shit happens
   */
  public long size() throws IOException;
  
  /**
   * Creates the cache. 
   * @throws IOException when shit happens
   */
  public void open() throws IOException;
  
  /**
   * Closes the cache and frees resources. If the cache is persistent, if it keeps values between executions, this method 
   * persists the values in the device  
   * @throws IOException when shit happens
   */
  public void close() throws IOException;
  
  /**
   * This method sets a parameter for the initialization of the cache
   * @param param the name of the parameter
   * @param value the parameter value
   */
  public void setParam( String param, String value);
  
  /**
   * Returns the value for a configuration parameter  
   * @param param the name of the parameter
   * @return the value for a configuration parameter
   */
  public String getParam( String param);
  
  /**
   * If the cache is persistent, if it keeps values between executions, this method 
   * persists the values in the device  
   * @throws IOException when shit happens
   */
  public void store() throws IOException; 
  
  /**
   * If the cache is persistent, if it keeps values between executions, this method loads all the persisted
   * values  
   * @throws IOException when shit happens
   */
  public void load() throws IOException; 

}