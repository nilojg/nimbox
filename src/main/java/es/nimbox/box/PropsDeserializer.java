package es.nimbox.box;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

class PropsDeserializer extends StdDeserializer<Props> {
  private static final long serialVersionUID = 1L;

  public PropsDeserializer() {
    super( Props.class);
  }

  @Override
  public Props deserialize( JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    ObjectCodec oc = p.getCodec();
    JsonNode node = oc.readTree( p);
    
    return Props.fromJSON( node.toString());
  }
  
}