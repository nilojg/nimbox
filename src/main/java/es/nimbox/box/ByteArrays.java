package es.nimbox.box;

/**
 * This class contains some helper methods to handle <code>byte[]</code>, like concatenation or getting sub-arrays.
 * 
 * 
 * @author nilo.gonzalez
 *
 */
public class ByteArrays {
  
  private ByteArrays() {}

	/**
	 * This method returns a <code>byte[]</code> which is the result of concatenating <code>a</code> and <code>b</code>.  If
	 * <code>a</code> or <code>b</code> are <code>null</code>, an empty array is used.
	 * @param a the first <code>byte[]</code>
	 * @param b the second <code>byte[]</code>
	 * @return the concatenation of <code>a</code> and <code>b</code>
	 */
  public static byte[] concat( byte[] a, byte[] b) {
    int al = a != null ? a.length : 0;
    int bl = b != null ? b.length : 0;
    
    byte res[] = new byte[ al + bl ];
    
    if ( a != null ) {
      System.arraycopy( a, 0, res, 0, a.length);
    }
    
    if ( b != null ) {
      System.arraycopy( b, 0, res, al, bl);
    }
    
    return res;
  }
  
  /**
   * This method returns a <code>byte[]</code> with the contents of the array <code>arr</code> from <code>from</code> until 
   * the end of the array
   * @param arr the array
   * @param from the index
   * @return a <code>byte[]</code> with the contents of the array <code>arr</code> from <code>from</code> until 
   * the end of the array
   * @throws IndexOutOfBoundsException if <code>from</code> is out the bounds of the array
   * @throws NegativeArraySizeException if <code>from</code> is out the bounds of the array
   */
  public static byte[] subarray( byte arr[], int from) {
    if ( arr == null ) {
      return null;
    }
    
  	return subarray( arr, from, arr.length - from);
  }
  
  /**
   * This method returns a <code>byte[]</code> with <code>len</code> elements of the array <code>arr</code> from <code>from</code>.
   * If <code>from</code>+<code>len</code> is bigger than the length of <code>arr</code>, the resulting <code>byte[]</code> 
   * contains the elements until the end of <code>arr</code>.
   * @param arr the array
   * @param from the index
   * @param len the number of bytes of the resulting array
   * @return a <code>byte[]</code> with <code>len</code> elements of the array <code>arr</code> from <code>from</code>
   * @throws IndexOutOfBoundsException if <code>from</code> is out the bounds of the array
   * @throws NegativeArraySizeException if <code>from</code> is out the bounds of the array
   */
  public static byte[] subarray( byte arr[], int from, int len) {
    if ( arr == null ) {
      return null;
    }
    
    int max = from + len < arr.length ? len : arr.length - from;
  	byte res[] = new byte[max];
  	
  	for ( int i = 0; i < res.length && i+from < arr.length; i++) {
  		res[i] = arr[i+from];
  	}
  	
  	return res;
  }

  /**
   * This method fills the array <code>arr</code> with the <code>value</code>
   * @param arr the <code>byte[]</code>
   * @param value the value
   * @return the array <code>arr</code> filled with <code>value</code>
   */
  public static byte[] fill( byte arr[], byte value) {
  	return fill( arr, value, 0);
  }
  
  /**
   * This method fills the array <code>arr</code> with the <code>value</code> parting form the index <code>from</code>
   * @param arr the <code>byte[]</code>
   * @param value the value
   * @param from the initial index
   * @return the array <code>arr</code> filled with <code>value</code> parting form the index <code>from</code>
   */
  public static byte[] fill( byte arr[], byte value, int from) {
  	return fill( arr, value, from, arr.length - from);
  }
  
  /**
   * This method fills <code>len</code> elements of the array <code>arr</code> with the <code>value</code> parting 
   * form the index <code>from</code>
   * @param arr the <code>byte[]</code>
   * @param value the value
   * @param from the initial index
   * @param len the number of elements to fill
   * @return the array <code>arr</code> filled with <code>value</code> parting form the index <code>from</code>
   */
  public static byte[] fill( byte arr[], byte value, int from, int len) {
    if ( from < 0 || from >= arr.length ) {
      throw new IndexOutOfBoundsException( "Array length is '" + arr.length + "' and 'from' is " + from);
    }
    
  	for ( int i = from; i < from + len && i < arr.length; i++) {
  		arr[i] = value;
  	}
  	
  	return arr;
  }
  
  /**
   * This method returns the position of the first occurence of the subarray <code>token</code> in the array <code>arr</code>, 
   * or -1 if <code>token</code> cannot be found in <code>arr</code>
   * @param arr the array
   * @param token the token to be found
   * @return the position of the subarray <code>token</code> in the array <code>arr</code>, or -1 if <code>token</code>
   * cannot be found in <code>arr</code>
   */
  public static int indexOf( byte arr[], byte token[]) {
    return indexOf( arr, token, 0);
  }
  
  /**
   * This method returns the position of the first occurrence of the sub-array <code>token</code> in the array <code>arr</code>,
   * starting at <code>fromIndex</code>, or -1 if <code>token</code> cannot be found in <code>arr</code>
   * @param arr the array
   * @param token the token to be found
   * @param fromIndex the the index from which to start the search
   * @return the position of the subarray <code>token</code> in the array <code>arr</code>, or -1 if <code>token</code>
   * cannot be found in <code>arr</code>
   * @throws IndexOutOfBoundsException if <code>fromIndex</code> is negative
   */
  public static int indexOf( byte arr[], byte token[], int fromIndex) {
    int res = -1;
    
    for ( int i = fromIndex; i < arr.length; i++ ) {
      res = i;
      
      for ( int j = 0; j < token.length; j++) {
        if ( arr[i] != token[j] ) {
          break;
        }
        
        i++;
        
        if ( j == token.length - 1 ) {
          return res;
        }
      }
    }
    
    return -1;
  }
}
