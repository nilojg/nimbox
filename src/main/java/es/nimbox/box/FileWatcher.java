package es.nimbox.box;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * <p>This class implements a component that has an eye in the files you choose and throws alarms whenever those files
 * change.
 * Each FileWatcher instance can handle an, in theory, unlimited number of predefined alerts, but you know how these things are...</p>
 * 
 * <p>The FileWatcher creates a new background Thread that will wake up from time to time (the defect interval is 500 ms, but you can
 * change this), then it will take a look on all the files, check if they have changed, and will raise alarms if some file
 * has changed. The accurate of the timing depends on the size of the interval, so
 * if you need more precision than 500 ms set a more little value, but that will have some impact in CPU consumption.</p>
 * 
 * <p>When an alarm raises, the FileWatcher will notify the alarm to a list of listeners with the information of the raised alarm,
 * and the <code>FileWatcherEvent</code> event contains information about the changed file, with the old and the new values.
 * Then you can handle it as you prefer,
 * but it's quite important to test that the <code>alarmId</code> of the event is one of your alarms, specially if you are
 * using the default singleton FileWatcher. Will see something of this later in the examples.</p>  
 * 
 * <p>The listeners execute in the FileWatcher thread, so <b>they MUST be fast or they will freeze the FileWatcher thread</b>.</p> 
 * 
 * <p>Setting an alarm is quite easy, just create the FileWatcher, set a listener to handle the events and add the alarm. Let's see:</p>
 * <pre>
 * // create the FileWatcher. 
 * FileWatcher simple = new FileWatcher();
 * 
 * // optional. This sets a wake up interval of 1000 ms. Not very accurate, but cheap in CPU. And this is only an example...
 * simple.setThreadSleep( 1000);
 * 
 * // adding a listener to the FileWatcher that simply writes the event in standard output  
 * simple.addListener( new FileWatcherListener() {
 *   public void fileWatcherAlert( FileWatcherEvent ev) {
 *     System.out.println( ev);
 *   }      
 * });
 * 
 * // adding an alarm for a directory that will rise whenever the directory is created, deleted or a file is created or deleted
 * // inside it 
 * int alarmDir = simple.addFileToWatch( new File( "/tmp"), FileWatcher.CHANGE_DIR | FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
 * 
 * // adding an alarm for a file that will rise whenever the file is created, deleted or changed, using its size, modification time and
 * // MD5 hash to detect the changes  
 * int alarmFile = simple.addFileToWatch( new File( fileName), 
 *                                              FileWatcher.CHANGE_MD5 | FileWatcher.CHANGE_SIZE | FileWatcher.CHANGE_TIME | 
 *                                              FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
 * 
 * // start the FileWatcher thread
 * simple.startFileWatcher();
 * 
 * ... do other things
 * 
 * // this stops the FileWatcher thread at any moment
 * simple.stopFileWatcher();
 * </pre>
 * 
 * 
 * <p>It's easy to use, but there's a point about threads handling that is important to talk about. You can start the FileWatcher thread as any
 * other thread, using the <code>start</code> method, but in that case you MUST remember to stop the thread invoking <code>stopFileWatcher()</code> or 
 * your application will never end because there will be a running thread and the JVM will wait until the FileWatcher thread ends.</p>
 * <pre>
 * // start the FileWatcher thread as any other thread
 * simple.start();
 * 
 * ... do other things
 * 
 * // this is really important or your application will never end
 * simple.stopFileWatcher();
 * </pre>
 * 
 * <p>If you call the <code>startFileWatcher()</code> method the thread will finish with the JVM, so your application will end as you expect even if you
 * forget to invoke the <code>stopFileWatcher()</code> method. You can forget the FileWatcher thread except if you want to kill the FileWatcher
 * before the application ends. The easy way is to use <code>startFileWatcher()</code> and forget it.</p> 
 * 
 * <p>This is because the <code>startFileWatcher()</code> method calls <code>setDaemon( true)</code>, which tells the JVM "when you are going to die, 
 * kill this thread too" instead the default "don't die until this thread dies, wait until the end of times". You can use the <code>start</code>
 * method an invoke <code>setDaemon</code> by your own, but, why not to take the easy way, call <code>startFileWatcher()</code> and forget the 
 * whole thing?</p>
 *
 * <p>You can create as many FileWatcher instances as you want, but remember that each instance is a living thread. However, in the most common
 * scenario you will only need a single instance for your whole application handling all the alarms. In that case you can use the <i>default</i>
 * FileWatcher, a singleton instance that will be created the first time you use it. In this case, the thread starts and finishes by its
 * own hand, so you only have to add and process alarms:</p>
 * 
 * * <pre>
 * // get the default instance. You can use FileWatcher.getDefaultFileWatcher() a lot, but I'm quite lazy...
 * FileWatcher fw = FileWatcher.getDefaultFileWatcher();
 *
 * // adding an alarm for a directory that will rise whenever the directory is created, deleted or a file is created or deleted
 * // inside it 
 * int alarmDir = fw.addFileToWatch( new File( "/tmp"), FileWatcher.CHANGE_DIR | FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
 * 
 * // adding an alarm for a file that will rise whenever the file is created, deleted or changed, using its size, modification time and
 * // MD5 hash to detect the changes  
 * int alarmFile = fw.addFileToWatch( new File( fileName), 
 *                                            FileWatcher.CHANGE_MD5 | FileWatcher.CHANGE_SIZE | FileWatcher.CHANGE_TIME | 
 *                                            FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
 * 
 * 
 * // register a listener 
 * fw.addListener( new FileWatcherListener() {
 *   public void fileWatcherAlert( FileWatcherEvent ev) {
 *     if ( ev.getAlarmId() == alarmDir || ev.getAlarmId() == alarmFile ) { 
 *       System.out.println( ev);
 *     }
 *   }      
 * });
 * 
 * ... do other things
 * </pre>
 * 
 * <p>The FileWatcher thread will finish when your application and the JVM finishes, so it's quite easy to use. The only problem is that the whole application
 * shares a single FileWatcher, and all listeners are notified with all the alarms, which can give you some headache if a part of your application gets the
 * alarms prepared for another part of your application. When you register an alarm, the <code>addFileToWatch</code> returns an <code>id</code> for the alarm, 
 * and you can use that <code>id</code> to be sure that the captured event has been raised by your alarm.</p>
 * 
 * <p>One last <b>IMPORTANT</b> word about performance. Between the criteria that you can use to screen the changes in the files is the MD5 hash of
 * of a file (<code>CHANGE_MD5</code>). That means that <b>the MD5 hash of the file will be calculated each time the thread arises</b>, and this can 
 * take some time. For the most common uses the size (<code>CHANGE_SIZE</code>) or the modification time (<code>CHANGE_TIME</code>) are enough to 
 * know if a file is changed and are much faster that calculating the MD5 hash of the file. By far. Have this in mind before using the
 * <code>CHANGE_MD5</code> alarm type.</p>   
 * 
 * @author nilo
 *
 */
public class FileWatcher extends Thread {
  /**
   * The default interval between thread wake ups. 500 milliseconds. 
   */
  public static final long GENERAL_INTERVAL = 500;
  private static int ID_ALERT = 0;
  
  /**
   * Watchers of type CHANGE_SIZE will rise an alarm when the file changes its size
   */
  public static final long CHANGE_SIZE    = 1;
  
  /**
   * Watchers of type CHANGE_TIME will rise an alarm when the file changes its modification time
   */
  public static final long CHANGE_TIME    = 2;
  
  /**
   * Watchers of type CHANGE_MD5 will rise an alarm when the file changes its contents. The watcher will 
   * calculate the MD5 hash of the file, which could take some time, so use it only when you really 
   * need it
   */
  public static final long CHANGE_MD5     = 4;
  
  /**
   * Watchers of type FILE_CREATE will rise an alarm when the file is created
   */
  public static final long FILE_CREATE    = 8;
  
  /**
   * Watchers of type FILE_DELETE will rise an alarm when the file is deleted
   */
  public static final long FILE_DELETE    = 16;
  
  /**
   * Watchers of type CHANGE_DIR will rise an alarm when the directory contents change
   */
  public static final long CHANGE_DIR     = 32;
  
  
  // The effective interval between thread wake ups, Initialized to GENERAL_INTERVAL, but can be changed with  setThreadSleep
  private long threadSleep;
  
  // To stop the thread
  private boolean stop;

  // The listeners
  private List<FileWatcherListener> lListeners;
  
  // And the alarms, in a list that will be sorted
  private List<Alarm> lAlarms;
  
  
  // The dummy lcass used to store the alarms
  private class Alarm implements Comparable<Alarm> {
    public int id;
    public File file;
    
    public long eventType;
    
    public long lastTime;
    public long lastSize;
    public String lastMD5;
    public boolean exists;
    public File[] dirContent;
    
    public Alarm( File file, long eventType) {
      this.id = ID_ALERT++;
      this.file = file;
      this.eventType = eventType;
      this.dirContent = new File[0];
      
      if ( ( eventType & CHANGE_SIZE ) != 0 ) {
        lastSize = file.length();
      }
      if ( ( eventType & CHANGE_TIME ) != 0 ) {
        lastTime = file.lastModified();
      }
      if ( ( eventType & CHANGE_MD5 ) != 0 ) {
        lastMD5 = FileWatcher.calcMD5( file);
      }
      if ( ( eventType & CHANGE_DIR ) != 0 ) {
        if ( file.exists() && file.isDirectory() ) {
          dirContent = new File[0];//file.listFiles();
        }
      }
      if ( ( eventType & FILE_CREATE ) != 0 
             ||
           ( eventType & FILE_DELETE ) != 0 ) {
        exists = file.exists();
      }
    }
    
    @Override
    public int compareTo( Alarm other) {
      return this.file.getAbsolutePath().compareTo( other.file.getAbsolutePath());
    }

    @Override
    public String toString() {
      // Just for debug
      String res = "Alarm [id=" + id + ", file=" + file + ", eventType=" + eventType + ", lastTime=" + lastTime + ", lastSize=" + lastSize + ", lastMD5=" + lastMD5
          + ", exists=" + exists + "]";
      
      for ( File ff : dirContent ) {
        res += "\n---" + ff.getAbsolutePath();
      }
      return res;
    }
  }

  // The singleton for the default FileWatcher
  private static FileWatcher defaultFileWatcher;
  

  /**
   * This method returns the instance of the default FileWatcher. This instance is a singleton.
   * @return the instance of the default FileWatcher
   */
  public static FileWatcher getDefaultFileWatcher() {
    if ( defaultFileWatcher == null ) {
      defaultFileWatcher = new FileWatcher();
      
      defaultFileWatcher.startFileWatcher();
    }
    
    return defaultFileWatcher;
  }
  
  /**
   * This method stops the default FileWatcher instance.
   */
  public static void stopDefaultFileWatcher() {
    getDefaultFileWatcher().stopFileWatcher();
  }
  

  /**
   * Default constructor
   */
  public FileWatcher() {
    lListeners = new ArrayList<FileWatcherListener>();
    lAlarms = new ArrayList<Alarm>();
    
    threadSleep = GENERAL_INTERVAL;
    stop = false;
  }
  
  /**
   * This method returns the interval, in milliseconds, between thread wake ups 
   * @return the interval, in milliseconds, between thread wake ups
   */
  public long getThreadSleep() {
    return threadSleep;
  }
  
  /**
   * This method sets the interval, in milliseconds, between thread wake ups
   * @param threadSleep the interval, in milliseconds, between thread wake ups
   */
  public void setThreadSleep( long threadSleep) {
    this.threadSleep = threadSleep;
  }
  
  /**
   * This method stops the FileWatcher thread. You can only stop the thread ONE time, so be really careful
   * with this method and the default thread, because if you stop the default thread, it will never restart
   * again...
   */
  public void stopFileWatcher() {
    stop = true;
  }
  
  /**
   * This method starts the FileWatcher thread
   */
  public void startFileWatcher() {
    if ( this.isAlive() ) {
      return;
    }
    
    setDaemon( true);
    start();
  }
  
  /**
   * This method sets a watcher of <code>file</code> which will rise an alarm whenever a change of type
   * <code>eventTypes</code> appears. Supported events are CHANGE_MD5, CHANGE_SIZE, CHANGE_TIME, 
   * FILE_DELETE, FILE_CREATE and CHANGE_DIR, and if you want that the watcher registers more than
   * one event, use "|" to unite them, like this:
   * <pre>
   * int alarmDir = fw.addFileToWatch( new File( path), FileWatcher.CHANGE_DIR | FileWatcher.FILE_DELETE | FileWatcher.FILE_CREATE);
   * </pre> 
   * @param file the file to watch
   * @param eventTypes the type of the events to watch
   * @return an integer with the id of the alarm
   */
  public int addFileToWatch( File file, long eventTypes) {
    Alarm al = new Alarm( file, eventTypes);
    
    lAlarms.add( al);
    
    return al.id;
  }
  
  @Override
  public void run() {
    while( !stop ) {

      try {
        List<Alarm> lCopy = new ArrayList<>( lAlarms);

        Iterator<Alarm> it = lCopy.iterator();
        while ( it.hasNext() ) {
          Alarm al = it.next();
          
          FileWatcherEvent ev = new FileWatcherEvent( al.id, al.file);
          
          if ( doEval( al, ev) ) {
            fireEvents( ev);
          }
        }
      }
      catch ( Exception ex) {
        // The watchdog loop shouldn't die NEVER!!!!
      }

      Utils.sleep( this.getThreadSleep());
    }
  }
  
  /**
   * This method is invoked to test if the file has changed its size. <br>
   * It's <code>protected</code> because you could want to override the way the test is done. 
   * @param al the alarm
   * @param ev the event
   * @return true if the file has been changed
   */
  protected boolean evalSize( Alarm al, FileWatcherEvent ev) {
    if ( !al.file.exists() ) {
      al.lastSize = 0;
      return false;
    }
    
    long nSize = al.file.length();
    
    if ( al.lastSize != nSize ) {
      ev.doEventSize( al.lastSize, nSize);
      al.lastSize = nSize;
      
      return true;
    }
    
    return false;
  }
  
  /**
   * This method is invoked to test if the file has changed its last modification time. <br>
   * It's <code>protected</code> because you could want to override the way the test is done. 
   * @param al the alarm
   * @param ev the event
   * @return true if the file has been changed
   */
  protected boolean evalTime( Alarm al, FileWatcherEvent ev) {
    if ( !al.file.exists() ) {
      al.lastTime = 0;
      return false;
    }
    
    long nTime = al.file.lastModified();
    
    if ( al.lastTime != nTime ) {
      ev.doEventTime( al.lastTime, nTime);
      al.lastTime = nTime;
      
      return true;
    }
    
    return false;
  }
  
  /**
   * This method is invoked to test if the file has changed its contents, getting the MD5 hash. <br>
   * It's <code>protected</code> because you could want to override the way the test is done. 
   * @param al the alarm
   * @param ev the event
   * @return true if the file has been changed
   */
  protected boolean evalMD5( Alarm al, FileWatcherEvent ev) {
    if ( !al.file.exists() ) {
      al.lastMD5 = "";
      return false;
    }
    
    String actual = calcMD5( al.file);
    
    if ( !actual.equals( al.lastMD5) ) {
      ev.doEventMD5( al.lastMD5, actual);
      al.lastMD5 = actual;

      return true;
    }
    
    return false;
  }
  
  /**
   * This method is invoked to test if a directory has changed its contents. <br>
   * It's <code>protected</code> because you could want to override the way the test is done. 
   * @param al the alarm
   * @param ev the event
   * @return true if the file has been changed
   */
  protected boolean evalDIR( Alarm al, FileWatcherEvent ev) {
    if ( !al.file.exists() ) {
      return false;
    }
    
    File actual[] = al.file.listFiles();
    if ( actual == null ) {
      return false;
    }
    
    Arrays.sort( actual);
    
    // If different sizes, something has changed and we don't need to look closer
    if ( al.dirContent.length != actual.length ) {
      ev.doEventDirChange( al.dirContent, actual);
      al.dirContent = actual;
      
      return true;
    }
    
    Arrays.sort( al.dirContent);
    
    // There can be changes in the directory even if it contains the same number of things
    for ( int i = 0; i < actual.length; i++) {
      if ( !actual[i].equals( al.dirContent[i]) ) {
        ev.doEventDirChange( al.dirContent, actual);
        al.dirContent = actual;
        
        return true;
      }
    }
    
    return false;
  }
  
  
  private boolean doEval( Alarm al, FileWatcherEvent ev) {
    boolean res = false;

    boolean bCreate = 0 != (al.eventType & FILE_CREATE);
    boolean bDelete = 0 != (al.eventType & FILE_DELETE);
    boolean bSize   = 0 != (al.eventType & CHANGE_SIZE);
    boolean bTime   = 0 != (al.eventType & CHANGE_TIME);
    boolean bMD5    = 0 != (al.eventType & CHANGE_MD5);
    boolean bDir    = 0 != (al.eventType & CHANGE_DIR);
    
    // Sometimes is good to overwrite the event type
    long overwriteEvType = -1;
    
    if ( bCreate && al.file.exists() != al.exists ) {
      ev.doEventCreate();
      al.exists = true;
      res |= true;
      
      // Update the rest
      bSize = bTime = bMD5 = bDir = true;
      
      overwriteEvType = FILE_CREATE;
    }
    if ( bDelete && al.file.exists() != al.exists ) {
      ev.doEventDelete();
      al.exists = false;
      res |= true;
      
      // Update the rest
      bSize = bTime = bMD5 = bDir = true;
      
      overwriteEvType = FILE_DELETE;
    }

    if ( bSize && al.file.exists() && al.file.length() != al.lastSize ) {
      res |= evalSize( al, ev);
    }
    if ( bTime && al.file.exists() && al.file.lastModified() != al.lastTime ) {
      res |= evalTime( al, ev);
    }
    if ( bMD5 ) {
      res |= evalMD5( al, ev);
    }
    if ( bDir && al.file.exists() && al.file.isDirectory() ) {
      res |= evalDIR( al, ev);
    }
    
    // Overwrite the type event. This is just esthetic. This way CREATE and DELETE doesn't contain SIZE, TIME, etc, events too.
    // A new file always have a different size or time than one second ago...
    if ( overwriteEvType != -1 ) {
      ev.setEventType( overwriteEvType);
    }
    
    return res;
  }
  
  /**
   * This method registers a listener to process the alarms
   * @param listener a listener to process the alarms
   */
  public void addListener( FileWatcherListener listener) {
    lListeners.add( listener);
  }
  
  /**
   * This method removes the listener <code>listener</code>
   * @param listener the listener to be removed
   */
  public void removeListener( FileWatcherListener listener) {
    lListeners.remove( listener);
  }
  
  private void fireEvents( FileWatcherEvent ev) {
    try {
      for ( FileWatcherListener ll : lListeners ) {
        ll.fileWatcherAlert( ev);
      }
    }
    catch ( Exception th) {
      // if the listener code throws an exception, the FileWatcher thread will die, so let's capture whatever
      // and show a really ugly exception by standard output...
      System.out.println( "FileWatcher listener has found an exception : \n");
      th.printStackTrace();
    }
  }
  
  
  private static String calcMD5( File file) {
    if ( file == null || !file.exists() || !file.isFile() || !file.canRead() ) {
      return "";
    }
    
    MessageDigest md;
    try {
      md = MessageDigest.getInstance( "MD5");
    }
    catch ( NoSuchAlgorithmException e) {
      return "";
    }
    
    try ( InputStream is = new FileInputStream( file);
          DigestInputStream dis = new DigestInputStream( is, md)) {
      byte[] buffer = new byte[4096];
      while ( dis.read(buffer) > -1) {}
    }
    catch ( Exception ex) {
      return "";
    }
    
    byte[] digest = md.digest();
        
    return Formats.getHexDump( digest);
  }
}
