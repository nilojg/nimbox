package es.nimbox.box;

import java.io.Serializable;
import java.text.Format;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class implements an <i>elastic</i> bean, a generic bean that can store data of different types. In fact, it's a map
 * that stores data as pairs key-value. The only advantage over a <code>Map&lt;String,Object&gt;</code> is that this class can
 * be exported and imported to XML and JSON very easily. If you have a JSON as input you can try to create an <code>ElasticBox</code>
 * with it. It would be better to create an object which maps <i>exactly</i> the JSON and use the {@link Formats#fromJson(String, Class)}
 * method, but using an <code>ElasticBox</code> could save a lot of work if you JSON isn't too complicated and you can access
 * the data using keys.<br>
 * Another advantage of <code>ElasticBox</code> is that you can store in it data when you don't know exactly how many data
 * you will have, for example, after a <code>select *</code>, where you don't know exactly the number of fields you will get.<br>
 * It's quite easy to use:
 * <pre>
 * // Create the box
 * ElasticBox eb = new ElasticBox();
 * 
 * // Set the ID. If you don't set your own ID, the ElasticBox will create one using UIDGen    
 * eb.setId( "1");
 * 
 * // Add data as pairs key-value
 * eb.set( "A", "a");
 * eb.set( "B", 1);
 * eb.set( "C", 1.2);
 * eb.set( "D", 2l);
 * 
 * // And get data
 * assertEquals( "1", eb.getId());
 * assertEquals( "a", eb.get( "A"));
 * assertTrue( 1 == eb.getAsInt( "B"));
 * assertTrue( 1.2 == eb.getAsDouble( "C"));
 * assertTrue( 2l == eb.getAsLong( "D"));
 * 
 * // And you can add lists and maps
 * List&lt;String&gt; list = new ArrayList&lt;String&gt;();
 * list.add( "hola");
 * list.add( "y");
 * list.add( "adios");
 * list.add( "mundo");
 * list.add( "cruel");
 * 
 * Map&lt;String,Object&gt; map2 = new HashMap&lt;&gt;();
 * map2.put( "m1", "Azul");
 * map2.put( "m2", 23);
 * map2.put( "m3", date);
 * 
 * eb.set( "lista", list);
 * eb.set( "mapa1", map2);
 * </pre>
 * 
 * You can export the box to JSON or XML, and import it from JSON or XML:
 * <pre>
 * ElasticBox eb1 = getElasticBox();
 * String json1 = eb1.toJSON();
 *     
 * ElasticBox eb2 = ElasticBox.fromJSON( json1);
 * String json2 = eb2.toJSON();
 * 
 * assertEquals( json1, json2);
 * 
 * ElasticBox eb1 = getElasticBox();
 * String xml1 = "&lt;a&gt;" + eb1.toXML() + "&lt;/a&gt;";
 *     
 * ElasticBox eb2 = ElasticBox.fromXML( xml1);
 * String xml2 = "&lt;a&gt;" + eb2.toXML() + "&lt;/a&gt;";
 * 
 * assertEquals( xml1, xml2);
 * </pre>
 *
 * 
 * @author nilo
 *
 */
public class ElasticBox implements Serializable, Comparable<ElasticBox> {
  private static final long serialVersionUID = -5092096722249487769L;

  /**
   * The key for the id 
   */
  public static final String ID   = "id";
  
  /**
   * The "type" value used for writing the XML and JSON files
   */
  public static final String TYPE = "type";
  
  /**
   * The id
   */
  private String id;  // NOSONAR
  
  /**
   * The values store
   */
  protected Map<String,Object> mVars;

  /**
   * This constructor creates an empty <code>ElasticBox</code>
   */
  public ElasticBox() {
    super();
    
    mVars = new HashMap<>();
  }
  
  /**
   * This method creates an <code>ElasticBox</code> identified by <code>id</code>
   * @param id the identifier of the box
   */
  public ElasticBox( String id) {
    this();
    
    setId( id);
  }
  
  /**
   * This method creates a new <code>ElasticBox</code> with a copy of <code>other</code>. <b>It is NOT a <code>clone()</code>, the
   * two objects share references</b>.<br>
   * If you want a completely new object, try:
   * <pre>
   * ElasticBox other = ....;
   * ElasticBox eb2 = ElasticBox.fromXML( "&lt;a&gt;" + other.toXML() + "&lt;/a&gt;");
   * </pre>
   * @param other the other <code>ElasticBox</code>
   * @throws ParseException if something goes wrong
   */
  public ElasticBox( ElasticBox other) {
    this();
    
    setId( other.getId());
    for ( String field : other.getFields() ) {
      this.set( field, other.get( field));
    }
  }
  
  
  /**
   * This method returns the <code>id</code> of the <code>ElasticBox</code>
   * @return the <code>id</code> of the <code>ElasticBox</code>
   */
  public String getId() {
    if ( id == null ) {
      id = UIDGen.UID();
    }
    
    return id;
  }

  /**
   * This method sets the <code>id</code> of the <code>ElasticBox</code>
   * @param id the <code>id</code> of the <code>ElasticBox</code>
   */
  public void setId( String id) {
    this.id = id;
  }
  
  /**
   * This method returns the names of the properties of the <code>ElasticBox</code>. <br>
   * Only the "first level" fields are returned. If this <code>ElasticBox</code> contains another <code>ElasticBox</code>, the 
   * properties of the inner <code>ElasticBox</code> are not returned.
   * @return the names of the properties of the <code>ElasticBox</code>
   */
  public Collection<String> getFields() {
    return new ArrayList<>( mVars.keySet());
  }
  
  /**
   * This method returns the names of the properties of the <code>ElasticBox</code> in alphabetical order.  <br>
   * Only the "first level" fields are returned. If this <code>ElasticBox</code> contains another <code>ElasticBox</code>, the 
   * properties of the inner <code>ElasticBox</code> are not returned.
   * @return the names of the properties of the <code>ElasticBox</code> in alphabetical order
   */
  public Collection<String> getSortedFields() {
    List<String> res = (List<String>)getFields();
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns the names of the properties of the <code>ElasticBox</code> which names match the
   * regular expression <code>regex</code>.  <br>
   * Only the "first level" fields are returned. If this <code>ElasticBox</code> contains another <code>ElasticBox</code>, the 
   * properties of the inner <code>ElasticBox</code> are not returned.
   * @param regex the regular expression
   * @return the names of the properties of the <code>ElasticBox</code>
   */
  public Collection<String> getFields( String regex) {
    Collection<String> res = getFields();
    
    Iterator<String> it = res.iterator();
    while ( it.hasNext() ) {
      String field = it.next();
      
      if ( !field.matches( regex) ) {
        it.remove();
      }
    }
    
    return res;
  }
  
  /**
   * This method returns the names of the properties of the <code>ElasticBox</code> which names match the
   * regular expression <code>regex</code> in alphabetical order.  <br>
   * Only the "first level" fields are returned. If this <code>ElasticBox</code> contains another <code>ElasticBox</code>, the 
   * properties of the inner <code>ElasticBox</code> are not returned.
   * @param regex the regular expression
   * @return the names of the properties of the <code>ElasticBox</code>
   */
  public Collection<String> getSortedFields( String regex) {
    List<String> res = (List<String>)getFields( regex);
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.
   * @param field the name of the field
   * @return the value of the field
   */
  public Object get( String field) {
    return mVars.get( field);
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The parameters <code>value</code> 
   * and <code>field</code> can be <code>null</code>
   * @param field the name of the field
   * @param value the value of the field
   */
  public void set( String field, Object value) {
    mVars.put( field, value);
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. If the object 
   * <code>formatter</code> is not <code>null</code>, the object <code>value</code> will be converted into a String
   * using its <code>toString</code> method and parsed with it.  
   * The parameter <code>field</code> can be <code>null</code>, but <code>value</code> and <code>formatter</code>
   * not.
   * @param field the name of the field
   * @param value the value of the field
   * @param formatter the object used to parse the String representation of the <code>value</code>
   * @throws ParseException when something goes wrong
   */
  public void set( String field, Object value, Format formatter) throws ParseException {
    if ( formatter != null ) {
      try {
        value = formatter.parseObject( value.toString());
      }
      catch ( java.text.ParseException e) {
        throw new ParseException( e.getMessage(), e);
      }
    }
    
    mVars.put( field, value);
  }

  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The object will
   * be stored as a <b>Date</b>, obtained after converting the <code>value</code> to String using its 
   * <code>toString</code> method and parsing it with a <code>SimpleDateFormat</code> with the format 
   * <code>dateFormat</code>.  
   * The parameter <code>field</code> can be <code>null</code>, but <code>value</code> and <code>dateFormat</code>
   * not.
   * @param field the name of the field
   * @param value the value of the field
   * @param dateFormat the format used to parse the String representation of the <code>value</code>
   * @throws ParseException when something goes wrong
   */
  public void set( String field, Object value, String dateFormat) throws ParseException {
    mVars.put( field, Utils.parseTime( value.toString(), dateFormat));
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The parameters <code>value</code> 
   * and <code>field</code> can be <code>null</code>
   * @param field the name of the field
   * @param value the value of the field
   */
  public void set( String field, int value) {
    set( field, (Object)value);
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The parameters <code>value</code> 
   * and <code>field</code> can be <code>null</code>
   * @param field the name of the field
   * @param value the value of the field
   */
  public void set( String field, long value) {
    set( field, (Object)value);
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The parameters <code>value</code> 
   * and <code>field</code> can be <code>null</code>
   * @param field the name of the field
   * @param value the value of the field
   */
  public void set( String field, double value) {
    set( field, (Object)value);
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The parameters <code>value</code> 
   * and <code>field</code> can be <code>null</code>
   * @param field the name of the field
   * @param value the value of the field
   */
  public void set( String field, float value) {
    set( field, (Object)value);
  }
  
  /**
   * This method sets the <code>value</code> associated to the field with name <code>field</code>. The parameters <code>value</code> 
   * and <code>field</code> can be <code>null</code>
   * @param field the name of the field
   * @param value the value of the field
   */
  public void set( String field, boolean value) {
    set( field, (Object)value);
  }
  
  
  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>Integer</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public Integer getAsInt( String field) {
    Object res = get( field);
    
    return res == null ? null : Integer.valueOf( res.toString().trim());
  }

  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>Long</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public Long getAsLong( String field) {
    Object res = get( field);
    
    return res == null ? null : Long.valueOf( res.toString().trim());
  }

  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>Double</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public Double getAsDouble( String field) {
    Object res = get( field);
    
    return res == null ? null : Double.valueOf( res.toString().trim());
  }

  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>Float</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public Float getAsFloat( String field) {
    Object res = get( field);
    
    return res == null ? null : Float.valueOf( res.toString().trim());
  }

  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>Boolean</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public Boolean getAsBoolean( String field) {
    Object res = get( field);
    
    return res == null ? null : Boolean.valueOf( res.toString().trim());
  }

  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>String</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public String getAsString( String field) {
    Object res = get( field);
    
    return res == null ? null : res.toString();
  }

  /**
   * This method returns the value stored with the key <code>field</code>. The method <i>can</i> return <code>null</code> if
   * the field doesn't exist or if the field exists and has a <code>null</code> value.<br>
   * The method tries to convert the value to <code>Date</code> which could raise an exception if the stored value cannot be converted.
   * @param field the name of the field
   * @return the value of the field
   */
  public Date getAsDate( String field) {
    Object res = get( field);
    
    if ( res == null ) {
      return null;
    }
    else if ( res instanceof Date ) {
      return (Date)res;
    }
    else {
      return Utils.parseTime( res.toString());
    }
  }
  
  
  

  @Override
  public int compareTo( ElasticBox other) {
    return getId().compareTo( other.getId());
  }

  @Override
  public boolean equals( Object other) {
    if ( other == null ) {
      return false;
    }
    
    String thisStr = toString();
    String otherStr = other.toString();
    
    return thisStr.equals( otherStr);
  }

  @Override
  public int hashCode() {
    return getId().hashCode();
  }

  @Override
  public String toString() {
    return toJSON();
  }
  
  
  /**
   * This method returns a <code>String</code> with a JSON representation of this box. The method tries to do its best converting the insides
   * in JSON using <code>toString</code> methods. Instances of <code>Map</code> and <code>Collection</code> are mapped in JSON lists between
   * square brackets.
   * @return a <code>String</code> with a JSON representation of this box.
   */
  public String toJSON() {
    StringBuilder res = new StringBuilder();
    
    res.append( "{");

    res.append( "\"" + ID + "\": \"" + getId() + "\"");

    for ( String field : getSortedFields() ) {
      Object value = get( field);
      
      if ( value == null ) {
        continue;
      }
      
      String scapedQuotes = Utils.changeAll( field, "\"", "\\\"");
      
      res.append( ", \"");
      res.append( scapedQuotes);
      res.append( "\": ");
      
      res.append( getJSON( value));
    }
    
    res.append( "}");
    
    return res.toString();
  }
  
  /**
   * This method creates an <code>ElasticBox</code> with the data contained in the <code>json</code>. List between square brackets are converted
   * in <code>ArrayList</code>. 
   * @param json the JSON
   * @return an <code>ElasticBox</code> containing the data in the JSON
   * @throws ParseException when something goes wrong
   */
  public static ElasticBox fromJSON( String json) throws ParseException {
    if ( json == null || json.trim().equals( "") ) {
      return null;
    }
    
    ElasticBox res = new ElasticBox();
    
    JSONParser jp = new JSONParser( json);
    int i = 0;
    
    for ( Pair<String,Object> p : jp.getElements() ) {
      String key = p.first;
      Object value = p.second;
      
      if ( key.equals( "") ) {
        key = "anonymous" + i++;
      }
      
      if ( value instanceof List ) {
        res.set( key, value);
      }
      else if ( key.equals( ID) ) {
        res.setId( value.toString());
      }
      else {
        res.set( key, value);
      }
    }
    
    return res;
  }  

  /**
   * This method returns a <code>String</code> with a XML representation of this box. The method tries to do its best converting the insides
   * in XML using <code>toString</code> methods.<br> 
   * The resulting XML contains information of stored types so the original <code>ElasticBean</code> can be reconstructed using the 
   * {@link #fromXML(String)} method. The price is a "heavier" XML.
   * @return a <code>String</code> with a XML representation of this box.
   */
  public String toXML() {
    return toXML( true);
  }
  
  /**
   * This method returns a <code>String</code> with a XML representation of this box. The method tries to do its best converting the insides
   * in XML using <code>toString</code> methods. <br>
   * The result XML contains information of stored types depending on the parameter <code>keepTypeInfo</code>. The type info will be there only 
   * if the <code>keepTypeInfo</code> parameter is <code>true</code>, so the original <code>ElasticBean</code> could be reconstructed using the 
   * {@link #fromXML(String)} method. If <code>keepTypeInfo</code> parameter is <code>false</code>, there won't be type information in the 
   * resulting XML, a much cleaner XML very appropriate if you want to share it with another application, but less expressive. The 
   * {@link #fromXML(String)} method will not have enough information to reconstruct the <code>ElasticBox</code> and all the child nodes will
   * be stored inside <code>ArrayList</code> objects. If the XML is flat, everything will be OK.
   * @param keepTypeInfo if <code>true</code>, the resulting XML contains type info. If <code>false</code>, not.
   * @return a <code>String</code> with a XML representation of this box.
   */
  public String toXML( boolean keepTypeInfo) {
    StringBuilder res = new StringBuilder();

    res.append( "<id><![CDATA[" + getId() + "]]></id>");
    
    for ( String field : getSortedFields() ) {
      Object value = get( field);
      
      res.append( "<");
      res.append( field);
      if ( keepTypeInfo ) {
        res.append( " " + TYPE + "='" + value.getClass().getCanonicalName() + "'");
      }
      res.append( ">");
      
      res.append( getXML( value, keepTypeInfo));
      
      res.append( "</");
      res.append( field);
      res.append( ">");
    }
    
    return res.toString();
  }
  
  /**
   * This method creates an <code>ElasticBox</code> with the data contained in the <code>xml</code>. The success depends on how the XML has been created.
   * Read the {@link #toXML()} and {@link #toXML(boolean)} documentation to get more information.
   * @param xml the XML
   * @return an <code>ElasticBox</code> containing the data in the XML
   * @throws ParseException when something goes wrong
   */
  public static ElasticBox fromXML( String xml) throws ParseException {
    if ( xml == null || xml.trim().equals( "") ) {
      return null;
    }
    
    Node root = XMLUtils.str2node( xml);
    
    return fromXML( root);
  }
  
  /**
   * This method creates an <code>ElasticBox</code> with the data contained in the <code>xml</code>. The success depends on how the XML has been created.
   * Read the {@link #toXML()} and {@link #toXML(boolean)} documentation to get more information.
   * @param xml the XML
   * @return an <code>ElasticBox</code> containing the data in the XML
   * @throws ParseException when something goes wrong
   */
  public static ElasticBox fromXML( Node xml) throws ParseException {
    if ( xml == null || !xml.hasChildNodes() ) {
      return null;
    }
    
    ElasticBox res = new ElasticBox();
    try {
      NodeList lNodes = xml.getChildNodes();
      for ( int i = 0; i < lNodes.getLength(); i++) {
        Node node = lNodes.item( i);
        
        if ( node.getNodeName().trim().equals( ID) ) {
          res.setId( node.getTextContent().trim());
        }
        else {
          String name = node.getNodeName().trim();
          Object obj = doNode( node);
          
          res.set( name, obj);
        }
      }
    }
    catch ( Exception ex) {
      throw new ParseException( "Cannot parse XML :" + ex.getMessage(), ex);
    }
    
    return res;
  }
  
  
  @SuppressWarnings( "unchecked")
  private static Object doNode( Node node) throws Exception {
    String value = node.getTextContent().trim();
    String type = "java.lang.String";
    
    if ( node.getAttributes() != null && node.getAttributes().getNamedItem( TYPE) != null ) {
      type = node.getAttributes().getNamedItem( TYPE).getTextContent().trim();
    }
    else if ( node.hasChildNodes() && node.getChildNodes().getLength() > 1 ) {
      type = "java.util.ArrayList";
    }
    
    Object obj = getInstance( type, value, node);
    if ( obj instanceof Collection ) {
      doCollection( (Collection<Object>)obj, node);
    }
    else if ( obj instanceof Map ) {
      doMap( (Map<String,Object>)obj, node);
    }
    
    return obj;
  }
  
  /**
   * This method fills a Collection with all the sons of the XML node <code>nodeFather</code>
   * @param list the list to fill
   * @param nodeFather the node
   * @throws Exception because shit happens
   */
  private static void doCollection( Collection<Object> list, Node nodeFather) throws Exception {
    NodeList lNodes = nodeFather.getChildNodes();
    for ( int i = 0; i < lNodes.getLength(); i++) {
      Node node = lNodes.item( i);
      
      Object item = doNode( node);
      list.add( item);
    }
  }
  
  /**
   * This method fills a Map with all the sons of the XML node <code>nodeFather</code>
   * @param map the map to fill
   * @param nodeFather the node
   * @throws Exception because shit happens
   */
  private static void doMap( Map<String,Object> map, Node nodeFather) throws Exception {
    NodeList lNodes = nodeFather.getChildNodes();
    for ( int i = 0; i < lNodes.getLength(); i++) {
      Node node = lNodes.item( i);
      
      String name = node.getNodeName().trim();
      Object item = doNode( node);
      
      map.put( name, item);
    }
  }
  
  /**
   * This method returns an instance of type <code>type</code> with the value inside the String <code>value</code>.
   * @param type the type
   * @param value the value
   * @param node the node
   * @return the instance
   * @throws Exception because shit happens, specially if the type hasn't a default constructor
   */
  private static Object getInstance( String type, String value, Node node) throws Exception {
    Object res = null;
    
    if ( type.equals( "java.lang.String") ) {
      res = value;
    }
    else if ( type.equals( "java.lang.Integer") ) {
      res = Integer.valueOf( value);
    }
    else if ( type.equals( "java.lang.Double") ) {
      res = Double.valueOf( value);
    }
    else if ( type.equals( "java.lang.Long") ) {
      res = Long.valueOf( value);
    }
    else if ( type.equals( "java.lang.Float") ) {
      res = Float.valueOf( value);
    }
    else if ( type.equals( "java.lang.Boolean") ) {
      res = Boolean.valueOf( value);
    }
    else if ( type.equals( "java.util.Date") ) {
      res = Utils.parseTime( value);
    }
    else if ( type.equals( "es.nimbox.box.ElasticBox") ) {
      res = fromXML( node);
    }
    else {
      Class <?> clazz = Class.forName( type);
      
      res = clazz.getDeclaredConstructor().newInstance();
    }
    
    return res;
  }
  
  
  private String getXML( Object value, boolean keepTypeInfo) {
    StringBuilder res = new StringBuilder();
    
    if ( value instanceof ElasticBox ) {
      res.append( ((ElasticBox)value).toXML( keepTypeInfo));
    }
    else if ( value instanceof Collection ) {
      int i = 0;
      for ( Object obj : (Collection<?>)value) {
        res.append( "<item" + i);
        if ( keepTypeInfo ) {
          res.append( " " + TYPE + "='" + obj.getClass().getCanonicalName() + "'");
        }
        res.append( ">");
        res.append( getXML( obj, keepTypeInfo));
        res.append( "</item" + i + ">");
        i++;
      }
    }
    else if ( value instanceof Map ) {
      Map<?,?> map = ((Map<?,?>)value);

      for ( Map.Entry<?,?> entry : map.entrySet()) {
        Object key = entry.getKey();
        Object obj = entry.getValue();
        
        res.append( "<" + key.toString());
        if ( keepTypeInfo ) {
          res.append( " " + TYPE + "='" + obj.getClass().getCanonicalName() + "'");
        }
        res.append( ">");
        res.append( getXML( obj, keepTypeInfo));
        res.append( "</" + key + ">");
      }
    }
    else {
      res.append( "<![CDATA[");
      if ( value instanceof Date ) {
        res.append( Utils.formatTime( (Date)value));
      }
      else {
        res.append( value.toString());
      }
      res.append( "]]>");
    }

    return res.toString();
  }
  
  
  /**
   * This method does the tough job with the JSON, looking for the type of the stored objects and trying to
   * do its best to create the JSON. The method loops the Collections, the Maps and formats the dates.  
   */
  private String getJSON( Object value) {
    StringBuilder res = new StringBuilder();
    
    boolean first = true;
    
    if ( value instanceof ElasticBox ) {
      //res.append( "{");
      res.append( ((ElasticBox)value).toJSON());
      //res.append( "}");
    }
    else if ( value instanceof Collection ) {
      res.append( "[");
      for ( Object obj : (Collection<?>)value) {
        if ( first ) {
          first = false;
        }
        else {
          res.append( ", ");
        }
        res.append( getJSON( obj));
      }
      res.append( "]");
    }
    else if ( value instanceof Map ) {
      res.append( "[");
      for ( Object obj : ((Map<?,?>)value).values()  ) {
        if ( first ) {
          first = false;
        }
        else {
          res.append( ", ");
        }
        res.append( getJSON( obj));
      }
      res.append( "]");
    }
    else if ( value instanceof Date ) {
      res.append( "\"");
      res.append( Utils.formatTime( (Date)value));
      res.append( "\"");
    }
    else if ( value != null ) {
      res.append( "\"");
      
      // Escape the quotes
      String text = value.toString();
      text = Utils.changeAll( text, "\\\"", "==============**************===============");
      text = Utils.changeAll( text, "\"", "\\\"");
      text = Utils.changeAll( text, "==============**************===============", "\\\"");
      
      res.append( text);
      res.append( "\"");
    }
    
    return res.toString();
  }
  
  

}
