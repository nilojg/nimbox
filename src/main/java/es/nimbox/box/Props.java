package es.nimbox.box;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * <p>This class implements a simple properties and object store. It's like a Properties object with vitamins because it has a method to
 * substitute properties variables by its value in Strings.</p>
 * 
 * <p>The properties can be added using the <code>setProperty</code> methods, with String, int, long, double and boolean flavors, and the
 * values can be read from the store with the <code>getProperty</code> and <code>getPropertyAs<i>TYPE</i></code> methods.</p>
 * 
 * <p>Properties can be added too using the <code>addFile</code> methods, which take a file in "standard properties format" and add all the 
 * values to the store. The <code>addFile</code> methods receive the file as a <code>File</code> of String with the name of the properties
 * file to read, which will be looked for in the CLASSPATH and in the file system, by this order, as the name of an environment variable
 * which contains the name of the properties file, or as an <code>InputStream</code> object, which allows to you to locate the properties
 * file in more exotic locations, like a socket, for example.</p>
 * 
 * <p>The most powerful feature of the <code>Props</code> object is that values are not fixed, they can change according with the values of
 * other properties. For example, suppose that your application uses a webservice which endpoint URL is something like "SERVER/ws/add". The
 * "/ws/add" is always the same, but the "SERVER" part changes depending on the environment, like "develop:8080", or "production.acme.com".
 * You can create a property named "endpointUrl" like this in a file named "connection.props":</p>
 * <pre>
 * 
 * endpointUrl=${host}/ws/add
 * </pre>  
 * 
 * <p>Notice the <code>${host}</code> in the value. That means that when invoking the <code>getProperty</code> method, the part <code>${host}</code>
 * will be substituted by the value of a property named <code>host</code>, so you can create another two configuration files, one with the
 * <code>host</code> property with the value for develop and another one with the production environment.</p>
 * <pre>
 * File : develop.props
 * 
 * host=develop:8080
 * -------------
 * File : production.props
 * 
 * host=production.acme.com
 * </pre>
 * 
 * <p>If you create an operative system environment variable named "EXEC_ENV" in the execution environments with the value "develop.props" for 
 * develop and "production.props" in production, the next code will print the correct value for all the environment:</p>
 * 
 * <pre>
 * 
 * Conf cnf = new Conf();
 * cnf.addFile( "EXEC_ENV");  // load the properties file stored in the EXEC_ENV environment variable
 * cnf.addFile( "connection.props");  // add the general values
 * 
 * System.out.println( cnf.getProperty( "endpointUrl"));
 * </pre>
 * 
 * <p>The printed value will change the <code>${host}</code> with the correct value depending on the loaded file, which will be read from the value
 * of an environment variable. You can define all the properties using "expansion variables" between '${' and '}' for the really environment
 * depending values and get the properties with the final values. This approach fits quite good with Docker applications because Docker makes
 * really easy to send environment variables to the applications running in containers.</p>
 * 
 * <p>The "expansion variables" between '${' and '}' can be recursive, for example, it's legal to define a property like 
 * <code>${host${num}}/value</code> if the properties <code>num</code> and <code>host1</code>, <code>host2</code>... <code>hostN</code> exist.</p>
 * 
 * <p>Using the method <code>fulfill</code> with a String will expand the variables, like this:</p>
 * 
 * <pre>
 * 
 * System.out.println( "The endpoint is ${endpointUrl}");
 * </pre> 
 * 
 * <p>This code will print "The endpoint is production.acme.com/ws/add"</p>
 * 
 * <p>The store reads the System properties, so something like this will work:</p>
 * 
 * <pre>
 * 
 * System.out.println( "The temp directory is ${java.io.tmpdir}");
 * </pre> 
 * 
 * 
 * <p>The store can contain general <code>Object</code> instances too, using the methods <code>setObject</code> and <code>getObject</code>, which 
 * allows to use the <code>Props</code> object as a simply 'application context'</p>
 * 
 * <p>Finally, there's a public static instance of the Props object ready to use in the <code>Props.common</code>.</p>
 * 
 * @author nilo
 *
 */

@JsonDeserialize( using=PropsDeserializer.class)
public class Props {  
  /**
   * The default "begin token" for placeholders. ${
   */
  public static final String TK_BEGIN = "${";
  
  /**
   * The default "end token" for placeholders. ${
   */
  public static final String TK_END   = "}";
  
  private static final String D_TK_BEGIN = "__*__*__";
  private static final String D_TK_END   = "___*___";

  /**
   * This is the reserved word to include another property files
   */
  public static final String TK_INCLUDE = "$INCLUDE";



  // This property contains the number of System properties stored
  @JsonIgnore
  private int systemPropertiesSize;
  
  private Map<String,String> mProps;
  
  @JsonIgnore
  private Map<String,Object> mObjects;
  
  // This thing stores the alarm ids for the files that are refreshed, so the watchdogs can be deactivated
  @JsonIgnore
  private Map<String,Integer> mWatchDogAlarms;
  
  @JsonIgnore
  private WatchDog wd;

  @JsonIgnore
  private Map<String,String> mIncludedFiles;
  
  /**
   * This object is a SINGLETON and shared properties store. It contains all the system properties, and is useful if you want to
   * have your properties in a static, shared and single place. But it's a static object...
   */
  public final static Props common = new Props();
  
  /**
   * This class implements a listener than will reload some property files if the {@link Props#addFile( String file, long refreshTime)}
   * is invoked
   * 
   * @author nilo 
   */
  private class RefreshFileListener implements WatchDogListener {
    int alarmId;
    
    public RefreshFileListener( int id) {
      this.alarmId = id;
    }

    @Override
    public void watchDogAlert( WatchDogEvent ev) {
      if ( ev.getAlarmId() == alarmId ) {
        try {
          ((Props)ev.getOrigin()).addFile( ev.getMsg());
        }
        catch ( IOException e) {
          System.out.println( "Cannot reload the file [" + ev.getMsg() + "] : " + e.getMessage());
          e.printStackTrace();
        }
      }
    }
    
  }

  /**
   * This constructor creates a fresh properties store. <br>
   * The method imports the system properties.
   */
  public Props() {
    mProps = new HashMap<String,String>();
    mObjects = new HashMap<String,Object>();
    mWatchDogAlarms = new HashMap<String, Integer>();
    mIncludedFiles = new HashMap<>();
    
    addSystemProperties();
  }
  

  /**
   * This constructor creates a fresh properties store. If <code>importCommon</code> is <code>true</code>, the constructor 
   * imports all the properties from the singleton <code>common</code> store.<br>
   * The method imports the system properties.
   * @param importCommon if is <code>true</code>, the constructor imports all the properties from the singleton <code>common</code> store.<br>
   */
  public Props( boolean importCommon) {
    this();
    
    if ( importCommon && common != null ) {
      mProps.putAll( Props.common.mProps);
      mObjects.putAll( Props.common.mObjects);
    }
  }

  /**
   * This constructor initializes the properties reading a properties file
   * 
   * @param file the properties file to read
   * @throws IOException if there are any problems reading the file
   */
  public Props( String file) throws IOException {
    this();
    
    addFile( file);
  }

  /**
   * This constructor creates a new <code>Props</code> object copying all the values in the object <code>conf</code>
   * @param conf the Props object to copy
   */
  public Props( Props conf) {
    this();
    
    mProps.putAll( conf.mProps);
    mObjects.putAll( conf.mObjects);

    systemPropertiesSize = conf.systemPropertiesSize;
  }
  
  private void addSystemProperties() {
    systemPropertiesSize = 0;
    
    for ( Object sp : System.getProperties().keySet() ) {
      String key = sp.toString();
      mProps.put( key, System.getProperty( key));
      
      systemPropertiesSize++;
    }
  }

  /**
   * This method erases the stored properties
   */
  public void clear() {
    mProps.clear();
    mObjects.clear();
    mIncludedFiles.clear();
    
    addSystemProperties();
  }
  
  /**
   * This method erases the stored properties that start with <code>prefix</code>
   * @param prefix the prefix
   */
  public void clear( String prefix) {
    Iterator<String> itP = getKeys( prefix).iterator();
    while ( itP.hasNext() ) {
      removeProperty(  itP.next());
    }
    
    Iterator<String> itO = getObjectsKeys( prefix).iterator();
    while ( itO.hasNext() ) { 
      removeObject( itO.next());
    }
  }
  
  /**
   * This method returns <code>true</code> if the property exists in the store and <code>false</code> otherwise.
   * @param property the property key
   * @return <code>true</code> if the property exists in the store and <code>false</code> otherwise
   */
  public boolean containsProperty( String property) {
    return mProps.containsKey( property);
  }
  
  /**
   * This method returns <code>true</code> if the object exists in the store and <code>false</code> otherwise.
   * @param key the object key
   * @return <code>true</code> if the object exists in the store and <code>false</code> otherwise
   */
  public boolean containsObject( String key) {
    return mObjects.containsKey( key);
  }
  
  /**
   * This method returns the number of stored properties
   * @return the number of stored properties
   */
  public int size() {
    return mProps.size() - systemPropertiesSize;
  }
  
  /**
   * This method returns the number of stored objects
   * @return the number of stored properties
   */
  public int sizeObjects() {
    return mObjects.size();
  }
  
  /**
   * This method returns all the stored property keys
   * @return a Set with all the stored property keys
   */
  public Set<String> getKeys() {
    HashSet<String> res = new HashSet<>( mProps.keySet());
    
    res.removeAll( System.getProperties().keySet());
    
    return res;
  }
  
  /**
   * This method returns all the stored property keys in a sorted list
   * @return all the stored property keys in a sorted List
   */
  public List<String> getKeysSorted() {
    List<String> res = new ArrayList<String>();
    
    synchronized ( mProps) {
      for ( String key : getKeys()) {
        res.add( key);
      }
    }
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns all the stored property keys that match the regular expression <code>regex</code>.
   * @param regex the regular expression
   * @return the stored property keys that match the regular expression <code>regex</code>
   */
  public List<String> getKeys( String regex) {
    List<String> res = new ArrayList<String>();
    
    for ( String key :  getKeys()) {
      if ( key.matches( regex) ) {
        res.add( key);
      }
    }
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns all the stored objects keys
   * @return a Set with all the stored objects keys
   */
  public Set<String> getObjectsKeys() {
    return new HashSet<String>( mObjects.keySet());
  }
  
  /**
   * This method returns all the stored object keys that match the regular expression <code>regex</code>.
   * @param regex the regular expression
   * @return the stored object keys that match the regular expression <code>regex</code>
   */
  public List<String> getObjectsKeys( String regex) {
    List<String> res = new ArrayList<String>();
    
    synchronized ( mObjects) {
      for ( String key : mObjects.keySet()) {
        if ( key.matches( regex) ) {
          res.add( key);
        }
      }
    }
    
    return res;
  }
  
  /**
   * This method adds all the properties contained in the stream <code>is</code>. The format <b>must</b> be the regular properties file format.
   * @param is the origin of the data
   * @throws IOException when something goes wrong
   * @throws NullPointerException if the argument <code>is</code> is null
   */
  public void addFile( InputStream is) throws IOException {
    try ( BufferedReader reader = new BufferedReader( new InputStreamReader( is)); ) {
      String line = null;

      while ( null != ( line = reader.readLine() ) ) {
        // Comments -> continue
        if ( line.trim().equals( "") || line.trim().startsWith( "#") || line.trim().startsWith( "//") ) {
          continue;
        }

        // If it's an INCLUDE, well... include the file
        if ( line.trim().startsWith( TK_INCLUDE) ) {
          String fileName = line.trim().substring( TK_INCLUDE.length()).trim();

          // This thing is to avoid a deadlock if a file A includes B and B includes A
          if ( !mIncludedFiles.containsKey( fileName)) {
            mIncludedFiles.put( fileName, fileName);
            addFile( fileName);
          }

          continue;
        }

        // Line without '=' -> continue
        int ix = line.indexOf( "=");
        if ( ix == -1 ) {
          continue;
        }

        // Read values
        String key = line.substring( 0, ix).trim();
        String value = line.substring( ix+1).trim();

        // No key -> continue
        if ( key.equals( "") ) {
          continue;
        }

        // add the property
        setProperty( key, value);
      }
    }
  }
  
  /**
   * <p>This method adds all the properties contained in the file <code>file</code>. The method will try to find the file in the CLASSPATH. If the
   * file cannot be found in the CLASSPATH, then it tries to find it in the file system. And if it cannot be found in the CLASSPATH or in the
   * file system, well, it throws an exception.</p> 
   * The format <b>must</b> be the regular properties file format.
   * @param file the file
   * @throws IOException when something goes wrong
   */
  public void addFile( String file) throws IOException {
    try ( InputStream is = Utils.findFile( file) ) {
      addFile( is);
    }
    catch ( Exception ex) {
      throw new IOException( "Cannot find the file [" + file + "] or its format is wrong");
    }
  }
  
  /**
   * <p>This method adds all the properties contained in the file <code>file</code>. The method will try to find the file in the CLASSPATH. If the
   * file cannot be found in the CLASSPATH, then it tries to find it in the file system. And if it cannot be found in the CLASSPATH or in the
   * file system, well, it throws an exception.</p>
   * <p>The file will be read after <code>refreshTime</code> millis, which is quite useful if you want that your application was
   * aware of changes in the configuration files</p>
   * The format <b>must</b> be the regular properties file format.
   * @param file the file
   * @param refreshTime the time, in millis, between refreshing the content
   * @throws IOException when something goes wrong
   */
  public void addFile( String file, long refreshTime) throws IOException {
    try ( InputStream is = Utils.findFile( file) ) {
      addFile( is);
      
      // Set an alarm to reload the file
      if ( wd == null ) {
        wd = WatchDog.getDefaultWatchDog();
      }
      int alarmId = wd.registerAlarmInterval( this, file, refreshTime);
      
      // Set the listener to process the alarms
      wd.addListener( new RefreshFileListener( alarmId));
      
      // Add the file with its alarmId to the collection
      mWatchDogAlarms.put( file, alarmId);
    }
    catch ( Exception ex) {
      throw new IOException( "Cannot find the file [" + file + "] or its format is wrong");
    }
  }
  
  /**
   * <p>This method adds all the properties contained in the file <code>file</code>. The method will try to find the file in the CLASSPATH. If the
   * file cannot be found in the CLASSPATH, then it tries to find it in the file system. And if it cannot be found in the CLASSPATH or in the
   * file system, well, it throws an exception.</p> 
   * The format <b>must</b> be the regular properties file format.
   * @param file the file
   * @throws IOException when something goes wrong
   * @throws NullPointerException if the argument <code>file</code> is null
   */
  public void addFile( File file) throws IOException {
    addFile( file.getCanonicalPath());
  }
  
  /**
   * <p>This method adds all the properties contained in the file which name is stored in the environment variable <code>varenv</code>. 
   * The method will try to find the file in the CLASSPATH. If the file cannot be found in the CLASSPATH, then it tries to find it in 
   * the file system. And if it cannot be found in the CLASSPATH or in the file system, well, it throws an exception.</p> 
   * The format <b>must</b> be the regular properties file format.
   * @param varenv the name of the environment variable that contains the path to the configuration file
   * @throws IOException when something goes wrong
   * @throws NullPointerException if the argument <code>varenv</code> is null
   */
  public void addFileFromENV( String varenv) throws IOException {
    String file = System.getenv( varenv);
    
    if ( file == null ) {
      throw new IOException( "Environment variable '" + varenv + "' does not exist");
    }
    
    addFile( file);
  }
  
  /**
   * This method add several properties in one call
   * @param params a Map with the properties to add
   */
  public void addProperties( Map<String,String> params) {
    mProps.putAll( params);
  }
  
  /**
   * This method add several objects in one call
   * @param params a Map with the objects to add
   */
  public void addObjects( Map<String,Object> params) {
    mObjects.putAll( params);
  }
  
  /**
   * This method stops the refreshing of the <code>file</code>. If the file wasn't in the list of refreshed files, the method
   * returns without error.
   * @param file the file to not refresh any more
   */
  public void stopRefreshingFile( String file) {
    if ( wd == null ) {
      return;  // No watchdog, no play
    }
    
    if ( mWatchDogAlarms.containsKey( file) ) {
      int id = mWatchDogAlarms.get( file);
      wd.removeAlarm( id);
    }
  }
  
  /**
   * This method removes the stored property identified by key
   * @param key the property identifier
   * @return the removed property or null if the key isn't in the store
   */
  public String removeProperty( String key) {
    return mProps.remove( key);
  }
  
  /**
   * This method removes the stored object identified by key
   * @param key the object identifier
   * @return the removed object or null if the key isn't in the store
   */
  public Object removeObject( String key) {
    return mObjects.remove( key);
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> without expanding possible variables, the raw value the property
   * was created with. If the key is not found then <code>null</code> is returned
   * @param key the property
   * @return the value or null if it doesn't exist
   */
  public String getPropertyRaw( String key) {
    return mProps.get( key);
  }
  
  /**
   * This method returns the value of the property identified by key
   * 
   * @param key the property
   * @return the value or null if it doesn't exist
   */
  public String getProperty( String key) {
    String value = getPropertyRaw( key);
    
    if ( value != null ) {
      value = fulfill( value);
    }
    
    return value;
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code>. If the key is not found then <code>defaultValue</code> is used
   * @param key the property
   * @param defaultValue the default value
   * @return the value or null if it doesn't exist
   */
  public String getProperty( String key, String defaultValue) {
    String res = getProperty( key); 
    
    return res == null ? defaultValue : res;
  }

  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Integer</code> value. 
   * If the key is not found then <code>null</code> is returned.
   * @param key the property
   * @return the value or null if it doesn't exist
   * @throws NumberFormatException if the value can't be converted to Integer
   */
  public Integer getPropertyAsInt( String key) {
    String res = getProperty( key); 
    
    if ( res == null ) {
      return null;
    }
    
    return Integer.parseInt( res);
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Integer</code> value. If the key is not found or 
   * the value cannot be transformed into an Integer, then <code>defaultValue</code> is used
   * @param key the property
   * @param defaultValue the default value
   * @return the value
   */
  public Integer getPropertyAsInt( String key, int defaultValue) {
    int res;
    try {
      res = getPropertyAsInt( key);
    }
    catch ( Exception ex) {
      res = defaultValue;
    }
    
    return res;
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Boolean</code> value. 
   * If the key is not found then <code>null</code> is returned.
   * @param key the property
   * @return the value or null if it doesn't exist
   * @throws NumberFormatException if the value can't be converted to Boolean
   */
  public Boolean getPropertyAsBoolean( String key) {
    String res = getProperty( key); 
    
    if ( res == null ) {
      return null;
    }
    
    return Boolean.parseBoolean( res);
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Boolean</code> value. If the key is not found or 
   * the value cannot be transformed into a Boolean, then <code>defaultValue</code> is used
   * @param key the property
   * @param defaultValue the default value
   * @return the value
   */
  public Boolean getPropertyAsBoolean( String key, boolean defaultValue) {
    Boolean res = getPropertyAsBoolean( key);
    
    if ( res == null ) {
      res = defaultValue;
    }
    
    return res;
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Long</code> value. 
   * If the key is not found then <code>null</code> is returned.
   * @param key the property
   * @return the value or null if it doesn't exist
   * @throws NumberFormatException if the value can't be converted to Long
   */
  public Long getPropertyAsLong( String key) {
    String res = getProperty( key); 
    
    if ( res == null ) {
      return null;
    }
    
    return Long.parseLong( res);
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Long</code> value. If the key is not found or 
   * the value cannot be transformed into a Long, then <code>defaultValue</code> is used
   * @param key the property
   * @param defaultValue the default value
   * @return the value
   */
  public Long getPropertyAsLong( String key, Long defaultValue) {
    Long res = getPropertyAsLong( key);

    if ( res == null ) {
      res = defaultValue;
    }
    
    return res;
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Double</code> value. 
   * If the key is not found then <code>null</code> is returned.
   * @param key the property
   * @return the value or null if it doesn't exist
   * @throws NumberFormatException if the value can't be converted to Double
   */
  public Double getPropertyAsDouble( String key) {
    String res = getProperty( key); 
    
    if ( res == null ) {
      return null;
    }
    
    return Double.parseDouble( res);
  }
  
  /**
   * This method returns the value of the property identified by <code>key</code> as a <code>Double</code> value. If the key is not found or 
   * the value cannot be transformed into a Double, then <code>defaultValue</code> is used
   * @param key the property
   * @param defaultValue the default value
   * @return the value
   */
  public Double getPropertyAsDouble( String key, Double defaultValue) {
    Double res = getPropertyAsDouble( key);

    if ( res == null ) {
      res = defaultValue;
    }
    
    return res;
  }
  
  

  /**
   * This method sets the value of a property
   * 
   * @param key the key of the property
   * @param value the value for the property
   */
  public void setProperty( String key, String value) {
    synchronized ( mProps ) {
      mProps.put( key, value);
    }
  }  
  
  /**
   * This method sets the value of a property
   * 
   * @param key the key of the property
   * @param value the value for the property
   */
  public void setProperty( String key, int value) {
    setProperty( key, value + "");
  }    
  
  /**
   * This method sets the value of a property
   * 
   * @param key the key of the property
   * @param value the value for the property
   */
  public void setProperty( String key, long value) {
    setProperty( key, value + "");
  }  
  
  /**
   * This method sets the value of a property
   * 
   * @param key the key of the property
   * @param value the value for the property
   */
  public void setProperty( String key, double value) {
    setProperty( key, value + "");
  }
  
  /**
   * This method sets the value of a property
   * 
   * @param key the key of the property
   * @param value the value for the property
   */
  public void setProperty( String key, boolean value) {
    setProperty( key, value + "");
  }
  

  
  /**
   * This method returns the stored object identified by key
   * @param key the object identifier
   * @return the object identified by key or null if there isn't an object for key
   */
  public Object getObject( String key) {
    return mObjects.get( key);
  }
  
  /**
   * This method adds an object to the collection. The object <code>obj</code> will be identified by <code>key</code>
   * @param key the object identifier
   * @param obj the object to be stored
   */
  public void setObject( String key, Object obj) {
    synchronized ( mObjects ) {
      mObjects.put( key, obj);
    }
  }
  
  
  
  /**
   * <p>This method substitutes all the keys contained between "${" and "}" by its current value and returns the resulting String. If a key
   * doesn't have a value, the key is left in the String without changes.</p>
   * Really, the token markers are TK_BEGIN and TK_END, which values are ${ and }.
   * @param str the String to be fulfilled
   * @return the fulfilled String or null if <code>str</code> is null
   * @throws IllegalArgumentException when a key hasn't a finalizer ( "blablabla ${key blablabla")
   */
  public String fulfill( String str) {
  	String res = realFulfill( str);
  	
  	if ( res != null ) {
  	  res = Utils.changeAll(res, D_TK_BEGIN, TK_BEGIN);
  	  res = Utils.changeAll(res, D_TK_END, TK_END);
  	}
  	
  	return res;
  }
  
  private String realFulfill( String str) {
    if ( str == null ) {
      return null;
    }
    
    StringBuilder res = new StringBuilder();
    
    int ix = str.indexOf( TK_BEGIN);
    
    if ( ix == -1 ) {
      return str;
    }
    
    int mx = str.indexOf( TK_BEGIN, ix+1); 
    int fx = str.indexOf( TK_END, ix);
    
    if ( fx == -1 ) {
      return str;
    }
    
    res.append( str.substring( 0, ix));
    
    if ( mx == -1 || mx > fx ) { // There isn't a ${ between the first ${ and the }, so, there isn't inner variables like "gato${var${num}}pardo"
      String key = str.substring( ix+TK_BEGIN.length(), fx);
      String val = getPropertyRaw( key);
      
      if ( val != null ) {
        res.append( val);
      }
      else {
        res.append( D_TK_BEGIN + key + D_TK_END);  // No value for variable, so let the variable
      }
    }
    else {
      String inner = realFulfill( str.substring( mx, fx+1));
      res.append( str.substring( ix, mx));
      res.append( inner);
    }
    
    res.append( str.substring( fx + TK_END.length()));
    
    if ( res.toString().indexOf( TK_BEGIN) != -1 ) {
      return realFulfill( res.toString());
    }
    else {
      return res.toString();
    }
  }
  
  /**
   * This method returns an XML representation of all the properties stored in the Props.
   * @return an XML with all the properties
   */
  public String toXML() {
    StringBuilder res = new StringBuilder();
    
    for ( String key : getKeys() ) {
      res.append( "<prop key='" + key + "'><![CDATA[" + mProps.get( key) + "]]></prop>\n");
    }
    
    return res.toString();
  }
  
  @Override
  public String toString() {
    StringBuilder res = new StringBuilder();
    
    for ( String key : getKeys() ) {
      res.append( key);
      res.append( "=");
      res.append( mProps.get( key));
      res.append( "\n");
    }
    
    return res.toString();
  }
  
  /**
   * This method returns the JSON representation of the properties store
   * @return the JSON representation of the properties store
   */
  @JsonValue
  @JsonRawValue
  public String toJSON() {
    StringBuilder res = new StringBuilder( "{");
    
    boolean first = true;
    
    for ( String key : getKeysSorted() ) {
      if ( !first ) {
        res.append( ",");
      }
      first = false;
      
      res.append( "\"");
      res.append( key);
      res.append( "\": \"");
      res.append( getPropertyRaw( key));
      res.append( "\"");
    }
    
    return res.toString() + "}";
  }
  
  /**
   * This method returns a properties store with the data from the <code>json</code> representation of a properties store
   * @param json the JSON representation of a properties store
   * @return a properties store with the data from the <code>json</code> representation of a properties store
   */
  @SuppressWarnings( "unchecked")
  public static Props fromJSON( String json) {
    Props res = new Props( true);
    
    res.mProps = Formats.fromJson( json, res.mProps.getClass());
    res.addSystemProperties();
    
    return res;
  }
  
}
