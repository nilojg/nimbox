package es.nimbox.box;

/**
 * The listener interface to receive WatchDog events
 *  
 * @author nilo
 *
 */
public interface WatchDogListener {

  /**
   * invoked when an alert is raised
   * @param ev the event associated to the alert
   */
  public void watchDogAlert( WatchDogEvent ev);
}
