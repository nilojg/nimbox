
package es.nimbox.box;

import java.lang.annotation.*;

/**
 * <p>This annotation defines that a class attribute will be translated to XML as an XML element by the {@link XMLUtils#toXML(Object)}
 * methods.</p>
 * <p>There's a more complete explanation of the translation to XML in the {@link XMLClass} javadoc.</p> 
 * 
 * @see XMLClass
 * @see XMLAttribute
 * @see XMLUtils#toXML(Object)
 * 
 * @author nilo
 */
@Retention(value=RetentionPolicy.RUNTIME) 
@Target(value=ElementType.FIELD)
public @interface XMLElement {
  /**
   * The tag name. If it isn't defined, the variable name will be used 
   * @return the name of the tag
   */
  String name() default "";
}
