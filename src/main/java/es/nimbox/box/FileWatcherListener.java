package es.nimbox.box;

/**
 * This interface declares the methods that will process the events thrown by the FileWatcher
 * 
 * @author nilo
 *
 */
public interface FileWatcherListener {
  /**
   * invoked when an alert is raised
   * @param ev the event associated to the alert
   */
  public void fileWatcherAlert( FileWatcherEvent ev);
}
