package es.nimbox.box;


import java.util.concurrent.ThreadLocalRandom;

/**
 * This class implements a UUID generator. The class does its best to get a really unique identificator using
 * IP address, current time and random numbers, and allows you to add your preferred String if you want to add
 * something to the resulting UUID.<br>
 * It's easy to use as a class with static methods:
 * <pre>
 * String uid1 = UIDGen.UID();
 * // something like "c0a80226-1576003225353-a8bba7c1ce4eb99deaf0"
 * String uid2 = UIDGen.UID( "hola");
 * // something like "c0a80226-1576003225353-50404176c9a1a60883b2-hola"
 * 
 * UIDGen.getGenerator().generateWithIp( false);
 * String uid3 = UIDGen.UID();
 * // something like "1576003164769-474ff5b16ddd85dd8cbc"
 * </pre>
 * The String <code>uid1</code> is created with the IP, the current time and a random number. The String <code>uid2</code>
 * adds the String passed as parameter to the end. All the "numerical" values are printed in hexadecimal, and there's
 * a separator between they.<br>
 * It can be created as an object too, for example, if you need to have two or more different UUID types, or simply prefer
 * to use it in a framework as Spring. Create all the UIDGen you need, and use them in your way, for example to auto-wire the
 * UIDGen in your classes:
 * <pre>
 * UIDGen gen1 = new UIDGen();
 * String uid1 = gen1.getUID();
 * // something like "c0a80226-1576003225353-6ea7fcfc79741dcf8e34"
 * 
 * UIDGen gen2 = new UIDGen();
 * gen2.setSeparator( "**");
 * // something like "c0a80226**1576003225353**fbbbf99c6e88edab725b" 
 * </pre> 
 * To use it in a Spring application, for example in an SpringBoot application, you can create and customize the UIDGen in a 
 * method annotated as <code>@Bean</code> and simply autowire it in the classes that need to create an UID.<br>
 *
 * <pre>
 * &#64;Bean
 * public UIDGen uuidGenerator() {
 *   UIDGen gen = new UIDGen();
 *   gen.setSeparator( "**");
 *
 *   return gen;
 * }
 *
 * class Something {
 *   ...
 *   &#64;Autowired
 *   UIDGen uuidGen;
 *   ...
 * }
 * </pre>
 *   
 * The <code>UIDGen</code> can be customized in same ways:
 * <ul>
 *   <li>the method <code>generateWithIp( boolean)</code> enables or disables the use of the IP in the generation of the UUID.</li>
 *   <li>the method <code>generateWithTime( boolean)</code> enables or disables the use of the current time in the generation of the UUID.</li>
 *   <li>the method <code>generateWithRandom( boolean)</code> enables or disables the use of random numbers in the generation of the UUID.</li>
 *   <li>the method <code>setRandomSize( String)</code> allows you to choose the size of the random number, in bytes, so the number of chars
 *       in the generated UUID is double and always a pair number. By default, <code>DEFAULT_RANDOM_SIZE</code>.</li>
 *   <li>the method <code>generateWithSeparator( boolean)</code> enables or disables the use of a separator between the different parts
 *       of the UUID.</li>
 *   <li>the method <code>setSeparator( String)</code> allows you to choose the separator. By default, <code>DEFAULT_SEPARATOR</code>.</li>
 * </ul>
 * 
 * All these options should be enough to guarantee that the UUID is really unique. Even the time and the random number should be enough
 * to guarantee the uniqueness of the ID. The IP could be necessary in a High Availability environment, with several servers executing
 * the application, but determining the local IP is <i>really</i> tough in Java, because the local machine could have (and will have)
 * several net interfaces and it's not possible to be sure that the chosen IP is the desired IP.<br>
 * First it tries to connect to the default internet server, <code>DEFAULT_IP_PORTAL</code> (Stackoverflow), to get the local main IP. 
 * If it doesn't work, most probably because navigation to internet is prohibited in the server, then it tries to get any IP in the localhost,
 * which can be loopback IP. The methods <code>setIpTestPortal</code> and <code>setIpTestPort</code> allows you to change the address to
 * use to find the local IP, so you can set an IP in your internal net with granted access from the JVM.<br>
 * 
 * @author nilo
 *
 */
public class UIDGen {

  /**
   * The default UUID section separator, a hyphen
   */
  public static final String DEFAULT_SEPARATOR    = "-";
  
  /**
   * The default UUID random part size, 10 characters
   */
  public static final int    DEFAULT_RANDOM_SIZE  = 10;
  

  private boolean bIp = true;
  private boolean bTime = true;
  private boolean bRandom = true;
  private boolean bSeparator = true;
  private boolean bThreadNum = true;
  
  private String ipPortal = Utils.DEFAULT_IP_PORTAL;
  private int ipPort = Utils.DEFAULT_IP_PORT;
  
  private int randomSize = DEFAULT_RANDOM_SIZE;
  
  private String separator = DEFAULT_SEPARATOR;
  private String ownIP;

  private static UIDGen common;

  /**
   * The constructor creates an <code>UIDGen</code> object that generates UUIDs with IP, current time and a random number of
   * ten bytes. The separator is a '-' character.
   */
  public UIDGen() {
    super();

    reset();
  }
  
  /**
   * This method returns a singleton instance of the <code>UIDGen</code>
   * @return a singleton instance of the <code>UIDGen</code>
   */
  public static UIDGen getGenerator() {
    if ( common == null ) {
      common = new UIDGen();
    }
    
    return common;
  }
  
  /**
   * This method returns a UUID using the singleton instance
   * @return a UUID
   */
  public static String UID() {
    return getGenerator().getUID();
  }
  
  /**
   * This method returns a UUID using the singleton instance with the <code>cad</code> String appended in the end
   * @param cad the String appended to the end of the UUID
   * @return and UUID
   */
  public static String UID( String cad) {
    return getGenerator().getUID( cad);
  }
  
  
  
  /**
   * This method returns a UUID
   * @return a UUID
   */
  public String getUID() {
    return getUID( null);
  }

  /**
   * This method returns a UUID with the <code>cad</code> String appended in the end of the UUID
   * @param cad the String appended to the end of the UUID
   * @return and UUID
   */
  public String getUID( String cad) {
    StringBuilder res = new StringBuilder();

    boolean ant = calcUUID( res);

    if ( cad != null ) {
      if ( ant ) {
        res.append( separator);
      }
      res.append( cad);
    }

    return res.toString();
  }

  /**
   * This method returns a UUID with the <code>cad</code> String appended in the end of the UUID
   * @param cad the String appended to the end of the UUID
   * @return and UUID
   */
  public String getUIDpostfix( String cad) {
    return getUID( cad);
  }

  /**
   * This method returns an UID with the <code>cad</code> String appended in the start of the UID
   * @param cad the String appended to the end of the UID
   * @return and UID
   */
  public String getUIDprefix( String cad) {
    StringBuilder res = new StringBuilder();

    boolean ant = calcUUID( res);

    if ( cad != null ) {
      cad += ant ? separator : "";

      res.insert( 0, cad);
    }

    return res.toString();
  }

  private boolean calcUUID( StringBuilder res) {
    boolean ant = false;

    if ( bIp ) {
      res.append( Utils.getIP( ipPortal, ipPort));
      ant = true;
    }

    if ( bThreadNum ) {
      if ( ant && bSeparator ) {
        res.append( separator);
      }
      res.append( Thread.currentThread().getId());
      ant = true;
    }

    if ( bTime ) {
      if ( ant && bSeparator ) {
        res.append( separator);
      }
      res.append( System.currentTimeMillis());
      ant = true;
    }

    if ( bRandom ) {
      if ( ant && bSeparator ) {
        res.append( separator);
      }
      res.append( getRandom());
      ant = true;
    }


    return ant;
  }
  
  /**
   * This method resets the <code>UIDGen</code> to generate UUIDs with IP, current time and a random number of
   * ten bytes. The separator is a '-' character.
   */
  public void reset() {
    generateWithIp( true);
    generateWithTime( true);
    generateWithRandom( true);
    generateWithSeparator( true);
    setRandomSize( DEFAULT_RANDOM_SIZE);
    setSeparator( DEFAULT_SEPARATOR);
  }
  
  /**
   * This method returns the Internet address that will be used to figure out the output net interface. 
   * @return the Internet address that will be used to figure out the output net interface.
   */
  public String getIpTestPortal() {
    return ipPortal;
  }
  
  /**
   * This method sets the Internet address that will be used to figure out the output net interface. 
   * @param ipPortal the Internet address that will be used to figure out the output net interface. 
   */
  public void setIpTestPortal( String ipPortal) {
    this.ipPortal = ipPortal;
    ownIP = null;
  }
  
  /**
   * This method returns the port that will be used to figure out the output net interface. 
   * @return the port that will be used to figure out the output net interface.
   */
  public int getIpTestPort() {
    return ipPort;
  }
  
  /**
   * This method sets the port that will be used to figure out the output net interface. 
   * @param ipPort the port that will be used to figure out the output net interface. 
   */
  public void setIpTestPort( int ipPort) {
    this.ipPort = ipPort;
    ownIP = null;
  }
  
  /**
   * This method returns the separator
   * @return the separator
   */
  public String getSeparator() {
    return separator;
  }
  
  /**
   * This method sets the separator
   * @param separator the separator
   */
  public void setSeparator( String separator) {
    this.separator = separator;
  }
  
  /**
   * This method sets the size of the random number. The size of the random number part in the UUID will be
   * 2*<code>size</code> characters.
   * @param size the size of the random number
   */
  public void setRandomSize( int size) {
    randomSize = size;
  }
  
  /**
   * This method returns the size of the random number.
   * @return the size of the random number
   */
  public int getRandomSize() {
    return randomSize;
  }

  /**
   * This method returns <code>true</code> if the UUIDs will have an IP part, and <code>false</code> otherwise.
   * @return <code>true</code> if the UUIDs will have an IP part, and <code>false</code> otherwise.
   */
  public boolean containsIp() {
    return bIp;
  }

  /**
   * This method indicates if the <code>UIDGen</code> will generate UUIDs with an IP part.
   * @param bIp <code>true</code> if the generated UUIDs will have an IP part, and <code>false</code> otherwise.
   */
  public void generateWithIp( boolean bIp) {
    this.bIp = bIp;
  }

  /**
   * This method returns <code>true</code> if the UUIDs will have a current time part, and <code>false</code> otherwise.
   * @return <code>true</code> if the UUIDs will have a current time part, and <code>false</code> otherwise.
   */
  public boolean containsTime() {
    return bTime;
  }

  /**
   * This method indicates if the <code>UIDGen</code> will generate UUIDs with a current time part.
   * @param bTime <code>true</code> if the generated UUIDs will have a current time part, and <code>false</code> otherwise.
   */
  public void generateWithTime( boolean bTime) {
    this.bTime = bTime;
  }

  /**
   * This method returns <code>true</code> if the UUIDs will have a random number part, and <code>false</code> otherwise.
   * @return <code>true</code> if the UUIDs will have a random number part, and <code>false</code> otherwise.
   */
  public boolean containsRandom() {
    return bRandom;
  }

  /**
   * This method indicates if the <code>UIDGen</code> will generate UUIDs with a random number part.
   * @param bRandom <code>true</code> if the generated UUIDs will have a random number part, and <code>false</code> otherwise.
   */
  public void generateWithRandom( boolean bRandom) {
    this.bRandom = bRandom;
  }

  /**
   * This method returns <code>true</code> if the UUIDs will have a separator between parts, and <code>false</code> otherwise.
   * @return <code>true</code> if the UUIDs will have a separator between parts, and <code>false</code> otherwise.
   */
  public boolean containsSeparator() {
    return bSeparator;
  }

  /**
   * This method indicates if the <code>UIDGen</code> will generate UUIDs with a separator between parts.
   * @param bSeparator <code>true</code> if the generated UUIDs will have a separator between parts, and <code>false</code> otherwise.
   */
  public void generateWithSeparator( boolean bSeparator) {
    this.bSeparator = bSeparator;
  }
  
  /**
   * This method returns <code>true</code> if the UUIDs will have a current thread part, and <code>false</code> otherwise.
   * @return <code>true</code> if the UUIDs will have a current thread part, and <code>false</code> otherwise.
   */
  public boolean containsThread() {
    return bThreadNum;
  }

  /**
   * This method indicates if the <code>UIDGen</code> will generate UUIDs with a current thread part.
   * @param bThreadNum <code>true</code> if the generated UUIDs will have a current thread part, and <code>false</code> otherwise.
   */
  public void generateWithThread( boolean bThreadNum) {
    this.bThreadNum = bThreadNum;
  }
  

  /**
   * This method returns a random String
   * @return a random String
   */
  protected  String getRandom() {
    byte bufRandom[] = new byte[randomSize];
    ThreadLocalRandom.current().nextBytes( bufRandom);
    
    return Formats.getHexDump( bufRandom);
  }
}

