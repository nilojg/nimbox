package es.nimbox.box;

import java.io.File;

/**
 * This class contains some information indicating that a change in a file or directory has been detected by a FileWatcher. 
 * This event is passed as argument to a FileWatcherListener instance registered in the FileWatcher.
 * 
 * @author nilo
 */
public class FileWatcherEvent {
  private static long ID_SEED = 0;

  private long eventId;
  private long eventTime;
  private int alarmId;
  private File file;

  private long eventType = 0x00;
  
  private long oldSize = -1;
  private long newSize = -1;
  private long oldTime = -1;
  private long newTime = -1;
  private String oldMD5 = null;
  private String newMD5 = null;
  private File oldDirContent[] = null;
  private File newDirContent[] = null;
  
  /**
   * This constructor creates a new event for the <code>alarmId</code> change relative to the <code>file</code> file. 
   * @param alarmId the alarm that throws the event
   * @param file the file that raises the event
   */
  public FileWatcherEvent( int alarmId, File file) {
    synchronized ( FileWatcherEvent.class) {
      this.eventId = ID_SEED++;
    }
    
    this.eventTime = System.currentTimeMillis();
    this.alarmId = alarmId;
    this.file = file;
  }
  
  /**
   * This method fulfills the attributes of a event thrown by a change of the size of a file   
   * @param oldSize the old size of the file
   * @param newSize the new size of the file
   */
  public void doEventSize( long oldSize, long newSize) {
    eventType |= FileWatcher.CHANGE_SIZE;
    this.oldSize = oldSize;
    this.newSize = newSize;
  }
  
  /**
   * This method fulfills the attributes of a event thrown by a change of the modification time of a file
   * @param oldTime the old modification time
   * @param newTime the new modification time
   */
  public void doEventTime( long oldTime, long newTime) {
    eventType |= FileWatcher.CHANGE_TIME;
    this.oldTime = oldTime;
    this.newTime = newTime;
  }
  
  /**
   * This method fulfills the attributes of a event thrown by a change of the MD5 hash of a file.
   * @param oldMD5 the old MD5 hash
   * @param newMD5 the new MD5 hash
   */
  public void doEventMD5( String oldMD5, String newMD5) {
    eventType |= FileWatcher.CHANGE_MD5;
    this.oldMD5 = oldMD5;
    this.newMD5 = newMD5;
  }

  /**
   * This method fulfills the attributes of a event thrown by the creation of a file
   */
  public void doEventCreate() {
    eventType |= FileWatcher.FILE_CREATE;
  }
  
  /**
   * This method fulfills the attributes of a event thrown by the deletion of a file
   */
  public void doEventDelete() {
    eventType |= FileWatcher.FILE_DELETE;
  }
  
  /**
   * This method fulfills the attributes of a event thrown by the changes in the content of a directory.
   * @param oldDirContent an array with the contents of the directory previously the change
   * @param newDirContent an array with the contents of the directory after the change
   */
  public void doEventDirChange( File oldDirContent[], File newDirContent[]) {
    eventType |= FileWatcher.CHANGE_DIR;
    this.oldDirContent = oldDirContent;
    this.newDirContent = newDirContent;
  }
  

  @Override
  public String toString() {
    String res = "FileWatcherEvent [eventId=" + eventId + ", eventTime=" + eventTime + ", alarmId=" + alarmId + 
                 ", file=" + file.getAbsolutePath() + 
                 ", eventType=" + Long.toBinaryString( eventType) +
                 ", oldSize=" + oldSize + ", newSize=" + newSize + 
                 ", oldTime=" + oldTime + ", newTime=" + newTime + 
                 ", oldMD5=" + oldMD5 + ", newMD5=" + newMD5 + "]";
    if ( oldDirContent != null ) {
      res += "\nOld content:";
      for ( File ff : oldDirContent ) {
        res += "\n- " + ff.getAbsolutePath();
      }
    }

    if ( newDirContent != null ) {
      res += "\nNew content:";
      for ( File ff : newDirContent ) {
        res += "\n- " + ff.getAbsolutePath();
      }
    }
    
    return res;
  }

  /**
   * Returns the id of the event
   * @return the id of the event
   */
  public long getEventId() {
    return eventId;
  }

  /**
   * Returns the time of the event
   * @return the time of the event
   */
  public long getEventTime() {
    return eventTime;
  }

  /**
   * Returns the id of the alarm
   * @return the id of the alarm
   */
  public int getAlarmId() {
    return alarmId;
  }

  /**
   * Returns the name of the file
   * @return the name of the file
   */
  public File getFile() {
    return file;
  }
  
  /**
   * This method sets the file name of the changed file
   * @param file the file name
   */
  public void setFile( File file) {
    this.file = file;
  }

  
  /**
   * Returns the type of the event
   * @return the type of the event
   */
  public long getEventType() {
    return eventType;
  }

  /**
   * This method sets the type of the event
   * @param eventType the type of the event
   */
  public void setEventType( long eventType) {
    this.eventType = eventType;
  }

  /**
   * Returns the file size before the event
   * @return the file size before the event
   */
  public long getOldSize() {
    return oldSize;
  }

  /**
   * This method sets the file size before the event
   * @param oldSize the file size before the event
   */
  public void setOldSize( long oldSize) {
    this.oldSize = oldSize;
  }

  /**
   * Returns the file size after the event
   * @return the file size after the event
   */
  public long getNewSize() {
    return newSize;
  }

  /**
   * This method sets the file size after the event
   * @param newSize the file size after the event
   */
  public void setNewSize( long newSize) {
    this.newSize = newSize;
  }

  /**
   * Returns the file last modification time before the event
   * @return the file last modification time before the event
   */
  public long getOldTime() {
    return oldTime;
  }

  /**
   * This method set the file last modification time before the event
   * @param oldTime the file last modification time before the event
   */
  public void setOldTime( long oldTime) {
    this.oldTime = oldTime;
  }
  /**
   * Returns the file last modification time after the event
   * @return the file last modification time after the event
   */
  public long getNewTime() {
    return newTime;
  }

  /**
   * This method set the file last modification time after the event
   * @param newTime the file last modification time after the event
   */
  public void setNewTime( long newTime) {
    this.newTime = newTime;
  }
  
  /**
   * Returns the file MD5 hash before the event
   * @return the file MD5 hash before the event
   */
  public String getOldMD5() {
    return oldMD5;
  }

  /**
   * This method sets the MD5 hash of the file before the event
   * @param oldMD5 the MD5 hash of the file before the event
   */
  public void setOldMD5( String oldMD5) {
    this.oldMD5 = oldMD5;
  }
  
  /**
   * Returns the file MD5 hash after the event
   * @return the file MD5 hash after the event
   */
  public String getNewMD5() {
    return newMD5;
  }

  /**
   * This method sets the MD5 hash of the file after the event
   * @param newMD5 the MD5 hash of the file after the event
   */
  public void setNewMD5( String newMD5) {
    this.newMD5 = newMD5;
  }
  
  /**
   * Returns a list with the contents of the directory before the event
   * @return a list with the contents of the directory before the event
   */
  public File[] getOldDirContent() {
    return oldDirContent;
  }

  /**
   * This method sets the contents of the directory before the event
   * @param oldDirContent the contents of the directory before the event
   */
  public void setOldDirContent( File[] oldDirContent) {
    this.oldDirContent = oldDirContent;
  }
  
  /**
   * Returns a list with the contents of the directory after the event
   * @return a list with the contents of the directory after the event
   */
  public File[] getNewDirContent() {
    return newDirContent;
  }

  /**
   * This method sets the contents of the directory after the event
   * @param newDirContent the contents of the directory after the event
   */
  public void setNewDirContent( File[] newDirContent) {
    this.newDirContent = newDirContent;
  }
  
  
}
