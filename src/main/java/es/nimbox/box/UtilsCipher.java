package es.nimbox.box;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

/**
 * <p>This class contains some utility methods about cryptography. It doesn't try to be great or complete, just to be useful
 * to encrypt some little things, like configuration properties like passwords, or some messages that could be stored in files
 * or sent by the network. If you need <i>real</i> cryptography you should read something about it and get a better implementation
 * that the default implementation from the JDK. </p>
 * <p>The class handles two kind of methods:</p>
 * <ul>
 *   <li>Symmetric key : this thing uses the same key (password) to encrypt and decrypt the message. This is the cryptography
 *                       used in the movies</li>
 *   <li>Asymmetric keys : this thing uses two different keys, one to encrypt and another one to decrypt the message. This is
 *                         the cryptography usually named "public key cryptography", and is the good one, but is "expensive"
 *                         because it needs more computer time</li>
 * </ul>
 * <p>There's a really huge amount of theory about cryptography, because in the real world, cryptography is a cool way to say "math".
 * In fact, "key" or "password" is "a number", so if you say that your password is "coconut", first the String "coconut" is converted 
 * in a number and then the "cryptographic machine" can start to work. This is more obvious if you use asymmetric cryptography, 
 * because public and private keys are numbers, perhaps you see something like a long line of '0d01010105000382010f003082010a028',
 * but that thing is in fact a number. And the key is better as longer it is...</p>
 * <p>The algorithms used to encrypt an decrypt are fixed and cannot be changed. </p>
 * <ul>
 *   <li>Symmetric key : AES/CBC/PKCS5Padding, with 256 bits keys</li>
 *   <li>Asymmetric keys : RSA/ECB/OAEPWithSHA1AndMGF1Padding, with 2048 bits keys</li>
 * </ul>  
 * <p>They are quite solid algorithms, but, again, if you need something really professional or more customized, use the JDK security
 * API.</p>
 * <p>There are very easy to use methods to generate new keys:</p>
 * <pre>
 * // Symmetric key, only one key to encrypt and decrypt
 * Key key = UtilsCipher.generateSymmetricKey();
 * 
 * // Asymmetric keys, one key for encrypt and another one to decrypt
 * KeyPair key = UtilsCipher.generateAsymmetricKeys();
 * Key public = key.getPublic();
 * Key private = key.getPrivate();
 * </pre>
 * 
 * <p>The keys can be created using previously stored values:</p>
 * <pre>
 * // Symmetric key, only one key to encrypt and decrypt
 * Key key = UtilsCipher.doSimmetricKeyWithPassword( "coconut");
 * 
 * // Asymmetric keys, one key for encrypt and another one to decrypt
 * Key public = UtilsCipher.doAsimmetricPublicKey( "0d01010105000....");
 * Key private = UtilsCipher.doAsimmetricPrivateKey( "4514724057134....");
 * </pre>
 * 
 * <p>So you could generate the keys with the <code>UtilsCipher.generate*</code> methods and store their values, returned by 
 * the <code>key.getEncoded()</code> method, in a file or something like that. Then you can send the key that the other part 
 * will need to decrypt the message by whatever method, and use the <code>UtilsCipher.do*</code> methods to regenerate the key
 * and then, decrypt the message.</p>
 * <p>To encrypt and decrypt a message is very easy. If you are using a symmetric algorithm is just:</p>
 * <pre>
 * // Generate the key
 * Key key = UtilsCipher.generateSymmetricKey();
 * 
 * // Create the cipher, setting the <code>UtilsCipher.TYPE_SYMMETRIC</code> to choose a symmetric cipher
 * UtilsCipher cip = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
 * cip.setKey( key);
 * 
 * // Encrypt
 * byte theEncryptedBuf[] = cip.encode( "My tailor is rich", "UTF-8");
 * 
 * // This is the key, and you can do something with it, like store it
 * Key key = cip.getKey();
 * storeKey( key.getEncoded());
 * 
 * ...
 * 
 * // Decrypt
 * UtilsCipher cip = new UtilsCipher( UtilsCipher.TYPE_SYMMETRIC);
 * 
 * byte storedKey[] = readKeyFromStore();
 * Key key = UtilsCipher.doSimmetricKeyWithPassword( storedKey);
 * 
 * cip.setKey( key);
 * 
 * String clear = cip.decode( theEncryptedBuf, "UTF-8");
 * </pre>
 * <p>If you are using an asymmetric algorithm is roughly the same, but in this case, you must remember that in this case you have
 * TWO keys, that use to be named "public" and "private", so encryption is done with a key, public or private, and decryption is
 * done with the other one, private or public.</p>
 * <pre>
 * // Generate the keys
 * KeyPair keyPair = UtilsCipher.generateAsymmetricKeys();
 * 
 * // Create the cipher, setting the <code>UtilsCipher.TYPE_ASYMMETRIC</code> to choose a asymmetric cipher
 * UtilsCipher cip = new UtilsCipher( UtilsCipher.TYPE_ASYMMETRIC);
 * 
 * // Encrypt, with one key, private key
 * cip.setKey( keyPair.getPrivate());
 * byte theEncryptedBuf[] = cip.encode( "My tailor is rich", "UTF-8");
 * 
 * // This are the keys, and you can do something with them, like store them
 * storePrivateKey( keyPair.getPrivate().getEncoded());
 * storePublicKey( keyPair.getPuplic().getEncoded());
 * ...
 * 
 * // Decrypt
 * UtilsCipher cip = new UtilsCipher( UtilsCipher.TYPE_ASYMMETRIC);
 * 
 * byte storedKey[] = readPublicKeyFromStore();
 * Key key = UtilsCipher.doAsimmetricPublicKey( storedKey);
 * 
 * cip.setKey( key);
 * 
 * String clear = cip.decode( theEncryptedBuf, "UTF-8");
 * </pre>
 * 
 * And that's all. Remember that there's a lot of theory about cryptography and that this is a very simple class to handle
 * little messages.
 * 
 * @author nilo
 */
public class UtilsCipher {
  /**
   * Symmetric cipher
   */
  public static final int TYPE_SYMMETRIC  = 0;
  
  /**
   * Asymmetric cipher
   */
  public static final int TYPE_ASYMMETRIC = 1;

  /**
   * AES
   */
  public static final String DEFAULT_SYMMETRIC_ALGORITHM  = "AES";
  
  /**
   * AES/CBC/PKCS5Padding
   */
  public static final String DEFAULT_SYMMETRIC_CIPHER     = "AES/CBC/PKCS5Padding";
  
  /**
   * 256
   */
  public static final int    DEFAULT_SYMMETRIC_SIZE       = 256;

  /**^
   * RSA
   */
  public static final String DEFAULT_ASYMMETRIC_ALGORITHM = "RSA";
  
  /**
   * RSA/ECB/OAEPWithSHA1AndMGF1Padding
   */
  public static final String DEFAULT_ASYMMETRIC_CIPHER    = "RSA/ECB/OAEPWithSHA1AndMGF1Padding";
  
  /**
   * 2048
   */
  public static final int    DEFAULT_ASYMMETRIC_SIZE      = 2048;

  private Cipher ciEncode;
  private Cipher ciDecode;

  private int type;
  private Key key;
  
  private static SecureRandom secureRandom;
  
  /**
   * This constructor initializes the class. It receives a single parameter to indicate what kind of cippher will be used. 
   * @param type TYPE_SYMMETRIC or TYPE_ASYMMETRIC
   * @throws NoSuchAlgorithmException if <code>type</code> is not TYPE_SYMMETRIC or TYPE_ASYMMETRIC
   */
  public UtilsCipher( int type) throws NoSuchAlgorithmException {
  	this.type = type;

  	init();
  }
  
  
  private void init() throws NoSuchAlgorithmException {
  	if ( key == null ) {
  		switch ( type ) {
  		case TYPE_SYMMETRIC : 
  			//key = generateSymmetricKey();
  			break;
  		case TYPE_ASYMMETRIC : 
  			break;
  		default : throw new IllegalArgumentException( "Invalid type. Only allowed types are TYPE_SYMMETRIC and TYPE_ASYMMETRIC.");
  		}
  	}
  }
  
  /**
   * This method returns a new symmetric key for AES algorithm of 256 bits
   * @return the new key
   * @throws NoSuchAlgorithmException if your JRE doesn't support the AES algorithm
   */
  public static Key generateSymmetricKey() throws NoSuchAlgorithmException {
    return generateSymmetricKey( DEFAULT_SYMMETRIC_SIZE);
  }
  
  /**
   * This method returns a new symmetric key for AES algorithm of <code>size</code> bits
   * @param size the key size
   * @return the new key
   * @throws NoSuchAlgorithmException if your JRE doesn't support the AES algorithm
   */
  public static Key generateSymmetricKey( int size) throws NoSuchAlgorithmException {
    KeyGenerator keyGenerator = KeyGenerator.getInstance( DEFAULT_SYMMETRIC_ALGORITHM);

    keyGenerator.init( size, getSecureRamdom());

    return keyGenerator.generateKey();
  }
  
  /**
   * This method generates a new pair of asymmetric keys for RSA algorithm of 2048 bits
   * @return the new keys
   * @throws NoSuchAlgorithmException if your JRE doesn't support the RSA algorithm
   */
  public static KeyPair generateAsymmetricKeys() throws NoSuchAlgorithmException {
    return generateAsymmetricKeys( DEFAULT_ASYMMETRIC_SIZE);
  }
  
  /**
   /**
   * This method generates a new pair of asymmetric keys for RSA algorithm of 2048 bits of <code>size</code> bits
   * @param size the size
   * @return the new keys
   * @throws NoSuchAlgorithmException if your JRE doesn't support the RSA algorithm
   */
  public static KeyPair generateAsymmetricKeys( int size) throws NoSuchAlgorithmException {
    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance( DEFAULT_ASYMMETRIC_ALGORITHM);
    keyPairGenerator.initialize( size);
    
    return keyPairGenerator.generateKeyPair();
  }
  
  /**
   * This method creates a key using the <code>password</code>, for example, "coconut". If <code>password</code> length is too
   * big or too little, its size will be adapted. The perfect password is DEFAULT_SYMMETRIC_SIZE bits, so DEFAULT_SYMMETRIC_SIZE / 8
   * bytes. 
   * @param password the password to use
   * @return the key
   */
  public static Key doSimmetricKeyWithPassword( String password) {
  	return new SecretKeySpec( normalizePasswdSize( password, DEFAULT_SYMMETRIC_SIZE / 8), 
  			                      DEFAULT_SYMMETRIC_ALGORITHM);
  }
  
  /**
   * This method creates a key using the <code>password</code> as an <code>byte[]</code>. If <code>password</code> length is too
   * big or too little, its size will be adapted. The perfect password is DEFAULT_SYMMETRIC_SIZE bits, so DEFAULT_SYMMETRIC_SIZE / 8
   * bytes. 
   * @param password the password
   * @return the key
   */
  public static Key doSimmetricKeyWithPassword( byte[] password) {
  	return new SecretKeySpec( normalizePasswdSize( password, DEFAULT_SYMMETRIC_SIZE / 8), 
  			                      DEFAULT_SYMMETRIC_ALGORITHM);
  }
  
  /**
   * This method creates a key using the <code>password</code> as an hexadecimal string. If <code>password</code> length is too
   * big or too little, its size will be adapted. The perfect password is DEFAULT_SYMMETRIC_SIZE bits, so DEFAULT_SYMMETRIC_SIZE / 8
   * bytes. 
   * @param hex the password as an hexadecimal string
   * @return the key
   */
  public static Key doSimmetricKeyWithPasswordHex( String hex) {
  	return new SecretKeySpec( normalizePasswdSize( Formats.getArrayFromHexString( hex), DEFAULT_SYMMETRIC_SIZE / 8), 
  			                      DEFAULT_SYMMETRIC_ALGORITHM);
  }
  
  /**
   * This method creates a PUBLIC key using an hexadecimal string containing a X509 formatted key.
   * @param hex an hexadecimal string containing a X509 formatted key.
   * @return the public key
   * @throws NoSuchAlgorithmException if your JRE doesn't supports AES
   * @throws InvalidKeySpecException if <code>hex</code> isn't properly formated as an X509 key
   */
  public static Key doAsimmetricPublicKey( String hex) throws NoSuchAlgorithmException, InvalidKeySpecException {
    return doAsimmetricPublicKey( Formats.getArrayFromHexString( hex));
  }
  
  /**
   * This method creates a PUBLIC key using the data in <code>key</code> containing a X509 formatted key.
   * @param key a <code>byte[]</code> containing a X509 formatted key.
   * @return the public key
   * @throws NoSuchAlgorithmException if your JRE doesn't supports AES
   * @throws InvalidKeySpecException if <code>key</code> isn't properly formated as an X509 key
   */
  public static Key doAsimmetricPublicKey( byte[] key) throws NoSuchAlgorithmException, InvalidKeySpecException {
    X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec( key);
    KeyFactory keyFactory = KeyFactory.getInstance( DEFAULT_ASYMMETRIC_ALGORITHM);
    
  	return keyFactory.generatePublic( pubKeySpec);
  }
  
  /**
   * This method creates a PRIVATE key using an hexadecimal string containing a PKCS8 formatted key.
   * @param hex an hexadecimal string containing a PKCS8 formatted key.
   * @return the private key
   * @throws NoSuchAlgorithmException if your JRE doesn't supports AES
   * @throws InvalidKeySpecException if <code>hex</code> isn't properly formated as an PKCS8 key
   */
  public static Key doAsimmetricPrivateKey( String hex) throws NoSuchAlgorithmException, InvalidKeySpecException {
    return doAsimmetricPrivateKey( Formats.getArrayFromHexString( hex));
  }
  
  /**
   * This method creates a PRIVATE key using the data in <code>key</code> containing a PKCS8 formatted key.
   * @param key a <code>byte[]</code> containing a PKCS8 formatted key.
   * @return the private key
   * @throws NoSuchAlgorithmException if your JRE doesn't supports AES
   * @throws InvalidKeySpecException if <code>key</code> isn't properly formated as an PKCS8 key
   */
  public static Key doAsimmetricPrivateKey( byte[] key) throws NoSuchAlgorithmException, InvalidKeySpecException {
    PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec( key);
    KeyFactory keyFactory = KeyFactory.getInstance( DEFAULT_ASYMMETRIC_ALGORITHM);
    
    return keyFactory.generatePrivate( keySpecPKCS8);
  }
  

  /**
   * This method returns the used key
   * @return the key
   */
  public Key getKey() {
    return key;
  }
  
  /**
   * This method sets the key to use
   * @param key the key
   */
  public void setKey( Key key) {
    this.key = key;
  }
  
  
  /**
   * This method returns a SecureRamdom number generator. It's used by this class, but you can use it in other places if you want
   * because they are a little bit expensive to create, and you only need one...
   * @return a SecureRandom object
   */
  public static SecureRandom getSecureRamdom() {
    if ( secureRandom == null ) {
      secureRandom = new SecureRandom();
      secureRandom.nextInt();
    }
    
    return secureRandom;
  }
  
  
  /**
   * This method encodes the <code>clear</code> contents and returns the cipher text, read as an UTF-8 String, 
   * or <code>null</code> if <code>clear</code> is <code>null</code>.
   * @param clear the <i>clear</i> text to encrypt
   * @return the cipher text, or <code>null</code> if <code>clear</code> is <code>null</code> (or Java suddenly
   *         doesn't know what 'UTF-8' is, which should be impossible)
   * @throws IllegalArgumentException when shit happens
   */
  public byte[] encode( String clear) {
    if ( clear == null ) {
      return null;
    }

    try {
      return encode( clear, "UTF-8");
    }
    catch ( UnsupportedEncodingException e) {
      return null;
    }
  }
  
  
  /**
   * This method encodes the <code>clear</code> contents and returns the cipher text, read as a <code>charset</code> String, 
   * or <code>null</code> if <code>clear</code> is <code>null</code>.
   * @param clear the <i>clear</i> text to encrypt
   * @param charset the charset
   * @return  the cipher text, or <code>null</code> if <code>clear</code> is <code>null</code>
   * @throws UnsupportedEncodingException if <code>charset</code> isn't accepted
   * @throws IllegalArgumentException when shit happens
   */
  public byte[] encode( String clear, String charset) throws UnsupportedEncodingException {
    if ( clear == null ) {
      return null;
    }

    return encode( clear.getBytes( charset));
  }
  
  
  /**
   * This method encodes the <code>clear</code> contents and returns the cipher text or <code>null</code> 
   * if <code>clear</code> is <code>null</code>.
   * @param clear the <i>clear</i> text to encrypt
   * @return the cipher text, or <code>null</code> if <code>clear</code> is <code>null</code>
   * @throws IllegalArgumentException when shit happens
   */
  public byte[] encode( byte clear[]) throws IllegalArgumentException {
    if ( clear == null ) {
      return null;
    }
    
    try {
      return getEncoder().doFinal( clear);
    }
    catch ( Exception e) {
      throw new IllegalArgumentException( "Cannot encode : " + e.getMessage(), e);
    }
  }
  
  
  /**
   * This method decodes the <code>cipher</code> contents and returns the clear text, 
   * or <code>null</code> if <code>cipher</code> is <code>null</code>.
   * @param cipher the <i>cipher</i> text to decrypt
   * @return the decrypted text, or <code>null</code> if <code>cipher</code> is <code>null</code>
   * @throws IllegalArgumentException when shit happens
   */
  public byte[] decode( byte[] cipher) {
    if ( cipher == null ) {
      return null;
    }
   
    try {
      return getDecoder().doFinal( cipher);
    }
    catch ( Exception e) {
      throw new IllegalArgumentException( "Cannot decode : " + e.getMessage(), e);
    }
  }
  
  
  /**
   * This method decodes the <code>cipher</code> contents and returns the clear text as a String in <code>charset</code>
   * charset, or <code>null</code> if <code>cipher</code> is <code>null</code>.
   * @param cipher the <i>cipher</i> text to decrypt
   * @param charset the charset to prepare the resulting String
   * @return the decrypted text, or <code>null</code> if <code>cipher</code> is <code>null</code>
   * @throws IllegalArgumentException when shit happens
   */
  public String decode( byte[] cipher, String charset) {
    byte buf[] = decode( cipher);
    if ( buf == null ) {
      return null;
    }
   
    try {
      return new String( buf, charset);
    }
    catch ( Exception e) {
      throw new IllegalArgumentException( "Cannot decode", e);
    }
  }
  
  /**
   * This method returns <code>true</code> if this class is configured to use asymmetric cryptography
   * @return <code>true</code> if this class is configured to use asymmetric cryptography
   */
  public boolean isAsymmetric() {
  	return type == TYPE_ASYMMETRIC;
  }
  
  
  /**
   * This method returns <code>true</code> if this class is configured to use symmetric cryptography
   * @return <code>true</code> if this class is configured to use asymmetric cryptography
   */
  public boolean isSymmetric() {
  	return type == TYPE_SYMMETRIC;
  }
  

  private Cipher getEncoder() {
    if ( ciEncode == null ) {
    	String alg = isSymmetric() ? DEFAULT_SYMMETRIC_ALGORITHM : DEFAULT_ASYMMETRIC_ALGORITHM;
    	
      try {
        ciEncode = Cipher.getInstance( alg);
        ciEncode.init( Cipher.ENCRYPT_MODE, key, getSecureRamdom());
      }
      catch ( Exception e) {
         e.printStackTrace();
      }
    }
    
    return ciEncode;
  }
  
  
  private Cipher getDecoder() {
    if ( ciDecode == null ) {
    	String alg = isSymmetric() ? DEFAULT_SYMMETRIC_ALGORITHM : DEFAULT_ASYMMETRIC_ALGORITHM;
    	
      try {
        ciDecode = Cipher.getInstance( alg);
        ciDecode.init( Cipher.DECRYPT_MODE, key, getSecureRamdom());
      }
      catch ( Exception e) {
         e.printStackTrace();
      }
    }
    
    return ciDecode;
  }
  
  
  private static byte[] normalizePasswdSize( String pass, int size) {
  	return normalizePasswdSize( pass.getBytes(), size);
  }
  
  
  private static byte[] normalizePasswdSize( byte[] pass, int size) {
  	if ( size < pass.length ) {
  		return ByteArrays.subarray( pass, 0, size);
  	}
  	
  	byte res[] = new byte[size];
  	
  	for ( int i = pass.length, j = 0; i < size; i++) {
  		res[i] = pass[j];
  		
  		j = j == pass.length ? 0: j++;
  	}
  	
  	return res;
  }
  
  
}



