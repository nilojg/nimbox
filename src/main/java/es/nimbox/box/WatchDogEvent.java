package es.nimbox.box;

/**
 * This class contains some information indicating that a watchdog alarm has occurred. This event is passed as argument
 * to a WatchDogListener instance registered in the WatchDog.
 * 
 * @author nilo
 *
 */
public class WatchDogEvent {
  private static long ID_SEED = 0;

  private long eventId;
  private long time;
  private int alarmId;
  private Object origin;
  private String msg;
  
  /**
   * This constructor creates the event associated to the <code>origin</code> object with the <code>msg</code> message.
   * @param alarmId the alarm id of the alarm that raises this event
   * @param origin the associated object
   * @param msg the message
   */
  public WatchDogEvent( int alarmId, Object origin, String msg) {
    super();
    
    synchronized ( WatchDogEvent.class) {
      this.eventId = ID_SEED++;
    }
    this.time = System.currentTimeMillis();
    
    this.alarmId = alarmId;
    this.origin = origin;
    this.msg = msg;
  }
  
  @Override
  public String toString() {
    return "WatchDogEvent [id=" + eventId + ", time=" + time + ", alarmId=" + alarmId + ", origin=" + origin + ", msg=" + msg + "]";
  }

  /**
   * This method returns the id of the alarm which raised the alarm
   * @return the id of the alarm which raised the alarm
   */
  public int getAlarmId() {
    return alarmId;
  }

  /**
   * This method sets the id of the alarm which raised the alarm
   * @param alarmId the id of the alarm which raised the alarm
   */
  public void setAlarmId( int alarmId) {
    this.alarmId = alarmId;
  }

  /**
   * This method returns the object associated with the event
   * @return the object associated with the event
   */
  public Object getOrigin() {
    return origin;
  }

  /**
   * This method sets the object associated with the event
   * @param origin the object associated with the event
   */
  public void setOrigin( Object origin) {
    this.origin = origin;
  }

  /**
   * This method returns the message of the event
   * @return the message of the event
   */
  public String getMsg() {
    return msg;
  }
  
  /**
   * This method sets the message of the event
   * @param msg the message of the event
   */
  public void setMsg( String msg) {
    this.msg = msg;
  }

  /**
   * This method returns the id of the event
   * @return the id of the event
   */
  public long getEventId() {
    return eventId;
  }

  /**
   * This method returns the time, in millis, when the event was created
   * @return the time, in millis, when the event was created
   */
  public long getTime() {
    return time;
  }
  
}
