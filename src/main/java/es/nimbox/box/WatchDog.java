package es.nimbox.box;

import java.util.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * <p>This class implements a watch dog, a component that throws alarms on predefined moments or at predefined intervals.
 * Each WatchDog instance can handle an, in theory, unlimited number of predefined alerts, but you know how these things are...</p>
 * 
 * <p>The WatchDog creates a new background Thread that will wake up from time to time (the defect interval is 50 ms, but you can
 * change this) and will raise alarms if their time has come. The accurate of the timing depends on the size of the interval, so
 * if you need more precision than 50 ms set a more little value, but that will have some impact in CPU consumption.</p>
 * 
 * <p>When an alarm raises, the WatchDog will notify the alarm to a list of listeners with the information of the raised alarm,
 * and the <code>WatchDogEvent</code> event contains information about an Object and a String message that you can handle as you prefer,
 * but it's quite important to test that the <code>alarmId</code> of the event is one of your alarms, specially if you are
 * using the default singleton WatchDog. Will see something of this later in the examples.</p>  
 * 
 * <p>The listeners execute in the WatchDog thread, so <b>they MUST be fast or they will freeze the WatchDog thread</b>.</p> 
 * 
 * <p>Setting an alarm is quite easy, just create the WatchDog, set a listener and add the alarm. Let's see:</p>
 * 
 * <pre>
 * // create the WatchDog. 
 * WatchDog simple = new WatchDog();
 * 
 * // optional. This sets a wake up interval of 500 ms. Not very accurate, but cheap in CPU. And this is only an example...
 * simple.setThreadSleep( 500);
 * 
 * // adding a listener to the WatchDog that simply writes the event in standard output  
 * simple.addListener( new WatchDogListener() {
 *   public void watchDogAlert( WatchDogEvent ev) {
 *     System.out.println( ev);
 *   }      
 * });
 * 
 * // adding an alarm that will rise each 2 seconds (2000 ms)
 * simple.registerAlarmInterval( null, "Each 2 seconds", 2000);
 * 
 * // adding an alarm that will rise in two seconds from now
 * simple.registerAlarmTime( null, "After 5 seconds", System.currentTimeMillis() + 5000);
 * 
 * // start the WatchDog thread
 * simple.startWatchDog();
 * 
 * ... do other things
 * 
 * // this stops the WatchDog thread at any moment
 * simple.stopWatchDog();
 * </pre>
 * 
 * <p>It's easy to use, but there's a point about threads handling that is important to talk about. You can start the WatchDog thread as any
 * other thread, using the <code>start</code> method, but in that case you MUST remember to stop the thread invoking <code>stopWatchDog()</code> or 
 * your application will never end because there will be a running thread and the JVM will wait until the WatchDog thread ends.</p>
 * <pre>
 * // start the WatchDog thread as any other thread
 * simple.start();
 * 
 * ... do other things
 * 
 * // this is really important or your application will never end
 * simple.stopWatchDog();
 * </pre>
 * 
 * <p>If you call the <code>startWatchDog()</code> method the thread will finish with the JVM, so your application will end as you expect even if you
 * forget to invoke the <code>stopWatchDog()</code> method. You can forget the WatchDog thread except if you want to kill the WatchDog
 * before the application ends. The easy way is to use <code>startWatchDog()</code> and forget it.</p> 
 * 
 * <p>This is because the <code>startWatchDog()</code> method calls <code>setDaemon( true)</code>, which tells the JVM "when you are going to die, 
 * kill this thread too" instead the default "don't die until this thread dies, wait until the end of times". You can use the <code>start</code>
 * method an invoke <code>setDaemon</code> by your own, but, why not to take the easy way, call <code>startWatchDog()</code> and forget the 
 * whole thing?</p>
 * 
 * <p>You can create as many WatchDog instances as you want, but remember that each instance is a living thread. However, in the most common
 * scenario you will only need a single instance for your whole application handling all the alarms. In that case you can use the <i>default</i>
 * WatchDog, a singleton instance that will be created the first time you use it. In this case, the thread starts and finishes by its
 * own hand, so you only have to add and process alarms:</p>
 * <pre>
 * // get the default instance. You can use WatchDog.getDefaultWatchDog() a lot, but I'm quite lazy...
 * WatchDog wd = WatchDog.getDefaultWatchDog();
 *
 * // adding an alarm that will rise each 2 seconds (2000 ms)
 * int alarmId_1 = simple.registerAlarmInterval( null, "Each 2 seconds", 2000);
 * 
 * // adding an alarm that will rise in two seconds from now
 * int alarmId_2 = simple.registerAlarmTime( null, "After 5 seconds", System.currentTimeMillis() + 5000);
 * 
 * // register a listener 
 * wd.addListener( new WatchDogListener() {
 *   public void watchDogAlert( WatchDogEvent ev) {
 *     if ( ev.getAlarmId() == alarmId_1 || ev.getAlarmId() == alarmId_2 ) { 
 *       System.out.println( ev);
 *     }
 *   }      
 * });
 * 
 * ... do other things
 * </pre>
 * 
 * <p>The WatchDog thread will finish when your application and the JVM finishes, so it's quite easy to use. The only problem is that the whole application
 * shares a single WatchDog, and all listeners are notified with all the alarms, which can give you some headache if a part of your application gets the
 * alarms prepared for another part of your application. When you register an alarm, the <code>registerAlarm*</code> returns an <code>id</code> for the alarm, 
 * and you can use that <code>id</code> to be sure that the captured event has been raised by your alarm.</p>
 * 
 * @author nilo
 *
 */
public class WatchDog extends Thread {
  /**
   * The default interval between thread wake ups. 50 milliseconds. 
   */
  public static final long GENERAL_INTERVAL = 50;
  private static int ID_ALERT = 0;
  
  // The effective interval between thread wake ups, Initialized to GENERAL_INTERVAL, but can be changed with  setThreadSleep
  private long threadSleep;
  
  // To stop the thread
  private boolean stop;

  // The listeners
  private List<WatchDogListener> lListeners;
  
  // And the alarms, in a list that will be sorted
  private List<Alarm> lAlarms;
  
  private class Alarm implements Comparable<Alarm> {
    public int id;
    public Object origin;
    public String msg;
    public long interval;
    public long wakeup;
    
    public Alarm( Object origin, String msg) {
      this.id = ID_ALERT++;
      this.origin = origin;
      this.msg = msg;
    }
    
    public WatchDogEvent getEvent() {
      return new WatchDogEvent( id, origin, msg);
    }

    @Override
    public int compareTo( Alarm other) {
      return (int)(this.wakeup - other.wakeup);
    }
  }
  
  // The singleton for the default WatchDog
  private static WatchDog defaultWatchDog;
  
  /**
   * This method returns the instance of the default WatchDog. This instance is a singleton.
   * @return the instance of the default WatchDog
   */
  public static WatchDog getDefaultWatchDog() {
    if ( defaultWatchDog == null ) {
      defaultWatchDog = new WatchDog();
      
      defaultWatchDog.startWatchDog();
    }
    
    return defaultWatchDog;
  }
  
  /**
   * This method stops the default WatchDog instance.
   */
  public static void stopDefaultWatchDog() {
    getDefaultWatchDog().stopWatchDog();
  }
  
  
  
  
  /**
   * Default constructor
   */
  public WatchDog() {
    //mAlarms = new HashMap<Integer,Alarm>();
    lListeners = new ArrayList<WatchDogListener>();
    lAlarms = new ArrayList<Alarm>();
    
    threadSleep = GENERAL_INTERVAL;
    stop = false;
  }
  
  /**
   * This method returns the interval, in milliseconds, between thread wake ups 
   * @return the interval, in milliseconds, between thread wake ups
   */
  public long getThreadSleep() {
    return threadSleep;
  }
  
  /**
   * This method sets the interval, in milliseconds, between thread wake ups
   * @param threadSleep the interval, in milliseconds, between thread wake ups
   */
  public void setThreadSleep( long threadSleep) {
    this.threadSleep = threadSleep;
  }
  
  /**
   * This method stops the WatchDog thread. You can only stop the thread ONE time, so be really careful
   * with this method and the default thread, because if you stop the default thread, it will never restart
   * again...
   */
  public void stopWatchDog() {
    stop = true;
  }
  
  /**
   * This method starts the WatchDog thread
   */
  public void startWatchDog() {
    if ( this.isAlive() ) {
      return;
    }
    
    setDaemon( true);
    start();
  }

  /**
   * <p>This method adds an alarm that will be raised at <code>time</code>. You can use the parameters <code>origin</code> 
   * and <code>msg</code> as you prefer.</p>
   * The method returns an <code>id</code> associated to the new alarm that you can use to process only your own alarms. 
   * @param origin an Object associated with the alarm 
   * @param msg a String associated with the alarm
   * @param time the time to rise the alarm
   * @return the alarm id
   */
  public int registerAlarmTime( Object origin, String msg, Date time) {
    return registerAlarmTime( origin, msg, time.getTime());
  }
  
  /**
   * <p>This method adds an alarm that will be raised at <code>time</code>. You can use the parameters <code>origin</code> 
   * and <code>msg</code> as you prefer.</p>
   * The method returns an <code>id</code> associated to the new alarm that you can use to process only your own alarms. 
   * @param origin an Object associated with the alarm 
   * @param msg a String associated with the alarm
   * @param time the time, in epoch milliseconds, to rise the alarm
   * @return the alarm id
   */
  public synchronized int registerAlarmTime( Object origin, String msg, long time) {
    Alarm alarm = new Alarm( origin, msg);
    alarm.interval = 0;
    alarm.wakeup = time;
    
    // Whenever an alarm is added, the alarm list must be recalculated
    lAlarms.add( alarm);
    Collections.sort( lAlarms);
    
    return alarm.id;
  }
  
  /**
   * <p>This method adds an alarm that will be raised each <code>time</code> milliseconds. You can use the parameters 
   * <code>origin</code> and <code>msg</code> as you prefer.</p>
   * The method returns an <code>id</code> associated to the new alarm that you can use to process only your own alarms. 
   * @param origin an Object associated with the alarm 
   * @param msg a String associated with the alarm
   * @param interval the interval, milliseconds, between alarms
   * @return the alarm id
   */
  public synchronized int registerAlarmInterval( Object origin, String msg, long interval) {
    Alarm alarm = new Alarm( origin, msg);
    alarm.interval = interval;
    alarm.wakeup = System.currentTimeMillis() + interval;
    
    // Whenever an alarm is added, the alarm list must be recalculated
    lAlarms.add( alarm);
    Collections.sort( lAlarms);
    
    return alarm.id;
  }
  
  /**
   * This method removes the alarm identified by the <code>id</code>.
   * @param id the alarm id
   */
  public void removeAlarm( int id) {
    synchronized (lAlarms) {
      lAlarms.removeIf(alarm -> alarm.id == id);
    }
  }
  
  /**
   * The WatchDog thread, you know... See the class javadoc to know all the possibilities to start and stop the thread 
   */
  @Override
  public void run() {
    while( !stop ) {
      long now = System.currentTimeMillis();
      
      if ( lAlarms.size() == 0 
           || 
           lAlarms.get( 0).wakeup > now) {
        Utils.sleep( threadSleep);
        continue;
      }
      
      try {
        List<Alarm> lCopy = new ArrayList<>( lAlarms);
        
        Iterator<Alarm> it = lCopy.iterator();
        while ( it.hasNext() ) {
          Alarm al = it.next();
          
          if ( al.wakeup > now ) {
            break;
          }
  
          // If the wake up time has passed, the alarm must be fired
          fireEvents( al.getEvent());
          
          // If there isn't a defined interval, it's a "one shot" alarm, so remove it from the list
          if ( al.interval == 0 ) {
            synchronized ( lAlarms ) {
              for ( int i = 0; i < lAlarms.size(); i++) {
                if ( lAlarms.get( i).id == al.id ) {
                  lAlarms.remove( i);
                  break;
                }
              }
            }
          }
          else {
            // If there's an interval, recalculate the next wake up time
            al.wakeup += al.interval;
          }
        }
      }
      catch ( Exception ex) {
        // This loop will never die...
      }
      
      Collections.sort( lAlarms);
    }
  }
  
  /**
   * This method register a listener to process the alarms
   * @param listener a listener to process the alarms
   */
  public void addListener( WatchDogListener listener) {
    lListeners.add( listener);
  }
  
  /**
   * This method removes the listener <code>listener</code>
   * @param listener the listener to be removed
   */
  public void removeListener( WatchDogListener listener) {
    lListeners.remove( listener);
  }
  
  
  
  private void fireEvents( WatchDogEvent ev) {
    try {
      for ( WatchDogListener ll : lListeners ) {
          ll.watchDogAlert( ev);
      }
    }
    catch ( Exception th) {
      // if the listener code throws an exception, the WatchDog thread will die, so let's capture whatever
      // and show a really ugly exception by standard output...
      System.out.println( "WatchDog listener has found an exception : \n");
      th.printStackTrace();
    }
  }
  
  
}
