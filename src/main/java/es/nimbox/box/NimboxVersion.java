package es.nimbox.box;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.jar.JarFile;

public class NimboxVersion {
  public static final String NIMBOX = "nimbox";
  public static final String DEFAULT_FILE_NAME = ".build.properties";


  private NimboxVersion() {
    // Don't you hate Sonar?
  }


  public static String getBuildPropertiesFor( String filePrefix) {
    return "/" + filePrefix + DEFAULT_FILE_NAME;
  }

  public static String getProperty( String property) throws IOException {
    return getProperty( getBuildPropertiesFor( NIMBOX), property);
  }

  public static String getProperty( String fileName, String property) throws IOException {
    try ( InputStream is = Utils.findFile( fileName) ) {
      return getProperty( is, property);
    }
    catch ( IOException ex) {
      throw ex;
    }
    catch ( Exception ex) {
      throw new IOException( ex);
    }
  }

  public static String getProperty( InputStream is, String property) throws IOException {
    Properties props = new Properties();
    props.load( is);

    return props.getProperty( property);
  }


  public static String getManifestPropertyFromJar( String jarName, String property) throws IOException {
    ClassLoader cl = NimboxVersion.class.getClassLoader();

    URL url = cl.getResource( jarName);

    try ( JarFile jarFile = new JarFile( url.getFile()) ) {
      return jarFile.getManifest().getMainAttributes().getValue(property);
    }
    catch ( NullPointerException ex) {
      throw new IOException( "Cannot open file '" + jarName + "'", ex);
    }
  }

}
