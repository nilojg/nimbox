package es.nimbox.box;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class implements a "pace maker", something to slow down the speed of the application at a given pace.
 * This is useful if you are going to access a server that cannot handle too much load, for example. You can
 * set a pace of 100 requests per second and use this class to ensure that you will not make more than 100
 * request per second.<br>
 * 
 * The class is a singleton, so, yes, you will have to use a factory method... but it's easy to use because
 * there are only two important methods, one to set the pace and another one to slow down the application.
 * This is an example of use:
 * <pre>
 * // Set the pace, with the identification "limitedServer"
 * PaceMaker.getPaceMaker().addPace( "limitedServer", PaceMaker.TYPE.PER_MINUTE, 600);
 * 
 * // Use the PaceMaker.
 * PaceMaker.getPaceMaker().makePace( "limitedServer");
 * </pre>
 * 
 * The method <code>addPace</code> sets a pace identified by a String. The third argument defines the number
 * of allowed petitions per time unit, which is set with the second argument. The allowed time units are:
 * <ul>
 *   <li>PaceMaker.TYPE.PER_SECOND</li>
 *   <li>PaceMaker.TYPE.PER_MINUTE</li>
 *   <li>PaceMaker.TYPE.PER_HOUR</li>
 * </ul>
 * 
 * The method <code>makePace</code> blocks the thread if the speed of the thread is faster than the allowed
 * pace.<br>
 * 
 * The <code>PaceMaker</code> can store as many paces as you want, each one identified by a String ("limitedServer"
 * in the example) and there are some other methods to get information about an identified pace, and some statistics 
 * about how many times a pace is used and how much time it sleeps the thread.<br>
 * 
 * 
 * 
 * @author nilo
 *
 */
public class PaceMaker {
  /**
   * Unit time 
   */
  public enum TYPE {
    /**
     * Seconds
     */
    PER_SECOND,
    /**
     * Minutes
     */
    PER_MINUTE,
    /**
     * Hours
     */
    PER_HOUR};
  
  private class TagData {
    public String id;
    
    public TYPE type;
    public int pace;
    public long accounting;
    public long interval;
    public long lastExec;
    public long pausedTime;
  }
  
  private Map<String,TagData> mPaceMakers;
  
  private static PaceMaker single;
  
  private PaceMaker() {
    mPaceMakers = new HashMap<>();
  }

  /**
   * Factory method to get the <code>PaceMaker</code>
   * @return an instance of the <code>PaceMaker</code>
   */
  public static PaceMaker getPaceMaker() {
    if ( single == null ) {
      single = new PaceMaker();
    }
    
    return single;
  }
  
  /**
   * This method adds a new pace associated to the identifier <code>id</code>. The pace allows <code>pace</code>
   * executions per <b>second</b>. 
   * @param id the identification of the pace
   * @param pace executions per second
   */
  public void addPace( String id, int pace) {
    addPace( id, TYPE.PER_SECOND, pace);
  }
  
  /**
   * This method adds a new pace associated to the identifier <code>id</code>. The pace allows <code>pace</code>
   * executions per <b>type</b> unit time. The allowed time units are:
   * <ul>
   *   <li>PaceMaker.TYPE.PER_SECOND</li>
   *   <li>PaceMaker.TYPE.PER_MINUTE</li>
   *   <li>PaceMaker.TYPE.PER_HOUR</li>
   * </ul>
   * @param id the identification of the pace
   * @param type the time unit
   * @param pace executions per time unit
   */
  public void addPace( String id, PaceMaker.TYPE type, int pace) {
    TagData td = new TagData();
    
    td.id = id;
    td.type = type;
    td.pace = pace;
    td.lastExec = 0; //System.currentTimeMillis();
    td.accounting = 0;
    td.pausedTime = 0;
    
    switch ( td.type ) {
      case PER_HOUR:
        td.interval = (3600 * 1000) / pace;
        break;
      case PER_MINUTE:
        td.interval = (60 * 1000) / pace;
        break;
      case PER_SECOND: 
      default:
        td.interval = 1000 / pace;
    }
    
    mPaceMakers.put( td.id, td);
  }
  
  /**
   * <p>This method blocks the current thread only if the speed of the thread is faster than the allowed
   * pace.</p>
   * <p>If the identification <code>id</code> doesn't exists, the method returns immediately</p>
   * @param id the identification of the pace
   */
  public void makePace( String id) {
    if ( id == null ) {
      return;
    }

    TagData td = mPaceMakers.get( id);
    
    if ( td == null ) {
      return;
    }
    
    synchronized ( td) {
      long next = td.lastExec + td.interval;
      long now = System.currentTimeMillis();
      
      if ( next > now ) {
        td.pausedTime += next - now;
        Utils.sleep( next - now);
      }
      
      td.accounting++;
      td.lastExec = System.currentTimeMillis();
    }
  }

  /**
   * This method returns a <code>Collection</code> with all the stored paces
   * @return a <code>Collection</code> with all the stored paces
   */
  public Collection<String> getIds() {
    List<String> res = new ArrayList<>( mPaceMakers.keySet());
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns the allowed executions per time unit of the pace indentified by <code>id</code> 
   * @param id the identification of the pace
   * @throws NullPointerException when the <code>id</code> is not found
   * @return the allowed executions
   */
  public int getPace( String id) {
    TagData td = mPaceMakers.get( id);
    
    if ( td == null ) {
      throw new NullPointerException( "Unknow ID [" + id + "]");
    }
    
    return td.pace;
  }

  /**
   * This method returns the minimum time in milliseconds between executions of the pace <code>id</code> to don't
   * exceed the executions limits per time unit.
   * @param id the identification of the pace
   * @throws NullPointerException when the <code>id</code> is not found
   * @return the minimum time in milliseconds between executions of the pace <code>id</code> to don't
   * exceed the executions limits per time unit
   */
  public long getInterval( String id) {
    TagData td = mPaceMakers.get( id);
    
    if ( td == null ) {
      throw new NullPointerException( "Unknow ID [" + id + "]");
    }
    
    return td.interval;
  }
  
  /**
   * This method returns how many times the pace <code>id</code> has been used.
   * @param id the identification of the pace
   * @throws NullPointerException when the <code>id</code> is not found
   * @return the number of times the pace <code>id</code> has been used.
   */
  public long getAccounting( String id) {
    TagData td = mPaceMakers.get( id);
    
    if ( td == null ) {
      throw new NullPointerException( "Unknow ID [" + id + "]");
    }
    
    return td.accounting;
  }
  
  /**
   * This method returns the total time in milliseconds that the pace <code>id</code> has stopped the
   * thread. It's useful to know if the current limit is too slow for the application.
   * @param id the identification of the pace
   * @throws NullPointerException when the <code>id</code> is not found
   * @return the total time in milliseconds that the pace <code>id</code> has stopped the
   * thread.
   */
  public long getPausedTime( String id) {
    TagData td = mPaceMakers.get( id);
    
    if ( td == null ) {
      throw new NullPointerException( "Unknow ID [" + id + "]");
    }
    
    return td.pausedTime;
  }
}


