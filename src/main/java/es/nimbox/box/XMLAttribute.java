
package es.nimbox.box;

import java.lang.annotation.*;

/**
 * <p>This annotation defines that a class attribute will be translated to XML as an XML attribute by the {@link XMLUtils#toXML(Object)}
 * methods.</p>
 * <p>There's a more complete explanation of the translation to XML in the {@link XMLClass} javadoc.</p> 
 * 
 * @see XMLClass
 * @see XMLElement
 * @see XMLUtils#toXML(Object)
 * 
 * @author nilo
 */
@Retention(value=RetentionPolicy.RUNTIME) 
@Target(value=ElementType.FIELD)
public @interface XMLAttribute {
  /**
   * The attribute name. If it isn't defined, the variable name will be used 
   * @return the name of the attribute
   */
  String name() default "";
}
