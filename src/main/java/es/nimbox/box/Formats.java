package es.nimbox.box;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * This class is a tool box for handling different formats, like JSON, HEX or Base64. 
 * 
 * @author nilo.gonzalez
 *
 */
public class Formats {
  
  private Formats() {}
  
  // Nasty hack to test deprecated methods
  @SuppressWarnings( "unchecked")
  private static <T> T getClass( Type typeOfT) {
    if ( typeOfT.getClass().isArray() ) {
      return (T)Array.newInstance( TypeFactory.rawClass( typeOfT), 0);
    }
    else {
      return (T)TypeFactory.rawClass( typeOfT);
    }
  }
  
  /**
   * <p>This method returns an instance of the class corresponding to the type <code>typeOfT</code> 
   * created with the values of <code>json</code>.</p>
   * The method uses the Jackson library, so the method can do what Jackson can do...
   * @param <T> the expected type
   * @param json the json
   * @param typeOfT the target class
   * @return an instance of <code>clazz</code> created with the values of <code>json</code>
   * @see <a href="https://github.com/FasterXML/jackson">Jackson</a>
   * @deprecated
   */
  @SuppressWarnings( "unchecked")
  @Deprecated
  public static <T> T fromJson( String json, Type typeOfT) {
    return (T)fromJson( json, getClass( typeOfT));
  }
  
  /**
   * <p>This method returns an instance of class <code>clazz</code> created with the values of <code>json</code>.</p>
   * The method uses the Jackson library, so the method can do what Jackson can do...
   * @param <T> the expected type
   * @param json the json
   * @param clazz the target class
   * @return an instance of <code>clazz</code> created with the values of <code>json</code>
   * @throws ParseException if shit happens
   * @see <a href="https://github.com/FasterXML/jackson">Jackson</a>
   */
  public static <T> T fromJson( String json, Class<T> clazz) throws ParseException {
    try {
      ObjectMapper mapper = new ObjectMapper();
      return mapper.readValue( json, clazz);
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      throw new ParseException( ex.getMessage(), ex);
    }
  }
  
  /**
   * <p>This method returns a String with the json representation of <code>obj</code>.</p>
   * The method uses the Jackson library, so the method can do what Jackson can do...
   * @param obj the object
   * @return a String with the json representation of <code>obj</code>
   * @throws ParseException if shit happens
   * @see <a href="https://github.com/FasterXML/jackson">Jackson</a>
   */
  public static String toJson( Object obj) throws ParseException {
    try {
      ObjectMapper mapper = new ObjectMapper();

      mapper.setVisibility( PropertyAccessor.FIELD, Visibility.ANY);
      mapper.setVisibility( PropertyAccessor.ALL, Visibility.ANY);
      
      return mapper.writeValueAsString( obj);
    }
    catch ( Exception ex) {
      throw new ParseException( ex.getMessage(), ex);
    }
  }
  
  /**
   * <p>This method returns a String with the json representation of <code>obj</code>. The <code>obj</code>
   * is parsed like an instance of type <code>typeOfT</code>.</p>
   * The method uses the Jackson library, so the method can do what Jackson can do...
   * @param obj the object
   * @param typeOfT the type of the object
   * @return a String with the json representation of <code>obj</code>
   * @see <a href="https://github.com/FasterXML/jackson">Jackson</a>
   * @see #toJson(Object)
   * @deprecated This method was necessary when the JSON stuff was done with GSON. Now the <code>typeOfT</code> parameter is not used,
   *             so it's better to use the <code>toJson( Object)</code> method
   */
  @Deprecated
  public static String toJson( Object obj, Type typeOfT) {
    return toJson( obj);
  }
  
  /**
   * <p>This method returns a String with the json representation of <code>obj</code>.</p>
   * <p>The returned json is "pretty" indented.</p>
   * The method uses the Jackson library, so the method can do what Jackson can do...
   * @param obj the object
   * @return a String with the json representation of <code>obj</code>
   * @throws ParseException if shit happens
   * @see <a href="https://github.com/FasterXML/jackson">Jackson</a>
   */
  public static String toJsonPretty( Object obj) throws ParseException {
    try {
      ObjectMapper mapper = new ObjectMapper();

      if ( obj instanceof String ) {
        return mapper.writerWithDefaultPrettyPrinter()
                     .writeValueAsString( mapper.readTree( obj.toString()));
      }
      else {
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString( obj);
      }
    }
    catch ( Exception ex) {
      throw new ParseException( ex.getMessage(), ex);
    }
  }

  /**
   * <p>This method returns the string <code>txt</code> wrapped in lines of <b>exactly</b>
   * <code>lineSize</code> characters, inserting <code>\n</code> characters. This method breaks
   * words.</p>
   * @param txt the text to wrap
   * @param lineSize the line size
   * @return a string with the text wrapped
   */
  public static String lineWrap( String txt, int lineSize) {
    return lineWrap( txt, lineSize, false);
  }

  /**
   * <p>This method returns the string <code>txt</code> wrapped in lines of <b>exactly</b>
   * <code>lineSize</code> characters, inserting <code>\n</code> characters. This method keeps
   * the last words of the line, making the line shorter by passing the last word to the next line.
   * So the lines could be shorter than <code>lineSize</code>.</p>
   * @param txt the text to wrap
   * @param lineSize the line size
   * @param keepWords it keeps the words complete, and breaks the words otherwise
   * @return a string with the text wrapped
   * @throws IllegalArgumentException if <code>lineSize</code> is less than 1 (and 1 is actually to little...)
   */
  public static String lineWrap( String txt, int lineSize, boolean keepWords) {
    if ( lineSize < 1) {
      throw new IllegalArgumentException( "Line length must be greater than 0");
    }

    StringBuilder wrapped = new StringBuilder();

    int ix = 0;
    while ( ix < (txt.length() - lineSize) ) {
      String line = txt.substring( ix, ix+lineSize);

      if ( !keepWords || ( txt.length()-1 == ix+lineSize ) ) {
        wrapped.append( line);
        wrapped.append("\n");
        ix += lineSize;
      }
      else {
        char lastChar = line.charAt( line.length()-1);
        char nextChar = txt.charAt( ix+lineSize);

        if ( lastChar != ' ' && nextChar != ' ' ) {
          // Broken word
          int newIx = line.lastIndexOf( " ");
          if ( newIx > 0 && newIx != line.length()-1 ) {
            line = txt.substring( ix, ix+newIx);  // This removes the last space
            ix = ix + newIx;
          }
          else {
            ix += lineSize;
          }
        }
        else {  // The list char is exactly a space, so break here the line
          line = line.substring( 0, line.length()-1);
          ix += lineSize;
        }

        if ( line.length() > 0 && line.charAt( 0) == ' ' ) {
          line = line.substring( 1);
        }

        wrapped.append( line);
        wrapped.append("\n");
      }
    }

    wrapped.append( txt.substring( ix));

    return wrapped.toString();
  }

  
  /**
   * <p>This method returns a String with the json representation of <code>obj</code>. The <code>obj</code>
   * is parsed like an instance of type <code>typeOfT</code>.</p>
   * <p>The returned json is "pretty" indented.</p>
   * The method uses the Jackson library, so the method can do what Jackson can do...
   * @param obj the object
   * @param typeOfT the type of the object
   * @return a String with the json representation of <code>obj</code>
   * @see <a href="https://github.com/FasterXML/jackson">Jackson</a>
   * @deprecated
   */
  @Deprecated
  public static String toJsonPretty( Object obj, Type typeOfT) {
    return toJsonPretty( obj);
  }
  
  /**
   * Returns <code>true</code> if the JSON is valid and <code>false</code> otherwise.
   * @param json the JSON to test
   * @return<code>true</code> if the JSON is valid and <code>false</code> otherwise.
   */
  public static boolean isValidJSON( String json) {
    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.readValue( json, Object.class);
    }
    catch ( Exception ex) {
      return false;
    }
    
    return true;
  }

  /**
   * This method returns a String with the BASE64 representation of the String in 
   * clear text <code>clear</code>
   * @param clear the text to be encripted
   * @return the BASE64 representation of <code>clear</code>
   * @throws NullPointerException if <code>clear</code> is null
   */
  public static String encodeBase64( byte[] clear) {
    if ( clear == null ) {
      throw new NullPointerException();
    }

    return Base64.getEncoder().encodeToString( clear);
  }
  
  /**
   * This method returns a byte array with the clear text decripted from <code>base64</code>, a
   * String encripted with BASE64
   * @param base64 the text to be decripted
   * @return the BASE64 representation of <code>clear</code>
   * @throws NullPointerException if <code>clear</code> is null
   */
  public static byte[] decodeBase64toBytes( String base64) {
    if ( base64 == null ) {
      throw new NullPointerException();
    }
    
    byte b64[] = base64.getBytes( StandardCharsets.UTF_8);
    byte bClear[] = Base64.getDecoder().decode( b64);
    
    return bClear;
  }

  /**
   * This method returns a String with the BASE64 representation of the String in 
   * clear text <code>clear</code>
   * @param clear the text to be encripted
   * @return the BASE64 representation of <code>clear</code>
   * @throws NullPointerException if <code>clear</code> is null
   */
  public static String encodeBase64( String clear) {
    if ( clear == null ) {
      throw new NullPointerException();
    }

    return Base64.getEncoder().encodeToString( clear.getBytes( StandardCharsets.UTF_8));
  }
  
  /**
   * This method returns a String with the clear text decrypted from <code>base64</code>, a
   * String encrypted with BASE64. This method 'cleans' the <code>base64</code> of spaces, tabs, '\n' and '\r' characters. 
   * @param base64 the text to be decrypted
   * @return the clear text of <code>base64</code>
   * @throws NullPointerException if <code>base64</code> is null
   * @throws IllegalArgumentException if <code>base64</code> is contains invalid characters
   * @see #decodeBase64(String, boolean)
   */
  public static String decodeBase64( String base64) {
    return decodeBase64( base64, true);
  }
  
  /**
   * This method returns a String with the clear text decrypted from <code>base64</code>, a
   * String encrypted with BASE64.<br>
   * If the parameter <code>cleanBlanks</code> is <code>true</code>, the <code>base64</code> parameter is cleaned of spaces, 
   * tabs, '\n' and '\r' characters to avoid that those characters, that are 'blank' or 'empty' characters to us but are
   * invalid for Base64, make the method throw an <code>IllegalArgumentException</code>. It's cheating, because the right behavior
   * should be throwing the exception to force the user to clean the <code>base64</code> parameter before invoking the method, but
   * it's a so common scenario that the parameter <code>cleanBlanks</code> is there to allow the user to choose the behavior of
   * the method, and even the {@link #decodeBase64(String)} method cleans by default.
   * @param base64 the text to be decrypted
   * @param cleanBlanks if <code>true</code>, the <code>base64</code> parameter is cleaned of spaces, tabs, '\n' and '\r' characters
   * @return the clear text of <code>base64</code>
   * @throws NullPointerException if <code>base64</code> is null
   * @throws IllegalArgumentException if <code>base64</code> is contains invalid characters
   */
  public static String decodeBase64( String base64, boolean cleanBlanks) {
    if ( base64 == null ) {
      throw new NullPointerException();
    }
    
    if ( cleanBlanks ) {
      base64 = base64.trim();
      base64 = Utils.changeAll( base64, " ", "");
      base64 = Utils.changeAll( base64, "\t", "");
      base64 = Utils.changeAll( base64, "\r", "");
      base64 = Utils.changeAll( base64, "\n", "");
    }
    
    byte b64[] = base64.getBytes( StandardCharsets.UTF_8);
    byte bClear[] = Base64.getDecoder().decode( b64);
    
    return new String( bClear, StandardCharsets.UTF_8);
  }
  
  /**
   * This method writes in the file <code>file</code> the content of <code>clear</code> encoded in
   * Base64.
   * @param clear the data to encode and write
   * @param file the file where the Base64 data will be written
   * @throws IOException when something goes wrong
   */
  public static void encodeBase64( String clear, String file) throws IOException {
    Formats.encodeBase64( clear, new File( file));
  }
  
  /**
   * This method writes in the file <code>file</code> the content of <code>clear</code> encoded in
   * Base64.
   * @param clear the data to encode and write
   * @param file the file where the Base64 data will be written
   * @throws IOException when something goes wrong
   */
  public static void encodeBase64( String clear, File file) throws IOException {
    try ( OutputStream os = new FileOutputStream( file); ) {
      Formats.encodeBase64( clear, os);
    }
    catch ( IOException ex) {
      throw ex;
    }
  }
  
  /**
   * This method writes in the stream <code>os</code> the content of <code>clear</code> encoded in
   * Base64.<br>
   * This method DOES close the <code>os</code> stream.
   * @param clear the data to encode and write
   * @param os the stream
   * @throws IOException when something goes wrong
   */
  public static void encodeBase64( String clear, OutputStream os) throws IOException {
    OutputStream b64 = Base64.getEncoder().wrap( os);
    
    b64.write( clear.getBytes());
    
    b64.close();
  }
  
  /**
   * This method reads <code>is</code> and stores its content in <code>os</code> encoded in Base64.<br>
   * This method DOES close the <code>os</code> stream.
   * @param is the input stream, in clear text
   * @param os the output stream, in Base64
   * @throws IOException when something goes wrong
   */
  public static void encodeBase64( InputStream is, OutputStream os) throws IOException {
    OutputStream b64 = Base64.getEncoder().wrap( os);
    
    int c;
    byte buf[] = new byte[1024];
    
    while ( -1 != (c = is.read( buf))) {
      b64.write( buf, 0, c);
    }
    
    b64.close();
  }
  
  
  /**
   * This method reads the file <code>file</code>, with Base64 encoded content, and returns a byte array with its contents
   * decoded in clear text.
   * @param file the input file, in Base64
   * @return the file contents in clear text
   * @throws IOException when something goes wrong
   */
  public static byte[] decodeBase64( File file) throws IOException {
    return decodeBase64File( file.getCanonicalPath());
  }
  
  /**
   * This method reads the file <code>fileName</code>, with Base64 encoded content, and returns a byte array with its contents
   * decoded in clear text.
   * @param fileName the input file, in Base64
   * @return the file contents in clear text
   * @throws IOException when something goes wrong
   */
  public static byte[] decodeBase64File( String fileName) throws IOException {
    try ( InputStream is = Utils.findFile( fileName)) {
      return decodeBase64( is); 
    }
    catch ( IOException ex) {
      throw ex;
    }
  }
  
  /**
   * This method reads the InputStream <code>is</code>, with Base64 encoded content, and returns a byte array with its contents
   * decoded in clear text.
   * @param is the input file, in Base64
   * @return the stream contents in clear text
   * @throws IOException when something goes wrong
   */
  public static byte[] decodeBase64( InputStream is) throws IOException {
    InputStream b64 = Base64.getDecoder().wrap( is);
    
    byte res[] = null;
    
    int c;
    byte buf[] = new byte[1024];
    
    while ( -1 != (c = b64.read( buf))) {
      res = ByteArrays.concat( res, ByteArrays.subarray( buf, 0, c));
    }
    
    return res;
  }
  
  /**
   * This method reads <code>is</code>, with Base64 encoded content, and stores it in <code>os</code> as clear text. <br>
   * This method DOES close the <code>os</code> stream.
   * @param is the input stream, in Base64
   * @param os the output stream, in clear
   * @throws IOException when something goes wrong
   */
  public static void decodeBase64( InputStream is, OutputStream os) throws IOException {
    InputStream b64 = Base64.getDecoder().wrap( is);
    
    int c;
    byte buf[] = new byte[1024];
    
    while ( -1 != (c = b64.read( buf))) {
      os.write( buf, 0, c);
    }
    
    b64.close();
  }
  
  /**
   * This method returns the hexadecimal representation of a byte buffer. If <code>buf</code> is null, returns an
   * empty String.
   * @param buf the buffer
   * @return the hexadecimal representation
   */
  public static String getHexDump( byte buf[]) {
    if ( buf == null ) {
      return "";
    }
    
    StringBuilder sb = new StringBuilder();
    for ( byte b : buf) {
      sb.append( String.format("%02x", b & 0xff));
    }
    
    return sb.toString();
  }
  
  
  private static char[] chars = {' ', '!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}', '~'};
  
  /**
   * This method returns the hexadecimal representation of a byte buffer formated in the familiar format
   * of the hexadecimal editors. If <code>buf</code> is null, returns an
   * empty String.
   * @param buf the buffer
   * @return the hexadecimal representation
   */
  public static String getFormatedHexDump( byte buf[]) {
    if ( buf == null ) {
      return "";
    }
    
    
    StringBuilder sb = new StringBuilder();
    StringBuilder plainText = new StringBuilder();
    
    int line = 0;
    int col = 8;
    
    for ( int i = 0; i < buf.length; i++) {
      byte b = buf[i];
      
      if ( i % 16 == 0 ) {
        sb.append( "  " + plainText);
        sb.append( "\n" + String.format( "%04X", 16 * line) + " ");
        
        line ++;
        col = 8;
        plainText = new StringBuilder();
      }

      if ( i % 8 == 0 ) {
        sb.append( " ");
        plainText.append( " ");
        col++;
      }
      
      sb.append( " ");
      sb.append( String.format("%02x", b & 0xff));
      
      col += 3;
      
      {
        if ( b >= 32 && b <= 126 ) {
          plainText.append( chars[b-32]);
        }
        else {
          plainText.append( ".");
        }
      }
    }
    
    if ( col > 8 ) {  // se nos ha quedado una columna colgada
      for ( int i = col; i < 59; i++) {
        sb.append( " ");
      }
      sb.append( plainText);
    }
    
    return sb.toString();
  }
  
  /**
   * This method returns a byte array with the contents of the hexadecimal String. If <code>s</code> is null, 
   * returns an empty array.
   * @param s the String with the hexadecimal code
   * @return the byte[]
   */
  public static byte[] getArrayFromHexString( String s) {
    if ( s == null ) {
      return new byte[0];
    }
    
    int len = s.length();
    byte[] res = new byte[len / 2];
    for (int i = 0; i < len; i += 2) {
        res[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                             + Character.digit(s.charAt(i+1), 16));
    }
    
    return res;
  }
  
  
  //**********************************************
  // IPs
  
  /**
   * This method returns an IPv4 address in a byte[4]. The address can be 4 numbers (127.0.0.1) or the 
   * server name (localhost). It throws an IOException because the method uses the method InetAddress.getByName( ip).getAddress()
   * to allow the user to use host names and IP addresses.
   * @param ip the ip
   * @return a byte[4] with the ip
   * @throws IOException when something goes wrong
   */
  public static byte[] ipStringToByteArray( String ip) throws IOException {
    return InetAddress.getByName( ip).getAddress();
  }
  
  /**
   * This method returns an IPv6 address in a byte[16]. The address can be IPv6 address (fde4:2c6e:55c4:0105:0a00:27ff:fe0b:7859) or the 
   * server name (localhost). It throws an IOException because the method uses the method InetAddress.getByName( ip).getAddress()
   * to allow the user to use host names and IP addresses.
   * @param ip the ip
   * @return a byte[4] with the ip
   * @throws IOException when something goes wrong
   */
  public static byte[] ip6StringToByteArray( String ip) throws IOException {
    return InetAddress.getByName( ip).getAddress();
  }
  
  /**
   * This method returns a String with an IP address extracted from the byte buffer <code>buf</code>.
   * @param buf the buffer
   * @return the IP address
   * @throws IllegalArgumentException when buf.length is different than 4 bytes
   */
  public static String byteArrayToIPString( byte[] buf) throws IllegalArgumentException {
    if ( buf.length != 4 ) {
      throw new IllegalArgumentException( "Wrong buffer size. Buffer must have 4 bytes length.");
    }
    
    StringBuilder res = new StringBuilder();
    
    res.append( (int) buf[0] & 0xFF);
    res.append( '.');
    res.append( (int) buf[1] & 0xFF);
    res.append( '.');
    res.append( (int) buf[2] & 0xFF);
    res.append( '.');
    res.append( (int) buf[3] & 0xFF);
    
    return res.toString();
  }
  
  /**
   * This method returns a String with an IPv6 address extracted from the byte buffer <code>buf</code>.
   * @param buf the buffer
   * @return the IP address
   * @throws IllegalArgumentException when buf.length is not a valid address
   */
  public static String byteArrayToIP6String( byte[] buf) throws IllegalArgumentException {
    String res = "";
    try {
      res = InetAddress.getByAddress( buf).toString(); 
    }
    catch ( Exception ex) {
      throw new IllegalArgumentException();
    }
    
    return res; 
  }
  
  
  //**********************************************
  // Numbers
  
  /**
   * This method returns a byte[] with the representation of <code>number</code>, being the  length of 
   * the returned byte[] <code>bufferSize</code>. If <code>bufferSize</code> is not big enough to allocate
   * the number, the data is lost. If <code>bufferSize</code> is bigger, the data is padded in the beginning
   * with zeros.
   * @param number the number
   * @param bufferSize the length of the buffer
   * @return the resulting byte array
   */
  public static byte[] longToByteArray( long number, int bufferSize) {
    byte val[] = new byte[bufferSize];
    Arrays.fill( val, (byte)0);

    BigInteger nn = new BigInteger( "" + number);
    byte nnb[] = nn.toByteArray();
    
    for ( int i = 1; i <= Math.min( nnb.length,  val.length); i++ ) {
      val[ val.length - i] = nnb[ nnb.length - i];
    }
    
    return val;
  }
  
  /**
   * This method return the integer value of a byte buffer
   * @param buf the byte buffer
   * @return the integer
   */
  public static int byteArrayToInt( byte[] buf) {
    String hex = Formats.getHexDump( buf);

    return Integer.parseInt( hex, 16);
  }
}
