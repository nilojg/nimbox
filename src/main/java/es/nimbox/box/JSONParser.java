package es.nimbox.box;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * This class is a helper to convert JSON in ElasticBox. It isn't public because it's quite specialized.
 * 
 * 
 * @author nilo
 *
 */
class JSONParser {
  private final static String REGEX_SEPARATOR = "ñññ";
  
  private List<String> lTokens;
  
  
  public JSONParser() {
    super();
    
    lTokens = new ArrayList<String>();
  }
  
  public JSONParser( String json) {
    this();
    
    parse( json);
  }
  
  
  public void parse( String json) {
    // A hack to handle quotes
    String scaped = REGEX_SEPARATOR + "ñññññññññ" + REGEX_SEPARATOR;
    
    json = Utils.changeAll( json, "\\\"",scaped);
    json = Utils.changeAll( json, "\"",REGEX_SEPARATOR + "\"" + REGEX_SEPARATOR);
    json = Utils.changeAll( json, ":", REGEX_SEPARATOR + ":" + REGEX_SEPARATOR);
    json = Utils.changeAll( json, ",", REGEX_SEPARATOR + "," + REGEX_SEPARATOR);
    json = Utils.changeAll( json, "[", REGEX_SEPARATOR + "[" + REGEX_SEPARATOR);
    json = Utils.changeAll( json, "]", REGEX_SEPARATOR + "]" + REGEX_SEPARATOR);
    json = Utils.changeAll( json, "{", REGEX_SEPARATOR + "{" + REGEX_SEPARATOR);
    json = Utils.changeAll( json, "}", REGEX_SEPARATOR + "}" + REGEX_SEPARATOR);

    json = Utils.changeAll( json, scaped, "\"");
    
    if ( lTokens == null ) {
      lTokens = new ArrayList<String>();
    }
    else {
      lTokens.clear();
    }
    
    cleanTokens( Arrays.asList( json.split( REGEX_SEPARATOR)), lTokens);
  }
  
  
  public Collection<Pair<String,Object>> getElements() throws ParseException {
    List<Pair<String,Object>> res = new ArrayList<Pair<String,Object>>();
    
    for ( int i = 0; i < lTokens.size(); i++ ) {
      String token = lTokens.get( i);
      
      Pair<String,Object> value = new Pair<String,Object>();
      
      if ( token.equals( "[") ) {
        // Really nasty hack to treat the JSONs that are only a list, without id. It's ugly, but works...
        lTokens.add( 0, ":");
        lTokens.add( 0, "");
        
        return getElements();
      }
      else {
        value.first = token;
        i++;
        if ( i >= lTokens.size() || !lTokens.get( i).equals( ":") ) {
          throw new ParseException( "Cannot parse JSON after '" + value.first + "'");
        }
        i++;
        
        if ( lTokens.get( i).equals( "[") ) {
          int end = getGroupEnd( lTokens, '[', ']', i);
          value.second = getValueAsList( lTokens, i, end);
          i = end;
        }
        else if ( lTokens.get( i).equals( "{") ) {
          int end = getGroupEnd( lTokens, '{', '}', i);
          String dumm = getValue( lTokens, i, end);
          value.second = ElasticBox.fromJSON( dumm);
          i = end;
        }
        else {
          value.second = lTokens.get( i);
        }
        
        if ( ++i >= lTokens.size() || !lTokens.get( i).equals( ",") ) { // The end
          res.add( value);
          
          break;
        }
      }
      
      res.add( value);
    }
    
    return res;
  }

  
  /* This method "cleans" the list of tokens. The list of tokens has a lot of things separated by the "json separators",
   * like : or , . If a value contains one of those separators, for example "12:22:00", the SINGLE VALUE has been separated
   * in three tokens, which is wrong. This method cleans the tokens array and unites the values in single values counting 
   * the " characters.
  */
  private void cleanTokens( List<String> rawTk, List<String> lTk) {
    if ( rawTk == null || rawTk.isEmpty() ) {
      return;
    }
    
    for ( int i = 0; i < rawTk.size(); i++) {
      String tk = rawTk.get( i);
      String trimmed = tk.trim();
      
      if ( trimmed.equals( "") ) {  // No empty or carriage return strings
        continue;
      }
      
      if ( trimmed.equals( "\"") ) {
        int end = getNextChar( rawTk, '"', i);
        
        String value = getValue( rawTk, i, end);
        i = end;
        
        //lTk.add( trimmed);          // The begin quote in the result list
        lTk.add( value.toString()); // The value
        //lTk.add( rawTk[i].trim());  // The last quote, trimmed
      }
      else {
        lTk.add( trimmed);
      }
    }

    if ( !lTk.isEmpty() && lTk.get( 0).trim().equals( "{") ) {
      lTk.remove( 0);
    }
    if ( !lTk.isEmpty() && lTk.get( lTk.size() - 1).trim().equals( "}") ) {
      lTk.remove( lTk.size() - 1);
    }
  }
  
  private int getGroupEnd( List<String> lTk, char initToken, char lastToken, int init) {
    int res = init+1;

    int nextClose = getNextChar( lTk, lastToken, init);
    int nextOpen = getNextChar( lTk, initToken, init);
    
    if ( nextOpen < nextClose ) {
      res = getGroupEnd( lTk, initToken, lastToken, nextOpen);
    }
    else {
      res = nextClose;
    }
    
    return res;
  }
  
  private int getNextChar( List<String> lTk, char token, int init) {
    init++;
    
    for ( int i = init; i < lTk.size(); i++ ) {
      String tk = lTk.get( i).trim();
      
      if ( tk.equals( token + "") ) {
        return i;
      }
    }
    
    return lTk.size() - 1;
  }
  
  private String getValue( List<String> lTk, int init, int end) {
    StringBuilder value = new StringBuilder();
    while ( ++init < end) {
      value.append( lTk.get( init));  // All the things in the value added here, without trimming
    }
    
    return value.toString();
  }
  
  private List<Object> getValueAsList( List<String> lTk, int init, int end) {
    List<Object> value = new ArrayList<Object>();
    while ( ++init < end) {
      if ( lTk.get( init).equals( "{") ) {
        // and object
        int objEnd = getGroupEnd( lTokens, '{', '}', init);
        String dumm = getValue( lTokens, init, objEnd);
        ElasticBox box = ElasticBox.fromJSON( dumm);
        value.add( box); 
        
        init = objEnd;
      }
      else if ( !lTk.get( init).equals( ",") ) {
        value.add( lTk.get( init));  // All the things in the value added here, without trimming
      } 
    }
    
    return value;
  }
}
