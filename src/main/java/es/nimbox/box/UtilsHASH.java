package es.nimbox.box;

import java.security.MessageDigest;

/**
 * <p>This class contains some utility methods about byte, hexadecimal and HASH operations, that could be
 * useful to generate UIDs.</p>
 * The methods could be in the <code>Utils</code> class, but that class was becoming quite big and generic...  
 * 
 * @author nilo
 */
public class UtilsHASH {
  /**
   * Enumeration type for the supported algorithms 
   */
  public static enum HASH_TYPE { 
    /**
     * MD5, which is obsolete, yes, but valid for some uses
     */
    MD5, 
    /**
     * SHA1 algorithm
     */
    SHA1, 
    /**
     * SHA256 algorithm
     */
    SHA256};

  private static MessageDigest mdMD5;
  private static boolean noMD5 = false;
  
  private static MessageDigest mdSHA1;
  private static boolean noSHA1 = false;
  
  private static MessageDigest mdSHA256;
  private static boolean noSHA256 = false;
  
  
  private static void initHASHDigester() {
    if ( mdMD5 == null && !noMD5) {
      try {
        mdMD5 = MessageDigest.getInstance( "MD5");
        noMD5 = false;
      }
      catch ( Exception ex) {
        noMD5 = true;
        System.out.println( "There isn's an appropiate MD5 HASH implementation : " + ex.getMessage());
      }
    }
    if ( mdSHA1 == null && !noSHA1) {
      try {
        mdSHA1 = MessageDigest.getInstance( "SHA-1");
        noSHA1 = false;
      }
      catch ( Exception ex) {
        noSHA1 = true;
        System.out.println( "There isn's an appropiate SHA1 HASH implementation : " + ex.getMessage());
      }
    }
    if ( mdSHA256 == null && !noSHA256) {
      try {
        mdSHA256 = MessageDigest.getInstance( "SHA-256");
        noSHA256 = false;
      }
      catch ( Exception ex) {
        noSHA256 = true;
        System.out.println( "There isn's an appropiate SHA256 HASH implementation : " + ex.getMessage());
      }
    }
  }
  
  //**********************************************
  // HASHs
  
  /**
   * This method returns the hash for the text in the byte buffer buf, starting in offset and 
   * taking len bytes
   * @param type the type of desired HASH
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the SHA1 hash for the text in buf
   */
  public static byte[] getHASHByteArray( HASH_TYPE type, byte[] buf, int offset, int len) {
    initHASHDigester();
    
    MessageDigest md = null;
    
    switch ( type ) {
      case MD5:
        if ( !noMD5 ) {
          md = mdMD5;
        }
        break;
      case SHA1:
        if ( !noSHA1 ) {
          md = mdSHA1;
        }
        break;
      case SHA256:
        if ( !noSHA256 ) {
          md = mdSHA256;
        }
        break;
    }
    
    if ( md == null ) {
      return new byte[0];
    }
    
    md.reset();
    md.update( buf, offset, len);

    return md.digest();
  }
  
  
  //**********************************************
  // MD5
  
  /**
   * This method returns the MD5 hash for the String text
   * @param text the String 
   * @return the MD5 hash for text
   */
  public static String getMD5( String text) {
    return getMD5( text.getBytes());
  }
  
  /**
   * This method returns the MD5 hash for the text in the byte buffer buf
   * @param buf the text 
   * @return the MD5 hash for the text in buf
   */
  public static String getMD5( byte[] buf) {
    return getMD5( buf, 0, buf.length);
  }
  
  /**
   * This method returns the MD5 hash for the text in the byte buffer buf, starting in offset and taking len bytes
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the MD5 hash for the text in buf
   */
  public static String getMD5( byte[] buf, int offset, int len) {
    byte[] resultByte = getMD5ByteArray( buf, offset, len);
    
    // Write MD5
    return Formats.getHexDump( resultByte);
  }
  
  /**
   * This method returns the MD5 hash for the text in the byte buffer buf
   * @param buf the text
   * @return the MD5 hash for the text in buf
   */
  public static byte[] getMD5ByteArray( byte[] buf) {
    return getMD5ByteArray( buf, 0, buf.length);
  }
  
  /**
   * This method returns the MD5 hash for the text in the byte buffer buf, starting in offset and taking len bytes
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the MD5 hash for the text in buf
   */
  public static byte[] getMD5ByteArray( byte[] buf, int offset, int len) {
    return getHASHByteArray( HASH_TYPE.MD5, buf, offset, len);
  }
  

  
  
  //**********************************************
  // SHA1
  
  /**
   * This method returns the SHA1 hash for the String text
   * @param text the String 
   * @return the SHA1 hash for text
   */
  public static String getSHA1( String text) {
    return getSHA1( text.getBytes());
  }
  
  /**
   * This method returns the SHA1 hash for the text in the byte buffer buf
   * @param buf the text 
   * @return the SHA1 hash for the text in buf
   */
  public static String getSHA1( byte[] buf) {
    return getSHA1( buf, 0, buf.length);
  }
  
  /**
   * This method returns the SHA1 hash for the text in the byte buffer buf, starting in offset and taking len bytes
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the MD5 hash for the text in buf
   */
  public static String getSHA1( byte[] buf, int offset, int len) {
    byte[] resultByte = getSHA1ByteArray( buf, offset, len);
    
    // Write SHA1
    return Formats.getHexDump( resultByte);
  }
  
  /**
   * This method returns the SHA1 hash for the text in the byte buffer buf
   * @param buf the text
   * @return the SHA1 hash for the text in buf
   */
  public static byte[] getSHA1ByteArray( byte[] buf) {
    return getSHA1ByteArray( buf, 0, buf.length);
  }
  
  /**
   * This method returns the SHA1 hash for the text in the byte buffer buf, starting in offset and taking len bytes
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the MD5 hash for the text in buf
   */
  public static byte[] getSHA1ByteArray( byte[] buf, int offset, int len) {
    return getHASHByteArray( HASH_TYPE.SHA1, buf, offset, len);
  }
  
  
  //**********************************************
  // SHA256
  
  /**
   * This method returns the SHA256 hash for the String text
   * @param text the String 
   * @return the SHA256 hash for text
   */
  public static String getSHA256( String text) {
    return getSHA256( text.getBytes());
  }
  
  /**
   * This method returns the SHA256 hash for the text in the byte buffer buf
   * @param buf the text 
   * @return the SHA256 hash for the text in buf
   */
  public static String getSHA256( byte[] buf) {
    return getSHA256( buf, 0, buf.length);
  }
  
  /**
   * This method returns the SHA256 hash for the text in the byte buffer buf, starting in offset and taking len bytes
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the MD5 hash for the text in buf
   */
  public static String getSHA256( byte[] buf, int offset, int len) {
    byte[] resultByte = getSHA256ByteArray( buf, offset, len);
    
    // Write SHA256
    return Formats.getHexDump( resultByte);
  }
  
  /**
   * This method returns the SHA256 hash for the text in the byte buffer buf
   * @param buf the text
   * @return the SHA256 hash for the text in buf
   */
  public static byte[] getSHA256ByteArray( byte[] buf) {
    return getSHA256ByteArray( buf, 0, buf.length);
  }
  
  /**
   * This method returns the SHA256 hash for the text in the byte buffer buf, starting in offset and taking len bytes
   * @param buf the text
   * @param offset the offset
   * @param len the number of bytes
   * @return the MD5 hash for the text in buf
   */
  public static byte[] getSHA256ByteArray( byte[] buf, int offset, int len) {
    return getHASHByteArray( HASH_TYPE.SHA256, buf, offset, len);
  }
  
  
}
