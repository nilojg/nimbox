package es.nimbox.box;

/**
 * Thrown whenever a parameter doesn't match the correct "format". The "format" can be a date format, XML, a regular expression...
 * This exception is <b>unchecked</b>.
 * 
 * @author nilo
 */
public class ParseException extends RuntimeException {
  private static final long serialVersionUID = 6462427332465461923L;

  /**
   * This constructor initializes the exception with a message
   * @param msg the message
   */
  public ParseException( String msg) {
    super( msg);
  }
  
  /**
   * This constructor initializes the exception with a message and a cause
   * @param msg the message
   * @param th the cause
   */
  public ParseException( String msg, Throwable th) {
    super( msg, th);
  }
  
}
