

package es.nimbox.box;


import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.*;
import java.nio.charset.StandardCharsets;

/**
 * This class is only a box with several utility functions to avoid writing the same code in every project, like capturing the
 * exception when doing a <code>Thread.sleep</code>...
 * 
 * @author nilo.gonzalez
 *
 */
public class Utils {
  /**
   * The default date format. "yyyy/MM/dd HH:mm:ss.SSS"
   */
  public static final String DEFAULT_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss.SSS";
  
  /**
   * The default max file length to read.  Integer.MAX_VALUE
   */
  public static final int DEFAULT_MAX_READ = Integer.MAX_VALUE;
  
  private static final int DEFAULT_BUFFERSIZE = 1024;

  /**
   * To get the IP information, the UUID pings a web portal, and this is the default web. Stackoverflow, because there isn't any other site...
   */
  public static final String DEFAULT_IP_PORTAL    = "www.stackoverflow.com";

  /**
   * To get the IP information, the UUID pings a web portal, and this is the default port, 80
   */
  public static final int    DEFAULT_IP_PORT      = 80;

  private static String ownIP;
  
  private Utils() {}
  
  /**
   * This method returns the date <code>data</code> as a String in format 'yyyy/MM/dd HH:mm:ss'
   * @param date the date
   * @return the date in format 'yyyy/MM/dd HH:mm:ss'
   */
  public static String formatTime( long date) {
    return new SimpleDateFormat( DEFAULT_DATE_FORMAT).format( new Date( date));
  }
  
  /**
   * This method returns the date <code>data</code> as a String in format 'yyyy/MM/dd HH:mm:ss'
   * @param date the date
   * @return the date in format 'yyyy/MM/dd HH:mm:ss'
   */
  public static String formatTime( Date date) {
    return new SimpleDateFormat( DEFAULT_DATE_FORMAT).format( date);
  }
  
  /**
   * This method returns the date <code>date</code> as a String in format <code>format</code>, as described in
   * SimpleDateFormat javadoc
   * @param date the date
   * @param format the format
   * @return the String representation of the date
   * @see java.text.SimpleDateFormat
   * @throws ParseException when something goes wrong
   */
  public static String formatTime( long date, String format) throws ParseException {
    return formatTime( new Date( date), format);
  }
  
  /**
   * This method returns the date <code>date</code> as a String in format <code>format</code>, as described in
   * SimpleDateFormat javadoc
   * @param date the date
   * @param format the format
   * @return the String representation of the date
   * @see java.text.SimpleDateFormat
   * @throws ParseException when something goes wrong
   */
  public static String formatTime( Date date, String format) throws ParseException {
    try {
      SimpleDateFormat _sdf = new SimpleDateFormat( format);
      return _sdf.format( date);
    }
    catch ( Exception e) {
      throw new ParseException( e.getMessage());
    }
  }
  
  /**
   * This method returns a Date with the value of the String <code>date</code>, parsed in format 'yyyy/MM/dd HH:mm:ss'
   * @param date the date as a String
   * @return a Date object
   * @throws ParseException when <code>date</code> cannot be parsed
   */
  public static Date parseTime( String date) throws ParseException {
    try {
      return new SimpleDateFormat( DEFAULT_DATE_FORMAT).parse( date);
    }
    catch ( Exception e) {
      throw new ParseException( e.getMessage());
    }
  }
  
  /**
   * This method returns a Date with the value of the String <code>date</code>, parsed in format <code>format</code>, as described in
   * SimpleDateFormat javadoc
   * @param date the date as a String
   * @param format the format
   * @return a Date object
   * @throws ParseException when <code>date</code> cannot be parsed
   * @see java.text.SimpleDateFormat
   */
  public static Date parseTime( String date, String format) throws ParseException {
    try {
      SimpleDateFormat _sdf = new SimpleDateFormat( format);
      return _sdf.parse( date);
    }
    catch ( Exception e) {
      throw new ParseException( e.getMessage());
    }
  }
  
  /**
   * This method changes all occurrences of oldToken by newToken in text. It's like String.replaceAll but without regex.
   * @param text the original text
   * @param oldToken the token to be changed
   * @param newToken the new token 
   * @return text with all occurrences of oldToken changed
   */
  public static String changeAll( String text, String oldToken, String newToken) {
    if ( text == null || text.length() == 0 ) {
      return text;
    }
    
    if ( oldToken == null || oldToken.length() == 0 ) {
      return text;
    }
    
    if ( newToken == null ) {
      return text;
    }
      
    int ix = 0;
    int fx = 0;
    
    StringBuilder res = new StringBuilder();
    
    while ( ( ix = text.indexOf(oldToken, fx)) != -1 ) {
      res.append( text.substring( fx, ix));
      res.append( newToken);
      
      fx = ix + oldToken.length();
    }
    
    res.append( text.substring( fx));
    
    return res.toString();
  }
  
  /**
   * This method removes from <code>text</code> all occurrences of each character n the String <code>chars</code>. It's useful
   * to remove all the special characters of a String. For example:
   * 
   * <pre>
   *  String clean = removeAll( "Françoise O'Brian\t\n from \"l'Hospitalet", "'\"\t\nç");
   *  assertEquals( "Franoise OBrian from lHospitalet", clean); 
   * </pre>
   * @param text the original text
   * @param chars an String with the characters to be removed  
   * @return text with all occurrences of the characters in <code>chars</code> removed
   */
  public static String removeAll( String text, String chars) {
    if ( text == null || chars == null ) {
      return text;
    }
    
    for( int i = 0; i < chars.length(); i++) {
      text = changeAll( text, ""+chars.charAt( i), "");
    }
    
    return text;
  }
  
  /**
   * This method returns a String with the String <code>str</code> repeated <code>n</code> times. 
   * @param str the String to repeat
   * @param n the number of repetitions 
   * @return a String with the String <code>str</code> repeated <code>n</code> times
   */
  public static String fill( String str, int n) {
    StringBuilder res = new StringBuilder();
    
    for ( int i = 0; i < n; i++) {
      res.append( str);
    }
    
    return res.toString();
  }
  
  /**
   * <p>This method returns a short version of the String <code>val</code>. If <code>length</code> is 0 or less, 
   * the method returns a substring of <code>val</code> until the first carriage return (the first line of a 
   * multiline String).</p>
   * If <code>length</code> is greater than zero, the method will return a substring of <code>val</code> of
   * <code>length</code> size, adding a "..." inside the length. 
   * @param val the String to cut
   * @param length the desired length
   * @return the String in the desired size
   */
  public static String cutString( String val, int length) {
    String res = val;
    
    int ln = 0;
    
    if ( length <= 0 ) {
      ln = val.indexOf( "\n");
      if ( ln == -1 ) {
        res = val;
      }
      else {
        res = val.substring( 0, ln).trim();
      }
    }
    else {
      ln = val.length();
      
      if ( ln > length ) {
        if ( length > 3 ) {
          res = val.substring( 0, length - 3) + "...";
        }
        else {
          res = val.substring( 0, length);
        }
      }
    }
    
    return res;
  }
  
  /**
   * This method returns an String with the members of an enumeration or whatever other thing, between brackets and separated
   * by commas. For example, lets see the next code:
   * <pre>
   * enum VALUES_TYPE { VAL1, VAL1, VAL1, VAL1};
   * 
   * System.out.println( "Valid values are: " + getArrayListed( VALUES_TYPE.values() );
   * </pre>
   * This code prints:
   * <pre>
   * Valid values are: 'VAL1', 'VAL1', 'VAL1', 'VAL1'
   * </pre>
   * This method is useful to print error codes.  
   * @param list the list of values 
   * @return an String with the values
   */
  public static String getArrayListed( Object[] list) {
    StringBuilder err = new StringBuilder();
    List<String> arr = new ArrayList<String>();
    
    for ( Object obj : list) {
      arr.add( obj.toString());
    }
    
    Collections.sort( arr);
    int i = 0;
    for ( String vv : arr ) {
      if ( i++ != 0 ) {
        err.append( ", ");
      }
      err.append( "'" + vv.toString() + "'");
    }
    
    return err.toString();
  }
  
  /**
   * This method stops the current thread <code>milis</code> miliseconds. It's just because I *hate* to write 
   * the original Thread.sleep method with an empty try-catch along the code....
   * @param milis the time to sleep
   */
  public static void sleep( long milis) {
    try {
      Thread.sleep( milis);
    }
    catch ( InterruptedException e) {}   // NOSONAR   because this method exits ONLY to avoid handle this exception. If the thread is interrupted, well, fuck the thread...
  }
  
  /**
   * <p>This method does an active wait until the <code>until</code> time, in millis since... well, you know.</p>
   * This wait IS CPU CONSUMING, but is very accurate. There are two waits:
   * <ol>
   * <li>First, there's a regular Thread.sleep until 1ms before the <code>until</code> time. This wait doesn't 
   * consume CPU.</li> 
   * <li>The last 1ms is in an active wait doing an "infinite" loop comparing the current time with the 
   * <code>until</code> moment. This consumes CPU. A pile.</li>  
   * </ol>
   * So the last millisecond there's a great CPU work but the method stops just in the <code>until</code> time. Doing
   * a <code>Thread.sleep( until - currentTime)</code> doesn't work because it isn't enough accurate. You could make
   * the <code>until - currentTime</code> when the millisecond is finishing, so you will get control again after the
   * sleep when your desired <code>until</code> time is finishing. This method is appropriate when you want to get a 
   * very sharp control of time, for example, in multimedia applications.
   * @param until the moment to return
   * @return the return time, in millis
   */
  public static long sleepUntil( long until) {
    long now = System.currentTimeMillis();
    
    // The first wait
    long firstWait = until - now - 1;
    if ( firstWait > 0 ) {
      Utils.sleep( firstWait);
    }
    
    // The wait
    while ( now < until ) {
      now = System.currentTimeMillis();
      Utils.sleep( 0);   // This consumes more CPU but it's more accurate, and here time is gold!!
    }
    
    return now;
  }
  
  /**
   * This method tries to find a file, first in the CLASSPATH and then in the file system. If the file
   * cannot be found, the method returns null.
   * @param file the file to find
   * @return an InputStream pointing to the file or null if the file cannot be found
   * @throws IOException when something nasty happens
   */
  public static InputStream findFile( String file) throws IOException {
    InputStream is = null;
    
    if ( file != null ) {
      is = Utils.class.getResourceAsStream( file);
      if ( is == null ) {
        try {
          is = new FileInputStream( file);
        }
        catch ( FileNotFoundException ex) {
          is = null;
        }
      }
    }
    
    return is;
  }
  
  /**
   * This method finds all the files contained in <code>dir</code>, in the absolute file system. The <code>mask</code> is a 
   * regular expression like <code>./.*.java</code>. If the directory is empty, the result is empty, but not null.
   * @param dir the directory
   * @param mask the mask
   * @return a File array with the results
   */
  public static File[] listFiles( File dir, final String mask) {
    File[] res = dir.listFiles( new FilenameFilter() {
      public boolean accept( File dir, String name) {
        return name.matches( mask);
      }
    });
    
    if ( res == null ) {  
      res = new File[0];
    }
    
    return res;
  }
  
  /**
   * This method normalizes the directory name in <code>dirName</code> changing all the '\' for '/' and adds one to the end 
   * if it doesn't have one yet.<br>
   * The self and back paths, like '.' and '..', will not be changed nor removed. 
   * @param dirName the directory name to normalize.
   * @return the directory name normalized
   */
  public static String formatDirName( String dirName) {
    return formatDirName( dirName, false);
  }
  
  /**
   * This method normalizes the directory name in <code>dirName</code> changing all the '/' and '\' for the same character 
   * and adds one to the end if it doesn't have one yet. In the end, if <code>systemSeparator</code> is <code>true</code>,
   * all separators will be the <code>File.separator</code> ('/' in Unix-like systems and '\' on Windows) and '/' otherwise.<br>
   * The self and back paths, like '.' and '..', will not be changed nor removed. 
   * @param dirName the directory name to normalize.
   * @param systemSeparator if <code>true</code>, will use <code>File.separator</code>, '/' otherwise.
   * @return the directory name normalized
   */
  public static String formatDirName( String dirName, boolean systemSeparator) {
    if ( dirName == null ) {
      return null;
    }
    
    String ch = systemSeparator? File.separator : "/";
    String old = systemSeparator? "/" : File.separator;
    
    dirName = changeAll( dirName, old, ch);
    
    if ( !dirName.endsWith( ch) ) {
      dirName = dirName + ch;
    }
    
    return dirName;
  }
  
  /**
   * This method returns all the substrings in <code>message</code> that match <code>regex</code>. If there aren't
   * any substring that matches the regex, the list is empty.  
   * @param message the message
   * @param regex the regular expression
   * @return a list with all the substrings that matches the regex.
   * @throws ParseException when the parameter <code>regex</code> is a malformed regular expression
   */
  public static List<String> extractRegex( String message, String regex) throws ParseException {
    List<String> res = new ArrayList<String>();
    
    try {
      Pattern pattern = Pattern.compile( regex);
      Matcher matcher = pattern.matcher( message);
      while( matcher.find() ) {
        res.add( matcher.group());
      }
    }
    catch ( Exception e) {
      throw new ParseException( e.getMessage());
    }
    
    return res;
  }
  
  /**
   * This method returns the first substring in <code>message</code> that matches <code>regex</code>. If there aren't
   * any substring that matches the regex, the method returns null.  
   * @param message the message
   * @param regex the regular expression
   * @return the first substring that matches the regex or null
   * @throws ParseException when the parameter <code>regex</code> is a malformed regular expression
   */
  public static String extractFirstRegex( String message, String regex) throws ParseException {
    String res = null;
    
    try {
      Pattern pattern = Pattern.compile( regex);
      Matcher matcher = pattern.matcher( message);
      if ( matcher.find() ) {
        res = matcher.group();
      }
    }
    catch ( Exception e) {
      throw new ParseException( e.getMessage());
    }
    
    return res;
  }
  
  /**
   * <p>This method returns the content of the file <code>is</code> as a String. The String is encoded with 
   * UTF-8 and is as much DEFAULT_MAX_READ characters.</p> 
   * It's only because I repeat this code again and again and again...
   * @param is the file
   * @return a String with the content of the file
   * @throws IOException when something goes wrong
   */
  public static String readFromInputStream( InputStream is) throws IOException {
    return readFromInputStream( is, StandardCharsets.UTF_8.name(), DEFAULT_MAX_READ);
  }
  
  /**
   * <p>This method returns the content of the file <code>is</code> as a String. The String is encoded with 
   * <code>charset</code> and is as much DEFAULT_MAX_READ characters.</p> 
   * It's only because I repeat this code again and again and again...
   * @param is the file
   * @param charset the charset
   * @return a String with the content of the file
   * @throws IOException when something goes wrong
   */
  public static String readFromInputStream( InputStream is, String charset) throws IOException {
    return readFromInputStream( is, charset, DEFAULT_MAX_READ);
  }
  
  /**
   * <p>This method returns the content of the file <code>is</code> as a String. The String is encoded with 
   * UTF-8 and is as much <code>maxRead</code> characters.</p> 
   * It's only because I repeat this code again and again and again...
   * @param is the file
   * @param maxRead the max number of characters to be read
   * @return a String with the content of the file
   * @throws IOException when something goes wrong
   */
  public static String readFromInputStream( InputStream is, int maxRead) throws IOException {
    return readFromInputStream( is, StandardCharsets.UTF_8.name(), maxRead);
  }
  
  /**
   * <p>This method returns the content of the file <code>is</code> as a String. The String is encoded with 
   * <code>charset</code> UTF-8 and is as much <code>maxRead</code> characters.</p> 
   * It's only because I repeat this code again and again and again...
   * @param is the file
   * @param charset the charset
   * @param maxRead the max number of characters to be read
   * @return a String with the content of the file
   * @throws IOException when something goes wrong
   */
  public static String readFromInputStream( InputStream is, String charset, int maxRead) throws IOException {
    StringBuilder body = new StringBuilder();
    
    byte[] buf = new byte[DEFAULT_BUFFERSIZE];
    int r = 0;
    while ( ( r = is.read( buf)) >= 0 && body.length() <= maxRead ) {
      body.append( new String( buf, 0, r, charset));
    }
    
    return body.toString();
  }
  
  /**
   * This method returns a byte array with all the content of the file <code>is</code>. The byte array can be quite big, so 
   * be careful...
   * @param is the file
   * @return a byte array with the whole file
   * @throws IOException when something goes wrong
   */
  public static byte[] readBytesFromInputStream( InputStream is) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    int c;
    byte buf[] = new byte[1024];
    
    while ( -1 != (c = is.read( buf))) {
      baos.write( buf, 0, c);
    }
    
    return baos.toByteArray();
  }
  
  /**
   * <p>This method writes in <code>os</code> the String <code>text</code> encoded with <code>charset</code>.</p>
   * It's here just because <code>readFromInputStream</code> exists. For the symmetry...
   * @param os the OutputStream
   * @param text the text to be written
   * @throws IOException when something goes wrong
   */
  public static void writeInOutputStream( OutputStream os, String text) throws IOException {
    writeInOutputStream( os, text, StandardCharsets.UTF_8.name());
  }
  
  /**
   * <p>This method writes in <code>os</code> the String <code>text</code> encoded with <code>charset</code>.</p>
   * It's here just because <code>readFromInputStream</code> exists. For the symmetry...
   * @param os the OutputStream
   * @param text the text to be written
   * @param charset the charset
   * @throws IOException when something goes wrong
   */
  public static void writeInOutputStream( OutputStream os, String text, String charset) throws IOException {
    os.write( text.getBytes( charset));
  }


  /**
   * This method tries to get the local IP. Getting the local IP isn't easy, because there could be several net interfaces, so
   * it first tries to ping the <code>getIpTestPortal()</code> site. This could fail if the program runs far from internet. If that
   * fails, calls <code>InetAddress.getLocalHost().getAddress()</code>.<br>
   * If everything fails, well, the IP will be 0.0.0.0.
   * @return the local IP.
   */
  public static String getIP() {
    return getIP( DEFAULT_IP_PORTAL, DEFAULT_IP_PORT);
  }

  public static String getIP( String ipPortal, int ipPort) {
    if ( ownIP == null ) {
      Socket socket = new Socket();
      byte[] buf = { 0,0,0,0 };
      boolean found = false;

      try {
        // First, open a socket to the obvious place ;P to get the preferred IP
        socket.connect( new InetSocketAddress( ipPortal, ipPort), 10);
        buf = socket.getLocalAddress().getAddress();
        found = true;
      }
      catch ( SocketTimeoutException ex) {
        // Timeout doesn't mind, because this has the proper output IP
        buf = socket.getLocalAddress().getAddress();
        found = !Arrays.equals( buf, new byte[]{0,0,0,0});
      }
      catch ( Exception ex) {
        // Nothing to do
      }

      try {
        socket.close();
      }
      catch ( IOException e) {
        // Nothing to do
      }

      // Bad luck, let's find any IP between the several IPs that THIS computer can have...
      if ( !found ) {
        try {
          buf = InetAddress.getLocalHost().getAddress();
        }
        catch ( UnknownHostException e1) {
          // Nothing to do
        }
      }

      ownIP = Formats.getHexDump( buf);
    }

    return ownIP;
  }
}



