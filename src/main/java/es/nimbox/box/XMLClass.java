
package es.nimbox.box;

import java.lang.annotation.*;


/**
 * <p>This annotation defines some parameters about the way an object is translated to XML by the {@link XMLUtils#toXML(Object)}
 * methods.</p>
 * <p>These annotations are optional, and if NONE of the attributes of the Object sent to the <code>toXML</code> is annotated, 
 * the <code>toXML</code> will
 * return a XML with ALL the "first generation" attributes of the class with their "String" values. This means that the 
 * <code>toXML</code> method will create a XML with the String representation of each attribute and it will loop all the 
 * arrays and <code>Collections</code> adding the String of each element in them, but nothing more. If the class contains and
 * object of another class the <code>toXML</code> method WILL NOT recourse in the attribute, just will print whatever it gets
 * from the <code>toString</code> of the attribute class. The first generation, no recursion. It's useful to debug...</p>
 * 
 * <p>In the other hand, if ANY attribute of the Object sent to the <code>toXML</code> is annotated, the <code>toXML</code> will
 * return a XML with ONLY the annotated attributes with <code>XMLElement</code> or <code>XMLAttribute</code>. The attributes
 * without annotation will be ignored, so you can define your classes in a way that the XML contains only the attributes that you
 * want to show and avoid that "boolean stop" that you use to stop a background thread.</p>
 * 
 * <p>The annotation <code>XMLClass</code> is applicated to the class level, and defines the name of the main tag, the name space of
 * the main tag and its children and the indent size of the generated XML.</p>
 * 
 * <p>The annotation <code>XMLAttribute</code> is applicated to the class attributes, and defines that a class attribute will be 
 * written in the XML as an attribute of the main tag. The optional <code>XMLAttribute</code> parameter <code>name</code> only 
 * define the name of the attribute. If the parameter <code>name</code> isn't set, then the name of the class attribute will be
 * used.</p> 
 * 
 * <p>The annotation <code>XMLElement</code> is applicated to the class attributes, and defines that a class attribute will be 
 * written in the XML as a tag of the main tag. The optional <code>XMLElement</code> parameter <code>name</code> only 
 * define the name of the tag. If the parameter <code>name</code> isn't set, then the name of the class attribute will be
 * used.</p> 
 *  
 * <p>The annotations <code>XMLElement</code> or <code>XMLAttribute</code> parameters define the name of the tag that will contain 
 * the value of the variable. If the <code>name</code> isn't set, then the name of the attribute will be used. For example:</p>
 * <pre>
 * &#64;XMLClass( name="Tut", namespace="nim", indent=4)
 * public class ToXMLTestAnnotated {
 *   &#64;XMLAttribute( name = "identification")
 *   int id;
 *   &#64;XMLAttribute
 *   int ix;
 * 
 *   &#64;XMLElement( name = "nombre")
 *   String field1;
 *   &#64;XMLElement
 *   String field2;
 *   String field3;
 *   ...
 * </pre>
 * <p>This code will generate an XML like this:</p>
 * <pre>
 * &lt;nim:Tut identification='1' ix='34'&gt;
 *     &lt;nim:nombre&gt;Vader&lt;/nim:nombre&gt;
 *     &lt;nim:field2&gt;black&lt;/nim:field2&gt;
 *     ...
 * </pre>
 * <p>Please, notice that the <code>field3</code> isn't included in the output and that the tags are indented four spaces.</p>
 * 
 * <p>All the annotations parameters are optional, but some parameters are more optional that others... If the parameter 
 * <code>name</code> is set, then the name value is set, dot. But the annotation parameters <code>namespace</code> and
 * <code>indent</code> can be overridden with some version of the {@link XMLUtils#toXML(Object)} methods, particularly the
 * {@link XMLUtils#toXML(Object, int, int, String)} method, which overrides the name space and the indentation of the
 * resulting XML.</p>
 * 
 * <p>Finally, if the indent value is zero, because the annotation attribute is zero (the default value) or because the 
 * parameters of the invocation to the {@link XMLUtils#toXML(Object)} methods are zero, the resulting XML is NOT indented
 * nor formated, just a single line with all the tags without any carriage return.</p>
 * 
 * @see XMLAttribute
 * @see XMLElement
 * @see XMLUtils#toXML( Object)
 * 
 * @author nilo
 */
@Retention(value=RetentionPolicy.RUNTIME) 
@Target(value=ElementType.TYPE)
public @interface XMLClass {
  /**
   * The namespace of the contents. If it isn't defined, there won't be a namespace
   * @return the namespace
   */
  String namespace() default "";
  
  /**
   * The tag name. If it isn't defined, the class name will be used 
   * @return the name of the tag
   */
  String name() default "";
  
  /**
   * The number of spaces used to indent the result. If it isn't defined, the XML won't be indented.
   * @return the number of spaces used to indent the resulting XML
   */
  int indent() default 0;
  
}
