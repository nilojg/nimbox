package es.nimbox.box;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * This class contains some utility methods to instantiate <i>beans</i>, Java objects with setters and getters, which will be
 * initialized using the setters passing parameters read from XMLs, Maps...
 * 
 * @author nilo
 *
 */
public class Reflect {
  
  private Reflect() {}

  /**
   * <p>This method creates a new instance of the class className invoking the setter of every parameter in the <code>xml</code> 
   * parameter. The parameter must be a valid XML with the format:</p>
   * <pre>
   * &lt;....&gt;
   *   &lt;param name="size"&gt;10&lt;/param&gt;
   *   &lt;param name="username"&gt;john&lt;/param&gt;
   *   &lt;param name="password"&gt;psssst&lt;/param&gt;
   * &lt;/...&gt;
   * </pre> 
   * <p>The method will create a new instance of <code>className</code> and will invoke the <code>setSize</code>, <code>setUsername</code>
   * and <code>setPassword</code> methods.</p>
   * The class <code>className</code> <b>must</b> have a constructor without arguments
   * @param className the class to instance
   * @param xml the parameters in XML format
   * @return a new instance of the object
   * @throws ParseException if there's any problem parsing XML
   * @throws ClassNotFoundException when the class can't be found
   * @throws InstantiationException if this Class represents an abstract class, an interface, an array class, a primitive type, or void; or if the class has no nullary constructor; or if the instantiation fails for some other reason.
   * @throws IllegalAccessException if the class or its nullary constructor is not accessible or if a setter Method object is enforcing Java language access control and the underlying method is inaccessible..
   * @throws NumberFormatException when one of the values must be converted no a number and the value can't be converted
   * @throws IllegalArgumentException if the method is an instance method and the specified object argument is not an instance of the class or interface declaring the underlying method (or of a subclass or implementor thereof); if the number of actual and formal parameters differ; if an unwrapping conversion for primitive arguments fails; or if, after possible unwrapping, a parameter value cannot be converted to the corresponding formal parameter type by a method invocation conversion
   * @throws InvocationTargetException if the underlying method throws an exception
   * @throws NoSuchMethodException if there isn't a default constructor without arguments
   */
  public static Object instanceBean( String className, String xml) throws ClassNotFoundException, 
                                                                          InstantiationException, 
                                                                          IllegalAccessException, 
                                                                          NumberFormatException, 
                                                                          IllegalArgumentException, 
                                                                          InvocationTargetException,
                                                                          NoSuchMethodException {
    return instanceBean( className, XMLUtils.str2node( xml));
  }
  
  /**
   * <p>This method creates a new instance of the class <code>className</code> invoking the setter of every parameter in the 
   * <code>xml</code> parameter. The parameter must be a
   * valid XML with the format:</p>
   * <pre>
   * &lt;....&gt;
   *   &lt;param name="size"&gt;10&lt;/param&gt;
   *   &lt;param name="username"&gt;john&lt;/param&gt;
   *   &lt;param name="password"&gt;psssst&lt;/param&gt;
   * &lt;/...&gt;
   * </pre>
   * <p>The method will create a new instance of <code>className</code> and will invoke the <code>setSize</code>, <code>setUsername</code>
   * and <code>setPassword</code> methods.</p>
   * The class <code>className</code> <b>must</b> have a constructor without arguments. 
   * @param className the class to instance
   * @param xml the parameters in XML format
   * @return a new instance of the object
   * @throws ParseException if there's any problem parsing XML
   * @throws ClassNotFoundException when the class can't be found
   * @throws InstantiationException if this Class represents an abstract class, an interface, an array class, a primitive type, or void; or if the class has no nullary constructor; or if the instantiation fails for some other reason.
   * @throws IllegalAccessException if the class or its nullary constructor is not accessible or if a setter Method object is enforcing Java language access control and the underlying method is inaccessible..
   * @throws NumberFormatException when one of the values must be converted no a number and the value can't be converted
   * @throws IllegalArgumentException if the method is an instance method and the specified object argument is not an instance of the class or interface declaring the underlying method (or of a subclass or implementor thereof); if the number of actual and formal parameters differ; if an unwrapping conversion for primitive arguments fails; or if, after possible unwrapping, a parameter value cannot be converted to the corresponding formal parameter type by a method invocation conversion
   * @throws InvocationTargetException if the underlying method throws an exception
   * @throws NoSuchMethodException if there isn't a default constructor without arguments
   */
  public static Object instanceBean( String className, Node xml) throws  ClassNotFoundException, 
                                                                         InstantiationException, 
                                                                         IllegalAccessException, 
                                                                         NumberFormatException, 
                                                                         IllegalArgumentException, 
                                                                         InvocationTargetException,
                                                                         NoSuchMethodException {
    Map<String,String> htParams = new HashMap<String,String>();
    
    try {
      NodeList list = xml.getChildNodes();
      for ( int i = 0; i < list.getLength(); i++) {
        Node node = list.item( i);
        
        String nodeName = node.getNodeName();
        
        if ( nodeName.equals( "param") ) {
          String pName = node.getAttributes().getNamedItem( "name").getTextContent().trim();
          String pValue = node.getTextContent();
          
          htParams.put( pName, pValue);
        }
      }
    }
    catch ( Exception e) {
      throw new ParseException( "Can't instantiate the DB Stack class. There's a problem with XML.", e);
    }
    
    return instanceBean( className, htParams);
  }
  
  /**
   * This method creates a new instance of the class <code>className</code> invoking the setter of every parameter in the 
   * <code>htParams</code> Map. The Map stores in the key
   * the parameter name, and its value in the value.
   * The class <code>className</code> <b>must</b> have a constructor without arguments.
   * @param className the class to instance
   * @param htParams the parameters
   * @return a new instance of the object
   * @throws ClassNotFoundException when the class can't be found
   * @throws InstantiationException if this Class represents an abstract class, an interface, an array class, a primitive type, or void; or if the class has no nullary constructor; or if the instantiation fails for some other reason.
   * @throws IllegalAccessException if the class or its nullary constructor is not accessible or if a setter Method object is enforcing Java language access control and the underlying method is inaccessible..
   * @throws NumberFormatException when one of the values must be converted no a number and the value can't be converted
   * @throws IllegalArgumentException if the method is an instance method and the specified object argument is not an instance of the class or interface declaring the underlying method (or of a subclass or implementor thereof); if the number of actual and formal parameters differ; if an unwrapping conversion for primitive arguments fails; or if, after possible unwrapping, a parameter value cannot be converted to the corresponding formal parameter type by a method invocation conversion
   * @throws InvocationTargetException if the underlying method throws an exception
   * @throws NoSuchMethodException if there isn't a default constructor without arguments
   */
  public static Object instanceBean( String className, Map<String,String> htParams) throws ClassNotFoundException, 
                                                                                           InstantiationException, 
                                                                                           IllegalAccessException, 
                                                                                           NumberFormatException, 
                                                                                           IllegalArgumentException, 
                                                                                           InvocationTargetException,
                                                                                           NoSuchMethodException {
    
    // REFLECTION UNLEASHED!!!!!
    // Enter at your own risk.
    
    // Carga de la clase del driver
    Class<?> clazz = Class.forName( className);
    
    // Nueva instancia que asignamos al DataSource de la clase
    Object res = clazz.getDeclaredConstructor().newInstance();
    
    // Y ahora, a setearle cositas
    for ( Iterator<String> it = htParams.keySet().iterator(); it.hasNext(); ) {
      String paramName = it.next();
      String paramValue = htParams.get( paramName);
      String setter = getSetterName( paramName);
      
      if ( setter == null ) {
        throw new IllegalArgumentException( "No valid setter found for [" + paramName + "] parameter");
      }
      
      // Pillamos el metodo para el setter adecuado
      Method meth = getSetMethod( clazz, setter);
      if ( meth == null ) {
        throw new IllegalArgumentException( "No valid setter found for [" + paramName + "] parameter");
      }
      
      Class<?>[] paramTypes = meth.getParameterTypes();
      
      // Ahora a invocar el metodo adecuado segun el argumento que tenga
      if ( paramTypes[0] == String.class ) {
        meth.invoke( res, paramValue);
      }
      else if ( paramTypes[0] == Integer.class || paramTypes[0] == int.class ) {
        meth.invoke( res, Integer.parseInt( paramValue));
      }
      else if ( paramTypes[0] == Long.class || paramTypes[0] == long.class ) {
        meth.invoke( res, Long.parseLong( paramValue));
      }
      else if ( paramTypes[0] == Float.class || paramTypes[0] == float.class ) {
        meth.invoke( res, Float.parseFloat( paramValue));
      }
      else if ( paramTypes[0] == Double.class || paramTypes[0] == double.class ) {
        meth.invoke( res, Double.parseDouble( paramValue));
      }
      else if ( paramTypes[0] == Boolean.class || paramTypes[0] == boolean.class ) {
        meth.invoke( res, Boolean.parseBoolean( paramValue));
      }
      else if ( paramTypes[0] == Short.class || paramTypes[0] == short.class ) {
        meth.invoke( res, Short.parseShort( paramValue));
      }
      else if ( paramTypes[0] == Byte.class || paramTypes[0] == byte.class ) {
        meth.invoke( res, Byte.parseByte( paramValue));
      }
      else { // Seguro que esto da excepcion, pero es cosa suya por tener tipos raros. Y con suerte, funciona el cast...
        meth.invoke( res, paramValue);
      }
    }
    
    return res;
  }
  
  /**
   * This method finds in the class clazz a method with name methodName and one parameter, because setters only have one parameter.
   * @param clazz the class
   * @param methodName the method to find
   * @return the method
   */
  private static Method getSetMethod( Class<?> clazz, String methodName) {
    Method[] meths = clazz.getMethods();
    
    for ( Method meth : meths ) {
      if ( meth.getName().equals( methodName) ) {
        Class<?>[] paramTypes = meth.getParameterTypes();
        
        // Un setter como es debido solo tiene un argumento
        if ( paramTypes.length == 1 ) {
          return meth;
        }
      }
    }
      
    return null;
  }
  
  /**
   * This method returns the setter name of a variable. For example, "date" returns "setDate".
   * @param var the variable name
   * @return the setter name or null if var is null or empty
   */
  private static String getSetterName( String var) {
    if ( var == null || var.equals( "") ) {
      return null;
    }
    
    return "set" + var.substring( 0, 1).toUpperCase() + var.substring( 1); 
  }
  
}
