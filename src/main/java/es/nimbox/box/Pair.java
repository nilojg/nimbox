package es.nimbox.box;

/**
 * This class provides a way to store a pair of objects of whatever type. It's like the C++ <code>std::pair</code> class, 
 * useful to return a pair of related objects from a function or to have a pair of things in a list.<br>
 * The attributes <code>first</code> and <code>second</code> are public for simplicity, but there are a pair of 
 * traditional setter and getter too.
 *
 * @author nilo
 *
 * @param <T> the type of the <code>first</code> object
 * @param <U> the type of the <code>second</code> object
 */
public class Pair<T,U> {
  
  /**
   * The first object of the pair.
   */
  public T first;
  
  /**
   * The second object of the pair
   */
  public U second;
  
  
  /**
   * This constructor initializes the object with null values
   */
  public Pair() {
    first = null;
    second = null;
  }
  
  /**
   * This constructor creates the <code>Pair</code>
   * @param first the first object of the pair
   * @param second the second object of the pair
   */
  public Pair( T first, U second) {
    this.first = first;
    this.second = second;
  }
  

  @Override
  public String toString() {
    StringBuilder res = new StringBuilder();

    res.append( "{[");
    res.append( first != null ? first.toString() : "null");
    res.append( "] : [");
    res.append( second != null ? second.toString() : "null");
    res.append( "]}");
    
    return res.toString();
  }

  @Override
  public boolean equals( Object other) {
    if ( other == null ) {
      return false;
    }
    
    if ( this.getClass() != other.getClass()) {
      return false;
    }
    
    Pair<?,?> _other = (Pair<?,?>)other;
    
    boolean bf = ( this.first == null && _other.first == null )
                 || 
                 ( this.first != null && this.first.equals( _other.first) );
    
    boolean bs = ( this.second == null && _other.second == null )
                 || 
                 ( this.second != null && this.second.equals( _other.second) );
    
    return  bf && bs;
  }
  
  @Override
  public int hashCode() {
    return this.toString().hashCode();
  }

  /**
   * This method returns the first component of the pair 
   * @return the first component of the pair
   */
  public T getFirst() {
    return first;
  }

  /**
   * This method set the first element of the pair
   * @param first the first element of the pair
   */
  public void setFirst( T first) {
    this.first = first;
  }

  /**
   * This method returns the second component of the pair 
   * @return the second component of the pair
   */
  public U getSecond() {
    return second;
  }

  /**
   * This method set the second element of the pair
   * @param second the second element of the pair
   */
  public void setSecond( U second) {
    this.second = second;
  }
  
  
}
