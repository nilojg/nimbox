package es.nimbox.box;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSException;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;


/**
 * <p>This class contains some utilities to work with XML. Some functions are useful to indent XML (<code>indentXML</code>), to find 
 * values using XPath (<code>find*</code> and <code>get*</code>), to transform a String in a XML <code>Node</code> 
 * (<code>str2node</code>) and vice versa (<code>indent</code>), to add or delete elements of an XML, to apply a XSL 
 * style sheet to a XML (<code>applyXSLT</code>) or to get the XML representation of a Java Object (<code>toXML</code>).</p>
 * 
 * <p>The aim of the functions are to NOT add external dependencies to your project, so the class uses the XML implementation present
 * in the Java standard library. This is good because doesn't put any jar in your project, but in the other hand the XML related
 * standards are quite old. For example, the XPath version is 1.0, which is quite old and its syntax is not very easy to use...</p>
 * 
 * <p>The functions are easy to use, but the <code>toXML</code> functions can be used with the annotations <code>XMLClass</code>,
 * <code>XMLAttribute</code> and <code>XMLElement</code>. To get a better understanding of the way of working of the 
 * <code>toXML</code> functions, please read the documentation of the <code>XMLClass</code> annotation.</p>
 * 
 * @see XMLClass
 * @see XMLAttribute
 * @see XMLElement
 * 
 * @author nilo
 *
 */
public class XMLUtils {
  
  private XMLUtils() {}
 
  /**
   * This method returns a XML node parsing the XML string passed as argument. Is useful to convert a String with XML in a Node object.
   * @param xml the XMl to parse
   * @return the XML as a Node
   * @throws ParseException if there're same problems parsing the XML
   */
  public static Node str2node( String xml) throws ParseException {
    return str2doc( xml).getDocumentElement();
  }
  
  /**
   * This method returns a XML document parsing the XML string passed as argument.
   * @param xml the XMl to parse
   * @return the XML as a Document
   * @throws ParseException if there're same problems parsing the XML
   */
  public static Document str2doc( String xml) throws ParseException {
    try {
      DocumentBuilderFactory parser = DocumentBuilderFactory.newInstance();
      parser.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD, "");
      parser.setAttribute( XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      DocumentBuilder db = parser.newDocumentBuilder();
      Document doc = db.parse( new InputSource( new StringReader( xml)));
  
      return doc;
    }
    catch ( Exception ex) {
      throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
    }
  }
  
  /**
   * <p>This method returns a String with the XML representation of the <code>obj</code> Object.</p>
   * To get a better understanding of the way of working of the <code>toXML</code> functions, please read the 
   * documentation of the <code>XMLClass</code> annotation.
   * @param obj the object
   * @return a String with the XML representation of the <code>obj</code> Object
   * @see XMLClass
   * @see XMLAttribute
   * @see XMLElement
   */
  public static String toXML( Object obj) {
    int indent = 0;
    
    XMLClass anClazz = obj.getClass().getAnnotation( XMLClass.class);
    if ( anClazz != null ) {
      indent = anClazz.indent();
    }
    
    return toXML( obj, indent, 0);
  }
  
  /**
   * <p>This method returns a String with the XML representation of the <code>obj</code> Object. The XML is prefixed with 
   * the namespace <code>nameSpace</code>.</p>
   * <p>If <code>nameSpace</code> is null or empty, the XML tags doesn't have namespace. </p>
   * To get a better understanding of the way of working of the <code>toXML</code> functions, please read the 
   * documentation of the <code>XMLClass</code> annotation.
   * @param obj the object
   * @param nameSpace the namespace
   * @return a String with the XML representation of the <code>obj</code> Object
   * @see XMLClass
   * @see XMLAttribute
   * @see XMLElement
   */
  public static String toXML( Object obj, String nameSpace) {
    int indent = 0;
    XMLClass anClazz = obj.getClass().getAnnotation( XMLClass.class);
    if ( anClazz != null ) {
      indent = anClazz.indent();
    }
    
    return toXML( obj, indent, 0, nameSpace);
  }
  
  /**
   * <p>This method returns a String with the XML representation of the <code>obj</code> Object. The XML is indented with 
   * <code>indent</code> spaces.</p>
   * <p>If <code>indent</code> is zero, the resulting XML doesn't have carriage returns, is formated
   * in a single line.</p>
   * To get a better understanding of the way of working of the <code>toXML</code> functions, please read the 
   * documentation of the <code>XMLClass</code> annotation.
   * @param obj the object
   * @param indent the indent for each tag
   * @return a String with the XML representation of the <code>obj</code> Object
   * @see XMLClass
   * @see XMLAttribute
   * @see XMLElement
   */
  public static String toXML( Object obj, int indent) {
    return toXML( obj, indent, 0);
  }
  
  /**
   * <p>This method returns a String with the XML representation of the <code>obj</code> Object. The XML is indented with 
   * <code>indent</code> spaces.</p>
   * <p>If <code>indent</code> is zero, the resulting XML doesn't have carriage returns, is formated
   * in a single line.</p>
   * <p>If <code>nameSpace</code> is null or empty, the XML tags doesn't have namespace. </p>
   * To get a better understanding of the way of working of the <code>toXML</code> functions, please read the 
   * documentation of the <code>XMLClass</code> annotation.
   * @param obj the object
   * @param indent the indent for each tag
   * @param nameSpace the namespace
   * @return a String with the XML representation of the <code>obj</code> Object
   * @see XMLClass
   * @see XMLAttribute
   * @see XMLElement
   */
  public static String toXML( Object obj, int indent, String nameSpace) {
    return toXML( obj, indent, 0, nameSpace);
  }
  
  /**
   * <p>This method returns a String with the XML representation of the <code>obj</code> Object. The XML is indented with 
   * <code>indent</code> spaces, with a plus indentation of <code>baseIndent</code> spaces in the beginning of the lines.</p>
   * <p>If <code>indent</code> AND <code>baseIndent</code> are zero, the resulting XML doesn't have carriage returns, is formated
   * in a single line.</p>
   * To get a better understanding of the way of working of the <code>toXML</code> functions, please read the 
   * documentation of the <code>XMLClass</code> annotation.
   * @param obj the object
   * @param indent the indent for each tag
   * @param baseIndent the indent for the whole XML
   * @return a String with the XML representation of the <code>obj</code> Object
   * @see XMLClass
   * @see XMLAttribute
   * @see XMLElement
   */
  public static String toXML( Object obj, int indent, int baseIndent) {
    String nameSpace = "";
    
    XMLClass anClazz = obj.getClass().getAnnotation( XMLClass.class);
    if ( anClazz != null ) {
      nameSpace = anClazz.namespace();
    }
    
    return toXML( obj, indent, baseIndent, nameSpace);
  }
  
  /**
   * <p>This method returns a String with the XML representation of the <code>obj</code> Object. The XML is indented with 
   * <code>indent</code> spaces, with a plus indentation of <code>baseIndent</code> spaces in the beginning of the lines, 
   * and the tags are prefixed with the namespace <code>nameSpace</code>.</p>
   * <p>If <code>indent</code> AND <code>baseIndent</code> are zero, the resulting XML doesn't have carriage returns, is formated
   * in a single line.</p>
   * <p>If <code>nameSpace</code> is null or empty, the XML tags doesn't have namespace. </p>
   * To get a better understanding of the way of working of the <code>toXML</code> functions, please read the 
   * documentation of the <code>XMLClass</code> annotation.
   * @param obj the object
   * @param indent the indent for each tag
   * @param baseIndent the indent for the whole XML
   * @param nameSpace the namespace
   * @return a String with the XML representation of the <code>obj</code> Object
   * @see XMLClass
   * @see XMLAttribute
   * @see XMLElement
   */
  public static String toXML( Object obj, int indent, int baseIndent, String nameSpace) {
    // First, some little things like name, nameSpace or indent size
    Class<?> clazz = obj.getClass();
    String name = clazz.getSimpleName();
    
    String baseTab = Utils.fill( " ", baseIndent);
    String tab = "";
    String lf = "";
    
    // If there's an annotation with a defined name, overwrite the name
    XMLClass anClazz = clazz.getAnnotation( XMLClass.class);
    if ( anClazz != null ) {
      name = anClazz.name();
    }
    
    // Prepare same values for the format
    tab = Utils.fill( " ", indent);
    lf = baseIndent + indent > 0 ? "\n" : "";
      
    if ( nameSpace != null && !nameSpace.trim().equals( "") && !nameSpace.endsWith( ":") ) {
      nameSpace += ":";
    }
    
    // Instead of looping two times to make the XML, one looking for attributes and a second looking for elements
    // let's store attributes and tags in two places and add them in the end
    StringBuilder attribs = new StringBuilder();
    StringBuilder elements = new StringBuilder();
    
    // Get ALL fields. The "ALL" is important, because if there's only ONE annotation in the class, ONLY the annotated
    // fields will be printed, so the fields list will be looped to see if one of the fields is annotated.
    Field lFields[] = clazz.getDeclaredFields();
    
    boolean annotated = false;
    for ( Field field : lFields ) {
      if ( field.getAnnotation( XMLAttribute.class) != null 
           ||
           field.getAnnotation( XMLElement.class) != null ) {
        annotated = true;
        break;
      }
    }
    
    // If there isn't an annotated field, then just print the whole object first generation fields, which is done in the
    // toXMLsimple method and everything is stored as elements, not attributes.
    if ( !annotated ) {
      // Let's print everything in the object, only with "deep" of one
      elements.append( toXMLSimple( lFields, obj, name, nameSpace, baseTab, tab, lf));
    }
    else {
      // Let's print only annotated values
      for ( Field field : lFields ) {
        String nameField = field.getName();

        if ( field.isSynthetic() || nameField.contains( "$jacocoData") ) {  // This is shit is because JaCoCo adds shit in the classes...
          continue;
        }

        // If a field isn't annotated, continue with the next field
        if ( field.getAnnotation( XMLAttribute.class) == null 
             &&
             field.getAnnotation( XMLElement.class) == null ) {
          continue;
        }

        // Let's keep if it's private, public...
        boolean access = field.isAccessible();  // JDK 1.8
        field.setAccessible( true);  // PUBLIC!!
        
        Object value;
        try {
          value = field.get( obj);
        }
        catch ( Exception e) {
          e.printStackTrace();
          value = null;
        }
        
        // Let's set the access again
        field.setAccessible( access);
        
        // Null fields aren't printed, and the "this" pointer from the inner-classes aren't printed either
        if ( value == null || nameField.startsWith( "this$") ) {
          continue;
        }
        
        // And the tough work is here...
        
        // Attributes are printed directly, invoking the toString method. If the 'user' annotates a Collection as an 
        // attribute, well, that's his problem... 
        if ( field.getAnnotation( XMLAttribute.class) != null ) {
          // Perhaps the field name is overwritten by the annotation
          if ( !((XMLAttribute)field.getAnnotation( XMLAttribute.class)).name().trim().equals( "") ) {
           nameField = ((XMLAttribute)field.getAnnotation( XMLAttribute.class)).name().trim();
          }
          
          attribs.append( " " + nameField + "='" + value + "'");
        }
        else if ( field.getAnnotation( XMLElement.class) != null ) {
          // This is an element, and this is the beginning of the problem
          
          // Perhaps the annotation overwrites the field name
          if ( !((XMLElement)field.getAnnotation( XMLElement.class)).name().trim().equals( "") ) {
            nameField = ((XMLElement)field.getAnnotation( XMLElement.class)).name().trim();
          }
          
          // If it's a collection, loop the collection 
          if ( value instanceof Collection ) {
            elements.append( baseTab + tab + "<" + nameSpace + nameField + ">" + lf);
            
            for ( Object oo : (Collection<?>)value ) {
              if ( oo.getClass().getAnnotation( XMLClass.class) != null ) {
                // If the collection item is an annotated class, make a recursive call to print the object
                elements.append( toXML( oo, indent, baseIndent + indent + indent, nameSpace) + lf);
              }
              else {
                // If it's a regular object, well, print it
                elements.append( baseTab + tab + tab + "<" + nameSpace + "_" + nameField + ">" 
                                + oo 
                                + "</" + nameSpace + "_" + nameField + ">" + lf);
              }
            }
            
            elements.append( baseTab + tab + "</" + nameSpace + nameField + ">" + lf);
          }
          // If it's an array, loop the array, doing the same that in the collection
          else if ( value.getClass().isArray() ) {
            elements.append( baseTab + tab + "<" + nameSpace + nameField + ">" + lf);
            
            for ( int i = 0; i < Array.getLength( value); i++ ) {
              Object oo = Array.get( value, i);

              if ( oo.getClass().getAnnotation( XMLClass.class) != null ) {
                // If the collection item is an annotated class, make a recursive call to print the object
                elements.append( toXML( oo, indent, baseIndent + indent + indent, nameSpace) + lf);
              }
              else {
                // If it's a regular object, well, print it
                elements.append( baseTab + tab + tab + "<" + nameSpace + "_" + nameField + ">" 
                                + oo 
                                + "</" + nameSpace + "_" + nameField + ">" + lf);
              }
            }
            
            elements.append( baseTab + tab + "</" + nameSpace + nameField + ">" + lf);
          }
          // If it's an annotated class, recursive call
          else if ( value.getClass().getAnnotation( XMLClass.class) != null ) {
            elements.append( toXML( value, indent, baseIndent + indent, nameSpace) + lf);
          }
          // Otherwise everything, just print the object calling the toString method
          else {
            elements.append( baseTab + tab + "<" + nameSpace + nameField + ">" + value + "</" + nameSpace + nameField + ">" + lf);
          }
        }
      }
    }
    
    // Finally, print the whole thing 
    StringBuilder res = new StringBuilder();
    res.append( baseTab + "<" + nameSpace + name);
    res.append( attribs);
    res.append( ">" + lf);
    res.append( elements);
    res.append( baseTab + "</" + nameSpace + name + ">");
    
    return res.toString();
  }
  
  private static String toXMLSimple( Field lFields[], Object obj, String name, String nameSpace, String baseTab, String tab, String lf) {
    StringBuilder res = new StringBuilder();
    
    for ( Field field : lFields ) {
      String nameField = field.getName();

      if ( field.isSynthetic() || nameField.contains( "$jacocoData") ) {  // This is shit is because JaCoCo adds shit in the classes...
        continue;
      }

      // Let's keep if it's private, public...
      boolean access = field.isAccessible();  // JDK 1.8
      field.setAccessible( true);  // PUBLIC!!

      Object value;
      try {
        value = field.get( obj);
      }
      catch ( Exception e) {
        value = null;
        e.printStackTrace();
      }
      
      // Let's set the right access
      field.setAccessible( access);
      
      // The inner classes have a "this$n" pointer...
      if ( nameField.startsWith( "this$") || value == null ) {
        continue;
      }
      
      if ( value instanceof Collection ) {
        res.append( baseTab + tab + "<" + nameSpace + nameField + ">" + lf);
        
        for ( Object oo : (Collection<?>)value ) {
          res.append( baseTab + tab + tab + "<" + nameSpace + "_" + nameField + ">" 
                    + oo 
                    + "</" + nameSpace + "_" + nameField + ">" + lf);
        }
        
        res.append( baseTab + tab + "</" + nameSpace + nameField + ">" + lf);
      }
      // If it's an array, loop the array, doing the same that in the collection
      else if ( value.getClass().isArray() ) {
        res.append( baseTab + tab + "<" + nameSpace + nameField + ">" + lf);
        
        for ( int i = 0; i < Array.getLength( value); i++ ) {
          Object oo = Array.get( value, i);
          res.append( baseTab + tab + tab + "<" + nameSpace + "_" + nameField + ">" 
                    + oo 
                    + "</" + nameSpace + "_" + nameField + ">" + lf);
        }
        
        res.append( baseTab + tab + "</" + nameSpace + nameField + ">" + lf);
      }
      else {
        res.append( baseTab + tab + "<" + nameSpace + nameField + ">" + value + "</" + nameSpace + nameField + ">" + lf);
      }
      
    }
    
    return res.toString();
  }
  
  
  
  /**
   * This method formats and indents an XML
   * @param xml the XML to be formated
   * @return the formated XML
   * @throws DOMException if something is wrong
   * @throws LSException if something is wrong
   */
  public static String indentXML( String xml) {
    return indent( str2node( xml));
  }
  
  /**
   * This method formats and indents an XML
   * @param xml the XML to be formated
   * @return the formated XML
   * @throws DOMException if something is wrong
   * @throws LSException if something is wrong
   */
  public static String indent( Node xml) {
    try {
      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS lsimpl = (DOMImplementationLS)registry.getDOMImplementation( "LS");
      LSSerializer lsWriter = lsimpl.createLSSerializer();
      lsWriter.getDomConfig().setParameter( "format-pretty-print", true);
      lsWriter.getDomConfig().setParameter( "xml-declaration", false);
      
      return lsWriter.writeToString( xml);
    }
    catch ( Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return "";
  }
  
  /**
   * This method formats and indents an XML
   * @param xml the XML to be formated
   * @return the formated XML
   * @throws DOMException if something is wrong
   * @throws LSException if something is wrong
   */
  public static String indent( Document xml) {
    try {
      DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      DOMImplementationLS lsimpl = (DOMImplementationLS)registry.getDOMImplementation( "LS");
      LSSerializer lsWriter = lsimpl.createLSSerializer();
      lsWriter.getDomConfig().setParameter( "format-pretty-print", true);
      lsWriter.getDomConfig().setParameter( "xml-declaration", false);
      
      return lsWriter.writeToString( xml.getDocumentElement());
    }
    catch ( Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    
    return "";
  }
  
  
  /**
   * This method creates a new node named <code>nodeName</code> and creates it as a child of the root element. If the Map <code>mAttributes</code> is
   * not null, the new node has attributes named as the keys of the map with the map values as attribute values.  
   * @param doc the document
   * @param nodeName the new node name
   * @param mAttributes the attributes for the new node. Can be <code>null</code>
   * @return the new node
   * @throws DOMException if something is wrong
   */
  public static Node createNode( Document doc, String nodeName, Map<String,String> mAttributes) {
    return createNode( doc, doc.getDocumentElement(), nodeName, mAttributes);
  }
  
  /**
   * This method creates a new node named <code>nodeName</code> and creates it as a child of the <code>father</code> element. If the Map <code>mAttributes</code> is
   * not null, the new node has attributes named as the keys of the map with the map values as attribute values.  
   * @param doc the document
   * @param father the father of the new node
   * @param nodeName the new node name
   * @param mAttributes the attributes for the new node. Can be <code>null</code>
   * @return the new node
   * @throws DOMException if something is wrong
   */
  public static Node createNode( Document doc, Node father, String nodeName, Map<String,String> mAttributes) {
    Node node = doc.createElement( nodeName);
    
    Element el = (Element)node;
    
    if ( mAttributes != null ) {
      for ( String attrName : mAttributes.keySet() ) {
        if ( attrName == null ) continue;
        
        String attrValue = mAttributes.get( attrName);
        
        if ( attrValue == null ) continue;
        
        el.setAttribute( attrName, attrValue);
      }
    }
    
    father.appendChild( node);
    
    return node;
  }
  
  /**
   * <p>This method adds the <code>newTag</code> to the XML in <code>xml</code>. 
   * </p>If there's some king of problem, the method returns the same XML received.
   * @param xml the XML
   * @param newTag the new tag
   * @return the XML with the appended new tag.
   * @throws RuntimeException when something goes wrong (very rare)
   */
  public static String addNode( String xml, String newTag) {
    return addNode( xml, newTag, false);
  }
  
  /**
   * <p>This method adds the <code>newTag</code> to the XML in <code>xml</code>. The parameter <code>substitute</code> defines
   * the behavior of the method when there is already an occurrence of <code>newTag</code> in the XML. If <code>substitute</code>
   * is true, then the tag is substituted by <code>newTag</code>, and if <code>substitute</code> is false, then 
   * <code>newTag</code> is appended.</p>
   * If there's some king of problem, the method returns the same XML received.
   * @param xml the XML
   * @param newTag the new tag
   * @param substitute the behavior of the method when newTag already exists
   * @return the XML with the appended new tag.
   * @throws RuntimeException when something goes wrong (very rare)
   */
  public static String addNode( String xml, String newTag, boolean substitute) {
    StringBuilder res = new StringBuilder();
    
    try {
      Node addingNode = str2node( newTag);
      String tagName = addingNode.getNodeName();
      
      Node root = str2node( "<a>" + xml + "</a>");
      NodeList list = root.getChildNodes();
      
      boolean found = false;
      for ( int i = 0; i < list.getLength(); i++) {
        Node node = list.item( i);
      
        if ( substitute && node.getNodeName().equals( tagName) ) {
          res.append( indent( addingNode));
          found = true;
        }
        else {
          res.append( indent( node));
        }
      }
      
      if ( !substitute || !found ) {
        res.append( indent( addingNode));
      }
    }
    catch ( Exception ex) {
      throw new RuntimeException( "Cannot add this tag [" + newTag + "] because shit happens: ", ex);
    }
    
    return res.toString();
  }
  
  /**
   * This method removes all the occurrences of tag <code>tagToRemove</code> in <code>xml</code>
   * @param xml the xml to process
   * @param tagToRemove the tag to remove
   * @return the xml without tag tagToRemove
   * @throws RuntimeException when something goes wrong (very rare)
   */
  public static String deleteNodes( String xml, String tagToRemove) {
    Node all = str2node( xml); 
    String res = null;
    
    try {
      NodeList list = getXMLNodes( all, tagToRemove);
      
      for ( int i = 0; i < list.getLength(); i++) {
        Node node = list.item( i);
      
        Node papi = node.getParentNode();
        if ( papi == null ) {
          continue;
        }
        
        papi.removeChild( node);
      }
      
      res = indent( all);
    }
    catch ( Exception ex) {
      throw new RuntimeException( "Cannot delete this tag [" + tagToRemove + "] because shit happens: ", ex);
    }
    
    return res;
  }
  
  
  /**
   * This method creates a new node named <code>newFatherText</code> and sets it as the father of <code>node</code>, surrounding
   * <code>node</code> by the new node.
   * @param doc the document
   * @param node the node to be surrounded
   * @param newFatherText the new node name
   * @return the new node
   * @throws DOMException if something is wrong
   */
  public static Node surroundNode( Document doc, Node node, String newFatherText) {
    return surroundNode( doc, node, newFatherText, null);
  }
  
  /**
   * This method creates a new node named <code>newFatherText</code> and sets it as the father of <code>node</code>, surrounding
   * <code>node</code> by the new node. If the Map <code>mAttributes</code> is
   * not null, the new node has attributes named as the keys of the map with the map values as attribute values.  
   * @param doc the document
   * @param node the node to be surrounded
   * @param newFatherText the new node name
   * @param mAttributes the attributes of the new nodes. Can be <code>null</code>
   * @return the new node
   * @throws DOMException if something is wrong
   */
  public static Node surroundNode( Document doc, Node node, String newFatherText, Map<String,String> mAttributes) {
    Node newFather = createNode( doc, newFatherText, mAttributes);
    
    if ( node == doc.getDocumentElement() ) { // The root
      Node papi = doc.getDocumentElement().getParentNode();  // The father of the father. The super-father!!

      papi.replaceChild( newFather, node);
      newFather.appendChild( node);
    }
    else {  // Insert the new father in the same position that was occupied by node
      node.getParentNode().insertBefore( newFather, node);
      newFather.appendChild( node);
    }
    
    return newFather;
  }
  
  
  /**
   * This method receives a xml file and a XPath expression, and returns a list with all the matching elements in the
   * XML
   * @param fileXML the xml file
   * @param xpath what you want to find, as an XPath expression
   * @return all nodes that matches the XPath xpath
   * @throws XPathExpressionException when something goes wrong
   * @throws ParseException when something goes wrong
   * @throws FileNotFoundException when something goes wrong
   */
   public static List<String> findXML( File fileXML, String xpath) throws XPathExpressionException, ParseException, FileNotFoundException {
    Element root = null;
    InputStream is = new FileInputStream( fileXML);
    
    try {
      DocumentBuilderFactory parser = DocumentBuilderFactory.newInstance();
      parser.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD, "");
      parser.setAttribute( XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      DocumentBuilder db = parser.newDocumentBuilder();
      Document doc = db.parse( is);
      root = doc.getDocumentElement();
    }
    catch ( Exception ex) {     
      throw new ParseException( "Cannot load the file '" + fileXML.getAbsolutePath() + "'. Probably it is a wrong XML file : " + ex.getMessage(), ex);
    }
    
    return findXML( root, xpath);
  }

 
  /**
   * This method receives a xml and a XPath expression, and returns a list with all the matching elements in the
   * XML
   * @param xml the xml
   * @param xpath is what you want to find, as an XPath expression
   * @return all nodes that matches the XPath xpath
   * @throws XPathExpressionException when something goes wrong
   * @throws ParseException when something goes wrong
   */
  public static List<String> findXML( String xml, String xpath) throws XPathExpressionException, ParseException {
    return findXML( str2node(xml), xpath);
  }
   
  
  /**
   * This method receives a xml and a XPath expression, and returns a list with all the matching elements in the
   * XML
   * @param xml the xml
   * @param xpath is what you want to find, as an XPath expression
   * @return all nodes that matches the XPath expression
   * @throws XPathExpressionException when something goes wrong
   */
  public static List<String> findXML( Node xml, String xpath) throws XPathExpressionException {
    List<String> res = new ArrayList<String>();

    NodeList hijos = getXMLNodes( xml, xpath);

    for ( int i = 0; i < hijos.getLength(); i++) {
      String value = hijos.item( i).getTextContent();

      res.add( value);
    }

    return res;
  }
  
  /**
   * This method returns a list with all the nodes selected by an XPath expression
   * @param xml the XMl to parse
   * @param xpath the XPath expression
   * @return the nodes selected
   * @throws XPathExpressionException when something goes wrong 
   * @throws ParseException when the String XML isn't a valid XML
   */
  public static NodeList getXMLNodes( String xml, String xpath) throws XPathExpressionException, ParseException {
    return getXMLNodes( str2node( xml), xpath);
  }
  
  /**
   * This method returns a list with all the nodes selected by an XPath expression
   * @param xml the XMl to parse
   * @param xpath the XPath expression
   * @return the nodes selected
   * @throws XPathExpressionException when something goes wrong 
   */
  public static NodeList getXMLNodes( Node xml, String xpath) throws XPathExpressionException {
    XPath xpathParser = XPathFactory.newInstance().newXPath(); 
    XPathExpression expr = xpathParser.compile( xpath);

    Object obj1 = expr.evaluate( xml, XPathConstants.NODESET);

    return (NodeList)obj1;
  }
  
  /**
   * This method returns a list with all the nodes selected by an XPath expression
   * @param xml the XMl to parse
   * @param xpath the XPath expression
   * @return the nodes selected
   * @throws XPathExpressionException when something goes wrong 
   */
  public static NodeList getXMLNodes( Document xml, String xpath) throws XPathExpressionException {
    return getXMLNodes( xml.getDocumentElement(), xpath);
  }
  
  /**
   * This method returns the first node selected by an XPath expression, or <code>null</code> if there
   * isn't any node that matches the XPath expression
   * @param xml the XMl to parse
   * @param xpath the XPath expression
   * @return the node selected or <code>null</code>
   * @throws XPathExpressionException when something goes wrong 
   */
  public static Node getFirstXMLNode( Node xml, String xpath) throws XPathExpressionException {
    XPath xpathParser = XPathFactory.newInstance().newXPath(); 
    XPathExpression expr = xpathParser.compile( xpath);

    NodeList list = (NodeList)expr.evaluate( xml, XPathConstants.NODESET);
    
    if ( list.getLength() == 0 ) {
      return null;
    }

    return list.item( 0);
  }
  
  /**
   * This method receives a xml and a XPAth expression and returns the first matching element or null if nothing in the 
   * xml matches the XPath expression
   * @param xml the xml
   * @param xpath is what you want to find, as an XPath expression
   * @return the value of the first matching element or null if cannot be found 
   * @throws XPathExpressionException when something goes wrong 
   * @throws ParseException when something goes wrong 
   */
  public static String getFirstValue( String xml, String xpath) throws XPathExpressionException, ParseException {
    List<String> lRes = findXML( xml, xpath);
    
    if ( lRes.size() > 0 ) {
      return lRes.get( 0);
    }
    
    return null;
  }
  
  /**
   * This method receives a xml and a XPAth expression and returns the first matching element or null if nothing in the 
   * xml matches the XPath expression
   * @param xml the xml
   * @param xpath is what you want to find, as an XPath expression
   * @return the value of the first matching element or null if cannot be found 
   * @throws XPathExpressionException when something goes wrong 
   * @throws ParseException when something goes wrong 
   */
  public static String getFirstValue( Node xml, String xpath) throws XPathExpressionException, ParseException {
    List<String> lRes = findXML( xml, xpath);
    
    if ( lRes.size() > 0 ) {
      return lRes.get( 0);
    }
    
    return null;
  }
  


  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( Node xml, File xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    try {
			applyXSLT( new StringReader( indent( xml)), 
			           new StreamSource( xslt), 
			           res);
		} 
    catch ( Exception ex) {
			throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
		}
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( Node xml, InputStream xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    try {
			applyXSLT( new StringReader( indent( xml)), 
			           new StreamSource( xslt), 
			           res);
		} 
    catch ( Exception ex) {
			throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
		}
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( Node xml, Reader xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    try {
			applyXSLT( new StringReader( indent( xml)), 
			           new StreamSource( xslt), 
			           res);
		} 
    catch ( Exception ex) {
			throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
		}
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( Node xml, String xslt) throws ParseException {
    StringWriter res = new StringWriter();
    
    try {
      applyXSLT( new StringReader( indent( xml)), 
                 new StreamSource( xslt), 
                 res);
    } 
    catch ( Exception ex) {
      throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
    }
    
    return res.getBuffer().toString();
  }
  
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( String xml, File xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    applyXSLT( new StringReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( String xml, InputStream xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    applyXSLT( new StringReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( String xml, Reader xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    applyXSLT( new StringReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( String xml, String xslt) throws ParseException {
    StringWriter res = new StringWriter();
 
    applyXSLT( new StringReader( xml), 
               new StreamSource( new StringReader(xslt)), 
               res);
    
    return res.getBuffer().toString();
  }
  
    
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( InputStream xml, File xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    applyXSLT( new InputStreamReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( InputStream xml, InputStream xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    applyXSLT( new InputStreamReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( InputStream xml, Reader xslt) throws ParseException {
  	StringWriter res = new StringWriter();
 
    applyXSLT( new InputStreamReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( InputStream xml, String xslt) throws ParseException {
    StringWriter res = new StringWriter();
 
    applyXSLT( new InputStreamReader( xml), 
               new StreamSource( xslt), 
               res);
    
    return res.getBuffer().toString();
  }
  
  
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and returns a String
   * with the result.
   * @param xml the XML document
   * @param xslt the XSLT
   * @return a String with the result
   * @throws ParseException when something goes wrong
   */
  public static String applyXSLT( File xml, File xslt) throws ParseException {
    StringWriter res = new StringWriter();
    
    try {
      applyXSLT( new FileReader( xml), 
                 new StreamSource( xslt), 
                 res);
    }
    catch ( FileNotFoundException ex) {
      throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
    }
    
    return res.getBuffer().toString();
  }
  
    
 
  /**
   * This method applies the XSLT <code>xslt</code> to the XML document <code>xml</code> and writes the result
   * in <code>output</code>
   * @param xml the XML document
   * @param xslt the XSLT
   * @param output the output
   * @throws ParseException when something goes wrong
   */
  public static void applyXSLT( Reader xml, StreamSource xslt, StringWriter output) throws ParseException {

    try {
      DocumentBuilderFactory parser = DocumentBuilderFactory.newInstance();
      parser.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD, "");
      parser.setAttribute( XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
      DocumentBuilder db = parser.newDocumentBuilder();
      Document doc = db.parse( new InputSource( xml));
  
      StreamResult result = new StreamResult( output);

      // The next lines are "nosonar" because this may work with the default parser, but not all the parsers have those attributes
      // and trying to set a non-existent attribute in a parser raises a very ugly exception, so this is yet another false
      // scary warning of sonar
      TransformerFactory tFactory = TransformerFactory.newInstance();  // NOSONAR

      //tFactory.setAttribute( XMLConstants.ACCESS_EXTERNAL_DTD, "");        // Compliant with Sonar, but not with Spring
      //tFactory.setAttribute( XMLConstants.ACCESS_EXTERNAL_STYLESHEET, ""); // Compliant with Sonar, but not with Spring
      
      Transformer transformer = tFactory.newTransformer( xslt);
      
      transformer.transform( new DOMSource( doc), result);
    }
    catch ( Exception ex) {
      throw new ParseException( "Can't parse [" + xml + "] : " + ex.getMessage(), ex);
    }
  }
}
