package es.nimbox.gui;

public class IconLoadingException extends RuntimeException {
  public IconLoadingException() {
  }

  public IconLoadingException(String message) {
    super(message);
  }

  public IconLoadingException(String message, Throwable cause) {
    super(message, cause);
  }

  public IconLoadingException(Throwable cause) {
    super(cause);
  }
}
