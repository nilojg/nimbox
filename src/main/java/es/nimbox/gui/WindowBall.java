
package es.nimbox.gui;


import es.nimbox.box.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Nilo J. Gonzalez 2015
 */
public class WindowBall extends JWindow {
  private static final long serialVersionUID = 1L;

  private static final int DEFAULT_DISTANCE = 150;
  private static final long DEFAULT_SLEEP = 10;
  private static final int DEFAULT_STEPS = 10;
  private static final int DEFAULT_RADIUS = 40;

  private static final Dimension screenSize = GuiUtils.getMaximumScreenSize();
  
  // The ball icon
  private ImageIcon icon;

  private static final int BRILLO_OFFSET = 25;
  private int brilloOffset;
  
  // The ball decoration mask, with brights, transparencies, shadows, etc
  private ImageIcon iconDecoration;
  
  // The radius
  private int radius = 50;
  
  // The opacity
  private float opacity = 0.5f;

  // The color
  private Color bgColor = new Color( 0,0,0);


  private int distance = DEFAULT_DISTANCE;
  private long sleep = DEFAULT_SLEEP;
  private int steps = DEFAULT_STEPS;

  // The father ball
  private WindowBall papi;

  // The list of children balls
  private transient List<Child> childs;
  
  private String command;

  // True if it's showing children, false otherwise
  boolean showingChildren;

  // The action listeners
  private transient List<WindowBallListener> listeners;
  
  
  private class Child {
    WindowBall child;
    Point locationFinal;
    boolean visible;
    
    public Child( WindowBall wb) {
      child = wb;
      wb.setVisible( false);
      visible = false;
      locationFinal = null;
      wb.papi = WindowBall.this;
      
      wb.addWindowBallListener( new MiWBL( this));
    }
    
    public void setVisible( boolean v) {
      visible = v;
      child.setVisible( v);
    }
  }

  public WindowBall( ImageIcon icon) {
    this( icon, DEFAULT_RADIUS);
  }

  public WindowBall( ImageIcon icon, int radius) {
    super();
    
    this.icon = icon;
    this.radius = radius;
    
    childs = new ArrayList<>();
    listeners = new ArrayList<>();

    showingChildren = false;
    
    command = "";
    
    recalculate();

    setLocation( screenSize.width / 2, screenSize.height / 2);

    MiML mial = new MiML();
    addMouseListener( mial);
    addMouseMotionListener( mial);
  }

  /**
   * This method shows the children
   */
  public void childShow() {
    childArrange();

    Point center = getLocation();

    for ( int inc = 1; inc <= steps; inc++ ) {
      for ( Child wc : childs) {
        int x = center.x + inc * (wc.locationFinal.x / steps);
        int y = center.y + inc * (wc.locationFinal.y / steps);

        Point p = pointInScreen( x, y, wc.child.radius);

        wc.child.setLocation( p.x, p.y);
        wc.setVisible( true);
      }

      Utils.sleep( sleep);
    }

    showingChildren = true;
  }

  /**
   * This method hides the children
   */
  public void childHide() {
    Point center = getLocation();

    if ( childs.isEmpty() ) {
      return;
    }

    for ( Child wc : childs) {
      wc.child.childHide();
    }

    boolean wait = false; // Esto es para que no haga un wait por lo hijos invisibles

    for ( int inc = steps - 1; inc > 0; inc-- ) {
      for ( Child wc : childs) {
        if ( wc.visible ) {
          int x = center.x + (wc.locationFinal.x / steps) * inc;
          int y = center.y + (wc.locationFinal.y / steps) * inc;

          if ( center.distance( x, y) < radius + wc.child.getRadius() ) {
            wc.setVisible( false);
          }

          wc.child.setLocation( x, y);

          wait = true;
        }
      }

      if ( wait ) {
        Utils.sleep( sleep);
      }
    }

    showingChildren = false;
  }


  // Events

  public void setActionCommand( String command) {
    this.command = command;
  }

  public String getActionCommand() {
    return command;
  }

  public void addWindowBallListener( WindowBallListener wbl) {
    listeners.add( wbl);
  }

  public void removeWindowBallListener( WindowBallListener wbl) {
    listeners.remove( wbl);
  }

  protected void fireEvent( WindowBallEvent ev) {
    for ( WindowBallListener wbl : listeners ) {
      if ( ev.getID() == WindowBallEvent.CHILD_HIDDEN ) {
        wbl.childHidden( ev);
      }
      else if ( ev.getID() == WindowBallEvent.CHILD_SHOWN ) {
        wbl.childShown( ev);
      }
      else if ( ev.getID() == WindowBallEvent.MOUSE_CLICKED ) {
        wbl.mouseClicked( ev);
      }
      else if ( ev.getID() == WindowBallEvent.POSITION_CHANGED ) {
        wbl.positionChanged( ev);
      }

    }
  }

  // Children

  /**
   * This method adds <code>wb</code> as children of this ball
   * @param wb the new child
   */
  public void addChild( WindowBall wb) {
    childs.add( new Child( wb));
  }

  /**
   * This method removes <code>wb</code> from the children of this object
   * @param wb the ball to remove from the list of children
   */
  public void removeChild( WindowBall wb) {
    Child encontrado = null;

    for ( Child wc : childs ) {
      if ( wc.child ==  wb ) {
        encontrado = wc;
        break;
      }
    }

    if ( encontrado != null ) {
      childs.remove( encontrado);
    }
  }

  /**
   * This method returns a copy of the list of the children of this ball
   * @return  a copy of the list of the children of this ball
   */
  public List<WindowBall> getChildren() {
    List<WindowBall> res = new ArrayList<>();

    for ( Child wc : childs ) {
      res.add( wc.child);
    }

    return res;
  }

  /**
   * This method cleans the list of children
   */
  public void clearChildren() {
    childs.clear();
  }


  /**
   * <p>Thie method sets the new <i>location</i> of the <code>wb</code> child of this object to <code>newLocation</code>.</p>
   * <p>The coordinates <code>newLocation</code> aren't an absolute point in the screen, but a relative location respect the
   * center of this ball.</p>
   * @param wb the child to relocate
   * @param newLocation the new relative location respect the center of this object
   */
  public void setChildLocation( WindowBall wb, Point newLocation) {
    for ( Child child : childs ) {
      if ( child.child == wb ) {
        child.locationFinal = newLocation;
      }
    }
  }

  /**
   * This method returns the <i>location</i> of the <code>wb</code> child relative respect the center of this ball
   * @param wb the child
   * @return the <i>location</i> of the <code>wb</code> child relative respect the center of this ball or <code>null</code>
   *         if <code>wb</code> is not a child of this ball
   */
  public Point getChildLocation( WindowBall wb) {
    for ( Child child : childs ) {
      if ( child.child == wb ) {
        return child.locationFinal;
      }
    }

    return null;
  }


  protected Point pointInScreen( int x, int y, int r) {
    if ( x < r ) {
      x = r;
    }

    if ( x > screenSize.width - r ) {
      x = screenSize.width - r;
    }

    if ( y < r ) {
      y = r;
    }

    if ( y > screenSize.height - r ) {
      y = screenSize.height - r;
    }

    return new Point( x,y);
  }

  /**
   * This method arranges the balls in the screen in a circle. If a child has been moved by the user or its location has been
   * set with <code>setChildLocation</code>, that location is respected.
   * @see #setChildLocation(WindowBall, Point)
   */
  public void childArrange() {
    int isPapi = 0;
    double initAngle = 0d;

    // To calculate the initial angle
    if ( papi != null ) {
      isPapi = 1;
      int xPapi = papi.getLocation().x - getLocation().x;
      int yPapi = papi.getLocation().y - getLocation().y;
      double d = papi.getLocation().distance( getLocation());

      initAngle = Math.acos( xPapi / d) * ( yPapi < 0 ? -1 : 1);
    }

    // The number of portions in the cake
    double radians = 2 * Math.PI / ( childs.size() + isPapi);

    for ( int i = 0; i < childs.size(); i++) {
      Child wc = childs.get( i);

      // if the child position has been set, respect the previous set
      if ( wc.locationFinal == null ) {
        int x = (int)(distance * Math.cos( (i) * radians + initAngle));
        int y = (int)(distance * Math.sin( (i) * radians + initAngle));

        wc.locationFinal = new Point( x, y);
      }
    }
  }

  /**
   * Move the child to <code>despX</code> and <code>despY</code>
   * @param despX the X
   * @param despY the Y
   */
  protected void childMove( int despX, int despY) {
    for ( Child wc : childs ) {
      if ( wc.locationFinal != null && wc.visible ) {
        wc.child.childMove( despX, despY);

        Point p = wc.child.getLocation();
        p.x -= despX;
        p.y -= despY;

        Point p2 = pointInScreen( p.x, p.y, wc.child.radius);
        wc.child.setLocation( p2);
        wc.locationFinal = new Point( p2.x - getLocation().x, p2.y - getLocation().y);
      }
    }
  }

  /**
   * This method hides the showed and shows the hidden
   */
  protected void childToggle() {
    int type = 0;

    if ( showingChildren ) {
      childHide();
      type = WindowBallEvent.CHILD_SHOWN;
    }
    else {
      childShow();
      type = WindowBallEvent.CHILD_HIDDEN;
    }

    WindowBallEvent ev = new WindowBallEvent( this, type, command, getLocation());
    fireEvent( ev);
  }


  /**
   * This method set the icon of this ball
   * @param icon the new icon
   */
  public void setIcon( ImageIcon icon) {
    this.icon = icon;

    recalculate();
  }

  /**
   * This method sets the radius of this ball
   * @param radius the radous
   */
  public void setRadius( int radius) {
    this.radius = radius;

    recalculate();
  }

  /**
   * This method returns the radius of this ball
   * @return the radius of this ball
   */
  public int getRadius() {
    return radius;
  }

  /**
   * This method returns the opacity of this ball
   * @return the opacity of this ball
   */
  public float getBallOpacity() {
    return opacity;
  }

  /**
   * This method returns the background color of this ball
   * @return the background color of this ball
   */
  public Color getBallBackground() {
    return bgColor;
  }

  /**
   * This method returns the interval between "steps" in the movement of the balls. Less is faster
   * @return the interval between "steps" in the movement of the balls. Less is faster
   */
  public long getSleep() {
    return sleep;
  }

  /**
   * This method sets the interval between "steps" in the movement of the balls. Less is faster
   * @param sleep the interval between "steps" in the movement of the balls. Less is faster
   */
  public void setSleep(long sleep) {
    this.sleep = sleep;
  }

  /**
   * This method returns the number of steps in the path of a ball hiding or showing. More is slower, but softer.
   * @return the number of steps in the path of a ball hiding or showing. More is slower, but softer.
   */
  public int getSteps() {
    return steps;
  }

  /**
   * This method sets the number of steps in the path of a ball hiding or showing. More is slower, but softer.
   * @param steps the number of steps in the path of a ball hiding or showing. More is slower, but softer.
   */
  public void setSteps(int steps) {
    this.steps = steps;
  }

  /**
   * This method returns the location of the <b>center</b> of this ball in the screen
   * @return the location of the center of this ball in the screen
   */
  @Override
  public Point getLocation() {
    Point res = super.getLocation();

    res.x += radius;
    res.y += radius;

    return res;
  }

  /**
   *
   * @param p the point defining the <b>center</b> of this ball
   */
  @Override
  public void setLocation( Point p) {
    setLocation( p.x, p.y);
  }

  /**
   * This method sets the location of the <b>center</b> of this ball
   * @param x the <i>x</i>-coordinate of the new <b>center</b> of this ball
   * @param y the <i>y</i>-coordinate of the new <b>center</b> of this ball
   */
  @Override
  public void setLocation( int x, int y) {
    super.setLocation( x - radius, y - radius);
  }

  @Override
  public void setBackground( Color bgColor) {
    super.setBackground( new Color( 0,0,0,0));
    
    this.bgColor = new Color( bgColor.getColorSpace(), bgColor.getColorComponents( null), opacity);
  }
  
  @Override
  public void setOpacity( float opacity) {
    this.opacity = opacity;
    
    setBackground( bgColor);
  }
  
  @Override
  public void paint( Graphics g) {
    Graphics2D g2d = (Graphics2D)g;
    g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    
    // Esto es para borrar el fondo y que no se vayan solapando repintados
    g2d.setBackground( new Color( 255, 255, 255, 0));
    g2d.clearRect( 0, 0, getWidth(), getHeight()); 
    
    g2d.setColor( bgColor);
    g2d.fillOval( brilloOffset,brilloOffset, 2*radius - brilloOffset, 2*radius - brilloOffset);
    
    g2d.drawImage( icon.getImage(), radius - icon.getIconWidth()/2, radius - icon.getIconHeight()/2, null);
    g2d.drawImage( iconDecoration.getImage(), 0,0, null);
  }
  



  protected void recalculate() {
    iconDecoration = GuiUtils.loadIcon( "/es/nimbox/gui/icons/ball-decoration.png");

    // Fit the size of the icon
    float iniW = iconDecoration.getIconWidth();
    iconDecoration = GuiUtils.resize( iconDecoration, 2*radius, 2*radius);
    float finW = iconDecoration.getIconWidth();

    brilloOffset = (int)(BRILLO_OFFSET * finW/iniW);

    this.icon = GuiUtils.resize( icon, (int)(radius * 1.3f), (int)(radius * 1.3f));

    setSize( new Dimension( 2*radius, 2*radius));
    setShape( new Ellipse2D.Double( 0,0, getWidth(),getHeight()));

    setBackground( bgColor);
  }

  
  private class MiWBL extends WindowBallAdapter {
    Child child;
    
    MiWBL( Child child) {
      this.child = child;
    }

    @Override
    public void positionChanged( WindowBallEvent ev) {
      child.locationFinal.x = ev.getPositionOnScreen().x - getLocation().x;
      child.locationFinal.y = ev.getPositionOnScreen().y - getLocation().y;
    }
  }
  
  
  private class MiML extends MouseAdapter {
    boolean onDrag = false;
    int dragX;
    int dragY;

    @Override
    public void mouseClicked( MouseEvent ev) {
      if ( ev.getClickCount() >= 2 ) {
        childToggle();
      }
      else {
        WindowBallEvent wbev = new WindowBallEvent( WindowBall.this, ev, command);
        fireEvent( wbev);
      }
    }
    
    @Override
    public void mousePressed( MouseEvent ev) {
      onDrag= true;
      dragX = ev.getXOnScreen() - getLocation().x;
      dragY = ev.getYOnScreen() - getLocation().y;
    }
    
    @Override
    public void mouseReleased( MouseEvent ev) {
      if ( onDrag ) {
        onDrag= false;
        WindowBallEvent wbev = new WindowBallEvent( WindowBall.this, WindowBallEvent.POSITION_CHANGED, command, ev.getLocationOnScreen());
        fireEvent( wbev);
      }
    }
    
    @Override
    public void mouseDragged( MouseEvent ev) {
      if ( onDrag ) {
        int newX = ev.getXOnScreen() - dragX;
        int newY = ev.getYOnScreen() - dragY;
        
        Point act = getLocation(); 
        
        childMove( act.x - newX, act.y - newY);
        setLocation( newX, newY);
      }
    }
  }
  
  
  
}
