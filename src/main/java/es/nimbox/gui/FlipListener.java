package es.nimbox.gui;

/**
 * This interface defines the method required to process the FlipEvent events thronwn by FlipFrames
 * 
 * @author nilo.gonzalez
 * 
 * @see FlipFrame
 * @see FlipEvent
 */
public interface FlipListener {

  public void flipEventPerformed( FlipEvent ev);
}
