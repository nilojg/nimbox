package es.nimbox.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;


/**
 * <p>This method implements a very simple <code>String</code> list with <i>ticks</i> on the left that becomes green
 * if the item is selected and gray if the item is unselected.</p>
 *
 * <p>It's <b>very</b> simple, so it only supports <code>String</code> values, which is the usual case, and handles its own
 * internal model, so don't wait anything fancy or very customizable. The original <code>JList</code> class it's very easy
 * to customize, so if you need something special, ok, go for it. This class is in NimBox just because I use to need a
 * <code>JList</code> to select <code>String</code> values again and again.</p>
 *
 * It's very easy to use:
 *
 * <pre>
 *   TickListModel model = new TickListModel();
 *   JTickList list = new JTickList( model);
 *
 *   model.add( "uno");
 *   model.add( "dos", false);
 *   model.add( "tres");
 *   model.add( "cuatro", false);
 *   model.add( "cinco");
 *   model.addItems( List.of( "gato", "perro", "elefante"), false);
 *   model.addItems( List.of( "estrella", "nebulosa", "planeta"), true);
 *
 *   mySuperPanel.add( new JScrollPane( list));
 *
 *   System.out.println( list.getModel().getSelectedNames());
 * </pre>
 */
public class JTickList extends JList<String> {
  private static final long serialVersionUID = 1L;

  private static final int TICK_SIZE = 20;

  private static final Color selFore = UIManager.getColor("List.selectionForeground");
  private static final Color selBack = UIManager.getColor( "List.selectionBackground");
  private static final Color textFore = UIManager.getColor( "List.foreground");
  private static final Color textBack = UIManager.getColor( "List.background");

  private static Icon imgTickOn;
  private static Icon imgTickOff;

  static {
    imgTickOn = GuiUtils.loadIcon("/es/nimbox//gui/icons/tick-on.png", TICK_SIZE, TICK_SIZE);
    imgTickOff = GuiUtils.loadIcon("/es/nimbox/gui/icons/tick-off.png", TICK_SIZE, TICK_SIZE);
  }


  private transient TickListModel model;



  public JTickList() {
    this( new TickListModel());
  }

  public JTickList( TickListModel model) {
    super();

    this.model = model;

    setModel( model);
    setCellRenderer( new MiListCellRenderer());
    setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
    addMouseListener( new MiListListener());
  }

  @Override
  public TickListModel getModel() {
    return model;
  }

  public void setModel( TickListModel model) {
    this.model = model;
    super.setModel(model);
  }


  private class MiListCellRenderer extends JLabel implements ListCellRenderer<String> {
    private static final long serialVersionUID = 1L;

    public MiListCellRenderer() {
      super();

      setOpaque( true);
    }

    @Override
    public Component getListCellRendererComponent( JList<? extends String> list,
                                                   String value, int index,
                                                   boolean isSelected, boolean cellHasFocus) {

      String literal = value;
      Icon icon = imgTickOff;

      TickListModel.TickListItem item = model.getItem( index);
      if ( item != null ) {
        literal = item.name;
        icon = item.selected ? imgTickOn : imgTickOff;
      }

      setText( literal);
      setIcon( icon);

      if ( isSelected ) {
        setForeground( selFore);
        setBackground( selBack);
      }
      else {
        setForeground( textFore);
        setBackground( textBack);
      }

      return this;
    }
  }

  private class MiListListener extends MouseAdapter {
    @Override
    public void mouseClicked( MouseEvent ev) {
      TickListModel.TickListItem item = model.getItem( JTickList.this.getSelectedIndex());

      if ( item != null && ev.getX() <= TICK_SIZE ) {
        item.selected = !item.selected;
        model.fireEvents();
      }
    }
  }



  public static void main( String[] args) throws Exception {
    TickListModel model = new TickListModel();

    JTickList lista = new JTickList( model);

    model.add( "uno");
    model.add( "dos", false);
    model.add( "tres");
    model.add( "cuatro", false);
    model.add( "cinco");
    model.addItems( List.of( "gato", "perro", "elefante"), false);
    model.addItems( List.of( "estrella", "nebulosa", "planeta"), true);

    JFrame frame = new JFrame( "TEst");
    frame.getContentPane().setLayout( new BorderLayout());
    frame.getContentPane().add( new JScrollPane( lista), BorderLayout.CENTER);

    frame.pack();
    frame.setVisible( true);
  }
}
