package es.nimbox.gui;


import es.nimbox.box.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Area;
import java.awt.geom.GeneralPath;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * This class implements a JFrame that "flips" whenever the user pushes a button in the titlebar. "Flip" means that the frame has got
 * two sides, the front and the back side, and when "flipping" the frame shows an animation to show the opposite side. It's like
 * having two frames, one in the back of the other.<br>
 * 
 * When the user rolls the mouse wheel the frame changes its opacity too.
 * 
 * @author nilo.gonzalez
 *
 */
public class FlipFrame extends JFrame {
  private static final long serialVersionUID = 1L;
  
  // Just to identify in the CardLayout what's in the front and in the back
  private static final String FRONT = "FRONT";
  private static final String BACK = "BACK";


  /**
   * The size of the border
   */
  protected static final int BORDER_WIDTH = 5;
  
  /**
   * The size of the title bar
   */
  protected static final int TITLE_HEIGHT = 30;
  
  /**
   * The time in ms used in a flip
   */
  protected static final long FLIP_TIME = 80;
  
  /**
   * how many frames in a flip
   */
  protected static final int FLIP_FRAMES = 25;
  
  /**
   * The title rectangle, without borders
   */
  protected Rectangle rTitle;
  
  protected Container pFondo;
  protected Container pFront;
  protected Container pBack;
  protected JPanel pRoot;
  
  /**
   * It's true when is showing the front pane
   */
  protected boolean showFront = true;
  
  /**
   * This is true when the window is flipping
   */
  private boolean onFlip;
  
  /**
   * The image to show while flipping
   */
  private transient BufferedImage shotImage;
  private transient BufferedImage shotFront;
  private transient BufferedImage shotBack;
  
  /**
   * This stores the horizontal shift because the perspective. Needed to keep in the center the image
   */
  int incX;
  
  protected static ImageIcon iClose_col;
  protected static ImageIcon iMin_col;
  protected static ImageIcon iMax_col;
  protected static ImageIcon iFlip_col;
  protected static ImageIcon iClose_bw;
  protected static ImageIcon iMin_bw;
  protected static ImageIcon iMax_bw;
  protected static ImageIcon iFlip_bw;
  
  protected boolean overClose;
  protected boolean overMin;
  protected boolean overMax;
  protected boolean overFlip;
  protected Rectangle recClose;
  protected Rectangle recMin;
  protected Rectangle recMax;
  protected Rectangle recFlip;
  
  
  private transient Path2D pathIn;
  private transient Path2D pathOut;
  private transient Path2D deformedPathIn;
  private transient Area areaTitle;
  
  private transient List<FlipListener> lListeners;
  
  // This property stores if we can take screenshots, which will save execution time
  static boolean robotAllow = false;
  static {
    try {
      new Robot().createScreenCapture( new Rectangle(0,0,1,1));
      robotAllow = true;
    }
    catch ( AWTException e) {}
  }
  
 
  public FlipFrame( String title) {
    super( title);
    
    loadIcons();
    
    lListeners = new ArrayList<>();

    recClose = new Rectangle( 0,0,0,0);
    recMin = new Rectangle( 0,0,0,0);
    recMax = new Rectangle( 0,0,0,0);
    recFlip = new Rectangle( 0,0,0,0);
    
    pBack = new JPanel( new BorderLayout());
    pFront = new JPanel( new BorderLayout());

    pFondo = new JPanel( new CardLayout());
    pFondo.add( pFront, FRONT);
    pFondo.add( pBack, BACK);

    pRoot = new JPanel( new BorderLayout());
    pRoot.setBorder( BorderFactory.createEmptyBorder( TITLE_HEIGHT, BORDER_WIDTH, BORDER_WIDTH, BORDER_WIDTH));
    pRoot.add( pFondo);

    super.setContentPane( pRoot);
    
    MiML miml = new MiML();
    addMouseListener( miml);
    addMouseMotionListener( miml);
    addMouseWheelListener( miml);
    
    addComponentListener( new MiCL());
    
    setUndecorated( true);
    setBackground( new Color( 0,0,0,0));
  }

  @Override
  public void setVisible( boolean visible) {
    if ( visible) {
      resizeInside();
    }

    super.setVisible( visible);
  }

  protected void resizeInside() {
    Dimension dTotal = getSize();

    Dimension dInside = new Dimension( dTotal.width - 2 * BORDER_WIDTH,
                                       dTotal.height - TITLE_HEIGHT - BORDER_WIDTH);

    rTitle = new Rectangle( BORDER_WIDTH, 0, dTotal.width - 2*BORDER_WIDTH, TITLE_HEIGHT);

    pathOut = new GeneralPath();
    pathOut.moveTo( 0,0);
    pathOut.lineTo( dTotal.width, 0);
    pathOut.lineTo( dTotal.width, dTotal.height);
    pathOut.lineTo( 0, dTotal.height);
    pathOut.closePath();

    pathIn = new GeneralPath();
    pathIn.moveTo( BORDER_WIDTH, TITLE_HEIGHT);
    pathIn.lineTo( dInside.width + BORDER_WIDTH, TITLE_HEIGHT);
    pathIn.lineTo( dInside.width + BORDER_WIDTH, dInside.height + TITLE_HEIGHT);
    pathIn.lineTo( BORDER_WIDTH, dInside.height + TITLE_HEIGHT);
    pathIn.closePath();

    areaTitle = new Area( pathOut);
    areaTitle.subtract( new Area(pathIn));
  }

  public void addFlipListener(FlipListener listener) {
    lListeners.add( listener);
  }
  
  public void removeFlipListener( FlipListener listener) {
    lListeners.remove( listener);
  }
  
  protected void fireEvents( FlipEvent ev) {
    for ( FlipListener fl : lListeners) {
      fl.flipEventPerformed( ev);
    }
  }

  /**
   * This method returns the container in the "front" of the frame
   * @return the container in the "front" of the frame
   */
  @Override
  public Container getContentPane() {
    return pFront;
  }

  /**
   * This method returns the container in the "back" of the Frame
   * @return the container in the "back" of the Frame
   */
  public Container getBackContentPane() {
    return pBack;
  }

  /**
   * This method executes the "flip" animation of the frame
   */
  public void flip() {
    if ( pBack.getComponentCount() == 0 ) {
      return;
    }
    
    fireEvents( new FlipEvent( this, FlipEvent.START_FLIP));
    
    CardLayout cl = (CardLayout)(pFondo.getLayout());
    
    onFlip = true;
    
    Rectangle rectOrig = getBounds();
    double halfHigh = rectOrig.height / 2.0;
    int incAngle = 180 / FLIP_FRAMES;
    
    boolean fliped = false;
    
    if ( showFront ) {
      shotImage = shotFront = getScreenShot( pFront);
    }
    else {
      shotImage = shotBack = getScreenShot( pBack);
    }
    
    int X = getX();
    int Y = getY();
    Dimension originalSize = getSize();
    
    for ( int i = 0; i <= 180; i += incAngle ) {
      double angle = i;
      
      incX = (int)(Math.sin( Math.toRadians( angle)) * ( halfHigh / 3));
      int incY = (int)(halfHigh - ( Math.cos( Math.toRadians( angle)) * halfHigh ));
      
      deformedPathIn = new GeneralPath();
      Path2D pOut = new GeneralPath();
      
      deformShape( pathIn, deformedPathIn, incX, incY);
      int desp = (int)deformShape( pathOut, pOut, incX, incY);
      
      areaTitle = new Area( pOut);
      areaTitle.subtract( new Area( deformedPathIn));
      
      if ( i >= 90 ) {
        incY *= -1;
        
        if ( !fliped ) {
          if ( showFront ) {
            cl.show( pFondo, BACK);
            shotImage = shotBack;
            showFront = false;
          }
          else {
            cl.show( pFondo, FRONT);
            shotImage = shotFront;
            showFront = true;
          }
          
          fliped = true;
        }
      }
      
      Rectangle rr = pOut.getBounds();
      rr.translate( X - desp,Y);
     
      setBounds( rr);
      setShape( pOut);
      
      repaint();
      RepaintManager.currentManager( this).paintDirtyRegions();
      
      Utils.sleep(FLIP_TIME / FLIP_FRAMES);
    }
    
    onFlip = false;
    
    setSize( originalSize);
    setLocation( X,Y);
    
    resizeInside();
    setShape( null);
    
    fireEvents( new FlipEvent( this, FlipEvent.END_FLIP));
  }

  @Override
  public void paint( Graphics g) {
    if ( !onFlip ) {
      super.paint( g);
    }
     
    Graphics2D g2d = (Graphics2D)g;
    g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    
    paintBorder( g2d);
  }

  protected void paintBorder( Graphics2D g2d) {
    if ( onFlip ) {
      Color bg = UIManager.getColor( "control"); 
      
      g2d.setColor( bg);
      g2d.fill( deformedPathIn);
      
      if ( shotImage != null ) {
        g2d.drawImage( shotImage, BORDER_WIDTH + incX, TITLE_HEIGHT, null);
      }
    }
    
    Color titleBg = new Color( 0,0,0, 150);
    
    g2d.setColor( titleBg);
    g2d.fill( areaTitle);
    
    g2d.setColor(Color.BLACK);
    g2d.drawRect( 0, 0, getSize().width-1, getSize().height-1);

    if ( !onFlip ) {
      Font f = UIManager.getFont( "Button.font");
      int y = TITLE_HEIGHT - ( TITLE_HEIGHT - f.getSize() ) / 2;   
          
      GuiUtils.paintShadowTitle( g2d, getTitle(), 5, y, Color.white, Color.black, 1, GuiUtils.TEXT_FAT_SHADOW, SwingConstants.HORIZONTAL);
      
      arrangeTitleButtons();

      if ( overClose ) iClose_col.paintIcon( this, g2d, recClose.x, recClose.y);
                  else iClose_bw.paintIcon( this, g2d, recClose.x, recClose.y);
      
      if ( this.isResizable() ) {
        if ( overMax ) iMax_col.paintIcon( this, g2d, recMax.x, recMax.y);
                  else iMax_bw.paintIcon( this, g2d, recMax.x, recMax.y);

        if ( overMin ) iMin_col.paintIcon( this, g2d, recMin.x, recMin.y);
                  else iMin_bw.paintIcon( this, g2d, recMin.x, recMin.y);
      }
      
      if ( pBack.getComponentCount() != 0  ) {
        if ( overFlip ) iFlip_col.paintIcon( this, g2d, recFlip.x, recFlip.y);
                   else iFlip_bw.paintIcon( this, g2d, recFlip.x, recFlip.y);
      }
    }
  }
  
  protected double deformShape( Path2D orig, Path2D dest, double incX, double incY) {
    float[] coords = new float[6];
    Path2D dummy = new GeneralPath();
    double desp = 0;
    
    PathIterator it = orig.getPathIterator( null);
    it.currentSegment( coords);
    dummy.moveTo( coords[0] - incX, coords[1] + incY);
    
    desp = Math.min( desp, coords[0] - incX);
    
    it.next();
    it.currentSegment( coords);
    dummy.lineTo( coords[0] + incX, coords[1] + incY);
    it.next();
    it.currentSegment( coords);
    dummy.lineTo( coords[0] - incX, coords[1] - incY);
    
    desp = Math.min( desp, coords[0] - incX);
    
    it.next();
    it.currentSegment( coords);
    dummy.lineTo( coords[0] + incX, coords[1] - incY);
    dummy.closePath();
    
    boolean first = true;
    it = dummy.getPathIterator( null);
    desp = Math.abs( desp);
    
    while ( !it.isDone() ) {
      it.currentSegment( coords);
      
      if ( first ) {
        dest.moveTo( coords[0] + desp, coords[1]);
        first = false;
      }
      else {
        dest.lineTo( coords[0] + desp, coords[1]);
      }
      
      it.next();
    }
    dest.closePath();
    
    return desp;
  }
  
  private static void loadIcons() {
    int h = TITLE_HEIGHT - 2*BORDER_WIDTH;
    
    if ( iClose_col != null ) {
      return;
    }

    try {
      iClose_col = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameClose_col.png", h, h);
      iClose_bw = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameClose_bw.png", h, h);

      iMax_col = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameMax_col.png", h, h);
      iMax_bw = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameMax_bw.png", h, h);

      iMin_col = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameMin_col.png", h, h);
      iMin_bw = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameMin_bw.png", h, h);

      iFlip_col = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameFlip_col.png", h, h);
      iFlip_bw = GuiUtils.loadIcon("/es/nimbox/gui/icons/frameFlip_bw.png", h, h);
    }
    catch ( Exception ex) {
      throw new CorruptNimboxException( "Cannot load FlipFrame icons : " + ex.getMessage());
    }
  }
  
  protected void arrangeTitleButtons() {
    int x = rTitle.width + BORDER_WIDTH;
    int y = BORDER_WIDTH;
    
    x -= iClose_col.getIconWidth();
    recClose = new Rectangle( x,y, iClose_col.getIconWidth(), iClose_col.getIconHeight());
    
    x -= BORDER_WIDTH;
    
    if ( this.isResizable() ) {
      x -= iMax_col.getIconWidth();
      recMax = new Rectangle( x,y, iMax_col.getIconWidth(), iMax_col.getIconHeight());

      x -= iMin_col.getIconWidth() + 1;
      recMin = new Rectangle( x,y, iMin_col.getIconWidth(), iMin_col.getIconHeight());
    }
    
    if ( pBack.getComponentCount() != 0 ) {
      x -= iFlip_col.getIconWidth() + BORDER_WIDTH;
      recFlip = new Rectangle( x,y, iFlip_col.getIconWidth(), iFlip_col.getIconHeight());
    }
  }
  
  protected BufferedImage getScreenShot( Component cmp) {
    BufferedImage res = null;
    
    try {
      Rectangle r = new Rectangle( cmp.getLocationOnScreen());
      r.setSize( cmp.getSize());
      
      res = new Robot().createScreenCapture( r);
    }
    catch ( AWTException e) {
      e.printStackTrace();
    }
    
    return res; 
  }
  
  private class MiML extends MouseAdapter {
    Point drag;
    Rectangle oldBounds;
    
    Cursor curH = new Cursor( Cursor.E_RESIZE_CURSOR);
    Cursor curV = new Cursor( Cursor.N_RESIZE_CURSOR);
    Cursor curDW = new Cursor( Cursor.NW_RESIZE_CURSOR);
    Cursor curDE = new Cursor( Cursor.NE_RESIZE_CURSOR);
    Cursor curN = new Cursor( Cursor.DEFAULT_CURSOR);
    
    boolean bT;
    boolean bD;
    boolean bL;
    boolean bR;
    boolean bResizing;

    @Override
    public void mouseMoved( MouseEvent ev) {
      Point p = ev.getPoint();
      
      boolean repaint = overClose || overMin || overMax || overFlip;

      overClose = overMin = overMax = overFlip = false;
      
      if ( recClose != null && recClose.contains( ev.getPoint()) ) {
        repaint = overClose = true;
      }
      if ( recMax != null && recMax.contains( ev.getPoint()) ) {
        repaint = overMax = true;
      }
      if ( recMin != null && recMin.contains( ev.getPoint()) ) {
        repaint = overMin = true;
      }
      if ( recFlip != null && recFlip.contains( ev.getPoint()) ) {
        repaint = overFlip = true;
      }
      
      if ( repaint ) {
        repaint();
        return;
      }
      
      if ( FlipFrame.this.isResizable() ) {
        if ( p.x < BORDER_WIDTH && p.y < BORDER_WIDTH ) {
          setCursor( curDW);
          bResizing = bT = bL = true;
          bD = bR = false;
        }
        else if ( p.x > getWidth() - BORDER_WIDTH && p.y < BORDER_WIDTH ) {
          setCursor( curDE);
          bResizing = bT = bR = true;
          bD = bL = false;
        }
        else if ( p.x < BORDER_WIDTH && p.y > getHeight() - BORDER_WIDTH ) {
          setCursor( curDE);
          bResizing = bD = bL = true;
          bT = bR = false;
        }
        else if ( p.x > getWidth() - BORDER_WIDTH && p.y > getHeight() - BORDER_WIDTH ) {
          setCursor( curDW);
          bResizing = bD = bR = true;
          bT = bL = false;
        }
        else if ( p.x < BORDER_WIDTH ) {
          setCursor( curH);
          bResizing = bL = true;
          bD = bR = bT = false;
        }
        else if ( p.x > getWidth() - BORDER_WIDTH ) {
          setCursor( curH);
          bResizing = bR = true;
          bT = bL = bD = false;
        }
        else if ( p.y < BORDER_WIDTH ) {
          setCursor( curV);
          bResizing = bT = true;
          bD = bR = bL = false;
        }
        else if ( p.y > getHeight() - BORDER_WIDTH ) {
          setCursor( curV);
          bResizing = bD = true;
          bR = bT = bL = false;
        }
        else {
          setCursor( curN);
          bResizing = bT = bL = bD = bR = false;
        }
      }
    }

    @Override
    public void mouseClicked( MouseEvent ev) {
      if ( isResizable() && 
           ( 
             ( rTitle.contains( ev.getPoint()) && ev.getClickCount() >= 2 )
             ||
             ( recMax.contains( ev.getPoint()) )
           )
         ) {
        if ( oldBounds == null ) {
          oldBounds = getBounds();

          setExtendedState(Frame.MAXIMIZED_BOTH);
        }
        else { 
          setBounds( oldBounds);
          oldBounds = null;
        }
        
        resizeInside();
        
        fireEvents( new FlipEvent( FlipFrame.this, FlipEvent.END_RESIZE));
      }
      else if ( recFlip.contains( ev.getPoint()) ) {
        flip();
      }
      else if ( recClose.contains( ev.getPoint()) ) {
        dispatchEvent( new WindowEvent( FlipFrame.this, WindowEvent.WINDOW_CLOSING));
      }
      else if ( recMin.contains( ev.getPoint()) ) {
        setExtendedState(Frame.ICONIFIED);
      }
    }
    
    @Override
    public void mouseDragged( MouseEvent ev) {
      if ( drag != null ) {
        if ( bResizing ) {
          Point np = ev.getPoint();
          int x = bR || bL ? np.x - drag.x : 0;
          int y = bT || bD ? np.y - drag.y : 0;
          
          Rectangle rect = getBounds();
          if ( bT ) {
            rect.y += y;
            rect.height -= y;
          }
          if ( bD ) {
            rect.height += y;
            drag.y = np.y;
          }
          if ( bL ) {
            rect.x += x;
            rect.width -= x;
          }
          if ( bR ) {
            rect.width += x;
            drag.x = np.x;
          }
          
          setBounds( rect);
        }
        else {
          Point np = ev.getLocationOnScreen();
          setLocation( new Point( np.x - drag.x, np.y - drag.y));
        }
      }
    }

    @Override
    public void mousePressed( MouseEvent ev) {
      Point p = ev.getPoint();
      
      if ( bResizing ) {
        drag = p;
        
        fireEvents( new FlipEvent( FlipFrame.this, FlipEvent.START_RESIZE));
      }
      else if ( rTitle.contains( p) ) {
        drag = p;
      }
      else {
        drag = null;
      }
    }

    @Override
    public void mouseReleased( MouseEvent ev) {
      if ( bResizing ) {
        fireEvents( new FlipEvent( FlipFrame.this, FlipEvent.END_RESIZE));
      }
      
      drag = null;
      bResizing = false;
    }

    @Override
    public void mouseWheelMoved( MouseWheelEvent ev) {
      if ( ev.getPoint().y > TITLE_HEIGHT ) {
        return;
      }
      
      float oldOpacity = getOpacity();
      
      float newOpacity = getOpacity() + (float)ev.getWheelRotation() * -5 / 100;

      if ( newOpacity < 0.1f ) newOpacity = 0.1f;
      if ( newOpacity > 1.0f ) newOpacity = 1.0f;
      
      if ( oldOpacity != newOpacity ) {
        setOpacity( newOpacity);
        resizeInside();
      
        fireEvents( new FlipEvent( FlipFrame.this, FlipEvent.CHANGE_OPACITY, newOpacity));
      }
    }
    
  }
  
  
  private class MiCL extends ComponentAdapter {
    @Override
    public void componentResized( ComponentEvent ev) {
      if ( !onFlip ) {
        resizeInside();
      }
    }
  }
}
