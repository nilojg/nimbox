package es.nimbox.gui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.JTextComponent;


/**
 * <p>This class implements a component that is basically a text field with a button to change the input method.
 * When the button is pushed, only hexadecimal characters are allowed. When to button is not selected, the
 * component is a regular <code>JTextField</code>.</p>
 *
 * <p>It's quite easy to use</p>
 *
 * <pre>
 *   // An empty JHexTextField, which initially accepts any input
 *   JHexTextField ht1 = new JHexTextField();
 *
 *   // A JHexTextField with the text "The initial text", which initially
 *   // accepts any input
 *   JHexTextField ht2 = new JHexTextField( "The initial text");
 *
 *   // A JHexTextField with the text "The initial text", which initially
 *   // only accepts HEX input (and yes, it can be initialized with any input)
 *   JHexTextField ht3 = new JHexTextField( "The initial text", true);
 *
 *   // A JHexTextField with the text "The initial text", which initially
 *   // accepts any input, but changes to "only hex" programmatically
 *   JHexTextField ht4 = new JHexTextField( "The initial text");
 *   ...
 *   ht4.setHexMode( true);
 *
 *   // A JHexTextField with the text "The initial text", which initially
 *   // accepts any input, but changes to "only hex forever" programmatically
 *   // The "hex" button cannot be deselected
 *   JHexTextField ht5 = new JHexTextField( "The initial text");
 *   ht4.setOnlyHex( true);
 * </pre>
 *
 * @author nilo.gonzalez
 *
 */
public class JHexTextField extends JPanel {
  private static final long serialVersionUID = 1L;

  protected static final ImageIcon hexIcon;

  protected JTextComponent textField;
  protected JToggleButton bHex;
  
  protected boolean componentEnabled;

  // The mode of the component
  protected boolean hexMode; 

  // If the component is ALWAYS in hex mode
  protected boolean onlyHex;

  static {
    try {
      hexIcon = GuiUtils.loadIcon( "/es/nimbox/gui/icons/hex.png");
    }
    catch ( Exception ex) {
      throw new CorruptNimboxException( "Cannot load HexTextField icons : " + ex.getMessage());
    }
  }
  
  public JHexTextField() {
    this( "");
  }
  
  public JHexTextField(String txt) {
    this( txt, false);
  }
  
  /**
   * <p>This constructor creates a <code>JHexTextField</code> with the <code>txt</code> content and in hexadecimal mode if
   * <code>hexMode</code> is true.</p>
   * @param txt the content
   * @param hexMode if this is true, the component will be in hexadecimal mode
   */
  public JHexTextField(String txt, boolean hexMode) {
    super( new BorderLayout());

    // Create components
    textField = new JTextField( txt);
    
    bHex = new JToggleButton( hexIcon);
    bHex.setSelected( hexMode);

    MiAL mial = new MiAL();
    bHex.addActionListener( mial);
    textField.addKeyListener( mial);
    
    onlyHex = false;
    setEnabled( true);
    setHexMode( hexMode);
    
    add( textField, BorderLayout.CENTER);
    add( bHex, BorderLayout.EAST);
  }
  
  /**
   * This method returns the <code>JTextComponent</code> inside this component
   * @return  the <code>JTextComponent</code> inside this component
   */
  public JTextComponent getTextComponent() {
    return textField;
  }

  /**
   * This method sets the text of the component
   * @param text the text of the component
   */
  public void setText( String text) {
    textField.setText( text);
  }
  
  /**
   * This method returns the text of the component
   * @return the text of the component
   */
  public String getText() {
    return textField.getText();
  }
  
  /**
   * This method returns true if the component only accepts hexadecimal forever
   * @return true if the component only accepts hexadecimal, false otherwise
   */
  public boolean isOnlyHex() {
    return onlyHex;
  }
  
  /**
   * If the parameter <code>onlyHex</code> is true, this component will only allow hexadecimal values forever
   * @param onlyHex true if you want that the component only accepts hexadecimal input from the user forever
   */
  public void setOnlyHex( boolean onlyHex) {
    this.onlyHex = onlyHex;
    
    if ( onlyHex ) {
      bHex.setSelected( true);
      setHexMode( true);
    }
  }
  
  /**
   * This method returns true if the component is in hexadecimal mode
   * @return true if the component is in hexadecimal mode
   */
  public boolean isHexMode() {
    return hexMode;
  }
  
  /**
   * This method set the component in hexadecimal mode if <code>hexMode</code> is true
   * @param hexMode true if you want that the component only accepts hexadecimal input from the user
   */
  public void setHexMode( boolean hexMode) {
    this.hexMode = hexMode;
    
    if ( bHex != null ) {
      bHex.setSelected( hexMode);
    }
  }

  @Override
  public String getToolTipText() {
    return textField.getToolTipText();
  }
  
  @Override
  public void setToolTipText( String toolTipText) {
    textField.setToolTipText( toolTipText);
  }

  @Override
  public Font getFont() {
    if ( textField == null ) {
      return UIManager.getFont( "TextPane.font");
    }
    
    return textField.getFont();
  }
  
  @Override
  public void setFont( Font font) {
    if ( textField != null ) {
      textField.setFont( font);
    }
  }

  @Override
  public boolean isEnabled() {
    return componentEnabled;
  }
  
  @Override
  public void setEnabled( boolean enabled) {
    this.componentEnabled = enabled;
    
    textField.setEnabled( componentEnabled);
    bHex.setEnabled( componentEnabled);
  }
  
  @Override
  public synchronized void addKeyListener( KeyListener kl) {
    textField.addKeyListener( kl);
  }
  
  @Override
  public synchronized void removeKeyListener( KeyListener kl) {
    textField.removeKeyListener( kl);
  }

  /**
   * <p>This methods adds an <code>ActionListener</code> that will be notified when the HEX button is
   * pressed</p>
   * @param al the <code>ActionListener</code>
   */
  public void addActionListener( ActionListener al) {
    bHex.addActionListener( al);
  }
  
  public void removeActionListener( ActionListener al) {
    bHex.removeActionListener( al);
  }

  
  private class MiAL extends KeyAdapter implements ActionListener {

    @Override
    public void keyTyped( KeyEvent ev) {
      if ( !hexMode ) {
        return;
      }
      
      char k = ev.getKeyChar();

      // Deletes, supr and cursors should be treated as usual
      if ( Character.isISOControl( k) ) {
        return;
      }

      // Only HEX-chars are allowed. If the key is not an HEX char, beep and consume the event so
      // it doesn't reach the JTextField component
      try {
        Integer.parseInt( "" + k, 16);
      }
      catch ( Exception ex) {
        Toolkit.getDefaultToolkit().beep();
        ev.consume();
      }
    }

    @Override
    public void actionPerformed( ActionEvent ev) {
      if ( onlyHex ) {
        setHexMode( true);
      }
      else {
        hexMode = bHex.isSelected();
      }
      
      textField.grabFocus();
    }
  }
}
