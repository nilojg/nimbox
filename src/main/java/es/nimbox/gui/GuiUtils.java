package es.nimbox.gui;

import es.nimbox.box.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.IOException;
import java.util.Arrays;

public class GuiUtils {

  private GuiUtils() {}


  /**
   * <p>This method copies the string <code>txt</code> to the clipboard</p>
   * @param txt the text to copy to the clipboard
   */
  public static void copy( String txt) {
    StringSelection copy = new StringSelection(txt);
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    clipboard.setContents( copy, null);
  }


  /**
   * <p>This method returns as a <b>String</b> the contents of the clipboard. If the contents of the
   * clipboard cannot be converted into a <code>String</code> or there is any other problem, the method
   * throws an exception</p>
   * @return the contents of the clipboard, as a <code>String</code>
   * @throws IOException when there is an IO problem
   * @throws UnsupportedFlavorException if the contents cannot be converted into a String
   */
  public static String paste() throws IOException, UnsupportedFlavorException {
    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    Transferable copy = clipboard.getContents(null);

    return copy.getTransferData( DataFlavor.stringFlavor).toString();
  }


  /**
   * <p>This method returns as a <b>String</b> the contents of the clipboard. If the contents of the
   * clipboard cannot be converted into a <code>String</code> or there is any other problem, the method
   * shows an error dialog with the title <code>title</code> and the message <code>message</code>, and then
   * returns <code>null</code></p>
   * @param title the title of the possible error dialog
   * @param message the message of the possible error dialog
   * @return the contents of the clipboard as a <code>String</code> or <code>null</code>
   */
  public static String paste( String title, String message) {
    try {
      return paste();
    }
    catch ( Exception ex ) {
      JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    return null;
  }

  /**
   * <p>This method returns <code>true</code> if the current configuration supports transparent windows, aka "translucency",
   * with <code>GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSLUCENT</code> option</p>
   * @return <code>true</code> if the current configuration supports transparent windows, aka "translucency"
   *         and <code>false</code> otherwise
   */
  public static boolean transparencyAllowed() {
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice gd = ge.getDefaultScreenDevice();

    return gd.isWindowTranslucencySupported( GraphicsDevice.WindowTranslucency.PERPIXEL_TRANSLUCENT);
  }


  /**
   * <p>This method returns an array with all the screens in the system</p>
   * @return an array with all the screens in the system
   */
  public static GraphicsDevice[] getScreens() {
    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    return ge.getScreenDevices();
  }

  /**
   * <p>This method returns the number of screens</p>
   * @return the number of screens
   */
  public static int getScreensNumber() {
    return getScreens().length;
  }


  /**
   * <p>This method returns the dimensions of the bigger screen. How a screen can have bigger width but more little height
   * than other, to determine which is the biggest screen, it uses the area of the screen, in pixels.</p>
   * @return the dimension of the biggest screen
   */
  public static Dimension getMaximumScreenSize() {
    Dimension screenSize = new Dimension();
    int biggestArea = 0;

    for (GraphicsDevice screen : getScreens()) {
      Rectangle screenBounds = screen.getDefaultConfiguration().getBounds();

      int area = screenBounds.width * screenBounds.height;

      if ( area > biggestArea ) {
        screenSize.width = screenBounds.width;
        screenSize.height = screenBounds.height;
      }
    }

    return screenSize;
  }



  /**
   * <p>This method returns a new <code>Rectangle</code> of dimension <code>toCenter</code> which is centered
   * inside a rectangle (window, screen, etc) of size <code>screen</code>.</p>
   * @param toCenter the thing to center
   * @param screen the background panel to contain the centered thing
   * @return a new <code>Rectangle</code> with the coordinates of the centered thing
   */
  public static Rectangle centerOneInOne( Dimension toCenter, Dimension screen) {
    return new Rectangle( screen.width / 2 - toCenter.width / 2,
                          screen.height / 2 - toCenter.height / 2,
                          toCenter.width,
                          toCenter.height);
  }

  /**
   * <p>This method returns a rectangle that centers an object of size <code>toCenter</code> in the current screen.</p>
   * @param toCenter the size of the thing to center in the screen
   * @return a new <code>Rectangle</code> with the coordinates of the centered thing
   */
  public static Rectangle centerOnScreen( Dimension toCenter) {
    Dimension scr = Toolkit.getDefaultToolkit().getScreenSize();

    return centerOneInOne( toCenter, scr);
  }

  /**
   * <p>This method canters the <code>window</code> in the current screen</p>
   * @param window the window to center
   */
  public static void centerOnScreen( Window window) {
    Rectangle bounds = centerOnScreen( window.getSize());

    window.setBounds( bounds);
  }


  /**
   * <p>This method returns a darker or lighter version of <code>color</code>
   * <i>shifting</i> the color components of <code>color</code>, so the returned color is
   * <code>shift</code> units more darker if <code>shift</code> is negative, and <code>shift</code> units
   * lighter if <code>shift</code> is positive.</p>
   * @param color the color to made darker or lighter
   * @param shift the units to shift
   * @return a darker or ligter version of <code>color</code>
   */
  public static Color shiftColor( Color color, int shift) {
    int r = color.getRed() + shift;
    int g = color.getGreen() + shift;
    int b = color.getBlue() + shift;

    r = ( r > 255 ? 255 : r);
    r = ( r < 0 ?  0 : r);

    g = ( g > 255 ? 255 : g);
    g = ( g < 0 ?  0 : g);

    b = ( b > 255 ? 255 : b);
    b = ( b < 0 ?  0 : b);

    return new Color( r,g,b);
  }

  /**
   * <p>This methods adds an alpha channel to <code>color</code></p>
   * @param color the color to make transparent
   * @param alpha the alpha value
   * @return a transparent version of <code>color</code>
   */
  public static Color getTransparentColor( Color color, int alpha) {
    if ( alpha < 0) alpha = 0;
    if ( alpha > 255 ) alpha = 255;

    if ( color == null ) {
      color = new Color( 100,100,100);
    }

    return new Color( color.getRed(), color.getGreen(), color.getBlue(), alpha);
  }

  /**
   * <p>This method returns a resized version of <code>image</code></p>
   * @param image the image to resize
   * @param width the final width
   * @param height the final height
   * @return the resized image
   */
  public static ImageIcon resize( ImageIcon image, int width, int height) {
    Image res = image.getImage().getScaledInstance( width, height, Image.SCALE_SMOOTH);

    return new ImageIcon( res);
  }

  /**
   * <p>This method loads an icon from <code>file</code> and resizes it to the size <code>width</code> and <code>height</code>.
   * It can be a classpath URL</p>
   * @param file the icon to load
   * @param width the final width
   * @param height the final height
   * @return the loaded icon with the desired size
   * @throws IconLoadingException when shit happens
   */
  public static ImageIcon loadIcon( String file, int width, int height) throws IconLoadingException {
    return resize( loadIcon( file), width, height);
  }

  /**
   * <p>This method loads an icon from <code>file</code>. It can be a classpath URL</p>
   * @param file the icon to load
   * @return the loaded icon
   * @throws IconLoadingException when shit happens
   */
  public static ImageIcon loadIcon( String file) throws IconLoadingException {
    try {
      return new ImageIcon(Toolkit.getDefaultToolkit()
                                  .createImage(Utils.readBytesFromInputStream(Utils.findFile(file))));
    }
    catch ( Exception ex) {
      throw new IconLoadingException( "Cannot load icon '" + file + "' : " + ex.getMessage(), ex);
    }
  }


  public static final int TEXT_THIN_SHADOW = 0;
  public static final int TEXT_FAT_SHADOW = 1;

  static final int MATRIX_FAT = 5;  // Esto define el grosor de la sombra de los titulos
  static Kernel kernelFat;

  static final int MATRIX_THIN = 3;  // Esto define el grosor de la sombra de los menus
  static Kernel kernelThin;

  static {
    float[] elements = new float[MATRIX_FAT * MATRIX_FAT];
    Arrays.fill(elements, 0.1f);

    int mid = MATRIX_FAT / 2+1;
    elements[mid*mid] = .2f;

    kernelFat = new Kernel( MATRIX_FAT,MATRIX_FAT, elements);

    elements = new float[MATRIX_THIN * MATRIX_THIN];
    Arrays.fill(elements, 0.1f);

    mid = MATRIX_THIN / 2+1;
    elements[mid*mid] = .2f;

    kernelThin = new Kernel( MATRIX_THIN,MATRIX_THIN, elements);
  }


  public static void paintShadowTitle( Graphics g, String title,
                                       int x, int y, Color frente, Color shadow,
                                       int desp, int tipo, int orientation) {

    // Si hay que rotar la fuente, se rota
    Font f = g.getFont();
    if ( orientation == SwingConstants.VERTICAL ) {
      AffineTransform rotate = AffineTransform.getRotateInstance(Math.PI / 2);
      f = f.deriveFont( rotate);
    }

    // Si hay que pintar sombra, se hacen un monton de cosas
    if ( shadow != null ) {
      int matrix = ( tipo == TEXT_THIN_SHADOW ? MATRIX_THIN : MATRIX_FAT);

      Rectangle2D
              rect = g.getFontMetrics().getStringBounds(title, g);

      int w;
      int h;

      if ( orientation == SwingConstants.HORIZONTAL ) {
        w = (int)rect.getWidth() + 6*matrix;    // Hay que dejar espacio para las sombras y el borde
        h = (int)rect.getHeight() + 6*matrix;   // que ConvolveOp ignora por el EDGE_NO_OP
      }
      else {
        h = (int)rect.getWidth() + 6*matrix;    // Hay que dejar espacio para las sombras y el borde
        w = (int)rect.getHeight() + 6*matrix;   // que ConvolveOp ignora por el EDGE_NO_OP
      }

      // La sombra del titulo
      BufferedImage iTitulo = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
      BufferedImage iSombra = new BufferedImage( w,h, BufferedImage.TYPE_INT_ARGB);

      Graphics2D g2 = iTitulo.createGraphics();
      g2.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

      g2.setFont( f);
      g2.setColor( shadow);
      g2.drawString( title, 3*matrix, 3*matrix);  // La pintamos en el centro

      ConvolveOp cop = new ConvolveOp(( tipo == TEXT_THIN_SHADOW ? kernelThin : kernelFat), ConvolveOp.EDGE_NO_OP, null);
      cop.filter( iTitulo, iSombra);              // A ditorsionar

      // Por fin, pintamos el jodio titulo
      g.drawImage( iSombra,
                   x - 3*matrix + desp,              // Lo llevamos a la posicion original y le sumamos 1
                   y - 3*matrix + desp,              // para que la sombra quede pelin desplazada
                   null);
    }

    // Si hay que pintar el frente, se pinta
    if ( frente != null ) {
      g.setFont( f);
      g.setColor( frente);
      g.drawString( title, x, y);
    }
  }
}
