package es.nimbox.gui;

public class CorruptNimboxException extends RuntimeException {
  public CorruptNimboxException() {
  }

  public CorruptNimboxException(String message) {
    super(message);
  }

  public CorruptNimboxException(String message, Throwable cause) {
    super(message, cause);
  }

  public CorruptNimboxException(Throwable cause) {
    super(cause);
  }
}
