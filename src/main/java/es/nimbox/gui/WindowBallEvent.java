package es.nimbox.gui;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Date;

/**
 * <p>An event triggered by the WindowBall. There are four event types:</p>
 * <ul>
 *   <li><code>MOUSE_CLICKED</code> : This is basically a <code>MouseEvent</code>, and it's triggered when
 *       the user clicks the WindowBall</li>
 *   <li><code>CHILD_SHOWN</code> : This event is triggered when the <code>WindowBall</code> shows its children,
 *       typically because the user double-clicks the ball</li>
 *   <li><code>CHILD_HIDDEN</code> : This event is triggered when the <code>WindowBall</code> hides its children,
 *       typically because the user double-clicks the ball</li>
 *   <li><code>POSITION_CHANGED</code> : This event is triggered when the <code>WindowBall</code> is moved on
 *       the screen</li>
 * </ul>
 * 
 * @author nilo.gonzalez
 *
 * @see WindowBall
 * @see WindowBallListener
 */
public class WindowBallEvent {

  /**
   * No mouse button clicked
   */
  public static final int NOBUTTON = MouseEvent.NOBUTTON;
  
  /**
   * This is MouseEvent.BUTTON1
   */
  public static final int BUTTON1  = MouseEvent.BUTTON1;
  /**
   * This is MouseEvent.BUTTON2
   */
  public static final int BUTTON2  = MouseEvent.BUTTON2;
  /**
   * This is MouseEvent.BUTTON3
   */
  public static final int BUTTON3  = MouseEvent.BUTTON3;
  
  /**
   * A mouse clicked event type
   */
  public static final int MOUSE_CLICKED    = MouseEvent.MOUSE_CLICKED;
  
  /**
   * The ball is showing its children
   */
  public static final int CHILD_SHOWN      = 100;
  /**
   * The ball is hiding its children
   */
  public static final int CHILD_HIDDEN     = 101;
  /**
   * The user is dragging the ball on the screen
   */
  public static final int POSITION_CHANGED = 102;
  
  private final WindowBall ball;
  private final int eventType;
  private final long when;
  private final String command;
  private final Point position;
  private final Point positionOnScreen;
  private final int clickCount;
  private final int button;
  
  boolean altDown;
  boolean altGraphDown;
  boolean ctrlDown;
  boolean shiftDown;
  boolean popupTrigger;

  public WindowBallEvent( WindowBall ball, int eventType, String command, Point position, Point positionOnScreen, int clickCount, int button, boolean altDown,
                          boolean altGraphDown, boolean ctrlDown, boolean shiftDown, boolean popupTrigger) {
    super();
    
    this.ball = ball;
    this.eventType = eventType;
    this.when = new Date().getTime();
    this.command = command;
    this.position = position;
    this.positionOnScreen = positionOnScreen;
    this.clickCount = clickCount;
    this.button = button;
    this.altDown = altDown;
    this.altGraphDown = altGraphDown;
    this.ctrlDown = ctrlDown;
    this.shiftDown = shiftDown;
    this.popupTrigger = popupTrigger;
  }
  
  public WindowBallEvent( WindowBall ball, int eventType, String command, Point positionOnScreen) {
    super();
    
    this.ball = ball;
    this.eventType = eventType;
    this.when = new Date().getTime();
    this.command = command;
    this.position = positionOnScreen;
    this.positionOnScreen = positionOnScreen;
    this.clickCount = 0;
    this.button = NOBUTTON;
    this.altDown = false;
    this.altGraphDown = false;
    this.ctrlDown = false;
    this.shiftDown = false;
    this.popupTrigger = false;
  }
  
  /**
   * This constructor tries to look like the <code>MouseEvent</code> constructor. For MOUSE_CLICKED events
   * @param ball the ball that fires the event
   * @param mouseEvent the mouse event
   * @param command the command
   */
  public WindowBallEvent( WindowBall ball, MouseEvent mouseEvent, String command) {
    super();
    
    this.ball = ball;
    this.eventType = MOUSE_CLICKED;
    this.when = mouseEvent.getWhen();
    this.command = command;
    this.position = mouseEvent.getPoint();
    this.positionOnScreen = mouseEvent.getLocationOnScreen();
    this.clickCount = mouseEvent.getClickCount();
    this.button = mouseEvent.getButton();
    this.altDown = mouseEvent.isAltDown();
    this.altGraphDown = mouseEvent.isAltGraphDown();
    this.ctrlDown = mouseEvent.isControlDown();
    this.shiftDown = mouseEvent.isShiftDown();
    this.popupTrigger = mouseEvent.isPopupTrigger();
  }
  
  public WindowBall getWindowBall() {
    return ball;
  }

  public int getID() {
    return eventType;
  }

  public long getWhen() {
    return when;
  }
  
  public String getActionCommand() {
    return command;
  }

  public Point getPosition() {
    return position;
  }

  public Point getPositionOnScreen() {
    return positionOnScreen;
  }

  public int getClickCount() {
    return clickCount;
  }

  public int getButton() {
    return button;
  }

  public boolean isAltDown() {
    return altDown;
  }

  public boolean isAltGraphDown() {
    return altGraphDown;
  }

  public boolean isCtrlDown() {
    return ctrlDown;
  }

  public boolean isShiftDown() {
    return shiftDown;
  }
  
  public boolean isPopupTrigger() {
    int num = MouseInfo.getNumberOfButtons();
    
    popupTrigger = false;
    
    if ( num == 1 ) {
      popupTrigger = isCtrlDown();
    }
    else  if ( num == 2 ) {
      popupTrigger = getButton() == MouseEvent.BUTTON2;
    }
    else  {
      popupTrigger = getButton() == MouseEvent.BUTTON3;
    }
    
    return popupTrigger;
  }
}
