package es.nimbox.gui;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>This class is a very simple model for <code>JTickList</code>.</p>
 * @see JTickList
 */
class TickListModel implements ListModel<String> {

  public class TickListItem {
    String name;
    boolean selected;

    public TickListItem( String name) {
      this( name, true);
    }

    public TickListItem(String name, boolean selected) {
      this.name = name;
      this.selected = selected;
    }
  }

  private List<ListDataListener> listeners;
  private List<TickListItem> lObjects;

  public TickListModel() {
    listeners = new ArrayList<>();
    lObjects = new ArrayList<>();
  }

  /**
   * This method adds <code>item</code> to the model, selected
   * @param item the item to add
   */
  public void add( String item) {
    lObjects.add(new TickListItem( item));
    fireEvents();
  }

  /**
   * This method adds <code>item</code> to the model with status <code>selected</code>
   * @param item the item to add
   * @param selected the status
   */
  public void add( String item, boolean selected) {
    lObjects.add( new TickListItem( item, selected));
    fireEvents();
  }

  /**
   * This method adds all the <code>String</code> contained in <code>items</code> into the list, all of them
   * with the status <code>selected</code>
   * @param items the item to add to the list
   * @param selected the status of <b>all</b> the new items
   */
  public void addItems( Collection<String> items, boolean selected) {
    items.forEach( item -> add( item, selected));
  }

  /**
   * This method removes all the items with <code>name.equals( item)</code> in the model
   * @param item the item to remove from the model
   */
  public void remove( String item) {
    lObjects.removeIf( i -> i.name.equals( item));

    fireEvents();
  }

  /**
   * This method removes all the items in the model
   */
  public void clear() {
    lObjects.clear();
    fireEvents();
  }

  /**
   * This method returns the position of the item <code>name</code> or -1 if <code>name</code> is not
   * in the list.
   * @param name the item to find
   * @return the position of the item <code>name</code> or -1 if <code>name</code> is not in the list
   */
  public int getPosition( String name) {
    for ( int i = 0; i < lObjects.size(); i++ ) {
      if ( lObjects.get(i).name.equals(name) ) {
        return i;
      }
    }

    return -1;
  }

  /**
   * This method returns the <b>first</b >item with <code>name.equals( name)</code> or <code>null</code>
   * if <code>item</code> is not in the list
   * @param name the item to find
   * @return the <code>TickListItem</code> of <code>null</code> if <code>name</code> is not in the list
   */
  public TickListItem getItem( String name) {
    int pos = getPosition(name);

    return getItem( pos);
  }

  /**
   * This method returns the <code>TickListItem</code> at the <code>index</code> or <code>null</code>
   * if <code>index</code> is out of the size of the list
   * @param index the index of the item
   * @return the <code>TickListItem</code> at the <code>index</code> or <code>null</code> if <code>index</code> is out
   *         of the size of the list
   */
  public TickListItem getItem( int index) {
    if ( index < 0 || index >= lObjects.size() ) {
      return null;
    }

    return lObjects.get( index);
  }

  /**
   * This method returns <code>true</code> if the item in <code>index</code> is selected and <code>false</code> otherwise
   * @param index the index of the item
   * @return  <code>true</code> if the item in <code>index</code> is selected and <code>false</code> otherwise.
   */
  public boolean isSelected( int index) {
    TickListItem item = getItem( index);

    return item != null && item.selected;
  }

  /**
   * This method changes the status if the item at <code>index</code> position to <code>selected</code>
   * @param index the position to change
   * @param selected the new status
   */
  public void setSelected( int index, boolean selected) {
    TickListItem item = getItem( index);

    if ( item != null ) {
      item.selected = selected;
    }
  }

  /**
   * This method returns the list of selected items names
   * @return the list of selected items names
   */
  public List<String> getSelectedNames() {
    return getSelectedItems().stream()
                             .map( item -> item.name)
                             .collect(Collectors.toList());
  }

  /**
   * This method returns the list of selected items
   * @return the list of selected items
   */
  public List<TickListItem> getSelectedItems() {
    return lObjects.stream()
                   .filter( item -> item.selected)
                   .collect(Collectors.toList());
  }

  @Override
  public String getElementAt( int index) {
    TickListItem res = getItem( index);

    return (res == null ? null : res.name);
  }

  @Override
  public int getSize() {
    return lObjects.size();
  }

  @Override
  public void addListDataListener( ListDataListener l) {
    listeners.add(l);
  }

  @Override
  public void removeListDataListener( ListDataListener l) {
    listeners.remove(l);
  }

  public void fireEvents() {
    for ( ListDataListener l : listeners ) {
      l.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, lObjects.size() - 1));
    }
  }
}
