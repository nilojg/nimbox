package es.nimbox.gui;

/**
 * The listener interface for receiving WindowBallEvents.
 * 
 * @author nilo.gonzalez
 *
 */
public interface WindowBallListener {

  /**
   * This method is invoked when a WindowBall is clicked 
   * @param ev the event
   */
  void mouseClicked( WindowBallEvent ev);
  
  /**
   * This method is invoked when a WindowBall shows its children 
   * @param ev the event
   */
  void childShown( WindowBallEvent ev);
  
  /**
   * This method is invoked when a WindowBall hides its children
   * @param ev the event
   */
  void childHidden( WindowBallEvent ev);
  
  /**
   * This method is invokated when a WindowBall changes its position on the screen because the user drags it
   * @param ev the event
   */
  void positionChanged( WindowBallEvent ev);
}
