package es.nimbox.gui;

import java.awt.*;
import java.util.Date;

/**
 * This class defines the events that a FlipFrame throws. There are 5 event types:
 * <ul>
 *   <li><code>START_FLIP</code>: fired when the flip action begins</li>
 *   <li><code>END_FLIP</code>: fired when the flip action ends</li>
 *   <li><code>START_RESIZE</code>: fired when the user starts resizing the frame</li>
 *   <li><code>END_RESIZE</code>: fired when the user finishs resizing the frame</li>
 *   <li><code>CHANGE_OPACITY</code>: fired when the user changes the opacity of the frame</li>
 * </ul>
 * 
 * These events are processed by a <code>FlipListener</code> instance.
 * 
 * @author nilo.gonzalez
 *
 * @see FlipFrame
 * @see FlipListener
 */
public class FlipEvent {
  
  public static final int START_FLIP     = 0;
  public static final int END_FLIP       = 1;
  public static final int START_RESIZE   = 2;
  public static final int END_RESIZE     = 3;
  public static final int CHANGE_OPACITY = 4;
  
  private final Component source;
  private final int type;
  private final Date when;
  private final float opacity;
  
  public FlipEvent( Component source, int type) {
    this( source, type, 0.0f);
  }
  
  public FlipEvent( Component source, int type, float opacity) {
    this.source = source;
    this.type = type;
    this.opacity = opacity;
    
    this.when = new Date();
  }
  
  @Override
  public String toString() {
    StringBuilder res = new StringBuilder();
    
    res.append( this.getClass() + "[");
    switch ( type ) {
      case START_FLIP     : res.append( "START_FLIP]");
                            break;
      case END_FLIP       : res.append( "END_FLIP]");
                            break;
      case START_RESIZE   : res.append( "START_RESIZE]");
                            break;
      case END_RESIZE     : res.append( "END_RESIZE]");
                            break;
      case CHANGE_OPACITY : res.append( "CHANGE_OPACITY (");
                            res.append( opacity);
                            res.append( ")]");
                            break;
      default: res.append( "UNKNOWN EVENT");
    }
    
    res.append( " at " + when);
    res.append( " on " + source.toString());
    
    return res.toString();
  }

  public Component getSource() {
    return source;
  }

  public int getID() {
    return type;
  }

  public Date getWhen() {
    return when;
  }

  public float getOpacity() {
    return opacity;
  }
  
  
}
