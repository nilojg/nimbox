package es.nimbox.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * <p>This method just returns a <code>JPanel</code> with a pair of buttons for "OK" and "Cancel".</p>
 */
public class JCancelOkPanel extends JPanel {


  /**
   * <p>This constructor creates a <code>JPanel</code> with a pair of buttons, with only text, and registers
   * the <code>ActionListener al</code> in each button. The command fired when the buttons are pressed are
   * <code>textOk</code> and <code>textCancel</code>.</p>
   * @param textOk the text for the OK button
   * @param textCancel the text for the CANCEL button
   * @param al the action listener to process the clicks
   */
  public JCancelOkPanel( String textOk, String textCancel, ActionListener al) {
    this( textOk, null, textCancel, null,al);
  }

  /**
   * <p>This constructor creates a <code>JPanel</code> with a pair of buttons, with text and icon, and registers
   * the <code>ActionListener al</code> in each button. The command fired when the buttons are pressed are
   * <code>textOk</code> and <code>textCancel</code>.</p>
   * @param textOk the text for the OK button
   * @param iconOk the icon for the OK button
   * @param textCancel the text for the CANCEL button
   * @param iconCancel the icon for the CANCEL button
   * @param al the action listener to process the clicks
   */
  public JCancelOkPanel( String textOk, ImageIcon iconOk,
                         String textCancel, ImageIcon iconCancel,
                         ActionListener al) {
    this( textOk, iconOk, textOk, textCancel, iconCancel, textCancel, al);
  }

  /**
   * <p>This constructor creates a <code>JPanel</code> with a pair of buttons, with text, icon and action command, and registers
   * the <code>ActionListener al</code> in each button</p>
   * @param textOk the text for the OK button
   * @param iconOk the icon for the OK button
   * @param commandOk the command fired when the OK button is pressed
   * @param textCancel the text for the CANCEL button
   * @param iconCancel the icon for the CANCEL button
   * @param commandCancel the command fired when the CANCEL button is pressed
   * @param al the text for the CANCEL button
   */
  public JCancelOkPanel( String textOk, ImageIcon iconOk, String commandOk,
                         String textCancel, ImageIcon iconCancel, String commandCancel,
                         ActionListener al) {
    this( textOk, iconOk, textOk, null, textCancel, iconCancel, textCancel, null, al);
  }

  /**
   *
   * @param textOk the text for the OK button
   * @param iconOk the icon for the OK button
   * @param commandOk the command fired when the OK button is pressed
   * @param tooltipOk the tooltip for the OK button
   * @param textCancel the text for the CANCEL button
   * @param iconCancel the icon for the CANCEL button
   * @param commandCancel the command fired when the CANCEL button is pressed
   * @param tootipCancel the tooltip for the CANCEL button
   * @param al the text for the CANCEL button
   */
  public JCancelOkPanel( String textOk, ImageIcon iconOk, String commandOk, String tooltipOk,
                         String textCancel, ImageIcon iconCancel, String commandCancel, String tootipCancel,
                         ActionListener al) {
    super();

    JButton bOK = new JButton( textOk, iconOk);
    bOK.setActionCommand( commandOk);
    if ( tooltipOk != null && !tooltipOk.isBlank() ) {
      bOK.setToolTipText( tooltipOk);
    }
    if ( al != null ) {
      bOK.addActionListener(al);
    }

    JButton bCancel = new JButton( textCancel, iconCancel);
    bCancel.setActionCommand( commandCancel);
    if ( tootipCancel != null && !tootipCancel.isBlank() ) {
      bCancel.setToolTipText( tootipCancel);
    }
    if ( al != null ) {
      bCancel.addActionListener( al);
    }

    setLayout( new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    c.insets = new Insets( 0,10, 0,0);

    c.gridx = 1; c.gridy = 0;
    c.anchor = GridBagConstraints.EAST;
    add( bCancel, c);

    c.gridx = 2;
    add( bOK, c);

    c.gridx = 0; c.weightx = 1;
    add( new JLabel( ""), c);

  }
}
