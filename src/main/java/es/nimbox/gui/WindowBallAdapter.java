package es.nimbox.gui;

/**
 * This method creates some empty method as convenience for creating WindowBallListener objects
 * 
 * @author nilo.gonzalez
 *
 */
public class WindowBallAdapter implements WindowBallListener {

  @Override
  public void mouseClicked( WindowBallEvent ev) {
    // Empty implementation for convenience
  }

  @Override
  public void childShown( WindowBallEvent ev) {
    // Empty implementation for convenience
  }

  @Override
  public void childHidden( WindowBallEvent ev) {
    // Empty implementation for convenience
  }

  @Override
  public void positionChanged( WindowBallEvent ev) {
    // Empty implementation for convenience
  }
}
