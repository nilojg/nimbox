package es.nimbox.gui;

import es.nimbox.box.Pair;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * <p>This class implements a <code>JComboBox</code> which shows key-value pairs. The keys are always <code>String</code>
 * for simplicity, but the items are of class <code>T</code>. Internally, key and value are stored as <code>Pair&lt;String,T&gt;</code>
 * objects.</p>
 *
 * <p>The combobox shows the key, the value or both depending on the property <code>showType</code>, which can take the
 * values <code>ONLY_KEY</code>, <code>ONLY_VALUE</code> or <code>BOTH</code>. The values are printed using the <code>toString</code>
 * method. If you need something more fancy, well, you need to write your own combo...</p>
 *
 * <p>The constructor takes two parameters:</p>
 * <ol>
 *   <li><code>SHOW_TYPE showType</code> : the type of the combo</li>
 *   <li><code>boolean withEmptyEntry</code> : if the combo adds an empty value in the first entry. The key for the
 *       first empty item is <code>EMPTY_ENTRY</code></li>
 * </ol>
 *
 * <p>It's quite easy to use:</p>
 * <pre>
 *   JKeyValueComboBox&lt;String&gt; cb1 = new JKeyValueComboBox&lt;&gt;( SHOW_TYPE.ONLY_VALUE, false);
 *   for ( int i = 0; i &lt; 100; i++) {
 *     cb1.addItem( "clave-" + i, "valor--" + i);
 *   }
 *
 *   JKeyValueComboBox&lt;Integer&gt; cb2 = new JKeyValueComboBox&lt;&gt;( SHOW_TYPE.ONLY_KEY, false);
 *   for ( int i = 0; i &lt; 100; i++) {
 *     cb2.addItem( "clave-" + i, i);
 *   }
 *
 *   JKeyValueComboBox&lt;Date&gt; cb3 = new JKeyValueComboBox&lt;&gt;( SHOW_TYPE.BOTH, true);
 *   long now = System.currentTimeMillis();
 *   for ( int i = 0; i &lt; 100; i++) {
 *     cb3.addItem( "clave-" + i, new Date( now + (i * 60 * 1000)));
 *   }
 *
 *   // And then, the typical...
 *   cb1.setMaximumRowCount( 3);
 *   cb1.addItemListener( ...);
 *   cb1.setSelectedIndex( 14);
 * </pre>
 *
 * <p>It's recommended to keep the <code>JKeyValueComboBox</code> as no-editable and handle the addition of items adding something
 * more appropriate to the <code>T</code> type and its key.</p>
 *
 *
 * @param <T> the item type
 */
public class JKeyValueComboBox<T> extends JComboBox<Pair<String,T>> {
  private static final long serialVersionUID = 1L;

  public enum SHOW_TYPE { ONLY_KEY, ONLY_VALUE, BOTH}

  public static final String EMPTY_ENTRY = "______EMPTY_VALUE_____";


  private transient List<Pair<String,T>> data;
  private SHOW_TYPE showType;

  public JKeyValueComboBox( SHOW_TYPE showType, boolean withEmptyEntry) {
    super();

    data = new ArrayList<>();
    this.showType = showType;

    if ( withEmptyEntry ) {
      addItem( EMPTY_ENTRY, null);
    }

    KeyValueRenderer renderer = new KeyValueRenderer();
    setRenderer( renderer);
  }


  /**
   * <p>This method returns an unmodifiable list which contains the items of the combo</p>
   * @return an unmodifiable list which contains the items of the combo
   */
  public Collection<Pair<String,T>> getItems() {
    return Collections.unmodifiableList( data);
  }

  /**
   * <p>This method add an item with <code>ley</code> and <code>value</code></p>
   * @param key the key of the new item
   * @param value the value of the nuew item
   */
  public void addItem( String key, T value) {
    addItem( new Pair<>(key, value));
  }

  /**
   * <p>This method replaces the current contents of the combo with the items from <code>newData</code></p>
   * @param newData the new items for the combo
   */
  public void addAllItems( Collection<Pair<String,T>> newData) {
    removeAllItems();
    for ( Pair<String,T> item : newData) {
      addItem( item);
    }
  }

  @Override
  public void addItem( Pair<String, T> item) {
    super.addItem( item);

    this.data.add( item);
  }

  @Override
  public void removeItem( Object anObject) {
    super.removeItem( anObject);
    data.remove( anObject);
  }

  @Override
  public void removeItemAt( int anIndex) {
    super.removeItemAt( anIndex);
    data.remove( anIndex);
  }

  @Override
  public void removeAllItems() {
    super.removeAllItems();

    data.clear();
  }

  public SHOW_TYPE getShowType() {
    return showType;
  }

  private class KeyValueRenderer extends JLabel implements ListCellRenderer<Pair<String,T>> {

    @Override
    public Component getListCellRendererComponent(JList<? extends Pair<String, T>> list, Pair<String, T> value, int index, boolean isSelected, boolean cellHasFocus) {
      if ( value.first.equals( EMPTY_ENTRY) ) {
        setText( " ");
      }
      else {
        switch ( showType ) {
          case ONLY_KEY:
            setText( value.first);
            break;
          case ONLY_VALUE:
            setText( value.second.toString());
            break;
          case BOTH:
            setText( value.first + ":" + value.second.toString());
            break;
        }
      }

      if ( isSelected ) {
        setBackground( list.getSelectionBackground());
        setForeground( list.getSelectionForeground());
      }
      else {
        setBackground( list.getBackground());
        setForeground( list.getForeground());
      }

      setOpaque( true);
      setFont( list.getFont());

      return this;
    }
  }
}
