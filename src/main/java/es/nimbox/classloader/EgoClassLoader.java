package es.nimbox.classloader;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Esta clase es basicamente un JARClassLoader que, en contra de la recomendacion, cuando se le pide algo primero busca
 * entre sus clases y solo si no lo encuentra pregunta al ClassLoader padre. Un egocentrico... <br>
 * 
 * No se recomienda su uso porque contradice la regla de delegacion de responsabilidad de los ClassLoader (creo que es 
 * la unica regla) pero puede simplificar la arquitectura de las aplicaciones al evitar tener que hacer clases cargadoras
 * o distribuir las clases de la aplicacion en varios jares.<br>
 * 
 * @author nilo.gonzalez
 *
 */
public class EgoClassLoader extends JARClassLoader {

  public String toString() {
    return "Nim.Esto EgoClassLoader";
  }
  
  /**
   * Default constructor.
   */
  public EgoClassLoader() {
    super();
  }
  
  @Override
  public InputStream getResourceAsStream( String name)  {
    String jar = htClases.get( name);
    if ( jar == null ) {
      // Si no esta en uno de nuestros jares, entonces devolvemos lo que de el padre, que tambien puede ser null
      return getClass().getClassLoader().getResourceAsStream( name);
    }
    
    try ( JarFile jarFich = new JarFile( jar); ) {
      JarEntry jarEntry = (JarEntry)jarFich.getEntry( name);  //Por cierto, manda huevos que pidas un JarEntry y tengas un ZipEntry...
      
      return jarFich.getInputStream( jarEntry);
    }
    catch ( IOException io)  {
      return null;
    }
  }
  
  /**
   * Este metodo devuelve una url referente a un jar, pero no es una URL que este en el classpath "oficial", asi que
   * la operatividad de este metodo depende de lo que se haga con esa url despues...
   */
  @Override
  public URL getResource( String name) {
    if ( name.startsWith( "/") ) {
      name = name.substring( 1);
    }
    
    String jar = htClases.get( name);
    if ( jar == null ) {
      // Si no esta en uno de nuestros jares, entonces devolvemos lo que de el padre, que tambien puede ser null
      return getClass().getClassLoader().getResource( name);
    }
    
    URL url = null;
    try {
      url = new URL( "jar:file:" + jar + "!" + (name.startsWith( "/")? name : "/" + name));
    }
    catch ( Exception ex) {
    }
       
    return url; 
  }

  /**
   * Este metodo devuelve una enumeracion con todos los objetos que terminan con el parametro name, tanto en 
   * este ClassLoader como en el padre
   */
  @Override
  protected Enumeration<URL> findResources( String name) throws IOException {
    final Vector<URL> v = new Vector<URL>();
    
    // Primero, los nuestros.
    for ( String key : htClases.keySet() ) {
      if ( key.endsWith( name) ) {
        String jar = (String)htClases.get( key);
        v.add( new URL( "jar:file:" + jar + "!" + (key.startsWith( "/")? key : "/" + key)));
      }
    }
    
    // Ahora, a por los de papa
    for ( Enumeration<URL> e = getClass().getClassLoader().getResources( name); e.hasMoreElements(); ) {
      v.add( e.nextElement());
    }
    
    return v.elements();
  }
  

  /**
   * Ya se que no se tiene que extender este metodo, pero es que si se extiende findClass como pone en el JavaDoc no se puede
   * llamar al metodo de la clase padre porque es protected. Se puede llamar al metodo de la clase padre de ESTE classloader, 
   * pero no al findClass del classloader padre de este.
   */
  @Override
  public Class<?> loadClass( String name) throws ClassNotFoundException {
    String cls = class2dir( name);

    String jar = htClases.get( cls);
    if ( jar == null ) {
      // Si no esta en uno de nuestros jares, pues que lo busque papi
      return getClass().getClassLoader().loadClass( name);
    }
    
    byte[] b = null;
    try {
      b = loadClassData( cls, jar);
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      throw new ClassNotFoundException( name);
    }
    
    throwEvent( new LoaderEvent( LoaderEvent.LOADED_CLASS, name));
    
    return defineClass( name, b, 0, b.length);
  }
  
  
}
