
package es.nimbox.classloader;

/**
 * This class implements an event used to notify interested parties that the ClassLoader has loaded a class, a JAR...
 * 
 * @author nilo.gonzalez
 *
 */
public class LoaderEvent {
  /**
   * Indicates that the ClassLoader starts to read a directory
   */
  public final static int BEGIN_DIR     = 0;
  
  /**
   * Indicates that the ClassLoader has finished to read a directory
   */
  public final static int END_DIR       = 1;
  
  /**
   * Indicates that the ClassLoader starts to read a jar file
   */
  public final static int BEGIN_JAR     = 2;
  
  /**
   * Indicates that the ClassLoader has finished to read a jar file
   */
  public final static int END_JAR       = 3;
  
  /**
   * Indicates that a class contained in a jar file has been read and indexed
   */
  public final static int READED_CLASS  = 4;
  
  /**
   * Indicates that a class has been loaded in memory
   */
  public final static int LOADED_CLASS  = 5; 
  
  
  /**
   * The event type
   */
  protected int id;
  
  /**
   * The name of the referring object event, a class, a jar file or a directory
   */
  protected String obj;
  
  /**
   * This variable is fulfilled ONLY if the event type is <code>BEGIN_DIR</code>, and contains the number of files
   * contained in the directory, so the interested parties can show progress bars, for example
   */
  protected int numFiles;
  
  /**
   * This constructor initializes de event
   * @param id the event id
   * @param obj the name of the referring object event, a class, a jar file or a directory
   */
  public LoaderEvent( int id, String obj) {
    this( id, obj, 0);
  }
  
  /**
   * This constructor initializes de event
   * @param id the event id
   * @param obj the name of the referring object event, a class, a jar file or a directory
   * @param numFiles this variable is fulfilled ONLY if the event type is <code>BEGIN_DIR</code>, and contains the number of files
   *                 contained in the directory, so the interested parties can show progress bars, for example
   */
  public LoaderEvent( int id, String obj, int numFiles) {
    this.id = id;
    this.obj = obj;
    this.numFiles = numFiles;
  }

  /**
   * This method returns the type of the event
   * @return the type of the event
   */
  public int getId() {
    return id;
  }

  /**
   * This method returns the name of the afected object, a jar file name, a directory name or a class name 
   * @return the object name
   */
  public String getObj() {
    return obj;
  }

  /**
   * If the event type is <code>BEGIN_DIR</code>, this method returns the number of files
   * contained in the directory
   * @return the number of files contained in the directory
   */
  public int getNumFiles() {
    return numFiles;
  }
  
  
}
