
package es.nimbox.classloader;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.*;
import java.util.jar.*;

/**
 * Esta clase implementa un ClassLoader que carga todos los jares de un directorio. Se pueden añadir tantos directorios como
 * sea necesario y por defecto carga todos los jares buscando recursivamente en subdirectorios, aunque ese comportamiento se 
 * puede evitar. Tambien se le pueden añadir Listerners similares a los que se usan en AWT y Swing para monitorizar la lectura
 * y carga de clases, lo que es especialmente util para mostrar pantallas de progreso en la carga de aplicaciones y depuracion.<br>
 * Cuando se le solicita una clase, primero intenta pedirsela al ClassLoader padre, el que ha cargado la instancia de
 * JARClassLoader, y solo cuando el padre no lo encuentra, lo busca entre sus propias clases. Esto implica que el cuerpo 
 * principal de la aplicacion debe empaquetarse en un jar a indexar con el JARClassLoader desde una pequeña clase cargadora.<br>
 * De esta manera, la aplicacion constara de un pequeño jar cargador y uno o varios directorios con ficheros jar entre los que
 * estara el jar principal de la aplicacion. La funcion main del jar cargador instanciara el JARClassLoader y la clase principal
 * de la aplicacion. La clase de la aplicacion se cargara teniendo como ClassLoader al JARClassLoader, por lo que podra instanciar
 * cualquier clase que se encuentre en los directorios de jares.<br>
 * 
 * @author nilo.gonzalez
 *
 */
public class JARClassLoader extends ClassLoader {
  public String toString() {
    return "Nim.Esto JARClassLoader";
  }
  
  /**
   * Esta Hasttable guarda el nombre de la clase y el jar donde se encuentra
   */
  protected Hashtable<String,String> htClases;
  
  /**
   * Este vector guarda la lista de ficheros JAR que se leeran.<br>
   * Es mas o menos, la lista de los ficheros JAR que estan en el classpath...
   */
  protected Vector<String> vJares;

  /**
   * Este vector guarda la lista de las clases a las que hay que notificar el progreso
   * de la carga de clases
   */
  protected Vector<ILoaderListener> vEvent;
  
  /**
   * Default constructor.
   */
  public JARClassLoader() {
    htClases = new Hashtable<String,String>();
    vJares = new Vector<String>();
    
    vEvent = new Vector<ILoaderListener>();
  }
  

  /**
   * Este metodo añade todos los jares que se encuentren en el directorio pasado por parametro, sin buscar
   * de manera recursiva en sus subdirectorios
   * @param dir el directorio a comprobar
   * @throws IOException when shit happens
   */
  public void addDir( String dir) throws IOException {
    addDir( dir, false);
  }

  /**
   * Este metodo añade todos los jares que se encuentren en el directorio pasado por parametro
   * buscando de manera recursiva en sus subdirectorios si el paremetro recursive es true
   * @param dir el directorio a explorar
   * @param recursive si es true, se busca en subdirectorios de manera recursiva
   * @throws IOException when shit happens
   */
  public void addDir( String dir, boolean recursive) throws IOException {
    try {
      File fDir = new File( dir);
      File jars[] = fDir.listFiles( new JarFilter());
     
      throwEvent( new LoaderEvent( LoaderEvent.BEGIN_DIR, dir, jars.length));
      
      for ( int i = 0; i < jars.length; i++) {
        if ( jars[i].isDirectory() ) {
          if ( recursive ) {
            addDir( jars[i].getCanonicalPath());
          }
        }
        else {
          addJar( jars[i].getCanonicalPath());
        }
      }
      
      throwEvent( new LoaderEvent( LoaderEvent.END_DIR, dir));
    }
    catch ( Exception ex) {
      throw new IOException( ex.getMessage() + "\n" + ex.getCause());
    }
  }
  
  /**
   * Este metodo quita todos los jares que se encuentren bajo el directorio pasado por parametro
   * @param dir el directorio a borrar
   */
  public void removeDir( String dir) {
    for ( String jar : vJares ) {
      if ( jar.startsWith( dir) ) {
        removeJar( jar);
      }
    }
  }
  
  /**
   * Este metodo añade un fichero jar a los controlados por el CLassLoader
   * @param nomFich el diretorio a añadir
   * @throws IOException when shit happens
   */
  public void addJar( String nomFich) throws IOException {
    vJares.add( nomFich);
    
    // Ahora hay que abrir el jar, ver que tiene, y meterlo todo en htClases.
    JarFile jarFich = new JarFile( nomFich);
    
    throwEvent( new LoaderEvent( LoaderEvent.BEGIN_JAR, nomFich));
    
    for ( Enumeration<JarEntry> e = jarFich.entries(); e.hasMoreElements(); ) {
      JarEntry je = e.nextElement();
      if ( !je.isDirectory() ) {
        throwEvent( new LoaderEvent( LoaderEvent.READED_CLASS, je.getName()));
        htClases.put( je.getName(), nomFich);
      }
    }
    
    jarFich.close();
    
    throwEvent( new LoaderEvent( LoaderEvent.END_JAR, nomFich));
  }
  
  /**
   * This method removes the <code>nomFich</code> jar file from the ClassLoader
   * @param nomFich the name of the jar to remove
   */
  public void removeJar( String nomFich) {
    // Fuera de la tabla de classpath
    for ( String jar : vJares ) {
      if ( jar.endsWith( nomFich) ) {
        vJares.remove( jar);
      }
    }
    
    // Y ahora, fuera todo su contenido
    for ( String cls : htClases.keySet() ) {
      String jar = htClases.get( cls);
      if ( jar.endsWith( nomFich) ) {  // endsWith y no equals porque podrian pasar solo el nombre del jar, sin su ruta.
        htClases.remove( cls);
      }
    }
  }
  
  /**
   * This method returns a list with all the loaded jars
   * @return a list with all the loaded jars
   */
  public List<String> getJarLoaded() {
    List<String> res = new ArrayList<String>();
    
    for ( String jj : vJares ) {
      res.add( jj);
    }
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns a list with all the loaded jars which names match the regular expression <code>regex</code> 
   * @param regex the regular expression
   * @return  a list with all the loaded jars which names match the regular expression <code>regex</code> 
   */
  public List<String> getJarLoaded( String regex) {
    List<String> res = new ArrayList<>();

    for ( String dd : getJarLoaded() ) {
      if ( dd.matches( regex) ) {
        res.add( dd);
      }
    }

    return res;
  }
  
  /**
   * This method returns a list with all the loaded classes
   * @return  a list with all the loaded jars
   */
  public List<String> getClassLoaded() {
    List<String> res = new ArrayList<>();

    for ( String dd : htClases.keySet() ) {
      res.add( dd);
    }
    
    Collections.sort( res);
    
    return res;
  }
  
  /**
   * This method returns a list with all the loaded classes which names match the regular expression <code>regex</code> 
   * @param regex the regular expression
   * @return a list with all the loaded classes which names match the regular expression <code>regex</code> 
   */
  public List<String> getClassLoaded( String regex) {
    List<String> res = new ArrayList<>();

    for ( String dd : getClassLoaded() ) {
      if ( dd.matches( regex) ) {
        res.add( dd);
      }
    }

    return res;
  }
  
  /**
   * This method Adds the specified listener to receive objects leaded events from this component. 
   * @param l the listener
   */
  public void addEventListener( ILoaderListener l) {
    vEvent.add( l);
  }
  
  /**
   * Removes a listener from the listener list 
   * @param l the listener to remove
   */
  public void removeEventListener( ILoaderListener l) {
    vEvent.remove( l);
  }
  
  
  /**
   * Este es un metodo que simplifica el uso de los ClassLoaders. Veamos un ejemplo de uso:
   * <pre>
   * JARClassLoader jcl = new JARClassLoader();
   * jcl.addDir( "/foo/bar/lib");
   * JARClassLoader.instanciateWithClassLoader( jcl, "foo.bar.TestClass");
   * </pre>
   * La clase TestClass sera cargada por el ClassLoader pasado por parametro y se invocara el constructor sin
   * parametros. A partir de ese momento, todo lo que cree la clase TestClass tendra como ClassLoader padre
   * el ClassLoader jcl, que en este caso sera un JARClassLoader que cargara las clases de lo que haya en el 
   * directorio /foo/bar. 
   * 
   * @param classLoader el ClassLoader a usar
   * @param className el nombre de la clase a cargar
   * @return la clase creada despues de ser invocada
   * @throws ClassNotFoundException when the requested class cannot be found
   * @throws InstantiationException when shit happens
   * @throws IllegalAccessException when shit happens
   * @throws InvocationTargetException when shit happens
   * @throws NoSuchMethodException if <code>className</code> doesn't have a default constructor without arguments
   */
  public static Class<?> instanciateWithClassLoader( ClassLoader classLoader, String className) throws ClassNotFoundException, 
                                                                                                       InstantiationException, 
                                                                                                       IllegalAccessException,
                                                                                                       InvocationTargetException,
                                                                                                       NoSuchMethodException {
    Class<?> cl = loadWithClassLoader( classLoader, className);
    cl.getDeclaredConstructor().newInstance();
    
    return cl;
  }
  
  /**
   * Este es un metodo que simplifica el uso de los ClassLoaders. Veamos un ejemplo de uso:
   * <pre>
   * JARClassLoader jcl = new JARClassLoader();
   * jcl.addDir( "/foo/bar/lib");
   * Class&lt;?&gt; cl = JARClassLoader.loadWithClassLoader( jcl, "foo.bar.TestClass");
   * cl.newInstance();
   * </pre>
   * La clase TestClass sera cargada, pero no creada, por el ClassLoader pasado por parametro. No se instanciara la clase, 
   * es decir, no se invocara ninguno de sus metodos o constructores, eso queda para el usuario. A partir de ese momento, 
   * todo lo que cree la clase TestClass tendra como ClassLoader padre el ClassLoader jcl, que en este caso sera un 
   * JARClassLoader que cargara las clases de lo que haya en el directorio /foo/bar. 
   * 
   * @param classLoader el ClassLoader a usar
   * @param className el nombre de la clase a cargar
   * @return la clase cargada
   * @throws ClassNotFoundException when the requested class cannot be found
   * @throws InstantiationException when shit happens
   * @throws IllegalAccessException when shit happens
   */
  public static Class<?> loadWithClassLoader( ClassLoader classLoader, String className) throws ClassNotFoundException, 
                                                                                                InstantiationException, 
                                                                                                IllegalAccessException {
    Class<?> cl = classLoader.loadClass( className);

    return cl;
  }
  
  /**
   * This method throws the event <code>ev</code> to all the registered listeners
   * @param ev the event
   */
  protected void throwEvent( LoaderEvent ev) {
    for ( ILoaderListener li : vEvent ) {
      li.actionPerformed( ev);
    }
  }
  
  
  /********************************************************************************************************************/
  /* Aqui empiezan las cosas serias ya...                                                                             */
  /********************************************************************************************************************/

  @Override
  public InputStream getResourceAsStream( String name)  {
    // Primero vamos a ver si lo tiene el ClassLoader padre
    InputStream in = getClass().getClassLoader().getResourceAsStream( name);
    
    if ( in == null ) {
      // No lo tiene papa, asi que vamos a ver si lo tenemos nosotros.
      String jar = (String)htClases.get( name);
      if ( jar == null ) {
        return null;     // No lo tenemos, no hay nada que hacer...
      }
      
      try ( JarFile jarFich = new JarFile( jar); ) {
        JarEntry jarEntry = (JarEntry)jarFich.getEntry( name);  //Por cierto, manda huevos que pidas un JarEntry y tengas un ZipEntry...
        in = jarFich.getInputStream( jarEntry);
      }
      catch ( IOException io)  {
        return null;
      }

    }
    
    
    return in;
  }
  
  /*
   * Este metodo devuelve una url referente a un jar, pero no es una URL que este en el classpath "oficial", asi que
   * la operatividad de este metodo depende de lo que se haga con esa url despues...
   */
  @Override
  public URL getResource( String name) {
    if ( name.startsWith( "/") ) {
      name = name.substring( 1);
    }
    
    String jar = null; 
     
    URL url = getClass().getClassLoader().getResource( name);
    if ( url != null ) {
      return url;
    }
    else { // No lo tiene papa, asi que vamos a ver si lo tenemos nosotros.
      jar = (String)htClases.get( name);
      if ( jar == null ) {
        return null;
      }
    }
    
    try {
      url = new URL( "jar:file:" + jar + "!" + (name.startsWith( "/")? name : "/" + name));
    }
    catch ( Exception ex) {
      return null;
    }
       
    return url; 
  }

  
  /*
   * Este metodo devuelve una enumeracion con todos los objetos que terminan con el parametro name, tanto en 
   * este ClassLoader como en el padre
   */
  @Override
  protected Enumeration<URL> findResources( String name) throws IOException {
    final Vector<URL> v = new Vector<URL>();
    
    // Primero, los de papa
    for ( Enumeration<URL> e = getClass().getClassLoader().getResources( name); e.hasMoreElements(); ) {
      v.add( e.nextElement());
    }
    
    // Ahora, a por los nuestros.
    for ( String key : htClases.keySet() ) {
      if ( key.endsWith( name) ) {
        String jar = (String)htClases.get( key);
        v.add( new URL( "jar:file:" + jar + "!" + (key.startsWith( "/")? key : "/" + key)));
      }
    }
    
    return v.elements();
  }
  

  /*
   * Ya se que no se tiene que extender este metodo, pero es que si se extiende findClass como pone en el JavaDoc no se puede
   * llamar al metodo de la clase padre porque es protected. Se puede llamar al metodo de la clase padre de ESTE classloader, 
   * pero no al findClass del classloader padre de este.
   */
  @Override
  public Class<?> loadClass( String name) throws ClassNotFoundException {
    String jar = null;
    String cls = class2dir( name);

    try {
      return getClass().getClassLoader().loadClass( name);
    }
    catch ( ClassNotFoundException ex) {
      // No lo tiene papa, asi que vamos a ver si lo tenemos nosotros.
      jar = (String)htClases.get( cls);
      if ( jar == null ) {
        throw new ClassNotFoundException();
      }
    }
    
    byte[] b = null;
    try {
      b = loadClassData( cls, jar);
    }
    catch ( Exception ex) {
      ex.printStackTrace();
      throw new ClassNotFoundException( name);
    }
    
    throwEvent( new LoaderEvent( LoaderEvent.LOADED_CLASS, name));
    
    return defineClass( name, b, 0, b.length);
  }

  
  /**
   * Esto solo lee la clase (o lo que sea) del jar y devuelve un array de bytes con lo leido.
   * @param name objeto a leer del jar
   * @param jar jar
   * @return el contenido del fichero leido
   * @throws ClassNotFoundException si no encuentra el objeto
   * @throws IOException si hay un error de lectura 
   */
  protected byte[] loadClassData( String name, String jar) throws ClassNotFoundException, IOException {
    byte[] b = null;
    
    try ( JarFile jarFich = new JarFile( jar); ) {
    
      JarEntry jarEntry = (JarEntry)jarFich.getEntry( name);  //Por cierto, manda huevos que pidas un JarEntry y tengas un ZipEntry...
      
      InputStream in = jarFich.getInputStream( jarEntry);
  
      int len = (int)jarEntry.getSize();
      int fin = 0;
      int offset = 0;
      
      b = new byte[len];
      
      while ( fin < len ) {
        len -= fin;
        offset += fin;
        fin = in.read( b, offset, len);
        if ( fin == -1 ) {
          jarFich.close();
          throw new ClassNotFoundException();
        }
      }
      
      in.close();
    }
    catch ( Exception ex  ) {
      if ( ex instanceof IOException ) {
        throw ex;
      }
      else {
        throw new IOException( "CANNOT load class '" + name + "' from '" + jar + "' : " + ex.getMessage(), ex);
      }
    }
      
    return b;
  }


  /**
   * Este metodo pasa los nombres de los recursos de como se usan en un programa a como se
   * guardan en un jar.<br>
   * Es decir, las clases se usan como paquete.paquete.nombre pero se guardan como 
   * paquete/paquete/nombre.class. Cuando alquien pida la clase paq.paq.nom este metodo le
   * devolvera paq/paq/nombre.class para que se pueda encontrar en htClases.
   * En el caso de que se pida un recurso, como un gif o algo por el estilo, no se toca.
   * @param cls the resource
   * @return the normalized name
   */
  protected String class2dir( String cls) {
    return cls.replace( '.', '/') + ".class";
  }
  
  /**
   * Pos eso. Muestra el contenido de las tablas para hacer debug.
   */
  protected void debug() {
    for ( String jar : vJares ) {
      System.out.println( "vCP>" + jar);
    }
    for ( String cls : htClases.keySet() ) {
      String jar = htClases.get( cls);
      System.out.println( "htClases>" + cls + "     -  " + jar);
    }
  }
  
  /*************************************************************************/
  /*
   * Para sacar solo los .jar del directorio
   */
  private class JarFilter implements FileFilter {
    public boolean accept( File name) {
      return    name.getName().toUpperCase().endsWith( ".JAR")  
             || name.getName().toUpperCase().endsWith( ".ZIP") 
             || name.isDirectory();
    }
  }
}