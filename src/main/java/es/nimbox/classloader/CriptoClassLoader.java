package es.nimbox.classloader;

import java.io.*;
import java.util.*;
import java.security.*;

import javax.crypto.*;

/**
 * Esta clase implementa un ClassLoader que carga clases que han sido previamente criptografiadas de manera que se puedan 
 * distribuir jares sin temor a que sean decompilados.<br>
 * Bueno, en realidad la proteccion solo frenara a los piratas que no sean demasiado serios, porque un .class para ejecutarse
 * debe desencriptarse, y una vez que este en memoria se puede copiar, pero al menos dificultara la copia.<br>
 * El CriptoClassLoader usa la infraestructura de seguridad que proporciona el JDK, JCA, asi que en mayor o menor medida
 * debera conocerla para sacar ventaja de esta clase.<br>
 * 
 * Antes de usar el CriptoClassLoader hemos de tener un jar encriptado y proporcionar al CriptoClassLoader un objeto Cipher capaz
 * de desencriptar el jar. Es decir, si el jar esta encriptado con AES, hemos de proporcionar al CriptClassLoader un objeto Cipher
 * que pueda desencriptar esas clases. Es importante resaltar que no es lo mismo tener un jar firmado (signed, con jarsigner)
 * que tener uno criptografiado. Un jar firmado es un jar en el que cada una de sus clases lleva un "chivato" para avisar cuando
 * uno de los malos la ha modificado, pero la clase se puede leer y decompilar. Un jar criptografiado es un jar en el que sus 
 * contenidos estan criptografiados de manera que los chicos malos no los pueden leer. Asi que no se puede usar jarsinger para
 * hacer este trabajo. La clase nim.esto.apps.CriptoJar le permitira encriptar jares usando el algoritmo AES, pero si quiere usar
 * otro algoritmo de encriptacion es facil modificarla. Basicamente toma cada uno de los ficheros de un fichero jar y los encripta.
 * Por ejemplo, para encriptar el jar nimrodlf.jar habria que escribir el siguente comando:
 * <pre>
 * java nim.esto.apps.CriptoJar -jar nimrodlf.jar -output /tmp/nimcrip -genkey
 * </pre>
 * Esto descomprimiria el fichero nimrodlf.jar en el directorio /tmp/nimcrip, encriptaria cada uno de sus ficheros con AES128 y 
 * escribiria un fichero con la clave usada, que conviene guardar porque sera necesaria para desencriptar las clases. Tras esto
 * hay que hacer limpieza en el contenido del directorio y reponer lo que no queramos que este encriptado (el directorio META-INF, 
 * basicamente) y crear el nuevo jar:
 * <pre>
 * cd /tmp/nimcrip
 * jar cvf nimrodlf.crip.jar .
 * </pre>  
 * 
 * Una vez que tenemos un fichero jar criptografiado, hemos de ponerlo en un directorio donde la aplicacion pueda leerlo, y es 
 * conveniente que sea un directorio diferente al del resto de los jares. Tipicamente una aplicacion necesitara varios (¿cientos?)
 * ficheros jar "normales" (drivers de jdbc, parsers de XML... los sospechosos habituales) y unos pocos jares criptografiados. Estos 
 * deben estar en uno o varios directorios separados, y todos los jares de un mismo directorio deben compratir el mismo algoritmo
 * de encriptacion:
 * <pre>
 * cp nimrodlf.crip.jar ..../lib/encrypted
 * </pre>
 * 
 * Ahora necesitaremos un objeto Cipher y cargar la clave con la que se encriptaron los ficheros:
 * <pre>
    // Cargar el fichero de la clave
    FileInputStream fis = new FileInputStream( "key.RAW");
    byte[] buf = new byte[ fis.available()];
    fis.read( buf);
    fis.close();

    // Convertirla en una clave de java
    SecretKeySpec spec = new SecretKeySpec( buf, "AES");
    Key key = spec;

    // Y crear el Cipher
    Cipher cip = Cipher.getInstance( "AES/ECB/PKCS5Padding");
   </pre>
 * Es importante que el Cipher sea igual al que se uso para encriptar el jar, que en el caso de usar la utilidad CriptoJar, usara
 * el algoritmo <b>AES/ECB/PKCS5Padding</b>. Una vez creado todo, basta con crear el objeto CriptoClassLoader y cargar los directorios:
 * <pre>
    CriptoClassLoader ccl = new CriptoClassLoader();
    
    // Los jares de este directorio estan encriptados con AES/ECB/PKCS5Padding con la clave del fichero key.RAW
    ccl.addDir( "lib/encrypted", cip, key);
    
    // Los jares de este directorio no estan encriptados, son normales
    ccl.addDir( "lib");
    
    // Tambien podemos añadir un unico jar
    ccl.addJar( "lib/encrypted/nimrodlf.crip.jar", cip, key);
    
    // E instanciamos una clase cualquiera...
    JARClassLoader.instanciateWithClassLoader( ccl, "com.nilo.plaf.nimrod.NimRODScrollBarUI");
    
    // U obtener los ficheros que hay en el
    InputStream is = ccl.getResourceAsStream( "index.txt");
    int r = 0;
    while ( ( r = is.read( buf) ) != -1 ) {
      System.out.println( new String( buf, 0, r));
    }
 * </pre>
 * Es importante recalcar que los CriptoClassLoader pueden cargar tanto jares normales como jares encriptados. CriptoClassLoader recordara
 * que jares estaban dentro de directorios añadidos como encriptados y que jares estaban en directorios normales.<br>
 * En este ejemplo se ha usado la aplicacion CriptoJar y se ha dejado la clave en un fichero, lo cual no parece muy inteligente porque
 * habra que pasar la clave junto a la aplicacion y sera bastante sencillo desencriptar el jar. En el mundo real el array de bytes <b>buf</b>
 * deberia cargarse de una manera mas exotica, como cargandola desde una mochila (si no sabe lo que es esto es que es joven ¡enhorabuena!) o
 * consultandolo en algun servicio web de gestion de licencias... Esto queda a su eleccion, pero en cualquier caso deberia ser algo
 * complicado.<br>
 * Despues ya puede usar el CriptoClassLoader como cualquier otro ClassLoader. Las clases se cargaran desencriptadas y los recursos que
 * obtenga llamando a getResourceAsStream le llegaran desencriptados, pero tenga en cuenta que si obtiene usted el contenido del jar
 * de otra manera, tendra que ocuparse usted de desencriptarlo por su cuenta. 
 * 
 * 
 * @see javax.crypto.Cipher
 * 
 * @author nilo.gonzalez
 *
 */
public class CriptoClassLoader extends JARClassLoader {

  /**
   * Esta clase guarda la asociacion entre un jar y el Cipher necesario para desencriptarlo 
   * 
   * @author nilo.gonzalez
   *
   */
  protected class CCrip {
    /**
     * El objeto que desencripta las clases
     */
    public Cipher cipher;
    
    /**
     * La clave para desenciptar
     */
    public Key key;
    
    /**
     * Constructor
     * @param cipher algoritmo de cifrado
     * @param key clave
     */
    public CCrip( Cipher cipher, Key key) {
      this.cipher = cipher;
      this.key = key;
    }
  }
  
  
  /**
   * En esta tabla se guardan las clases que se guardan encriptadas
   */
  protected Hashtable<String,CCrip> htEncriptadas;
  
  
  /**
   * Constructor por defecto
   */
  public CriptoClassLoader() {
    super();
    
    htEncriptadas = new Hashtable<String, CCrip>();
  }
  
  /**
   * Este metodo añade un directorio cuyos jares estaran encriptados, proporcionando tambien el Cipher y la clave
   * necesarios para desencriptarlos. Se añadiran todos los ficheros jar de este directorio y de sus subdirectorios
   * @param dir el directorio a analizar
   * @param cipher el objeto Cipher para desencriptar
   * @param key la clave
   * @throws IOException ante cualquier problema encontrado. Todas las posibles excepciones se encapsulan dentro de
   * una IOException, asi que es necesario consultar la causa
   */
  public void addDir( String dir, Cipher cipher, Key key) throws IOException {
    addDir( dir, cipher, key, true);
  }
  
  /**
   * Este metodo añade un directorio cuyos jares estaran encriptados, proporcionando tambien el Cipher y la clave
   * necesarios para desencriptarlos. Se añadiran todos los ficheros jar de este directorio y de sus subdirectorios
   * si el parametro recursive es true
   * @param dir el directorio a analizar
   * @param cipher el objeto Cipher para desencriptar
   * @param key la clave
   * @param recursive si es true se analizan los directorios de forma recursiva. Si es false solo se analiza el 
   * directorio dir
   * @throws IOException ante cualquier problema encontrado. Todas las posibles excepciones se encapsulan dentro de
   * una IOException, asi que es necesario consultar la causa
   */
  public void addDir( String dir, final Cipher cipher, final Key key, boolean recursive) throws IOException {
    // Con esto se carga el directorio normalmente y al mismo tiempo apuntamos todas las clases que se cargan dentro 
    // del directorio encriptado, de las que solo necesitamos saber el nombre para consultarlo despues.
    // Podria haber copiado el codigo del metodo addDir de JARClassLoader, pero esto es un poco mas limpio...
    ILoaderListener listener = new ILoaderListener () {
      @Override
      public void actionPerformed( LoaderEvent ev) {
        if ( ev.getId() == LoaderEvent.READED_CLASS ) {
          htEncriptadas.put( ev.getObj(), new CCrip( cipher, key));
        }
      }
    };
    
    this.addEventListener( listener);
    
    addDir( dir, recursive);
    
    // Hay que quitar el listener al terminar para no dejarlo puesto y meter todo
    this.removeEventListener( listener);
  }
  
  /**
   * Este metodo añade un fichero jar a los controlados por el CLassLoader
   * @param nomFich el diretorio a añadir
   * @param cipher el objeto Cipher para desencriptar
   * @param key la clave
   * @throws IOException when shit happens
   */
  public void addJar( String nomFich, final Cipher cipher, final Key key) throws IOException {
    // Con esto se carga el directorio normalmente y al mismo tiempo apuntamos todas las clases que se cargan dentro 
    // del directorio encriptado, de las que solo necesitamos saber el nombre para consultarlo despues.
    // Podria haber copiado el codigo del metodo addDir de JARClassLoader, pero esto es un poco mas limpio...
    ILoaderListener listener = new ILoaderListener () {
      @Override
      public void actionPerformed( LoaderEvent ev) {
        if ( ev.getId() == LoaderEvent.READED_CLASS ) {
          htEncriptadas.put( ev.getObj(), new CCrip( cipher, key));
        }
      }
    };
    
    this.addEventListener( listener);
    
    addJar( nomFich);
    
    // Hay que quitar el listener al terminar para no dejarlo puesto y meter todo
    this.removeEventListener( listener);
  }
  
  
  
  @SuppressWarnings("resource")  // Esta chapuza es necesaria porque el compilador da un warning en res. Podria evitarse
                                 // cambiando el codigo y usando return donde aparece res, pero el codigo seria mas feo
  public InputStream getResourceAsStream( String name)  {
    InputStream isPadre = super.getResourceAsStream( name);
    InputStream res = null;
    
    if ( isPadre != null ) {
      CCrip crip = htEncriptadas.get( name);
      if ( crip != null ) {
        // Esta encriptado, asi que devolvemos un CipherInputStream
        res = new CipherInputStream( isPadre, crip.cipher);
      }
      else {
        // Esta limpio, asi que devolvemos lo que dio el padre
        res = isPadre;
      }
    }
    
    return res;
  }
  
  
  /*
   * Esto solo lee la clase (o lo que sea) del jar y devuelve un array de bytes con lo leido.
   */ 
  @Override
  protected byte[] loadClassData( String name, String jar) throws ClassNotFoundException, IOException {
    byte[] buf = super.loadClassData( name, jar);
    
    CCrip crip = htEncriptadas.get( name);
    if ( crip != null ) {
      try {
        crip.cipher.init( Cipher.DECRYPT_MODE, crip.key);
    
        buf = crip.cipher.doFinal( buf);
      }
      catch ( Exception ex) {
        throw new IOException( ex.getMessage() + "\n" + ex.getCause(), ex);
      }
    }
    
    return buf;
  }
}

