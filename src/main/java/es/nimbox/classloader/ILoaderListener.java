package es.nimbox.classloader;

/**
 * Este interface define los metodos que deben tener todas las clases que quieran
 * ser notificadas del progreso del JARClassLoader
 * 
 * 
 * @author nilo.gonzalez
 *
 */
public interface ILoaderListener {
  /**
   * Este metodo es invocado cuando se produce un evento registrado, generalmente porque se ha 
   * procesado una entrada de un fichero jar
   * @param ev evento
   */
  public void actionPerformed( LoaderEvent ev);
}
