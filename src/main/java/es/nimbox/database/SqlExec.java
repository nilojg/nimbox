package es.nimbox.database;

import es.nimbox.box.ElasticBox;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>This class executes SQL sentences, queries and updates, both, and it can be used in a functional or classic way. The query
 * results are returned in a <code>ElasticBox</code>.</p>
 *
 * <p>To use the object, call the constructor, pass a <code>connection</code> and the SQL sentence to execute. Then, it depends in the
 * kind of SQL sentence that you want to execute.</p>
 *
 * <p>To execute a <i>write</i> SQL sentence, <code>insert</code>, <code>update</code> or <code>delete</code>, you must use the
 * method <code>executeUpdate</code>:</p>
 *
 * <pre>
 *    SqlExec se = new SqlExec( con);
 *    se.command( "insert into MY_TABLE VALUES (...)");
 *
 *    int updated = se.executeUpdate();
 *
 *    System.out.println( "Updated " + updated + " records in the table");
 * </pre>
 *
 * <p>The <code>executeUpdate</code> returns the number of updated records. You can use the class in a functional way too, although
 * you will not get the number of updated records:</p>
 *
 * <pre>
 *    SqlExec.build()
 *           .conn( con)
 *           .executeUpdate( "insert into MY_TABLE VALUES (...)");
 * </pre>
 *
 * <p>To execute a query you have to call the <code>executeQuery</code>, which returns a list of <code>ElasticBox</code> objects. How the
 * query can return a huge amount of records, you can use the <code>setMaxResults</code> method to limit the amount of returned records.</p>
 *
 * <pre>
 *    SqlExec se = SqlExec.build()
 *                        .conn( con)
 *                        .maxResults( max)
 *                        .command( "select * from MY_TABLE where ...");
 *
 *    int read = se.executeQuery();
 *
 *    List&lt;ElasticBox&gt; rs = se.getResults();
 * </pre>
 *
 * <p>Note that the <code>executeQuery</code> method returns the number of read records, and you need to call the <code>getResults</code> method
 * to return the <code>List</code> of results. To execute queries in a functional way use this snippet:</p>
 *
 * <pre>
 *    List&lt;ElasticBox&gt; rs = SqlExec.build()
 *                                 .conn( con)
 *                                 .maxResults( max)
 *                                 .executeQuery( "select * from MY_TABLE where ...");
 *                                 .getResults();
 * </pre>
 *
 * <p>Whatever method you use to execute the query, note that the SQL sentence contains an asterisk instead of a list of fields. The
 * <code>executeQuery</code> method will get the metadata from the result set and will populate the <code>ElasticBox</code> with
 * entries with the appropriate field name.</p>
 *
 * <p>The <code>SQLExec</code> objects can be reused. For example:</p>
 *
 * <pre>
 *    SqlExec se = new SqlExec( con);
 *
 *    for ( int i = 0; i &lt; 5; i++) {
 *      se.command( "insert into MY_TABLE VALUES (...)");
 *    }
 *
 *    se.command( "select count(*) from MY_TABLE")
 *      .executeQuery();
 * </pre>
 *
 * <p>In this example the <code>se</code> object is used 5 times to execute 5 inserts, and then is used to execute a query.</p>
 *
 * @see SqlConn
 * @see ElasticBox
 */
public class SqlExec {

  private Connection conn;

  private String command;

  private int maxResults;

  private List<ElasticBox> results;


  /**
   * <p>This constructor creates an empty <code>SQLExec</code></p>
   */
  public SqlExec() {
    conn = null;
    command = "";
    maxResults = Integer.MAX_VALUE;
    results = new ArrayList<>();
  }

  /**
   * <p>This constructor creates an <code>SQLExec</code> that will use the connection <code>con</code></p>
   * @param con the SQL connection
   */
  public SqlExec( Connection con) {
    this();

    this.conn = con;
  }

  /**
   * <p>This constructor creates a <code>SQLExec</code> with the SQL sentence <code>command</code>.</p>
   * <p>Remember to add a SQL connection later.</p>
   * @param command the SQL sentence
   */
  public SqlExec( String command) {
    this();

    this.command = command;
  }

  /**
   * <p>This constructor creates a <code>SQLExec</code> with the SQL sentence <code>command</code> and the maximum number of records
   * to read, which only has sense if the SQL sentence is a query.</p>
   * <p>Remember to add a SQL connection later.</p>
   * @param command the SQL sentence
   * @param maxResults the maximum number of records to read
   */
  public SqlExec( String command, int maxResults) {
    this();

    this.command = command;
    this.maxResults = maxResults;
  }

  /**
   * <p>This constructor creates a <code>SQLExec</code> with the SQL sentence <code>command</code> and the maximum number of records
   * to read, which only has sense if the SQL sentence is a query.</p>
   * @param conn the SQL connection to use
   * @param command the SQL sentence
   * @param maxResults the maximum number of records to read
   */
  public SqlExec( Connection conn, String command, int maxResults) {
    this();

    this.command = command;
    this.conn = conn;
    this.maxResults = maxResults;
  }

  public static SqlExec build() {
    return new SqlExec();
  }


  /**
   * <p>This method executes the <b>update</b> operation (<code>insert</code>, <code>update</code> or <code>delete</code>) that was
   * previously loaded using the <code>setCommand</code> method.</p>
   * @return the number of updated records
   * @throws SQLException if something goes wrong
   * @see #setCommand(String)
   */
  public int executeUpdate() throws SQLException {
    int res = 0;

    try ( Statement st = conn.createStatement(); ) {
      res = st.executeUpdate(command);
    }
    catch ( Exception ex) {
      throw new SQLException( "Cannot execute the SQL sentence : " + ex.getMessage(), ex);
    }

    return res;
  }

  /**
   * <p>This method executes the query that was previously loaded using the <code>setCommand</code> method. The method will return
   * the number of read records, which can be limited with the <code>setMaxResults</code> method. To get the results, you will
   * have to invoke the <code>getResults</code> method.</p>
   * @return the number of read records
   * @throws SQLException if something goes wrong
   * @see #setCommand(String)
   * @see #setMaxResults(int)
   * @see #getResults()
   */
  public int executeQuery() throws SQLException {
    try ( Statement st = conn.createStatement();
          ResultSet rs = st.executeQuery(command); ) {
      // First, get the names of the fields
      ResultSetMetaData metaData = rs.getMetaData();

      List<String> lFieldNames = new ArrayList<>();

      for ( int i = 0; i < metaData.getColumnCount(); i++) {
        lFieldNames.add( metaData.getColumnName( i + 1));
      }

      int count = 0;
      results.clear();

      // Start reading the results until the result set is over or maxResult
      while ( count++ < maxResults && rs.next() ) {
        ElasticBox box = new ElasticBox();

        for ( int i = 0; i < lFieldNames.size(); i++) {
          String fName = lFieldNames.get( i);

          fName = ( fName == null || fName.trim().isEmpty() ? "Field::" + i : fName);

          box.set( fName, rs.getObject( i+1));
        }

        results.add( box);
      }

      // Return the number of read records
      return results.size();
    }
    catch ( Exception ex) {
      throw new SQLException( "Cannot execute the SQL sentence : " + ex.getMessage(), ex);
    }
  }



  public SqlExec conn( Connection conn) {
    setConn( conn);
    return this;
  }

  public SqlExec command(String command) {
    setCommand(command);
    return this;
  }

  public SqlExec maxResults( int maxResults) {
    setMaxResults( maxResults);
    return this;
  }

  /**
   * <p>This method executes the <code>query</code>. The number of records to read can be limited with the <code>setMaxResults</code> method.</p>
   * <p>The method is intended to be used in a functional way, although can be mixed with the classic way too.</p>
   * @param query the query to execute
   * @return a reference to <code>this</code> object to chain calls in a functional way
   * @throws SQLException if something goes wrong
   * @see #setMaxResults(int)
   * @see #getResults()
   */
  public SqlExec executeQuery( String query) throws SQLException {
    setCommand( query);

    executeQuery();
    return this;
  }

  /**
   * <p>This method executes the <code>sentence</code>.</p>
   * <p>The method is intended to be used in a functional way, although can be mixed with the classic way too.</p>
   * @param sentence the SQL sentence to execute
   * @return a reference to <code>this</code> object to chain calls in a functional way
   * @throws SQLException if something goes wrong
   */
  public SqlExec executeUpdate( String sentence) throws SQLException {
    setCommand( sentence);

    executeUpdate();

    return this;
  }


  public Connection getConn() {
    return conn;
  }

  public void setConn(Connection conn) {
    this.conn = conn;
  }

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public int getMaxResults() {
    return maxResults;
  }

  public void setMaxResults(int maxResults) {
    this.maxResults = maxResults;
  }

  /**
   * <p>This method returns a <code>List</code> of <code>ElasticBox</code> with the results of the las executed query.</p>
   * @return a <code>List</code> of <code>ElasticBox</code> with the results of the las executed query.
   */
  public List<ElasticBox> getResults() {
    return results;
  }

}
