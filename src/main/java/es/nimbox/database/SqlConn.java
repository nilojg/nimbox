package es.nimbox.database;

import javax.sql.DataSource;
import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * <p>This class encapsulates a SQL connection. Has a limited support for the <i>builder</i> pattern and automatically loads theJDBC driver, so
 * it's quite easy to use.</p>
 *
 * <p>To create a <code>SQLConn</code> object simply do this:</p>
 * <pre>
 *   private String driver = "org.apache.derby.jdbc.EmbeddedDriver";
 *   private String jdbcURL = "jdbc:derby:memory:testDB;create=true";
 *
 *   SqlConn nc = new SqlConn();
 *   nc.setDriverClass( driver);
 *   nc.setUrl( jdbcURL);
 *
 *   Connection con = nc.getConnection();
 *
 *   ....
 *
 *   nc.close();
 * </pre>
 *
 * <p>You can use the builder pattern too:</p>
 *
 * <pre>
 *   Connection con = SqlConn.build()
 *                           .driverClass(driver)
 *                           .url(jdbcURL)
 *                           .getConnection();
 * </pre>
 *
 * <p>The last example doesn't close the <code>SQLConn</code>, which is not very polite, but the class implements the <code>Closeable</code>
 * interface, so it can be used in a <i>try-with-resources</i>, so the <code>SQLConn</code> object is closed automatically:</p>
 *
 * <pre>
 *   try ( SqlConn nc = new SqlConn()) {
 *     nc.setDriverClass( driver);
 *     nc.setUrl( jdbcURL);
 *
 *     Connection con = nc.getConnection();
 *
 *     ...
 *   }
 *   catch ( Exception ex) {
 *     ...
 *   }
 *
 *   try ( Connection con = SqlConn.build()
 *                                 .driverClass( driver)
 *                                 .url( jdbcURL)
 *                                 .getConnection(); ) {
 *     ...
 *   }
 *   catch ( Exception ex) {
 *     ...
 *   }
 * </pre>
 *
 * <p>In these examples <code>SQLConn</code> gets the connection <b>directly from the driver</b>, which perhaps is not
 * very correct because it doesn't use a <code>DataSource</code>. If you want to use a datasource, set the datasource
 * that <code>SQLConn</code> ust use instead of setting the driver:</p>
 * <pre>
 *   SqlConn nc = new SqlConn();
 *   nc.setDataset( ds);
 *
 *   Connection con = nc.getConnection();
 *
 *   ....
 *
 *   nc.close();
 *
 *   Connection con = SqlConn.build()
 *                           .datasource( ds)
 *                           .getConnection();
 * </pre>
 *
 * <p>Although if you have a <code>DataSource</code>, you can handle your own connections, of course. The big point of this class
 * is that it just loads the driver for you, and it contains the <code>setDatasource</code> method just for compatibility
 * with the datasources, so if you use <code>SQLConn</code> in your code you can use datasources too without changing the code</p>
 *
 * <p>Can be used with typical JDBC code or with <code>SqlExec</code>.</p>
 *
 * @see SqlExec
 */
public class SqlConn implements Closeable  {

  private String driverClass;
  private String url;
  private String user;
  private String password;

  private DataSource dataSource;

  private Connection conn;

  public SqlConn() {
    super();

    conn = null;

    driverClass = null;
    url = null;
    user = null;
    password = null;

    dataSource = null;
  }


  public static SqlConn build() {
    return new SqlConn();
  }


  /**
   * <p>This method returns a new SQL connection. If the <code>SQLConn</code> has been initialized with a <code>DataSource</code>
   * the <code>SQLConn</code> will ask the connection to the <code>DataSource</code>, and if it was initialized with a driver class name,
   * JDBC URL, user and password, the <code>SQLConn</code> will load the driver and will get the connection directly from the driver.</p>
   * @return a new SQL connection
   * @throws SQLException if something goes wrong
   */
  public Connection getConnection() throws SQLException {
    if ( conn == null ) {
      if ( dataSource != null ) {
        conn = dataSource.getConnection();
      }
      else {
        loadDriver();
        conn = DriverManager.getConnection(url, user, password);
      }
    }

    return conn;
  }

  @Override
  public void close() throws IOException {
    try {
      if ( conn != null && !conn.isClosed() ) {
        conn.close();
      }
    }
    catch ( SQLException e ) {
      throw new IOException( "Cannot close the SQL connection to '" + url + "' : " + e.getMessage(), e);
    }
  }

  /**
   * <p>This method returns the used driver class</p>
   * @return the used driver class
   */
  public String getDriverClass() {
    return driverClass;
  }

  /**
   * This method returns the JDBC URL
   * @return the JDBC URL
   */
  public String getUrl() {
    return url;
  }

  /**
   * This method returns the user of the connection 
   * @return the user of the connection 
   */
  public String getUser() {
    return user;
  }

  /**
   * This method returns the password
   * @return the password
   */
  public String getPassword() {
    return password;
  }

  /**
   * This method returns the <code>DataSource</code> used to connect
   * @return the <code>DataSource</code> used to connect
   */
  public DataSource getDataSource() {
    return dataSource;
  }


  /**
   * This method sets the driver class name
   * @param driverClass the driver class name
   */
  public void setDriverClass( String driverClass) {
    this.driverClass = driverClass;
  }

  /**
   * This method sets the JDBC URL
   * @param url the JDBC URL
   */
  public void setUrl( String url) {
    this.url = url;
  }

  /**
   * This method sets the user
   * @param user the user
   */
  public void setUser( String user) {
    this.user = user;
  }

  /**
   * This method sets the password
   * @param password the password
   */
  public void setPassword( String password) {
    this.password = password;
  }

  /**
   * This method sets the <code>DataSource</code> used to connect to the database. If the
   * user of <code>SqlConn</code> sets the <code>DataSource</code>, the <code>SqlConn</code>
   * takes the connection from the <code>DataSource</code> instead of creating its own
   * connections
   * @param dataSource the <code>DataSource</code> used to connect to the database
   */
  public void setDataSource( DataSource dataSource) {
    this.dataSource = dataSource;
  }

  /**
   * Builder pattern method to set the driver class name
   * @param driverClass the driver class name
   * @return this instance
   */
  public SqlConn driverClass(String driverClass) {
    setDriverClass(driverClass);
    return this;
  }

  /**
   * Builder pattern method to set the url
   * @param url the url
   * @return this instance
   */
  public SqlConn url(String url) {
    setUrl( url);
    return this;
  }

  /**
   * Builder pattern method to set the user
   * @param user the user
   * @return this instance
   */
  public SqlConn user(String user) {
    setUser( user);
    return this;
  }

  /**
   * Builder pattern method to set the password
   * @param password the password
   * @return this instance
   */
  public SqlConn password(String password) {
    setPassword( password);
    return this;
  }

  /**
   * Builder pattern method to set the dataSource
   * @param dataSource the dataSource
   * @return this instance
   */
  public SqlConn dataSource(DataSource dataSource) {
    setDataSource( dataSource);
    return this;
  }



  private void loadDriver() throws SQLException {
    if ( driverClass == null ) {
      throw new SQLException( "Cannot load the driver... because is null. Please, set the driver attribute");
    }

    try {
        Class.forName( driverClass);
    }
    catch ( ClassNotFoundException e ) {
      throw new SQLException( "Cannot load the driver '" + driverClass + "' : " + e.getMessage(), e);
    }
  }
}
