package es.nimbox.io;

import java.text.Format;
import java.util.ArrayList;
import java.util.List;

import es.nimbox.box.ElasticBox;

/**
 * This class provides some utilities for <code>BoxWriter</code>s, so it's a good base to start if you are going
 * to write a <code>BoxWriter</code>.
 *
 * @author nilo
 *
 */
public abstract class AbstractBoxWriter implements BoxWriter {
  
  /**
   * This list contains the list of fields
   */
  protected List<Field> lFields;
  
  /**
   * This property stores the number of read records
   */
  protected int recordsCounter;
  
  /**
   * This class contains the data that unites the identifiers from <code>ElasticBox</code> with the 
   * names in the destiny. And there's a possibility for adding a formatter too.  
   */
  protected class Field {
    String name;
    String title;
    Format format;
    
    /**
     * Default constructor
     * @param name field name
     * @param title field title
     * @param format the format
     */
    public Field( String name, String title, Format format) {
      super();
      this.name = name;
      this.title = title;
      this.format = format;
    }
  }
  
  
  /**
   * Default constructor
   */
  protected AbstractBoxWriter() {
    super();
    
    lFields = new ArrayList<>();
    recordsCounter = 0;
  }
  
  @Override
  public void close() throws Exception {
    lFields.clear();
    recordsCounter = 0;
  }
  
  @Override
  public int write( List<ElasticBox> items) throws Exception {
    int written = 0;
    
    for ( ElasticBox item : items ) {
      write( item);
      written++;
    }
    
    return written;
  }
  
  @Override
  public List<String> getFieldNames() {
    List<String> res = new ArrayList<>();
    int i = 0;
    
    for ( Field field : lFields ) {
      res.add( field.name != null && !field.name.trim().equals( "")? 
                    field.name : 
                    DEFAULT_FIELD_NAME + i);
      i++;
    }

    return res;
  }
  
  @Override
  public int getWrittenRecords() {
    return recordsCounter;
  }  
  
  @Override
  public void addField( String name) {
    addField( name, name);
  }
  
  @Override
  public void addField( String name, String title) {
    addField( name, title, null);
  }
  
  @Override
  public void addField( String name, String title, Format format) {
    lFields.add( new Field( name, title, format));
  }
  
  /**
   * This method adds <code>recordNumber</code> to the current number of written records. Usually, 1.
   * @param recordNumber the number to add to the current number of written records.
   */
  protected void addCountRecords( int recordNumber) {
    this.recordsCounter += recordNumber;
  }
  
  /**
   * This method sets the current number of written records to <code>recordNumber</code>
   * @param recordNumber the new value for the number of written records
   */
  protected void setCountRecords( int recordNumber) {
    this.recordsCounter = recordNumber;
  }
}
