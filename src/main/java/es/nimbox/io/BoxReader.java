package es.nimbox.io;

import java.util.List;

import es.nimbox.box.ElasticBox;

/**
 * This interface defines classes that read <code>ElasticBox</code> from wherever: files, databases... <br>
 * The idea is that the keys of the <code>ElasticBox</code> are determined for the origin of the data, for
 * example, the column names if the origin is a database table or a CSV, but some origin can not have
 * a name, for example <code>select count(*) from table</code> returns a single value without name because
 * the <code>count(*)</code> isn't identified. In cases like this the keys of the <code>ElasticBox</code>
 * will be determined by the <code>DEFAULT_FIELD_NAME</code> constant with the index of the column beginning
 * in 0. So <code>select count(*) from table</code> will result in an <code>ElasticBox</code> with a single
 * value identified by <code>DEFAULT_FIELD_NAME</code>0. This is <code>DUMMY_0</code> in practice.
 *
 * @author nilo
 *
 */
public interface BoxReader extends AutoCloseable {
  /**
   * The name for keys when the origin doesn't provide a name
   */
  public static final String DEFAULT_FIELD_NAME = "DUMMY_";

  /**
   * This method returns the number of read records.
   * @return the number of read records.
   */
  public int getReadRecords();
  
  /**
   * This method returns a <code>List</code> with the identifiers of the read <code>ElasticBox</code> 
   * @return a <code>List</code> with the identifiers of the read <code>ElasticBox</code> 
   */
  public List<String> getFieldNames();

  /**
   * This method reads a record from the origin and returns an <code>ElasticBox</code> with the data or <code>null</code>
   * if there isn't more data to read
   * @return  an <code>ElasticBox</code> with the data or <code>null</code>
   * if there isn't more data to read
   * @throws Exception when something goes wrong
   */
  public ElasticBox read() throws Exception;
  
  /**
   * This method reads as much <code>number</code> records from the origin and returns a <code>List&lt;ElasticBox&gt;</code>
   * with them. If there isn't values to read, the returned list is empty, but not <code>null</code> 
   * @param number the maximum number of records to read
   * @return a <code>List&lt;ElasticBox&gt;</code> with the read data
   * @throws Exception when something goes wrong
   */
  public List<ElasticBox> read( int number) throws Exception;
  
}
