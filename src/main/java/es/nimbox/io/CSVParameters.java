package es.nimbox.io;

import java.util.ArrayList;
import java.util.List;

import es.nimbox.box.Utils;

/**
 * This class allows the user to personalize the way a CSV is written, choosing the values of quotes, separators
 * or end lines. By default the chosen values are the usual values which are understood by almost all the programs
 * used to handle CSVs (Excel?), so:
 * <ul>
 *  <li>Fields separator: <b>;</b></li>
 *  <li>Field quote: <b>"</b></li>
 *  <li>Record separator: <b>\n</b> (end line)</li>
 * </ul>
 * These are quite safe configurations.
 *
 * @author nilo
 *
 */
public class CSVParameters {
  
  /**
   * Default character separator for fields values
   */
  public static final String DEFAULT_SEPARATOR = ";";
  
  /**
   * Default character separator for end line
   */
  public static final String DEFAULT_END = "\n";
  
  /**
   * Default character for quotes
   */
  public static final String DEFAULT_QUOTE = "\"";
  
  /**
   * Default date format
   */
  public static final String DEFAULT_DATE_FORMAT = "yyyy/MM/dd HH:mm:ss";
  
  /**
   * Default charset
   */
  public static final String DEFAULT_CHARSET = "UTF-8";

  private String separator;
  private String end;
  private String fieldQuote;

  private boolean headerInFirstLine;
  
  /**
   * This constructor sets default parameters
   */
  public CSVParameters() {
    super();
    
    headerInFirstLine = true;
    
    separator  = DEFAULT_SEPARATOR;
    end        = DEFAULT_END;
    fieldQuote = DEFAULT_QUOTE;
  }
  

  private String cleanQuotes( String fieldNameWithQuote) {
    if ( fieldNameWithQuote == null ) {
      return "";
    }
    if ( fieldQuote == null ) {
      fieldQuote = DEFAULT_QUOTE;
    }
    if ( fieldQuote.length() == 0 ) {
      return fieldNameWithQuote;
    }

    fieldNameWithQuote = fieldNameWithQuote.trim();

    if ( fieldNameWithQuote.equals( "") ) {
      return fieldNameWithQuote;
    }

    int ix1 = 0;
    int ix2 = fieldNameWithQuote.length();
    if ( fieldNameWithQuote.startsWith( fieldQuote) ) {
      ix1 += fieldQuote.length();
    }

    if ( fieldNameWithQuote.endsWith( fieldQuote) ) {
      ix2 -= fieldQuote.length();
    }

    if ( ix2 <= ix1 ) {
      return "";
    }

    return fieldNameWithQuote.substring( ix1, ix2);
  }

  List<String> parseCSVLine( String line) {
    List<String> res = new ArrayList<>();

    if ( line == null || line.trim().length() == 0 ) {
      return res;
    }

    if ( line.indexOf( end) > 0 ) {
      line = line.substring( 0, line.indexOf( end));
    }

    boolean addLast = false;

    if ( fieldQuote.length() > 0 ) {
      line = line.trim();
      
      String last = "";
      while ( true ) {
        line = Utils.changeAll( line, 
                                separator + separator, 
                                separator + fieldQuote + fieldQuote + separator);
        if ( last.equals( line) ) {
          break;
        }
        
        last = line;
      }
    }

    if ( line.startsWith( separator) ) {
      res.add( "");
    }
    if ( line.endsWith( separator) ) {
      addLast = true;
    }
    line = (line.startsWith( separator) ? "" : separator) + line;
    line = line + (line.endsWith( separator) ? "" : separator);
    
    int ix1 = 0;
    int ix2 = 0;
    int gap = 0;
    while ( ix1 < line.length() ) {
      
      if ( fieldQuote.length() > 0 ) {
        int ixQ1 = line.indexOf( fieldQuote, ix1);  // The opening quote, ixQ1
        if ( ixQ1 != -1 ) { 
          int ixQ2 = ixQ1 + fieldQuote.length();    // The closing quote will be after the opening quote, stored in ixQ2
          do {
            ixQ2 = line.indexOf( fieldQuote, ixQ2);
            
            // Let's see if THIS quote is escaped
            if ( line.charAt( ixQ2 - 1) == '\\' ) {
              // It's escaped, so let's find the next one
              ixQ2 = ixQ2 + fieldQuote.length();
            }
            else {
              // ixQ2 has the position of the closing quote, so break the loop
              break;
            }
          }
          while ( ixQ2 < line.length());
          
          if ( ixQ2 != -1 ) {
            gap = ixQ2 - ix1 - separator.length();
          }
        }
      }

      ix2 = line.indexOf( separator, ix1 + separator.length() + gap);
      if ( ix2 == -1 ) {
        break;
      }

      String fieldNameWithQuote = line.substring( ix1 + separator.length(), ix2);
      String fieldName = cleanQuotes( fieldNameWithQuote);

      res.add( fieldName);

      ix1 = ix2;
    }

    if ( addLast ) {
      res.add( "");
    }

    return res;
  }

  String escape( String value) {
    value = Utils.changeAll( value, "\\", "\\\\"); 
    value = Utils.changeAll( value, "\n", "\\n"); 
    value = Utils.changeAll( value, "\r", "\\r"); 
    value = Utils.changeAll( value, "\t", "\\t"); 
    value = Utils.changeAll( value, "\"", "\\\""); 
    value = Utils.changeAll( value, separator, "\\" + separator); 
    value = Utils.changeAll( value, "'", "\\'"); 
    
    return value;
  }
  
  String unescape( String value) {
    // I **hate** regular expressions
    value = value.replace( "\\\\", "*****ESCAPED*****");
    value = value.replace( "\\n", "\n");
    value = value.replace( "\\r", "\r");
    value = value.replace( "\\t", "\t");
    value = Utils.changeAll( value, "\\'", "'");
    value = Utils.changeAll( value, "\\" + separator, separator);
    value = Utils.changeAll( value, "\\\"", "\"");
    value = value.replace( "*****ESCAPED*****", "\\");
    
    return value;
  }
  
  /**
   * This method returns the field separator
   * @return the field separator
   */
  public String getSeparator() {
    return separator;
  }

  /**
   * This method set the character used to separate the field values
   * @param separator the character used to separate the field values
   */
  public void setSeparator( String separator) {
    this.separator = separator;
  }
  
  /**
   * This method returns the line end character
   * @return the line end character
   */
  public String getEnd() {
    return end;
  }

  /**
   * This method set the character used for the line end
   * @param end the character used for the line end
   */
  public void setEnd( String end) {
    this.end = end;
  }
  
  /**
   * This method returns the field quote character
   * @return the field quote character
   */
  public String getFieldQuote() {
    return fieldQuote;
  }

  /**
   * This method set the character used to quote the field values
   * @param fieldQuote the character used to quote the field values
   */
  public void setFieldQuote( String fieldQuote) {
    this.fieldQuote = fieldQuote;
  }

  /**
   * This method returns true if the first line of the CSV file contains the header with the field names and false otherwise
   * @return true if the first line of the CSV file contains the header with the field names and false otherwise
   */
  public boolean isHeaderInFirstLine() {
    return headerInFirstLine;
  }

  /**
   * If <code>headerInFirstLine</code> is true, this method indicates that the first line of the CSV file contains the header with the field names
   * @param headerInFirstLine if <code>headerInFirstLine</code> is true, indicates that the first line of the CSV file contains the header with the field names
   */
  public void setHeaderInFirstLine( boolean headerInFirstLine) {
    this.headerInFirstLine = headerInFirstLine;
  }
}
