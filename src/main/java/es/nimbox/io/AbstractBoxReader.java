package es.nimbox.io;

import java.util.ArrayList;
import java.util.List;

import es.nimbox.box.ElasticBox;

/**
 * This class provides some utilities for <code>BoxReader</code>s, so it's a good base to start if you are going
 * to write a <code>BoxReader</code>.
 *
 * @author nilo
 *
 */
public abstract class AbstractBoxReader implements BoxReader {

  /**
   * This property stores the number of read records
   */
  protected int recordsCounter;
  
  /**
   * This list contains the list of fields
   */
  protected List<String> lFieldNames;
  
  /**
   * Default constructor
   */
  protected AbstractBoxReader() {
    super();
    
    lFieldNames = new ArrayList<>();
    recordsCounter = 0;
  }
  
  @Override
  public void close() throws Exception {
    lFieldNames.clear();
    recordsCounter = 0;
  }

  
  @Override
  public List<String> getFieldNames() {
    return new ArrayList<>( lFieldNames);
  }
  
  @Override
  public List<ElasticBox> read( int number) throws Exception {
    List<ElasticBox> res = new ArrayList<>();
    
    ElasticBox item = null;
    while ( number-- > 0 ) {
      if ( null == ( item = read()) ) {
        break;
      }
      
      // This whole thing could be "while ( number-- > 0 && null !== ( item = read()) ) {"
      // but that loop condition depends on the order of the evaluation of the two conditions. It will work
      // because Java runs over a virtual machine and all that thing, but I remember the happy days when 
      // I was programming in C++ and something cries inside me if I left a condition that depends on the
      // evaluation order, even if I know that it's going to work...
      
      res.add( item);
    }
    
    return res;
  }
  
  @Override
  public int getReadRecords() {
    return recordsCounter;
  }  
  
  /**
   * This method adds <code>recordNumber</code> to the current number of read records. Usually, 1.
   * @param recordNumber the number to add to the current number of read records.
   */
  protected void addCountRecords( int recordNumber) {
    this.recordsCounter += recordNumber;
  }
  
  /**
   * This method sets the current number of read records to <code>recordNumber</code>
   * @param recordNumber the new value for the number of read records
   */
  protected void setCountRecords( int recordNumber) {
    this.recordsCounter = recordNumber;
  }
}
