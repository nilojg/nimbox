package es.nimbox.io;

import java.text.Format;
import java.util.List;

import es.nimbox.box.ElasticBox;

/**
 * This interface defines classes that write <code>ElasticBox</code> in wherever: files, databases... <br>
 * The idea is that the keys of the <code>ElasticBox</code> are translated into the destiny of the data
 * as, for example, column names if the destiny is a database table or a CSV. If the <code>ElasticBox</code>
 * has data without identification, which is quite rare, the name will be the <code>DEFAULT_FIELD_NAME</code> 
 * constant with the index of the column beginning in 0.<br>
 * The user can select the fields to write between all the properties of the <code>ElasticBox</code> using
 * the methods <code>addField</code>. If the user doesn't use those methods to select output fields, all the 
 * fields in the <code>ElasticBox</code> are written in alphabetic order.
 *
 * @author nilo
 *
 */
public interface BoxWriter extends AutoCloseable {
  /**
   * The name for keys when the origin doesn't provide a name
   */
  public static final String DEFAULT_FIELD_NAME = "DUMMY_";
  
  /**
   * This method returns the number of written records.
   * @return the number of written records.
   */
  public int getWrittenRecords();
  
  /**
   * This method returns a <code>List</code> with the identifiers of the written <code>ElasticBox</code> 
   * @return a <code>List</code> with the identifiers of the written <code>ElasticBox</code> 
   */
  public List<String> getFieldNames();

  /**
   * This method writes the <code>box</code>.
   * @param box the <code>ElasticBox</code> to write
   * @throws Exception when something goes wrong
   */
  public void write( ElasticBox box) throws Exception;
  
  /**
   * This method writes the <code>ElasticBox</code> contained in the list <code>items</code>
   * @param items the items to write
   * @return the number of written items 
   * @throws Exception when something goes wrong
   */
  public int write( List<ElasticBox> items) throws Exception;

  
  /**
   * This method select the property <code>name</code> to be written in the destiny. The output "title" (the database
   * table column name, the CSV column, etc) will be the value <code>name</code>, and the value will be written as a
   * string value.
   * @param name the field to write
   */
  public void addField( String name);
  
  /**
   * This method select the property <code>name</code> to be written in the destiny. The <code>title</code> will
   * be the name in the destiny (the database table column name, the CSV column, etc). The value will be written as a
   * string value.
   * @param name the field to write
   * @param title the "title" in the destiny of this field
   */
  public void addField( String name, String title);
  
  /**
   * This method select the property <code>name</code> to be written in the destiny. The <code>title</code> will
   * be the name in the destiny (the database table column name, the CSV column, etc).<br>
   * The value will be written using the formatter <code>format</code>. For example, if the value is a <code>Date</code>,
   * <code>format</code> can be a <code>SimpleDateFormat</code> to write the date using a given locale format. If 
   * <code>format</code> is <code>null</code>, the value will be written as a simple String
   * @param name the field to write
   * @param title the "title" in the destiny of this field
   * @param format the formatter to use
   */
  public void addField( String name, String title, Format format);
}
