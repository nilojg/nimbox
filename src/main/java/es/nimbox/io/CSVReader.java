package es.nimbox.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.List;

import es.nimbox.box.ElasticBox;

/**
 * This class implements a <code>BoxReader</code> that reads data from CSV files.
 * 
 * The class reads records from a CSV file and returns them as <code>ElasticBox</code> objects, one by one
 * or several in a <code>List</code>. This is an example:
 * <pre>
 * try ( CSVReader cr = new CSVReader( Utils.findFile( "File.csv")) ) {
 *   ElasticBox box = cr.read();
 *   ..do something with box..
 * }
 * catch ( Exception ex) {}
 * </pre>
 * This code reads a CSV named "File.csv" and fulfills the <code>ElasticBox</code> <code>box</code> with the contents
 * of the first line of the file.<br>
 * To read the whole file you can use the typical while-read loop:
 * <pre>
 * try ( CSVReader cr = new CSVReader( Utils.findFile( "File.csv")) ) {
 *   ElasticBox box = null;
 *   while ( null != ( box = cr.read() ) ) {
 *     System.out.println( box);
 *   }
 * }
 * catch ( Exception ex) {}
 * </pre>
 * This code reads all the lines of a CSV file and prints it in the standard output in JSON format, because <code>ElasticBox</code>
 * prints itself in JSON format.<br>
 * 
 * By default the class reads CSV files with the name of the fields in a header in the first line, where semicolons (;) 
 * are used as field separator, double quotes (") ar used to surround the values and carriage returns are used to 
 * separate records in lines, which is the de facto standard, but the shape of the read CSV can be personalized 
 * using the <code>CSVParameters</code> class, where you can set if there is a header or the characters used as 
 * quotes, separators, etc. Let's see and example:
 * <pre>
 * try ( CSVReader cr = new CSVReader( Utils.findFile( "File.csv")) ) {
 *   cr.getCSVParameters().setHeaderInFirstLine( false);
 *   cr.getCSVParameters().setSeparator( ",");
 *   cr.getCSVParameters().setFieldQuote( "");
 *   
 *   ElasticBox box = null;
 *   while ( null != ( box = cr.read() ) ) {
 *     System.out.println( box);
 *   }
 * }
 * catch ( Exception ex) {}
 * </pre>
 * This code reads a CSV file which hasn't got a header, uses commas (,) as value separators and without quotes.
 * Since there aren't any hint in the file about the name of the fields, the properties of the <code>ElasticBox</code>
 * objects will be named with the prefix <code>DEFAULT_FIELD_NAME</code> plus the numeric order of the field in the CSV
 * starting in zero. In the practice, the properties will be named "DUMMY_0", "DUMMY_1", etc.
 * 
 * <br><br>
 * <b>Limitations</b>: by now, each record MUST be in a single line of the CSV, which in the other hand is the default. So, 
 * if the values in the CSV contains carriage returns, they must be escaped. <code>CSVWRiter</code> writes the
 * <code>ElasticBox</code> this way.
 * 
 * @see CSVWriter
 * @see CSVParameters
 * @author nilo
 */
public class CSVReader extends AbstractBoxReader {
    
  /**
   * The stream
   */
  protected BufferedReader is;

  private boolean headerAlreadyRead;
  
  private CSVParameters params;
  
  private String fileName;
  

  /**
   * This constructor creates a CSVReader which will read the CSV file <code>file</code> using the charset {@link CSVParameters#DEFAULT_CHARSET}.
   * @param file the file
   * @throws IOException when something goes wrong
   */
  public CSVReader( String file) throws IOException {
    this( new File( file));
  }
  
  /**
   * This constructor creates a CSVReader which will read the CSV file <code>file</code> using the charset {@link CSVParameters#DEFAULT_CHARSET}.
   * @param file the file
   * @throws IOException when something goes wrong
   */
  public CSVReader( File file) throws IOException {
    this( new FileInputStream( file));
    
    this.fileName = file.getAbsolutePath();
  }
  
  /**
   * This constructor creates a CSVReader which will read the CSV file of <code>is</code> using the charset {@link CSVParameters#DEFAULT_CHARSET}.
   * <br>The InputStream <code>is</code> WILL BE CLOSED in the {@link close()} method
   * @param is the file
   * @throws IOException when something goes wrong
   */
  public CSVReader( InputStream is) throws IOException {
    this( is, CSVParameters.DEFAULT_CHARSET);
  }
  
  /**
   * This constructor creates a CSVReader which will read the CSV file of <code>is</code> using the charset <code>charset</code>.
   * <br>The InputStream <code>is</code> WILL BE CLOSED in the {@link close()} method
   * @param is the file
   * @param charset the charset
   * @throws IOException when something goes wrong
   */
  public CSVReader( InputStream is, String charset) throws IOException {
    this( new BufferedReader( new InputStreamReader( is, charset)));
  }
  
  /**
   * This constructor creates a CSVReader which will read the CSV file of <code>is</code> using the charset {@link CSVParameters#DEFAULT_CHARSET}.
   * <br>The InputStream <code>is</code> WILL BE CLOSED in the {@link close()} method
   * @param is the file
   * @throws IOException when something goes wrong
   */
  public CSVReader( Reader is) throws IOException {
    super();
    
    this.is = is instanceof BufferedReader ? (BufferedReader)is 
                                           : new BufferedReader( is);
    
    headerAlreadyRead = false;
    
    params = new CSVParameters();
  }
  
 
  /**
   * This method returns the <code>CSVParameters</code> object to personalize the way the file is created
   * @return the <code>CSVParameters</code> object to personalize the way the file is created
   */
  public CSVParameters getCSVParameters() {
    return params;
  }
  
  /**
   * This method returns the name of the CSV file. This value is know ONLY if the user uses the constructors
   * {@link #CSVReader(File)} or {@link #CSVReader(String)}. The other constructors gets already created streams or
   * writers, so the file name is unknown.
   * @return the name of the CSV file, or <code>null</code> if the value is unknown
   */
  public String getFileName() {
    return fileName;
  }
  
  
  
  @Override
  public void close() throws Exception {
    super.close();
    
    if ( is != null ) {
      is.close();
    }
  }
    
  @Override
  public List<String> getFieldNames() {
    try {
      readHeader();
    }
    catch ( IOException e) {
      e.printStackTrace();
    }
    
    return super.getFieldNames();
  }
  
  @Override
  public ElasticBox read() throws IOException {
    readHeader();
    
    String line = null;
    synchronized ( is) {
      line = is.readLine();
    }
    
    if ( line == null || line.trim().equals( "") ) {
      return null;
    }
    
    if ( getCSVParameters().getFieldQuote().length() > 0) {
      line = line.trim();
    }
    
    // There's a line to read, so parse it
    List<String> lValues = getCSVParameters().parseCSVLine( line);
    
    if ( lValues.isEmpty() ) {
      return null;
    }
    
    ElasticBox res = new ElasticBox();
      
    // First, read all the values and store them in the result bean
    // But there could be different number of "data" line than in the "header" line 
    
    // This will be the end of the loop, the min size of the two arrays
    int minSize = Math.min( lValues.size(), lFieldNames.size());
    
    for ( int i = 0; i < minSize; i++) {
      String fieldName = lFieldNames.get( i);
      String fieldValue = lValues.get( i);
      
      res.set( fieldName, getCSVParameters().unescape( fieldValue));
    }
      
    // Now, let's add the rest of the fields if there's more values than fields. With a default name
    for ( int i = minSize; i < lValues.size(); i++ ) {
      String fieldName = DEFAULT_FIELD_NAME + i;
      String fieldValue = lValues.get( i);
      
      res.set( fieldName, getCSVParameters().unescape( fieldValue));
      
      lFieldNames.add( fieldName);
    }

    synchronized ( this) {
      addCountRecords( 1);
    }
    
    return res;
  }


  
  
  /**
   * This method reads the header of the CSV file
   * @throws IOException when something goes wrong
   */
  protected void readHeader() throws IOException {
    if ( !params.isHeaderInFirstLine() ) {
      return;
    }
    
    if ( !headerAlreadyRead ) {
      lFieldNames = getCSVParameters().parseCSVLine( is.readLine());
      headerAlreadyRead = true;
    }
  }
}
