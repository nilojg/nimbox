package es.nimbox.io;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import es.nimbox.box.ElasticBox;

/**
 * This class implements a <code>BoxReader</code> that reads data from the <code>ResultSet</code> that
 * returns a query.<br>
 * 
 * The class is constructed with a JDBC connection or a <code>DataSource</code>. If is constructed with a 
 * <code>DataSource</code>, the class get a connection from the <code>DataSource</code> and the method 
 * <code>close()</code> returns the connection to the <code>DataSource</code> when the <code>SQLReader</code>
 * is closed.<br>
 * If the object is created with a connection, the <code>close()</code> method doesn't close the connection,
 * that's a work for the one that gets the connection.<br>
 * 
 * To use the <code>SQLReader</code> just get a <code>SQLConnection</code>, create the <code>SQLReader</code>,
 * pass the SQL query and begin to read. For example:
 * <pre>
 * 
 * Connection con = ....
 * try ( SQLReader cr = new SQLReader( con) ) {
 *   cr.setQuery( "select * from Usuarios");
 * 
 *   ElasticBox box = null;
 *   
 *   while ( null != ( box = cr.read() ) ) {
 *     System.out.println( box);
 *   }
 * }
 * catch ( Exception ex) {}
 * </pre>
 *
 * This code creates the <code>SQLReader</code>, passes the query and reads all the records in the table 'Usuarios'.
 *
 * @author nilo
 *
 */
public class SQLReader extends AbstractBoxReader {
  
  // This is because the SQLReader must ONLY close the connection if the SQLReader gets the connection
  // from the datasource, never if the connection was created outside the object and passed in the
  // constructor with a Connection parameter.
  private boolean fromDataSource = false;
  
  private DataSource ds;
  private Connection con;
  private Statement stmt;
  private ResultSet rs;
  
  private String query;
  
  /**
   * This constructor creates a reader that connects to the database with the connection <code>code</code>. The query to 
   * get the data must be set with the {@link #setQuery} method.
   * <br>The connection WILL NOT be closed.
   * @param con the connection
   */
  public SQLReader( Connection con) {
    this( con, null);
  }
  
  /**
   * This constructor creates a reader that connects to the database with the connection <code>code</code>. The query to 
   * get the data must be set with the {@link #setQuery} method.
   * <br>The connection will be returned to the datasource when the method {@link close} was invoked.
   * @param ds the datasource
   */
  public SQLReader( DataSource ds) {
    this( ds, null);
  }
  
  /**
   * This constructor creates a reader that connects to the database with the connection <code>code</code> and gets the 
   * data using <code>query</code>. 
   * <br>The connection WILL NOT be closed.
   * @param con the connection
   * @param query the query
   */
  public SQLReader( Connection con, String query) {
    super();
    
    this.con = con;
    this.query = query;
    lFieldNames = new ArrayList<String>();
    
    fromDataSource = false;
  }  
  
  /**
   * This constructor creates a reader that connects to the database with the connection <code>code</code> and gets the 
   * data using <code>query</code>. 
   * <br>The connection will be returned to the datasource when the method {@link close} was invoked.
   * @param ds the datasource
   * @param query the query
   */
  public SQLReader( DataSource ds, String query) {
    super();
    
    this.ds = ds;
    this.query = query;
    
    fromDataSource = true;
  }

  @Override
  public void close() throws Exception {
    super.close();
    
    if ( rs != null  ) {
      rs.close();
    }
    if ( stmt != null  ) {
      stmt.close();
    }
    
    if ( isFromDataSource() && con != null) {
      con.close();
    }
  }
  
  @Override
  public ElasticBox read() throws SQLException {
    openCursor();
    
    ElasticBox res = new ElasticBox();

    try {
      readMetadata( rs.getMetaData());
      
      if ( rs.next() ) {
        int i = 1;
        for ( String fieldName : lFieldNames ) {
          Object fieldValue = rs.getObject( i++);
          
          res.set( fieldName, fieldValue);
        }
        
        addCountRecords( 1);
      }
      else {
        return null;
      }
    }
    catch ( Exception ex) {
      throw new SQLException( ex.getMessage(), ex);
    }
    
    return res;
  }
  
  @Override
  public List<ElasticBox> read( int number) throws Exception {
    // Is more efficient to keep the loop here than use the base method, which reads the metadata *each* time....
    openCursor();
    
    List<ElasticBox> res = new ArrayList<>();
    
    readMetadata( rs.getMetaData());

    while ( number-- > 0 ) {
      if ( !rs.next() ) {
        break;
      }

      ElasticBox item = new ElasticBox();

      int i = 1;
      for ( String fieldName : lFieldNames ) {
        Object fieldValue = rs.getObject( i++);

        item.set( fieldName, fieldValue);
      }

      res.add( item);
    }

    return res;
  }
  
  
  private void readMetadata( ResultSetMetaData metaData) throws SQLException {
    // Let's add the field names
    for ( int i = 0; i < metaData.getColumnCount(); i++) {
      String fieldName = metaData.getColumnName( i + 1);
      
      if ( fieldName == null || fieldName.trim().equals( "") ) {
        fieldName = DEFAULT_FIELD_NAME + (i);
      }
      
      if ( !lFieldNames.contains( fieldName) ) { 
        lFieldNames.add( fieldName);
      }
    }
  }
  
  private void openCursor() throws SQLException {
    if ( query == null || query.trim().equals( "") ) {
      throw new SQLException( "Empty query");
    }
    
    if ( isFromDataSource() && ( con == null || con.isClosed() ) ) {
      con = ds.getConnection();
    }
    
    if ( stmt == null || stmt.isClosed() ) {
      stmt = con.createStatement();
    }
    if ( rs == null || rs.isClosed() ) {
      rs = stmt.executeQuery( query);
    }
  }
  

  /**
   * This method returns the query to get the data
   * @return the query to get the data
   */
  public String getQuery() {
    return query;
  }

  /**
   * This method sets the query to get the data
   * @param query the query to get the data
   */
  public void setQuery( String query) {
    this.query = query;
  }

  /**
   * This method returns true if the connection is obtained using a datasource and false if the connection
   * was passed from the caller code
   * @return  true if the connection is obtained using a datasource and false otherwise
   */
  public boolean isFromDataSource() {
    return fromDataSource;
  }

}
