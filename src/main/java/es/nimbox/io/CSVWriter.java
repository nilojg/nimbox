package es.nimbox.io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.Format;

import es.nimbox.box.ElasticBox;

/**
 * This class writes <code>ElasticBox</code> data in CSV files. It can write the <code>ElasticBox</code>
 * records one by one or several in one time getting the data from a list.<br>
 * It's (or should be) thread safe.<br>
 * 
 * For example:
 * <pre>
 * try ( CSVWriter cw = new CSVWriter( "File.csv") ) {
 *   cw.write( elasticBox1);
 *   cw.write( elasticBox1);
 * }
 * catch ( Exception ex) {}
 * </pre>
 * This code writes a CSV named "File.csv" with three lines, the header with the name of the properties of 
 * <code>elasticBox1</code> and the contents of <code>elasticBox1</code> and <code>elasticBox2</code>.<br>
 * 
 * The default generated CSV uses semicolon (;) as field separator, double quotes (") to surround the values
 * and carriage returns to separate records in lines, but the shape of the generated CSV can be personalized 
 * using the <code>CSVParameters</code> class, where you can set if there is a header or the characters used 
 * as quotes, separators, etc. Let's see and example:
 * <pre>
 * try ( CSVWriter cw = new CSVWriter( "File.csv") ) {
 *   cw.getCSVParameters().setHeaderInFirstLine( false);
 *   cw.getCSVParameters().setSeparator( ",");
 *   cw.getCSVParameters().setFieldQuote( "");
 *   
 *   cw.write( elasticBox1);
 *   cw.write( elasticBox1);
 * }
 * catch ( Exception ex) {}
 * </pre>
 * This code will generate a CSV without headers, without quotes surrounding the values, and separating the 
 * fields with commas.<br>
 * It's possible to write in the CSV only some properties of the <code>ElasticBox</code>. For example:
 * <pre>
 * try ( CSVWriter cw = new CSVWriter( "File.csv") ) {
 *   cw.addField( "ID");
 *   cw.addField( "Phone", "Mobile-phone");
 *   cw.addField( "Born", "Born date", new SimpleDateFormat( "yyyy/MM/dd"));
 *   
 *   cw.write( elasticBox1);
 *   cw.write( elasticBox1);
 * }
 * catch ( Exception ex) {}
 * </pre>
 * This code writes in the CSV only the fields "ID", "Phone" and "Born" between all the properties that the 
 * <code>ElasticBox</code> could have. "Phone" values will be written in a column with the name "Mobile-phone"
 * and the "Born" values will be written in a column with the name "Born date", and they will be formatted using
 * a <code>SimpleDateFormat</code> with the format "yyyy/MM/dd", because we suppose they are dates. This is the
 * CSV file:
 * <pre>
 * "ID";"Mobile-phone";"Born date"
 * "1";"9150808001";"2020/01/02"
 * "2";"9150808002";"2020/01/03"
 * </pre>
 * 
 * @see CSVReader
 * @see CSVParameters
 * @author nilo
 */
public class CSVWriter extends AbstractBoxWriter {
  
  /**
   * The output stream
   */
  protected BufferedWriter os;
  
  /**
   * This attribute is used to write the CSV header only once
   */
  private boolean headerAlreadyWritten;
  
  /**
   * The object that stores the objects that personalizes the CSV file
   */
  private CSVParameters params;
  
  /**
   * The filename
   */
  private String fileName;
  
  
  /**
   * This method creates a <code>CSVWriter</code> that writes the data in the path <code>file</code>
   * @param file the file to be created
   * @throws IOException when something goes wrong
   */
  public CSVWriter( String file) throws IOException {
    this( new File( file));
  }
  
  /**
   * This method creates a <code>CSVWriter</code> that writes the data in the path <code>file</code>
   * @param file the file to be created
   * @throws IOException when something goes wrong
   */
  public CSVWriter( File file) throws IOException {
    this( new FileOutputStream( file));
    
    this.fileName = file.getAbsolutePath();
  }
  
  /**
   * This method creates a <code>CSVWriter</code> that writes the data in <code>os</code>
   * @param os the output stream
   * @throws IOException when something goes wrong
   */
  public CSVWriter( OutputStream os) throws IOException {
    this( os, CSVParameters.DEFAULT_CHARSET);
  }
  
  /**
   * This method creates a <code>CSVWriter</code> that writes the data in <code>os</code> using the 
   * charset <code>charset</code>
   * @param os the output stream
   * @param charset the charset
   * @throws IOException when something goes wrong
   */
  public CSVWriter( OutputStream os, String charset) throws IOException {
    this( new BufferedWriter( new OutputStreamWriter( os, charset)));
  }
  
  /**
   * This method creates a <code>CSVWriter</code> that writes the data in <code>os</code>
   * @param os the output stream
   * @throws IOException when something goes wrong
   */
  public CSVWriter( Writer os) throws IOException {
    super();
    
    this.os = os instanceof BufferedWriter ? (BufferedWriter)os 
                                           : new BufferedWriter( os);
    
    headerAlreadyWritten = false;
    
    params = new CSVParameters();
  }
  
  
  @Override
  public void close() throws Exception {
    super.close();
    
    if ( os != null ) {
      os.close();
    }
  }

  /**
   * This method returns the <code>CSVParameters</code> object to personalize the way the file is created
   * @return the <code>CSVParameters</code> object to personalize the way the file is created
   */
  public CSVParameters getCSVParameters() {
    return params;
  }
  
  /**
   * This method returns the name of the CSV file. This value is know ONLY if the user uses the constructors
   * {@link #CSVWriter(File)} or {@link #CSVWriter(String)}. The other constructors gets already created streams or
   * writers, so the file name is unknown.
   * @return the name of the CSV file, or <code>null</code> if the value is unknown
   */
  public String getFileName() {
    return fileName;
  }
  
  @Override
  public void write( ElasticBox box) throws Exception {
    writeHeader( box);
    
    StringBuilder line = new StringBuilder();
    
    boolean first = true;
    for ( Field field : lFields ) {
      if ( !first ) {
        line.append( params.getSeparator());
      }
      else {
        first = false;
      }

      line.append( params.getFieldQuote());
      
      String value = "";
      Format format = field.format;
      if ( format == null ) {
        String cad = box.getAsString( field.name);
        value = cad != null ? cad : "";
      }
      else {
        Object obj = box.get( field.name);
        value = obj != null ? format.format( obj) : "";
      }
      
      line.append( getCSVParameters().escape( value));
      
      line.append( params.getFieldQuote());
    }
    
    line.append( params.getEnd());
    
    synchronized ( os) {
      os.write( line.toString());
      addCountRecords( 1);
    }
  }


  
  /**
   * This method writes the CSV header using the field names from <code>box</code> 
   * @param box the object used to get the header filed names
   * @throws IOException when something goes wrong
   */
  protected synchronized void writeHeader( ElasticBox box) throws IOException {
    if ( headerAlreadyWritten ) {
      return;
    }
    
    // If the user doesn't choose fields, let's print all the fields
    if ( lFields.isEmpty() ) {
      for ( String fieldName : box.getSortedFields() ) {
        lFields.add( new Field( fieldName, fieldName, null));
      }
    }
    
    if ( !params.isHeaderInFirstLine() ) {
      return;
    }
    
    // Write the header
    boolean first = true;
    for ( Field field : lFields ) {
      if ( !first ) {
        os.write( params.getSeparator());
      }
      else {
        first = false;
      }

      os.write( params.getFieldQuote());
      os.write( field.title);
      os.write( params.getFieldQuote());
    }
    
    os.write( params.getEnd());
    
    headerAlreadyWritten = true;
  }

}
